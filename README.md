PARCC-LDR
=========

PARCC Learning Delivery Records

Continious Integration invokes Ant with build.xml target.

Database drop script is dbdrop.sh. It drops existing LDR database, 
user and user permissions. It is executed on database server.

Database create script is dbcreate.sh. It creates LDR database, 
user and user permissions. It is executed on database server.

Configuration script configure.sh is executed on LDR web server. It 
configures database connection, LDR client request scheme, and 
creates LDR database objects.