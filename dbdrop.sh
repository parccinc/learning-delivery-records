#!/bin/sh
# This script drops database, unit-test database and user
# Usage ./dbdrop.sh dbhost=dbhost dbname=name dbuser=user mysqluser=user mysqlpwd=password ldrhost=host
# dbname is database name. Unit-test database name: "_unit_test" is appended to dbname.
# dbuser is database user
# mysqluser is MySQL admin user
# mysqlpwd is MySQL admin password
# ldrhost is LDR host connecting to database

LOGFILE=dbdrop.sh.log
echo `date` >$LOGFILE
DBHOST=
DBNAME=
DBNAMEUNITTEST=
MYSQLUSER=
MYSQLPWD=
DBUSER=
LDRHOST=

log() {
	COLOR=
	if [ $# -gt 1 ];then
		case "$2" in
			error)
				COLOR="31"
				;;

			warn)
				COLOR="33"
				;;
					
			ok)
				COLOR="32"
				;;
		esac
	fi

	local MSG=
	if [ -z "$COLOR" ];then
		MSG="$1"
	else
		MSG="\\E[0;""$COLOR""m""$1""\\E[0m"
	fi

	echo -e "$MSG" | tee -a "$LOGFILE"
}

if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			dbhost)
				DBHOST="${ARR[1]}"
				;;
				
			dbname)
				DBNAME="${ARR[1]}"
				DBNAMEUNITTEST="$DBNAME""_unit_test"
				;;

			dbuser)
				DBUSER="${ARR[1]}"
				;;

			mysqluser)
				MYSQLUSER="${ARR[1]}"
				;;
				
			mysqlpwd)
				MYSQLPWD="${ARR[1]}"
				;;
				
			ldrhost)
				LDRHOST="${ARR[1]}"
				;;
		esac
	done
fi

if [ -z "$DBNAME" ];then
	log "Database name is required." error
	exit 1
fi

if [ -z "$DBUSER" ];then
	log "Database user name is required." error
	exit 1
fi

if [ -z "$MYSQLUSER" ];then
	log "MySQL admin user is required." error
	exit 1
fi

if [ -z "$MYSQLPWD" ];then
	log "MySQL admin password is required." error
	exit 1
fi

if [ -z "$LDRHOST" ];then
	log "LDR host is required." error
	exit 1
fi

execcmd() {
	log "$1"
	local ERR=$((eval $1 >>$LOGFILE) 2>&1)
	if [ $? -gt 0 ];then
		log "$ERR" error
		exit 1
	fi
}

dbexists() {
	local RESULT=$($MYSQL "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$1'")
	if [ -z "$RESULT" ];then
		echo false
	else
		echo true
	fi
}

dbuserexists() {
	local RESULT=$($MYSQL "SELECT User FROM mysql.user WHERE User='$DBUSER' AND Host='$LDRHOST'")
	if [ -z "$RESULT" ];then
		echo false
	else
		echo true
	fi
}

if [ -z "$DBHOST" ];then
	MYSQL="mysql -u $MYSQLUSER -p$MYSQLPWD -s -N -e"
else
	MYSQL="mysql -h $DBHOST -u $MYSQLUSER -p$MYSQLPWD -s -N -e"
fi

RESULT=$(dbexists "$DBNAME")
if [ "$RESULT" = true ];then
	PROCESSLIST=`$MYSQL 'SELECT Id FROM INFORMATION_SCHEMA.PROCESSLIST'`
	for PROCESS in ${PROCESSLIST[@]};do
		execcmd "$MYSQL 'KILL $PROCESS'"
	done
	
	execcmd "$MYSQL 'DROP DATABASE $DBNAME'"
	RESULT=$(dbexists "$DBNAME")
	if [ "$RESULT" = true ];then
		log "Failed to drop database $DBNAME." error
		exit 1
	else
		log "Database $DBNAME dropped!" ok
	fi
else
	log "Database $DBNAME not found. Nothing to drop!" warn
fi

RESULT=$(dbexists "$DBNAMEUNITTEST")
if [ "$RESULT" = true ];then
	PROCESSLIST=`$MYSQL 'SELECT Id FROM INFORMATION_SCHEMA.PROCESSLIST'`
	for PROCESS in ${PROCESSLIST[@]};do
		execcmd "$MYSQL 'KILL $PROCESS'"
	done
	
	execcmd "$MYSQL 'DROP DATABASE $DBNAMEUNITTEST'"
	RESULT=$(dbexists "$DBNAMEUNITTEST")
	if [ "$RESULT" = true ];then
		log "Failed to drop database $DBNAMEUNITTEST." error
		exit 1
	else
		log "Database $DBNAMEUNITTEST dropped!" ok
	fi
else
	log "Database $DBNAMEUNITTEST not found. Nothing to drop!" warn
fi

RESULT=$(dbuserexists)
if [ "$RESULT" = true ];then
	execcmd "$MYSQL 'DROP USER '\\''$DBUSER'\\''@'\\''$LDRHOST'\\'"
	RESULT=$(dbuserexists)
	if [ "$RESULT" = true ];then
		log "Failed to drop user $DBUSER@$LDRHOST." error
		exit 1
	else
		log "Database user $DBUSER@$LDRHOST dropped!" ok
	fi
else
	log "Database user $DBUSER@$LDRHOST not found. Nothing to drop!" warn
fi

exit 0