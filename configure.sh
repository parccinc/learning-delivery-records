#!/bin/bash

# LDR configuration setup
# Run as root
# Usage bash configure.sh owner_group=group owner_user=user dbhost=host dbname=name dbuser=user dbpwd=pwd requestscheme=scheme appdir=dir client_user=username client_pwd=password log=false
# owner_group is group that owns directory where application is deployed (ex.: apache)
# owner_user is group that owns directory where application is deployed (ex.: apache)
# dbhost is LDR database host. If not specified, script will prompt for database host.
# dbname is LDR database name. If not specified, script will prompt for database name. For unit-test database, "_unit_test" is appended to dbname.
# dbuser is LDR database user. If not specified, script will prompt for database user.
# dbpwd is LDR database user password. If not specified, script will prompt for password.
# requestscheme is LDR client request scheme (http or https. If not specified, script will prompt for scheme.
# appdir is directory on Apache server where code is deployed.
# client_user is LDR client username.
# client_pwd is LDR client password.
# log enables this script log file (true or false). Default is false.

USELOG=false
LOGFILE=configure.sh.log
echo `date` >$LOGFILE
OWNER_GROUP=
OWNER_USER=
DBHOST=
DBNAME=
DBNAMEUNITTEST=
DBUSER=
DBPWD=
REQUESTSCHEME=
APPDIR=
LOGREQUESTS=
LOGRESPONSES=
SKIPPROCESSING=
LDRCLIENTUSER=
LDRCLIENTPWD=
LDRCLIENTHOSTNAME=
TESTKEY=
MYSQLUSER=
MYSQLPWD=
ADPHOST=
ADPREQUESTSCHEME=
ADPRESOURCEPATH=
SKIPVALIDATION=
DBFIRSTSTUDENT=
DBFIRSTTEST=
SKIPADPCOMM=
DISABLE_SELECT=
ADPS3BUCKET=
ADPS3CONTENT=
ADPS3RESULTS=
ADPPASSWORD=
ADPSECRET=
ADPUSERNAME=
AWSS3PROFILE=
AWSSQSPROFILE=
AWSSQSQUEUEURL=
AWSSQSREGION=
TESTSTATUSLOCKFILENAME=
ADPS3RESULTSBUCKET=
STORE_TEST_FORM_REVISIONS=
LOG_TEST_STATUS=



# Process script parameters
if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			owner_group)
				OWNER_GROUP="${ARR[1]}"
				;;

			owner_user)
				OWNER_USER="${ARR[1]}"
				;;

			dbhost)
				DBHOST="${ARR[1]}"
				;;

			dbname)
				DBNAME="${ARR[1]}"
				DBNAMEUNITTEST="$DBNAME""_unit_test"
				;;

			dbuser)
				DBUSER="${ARR[1]}"
				;;

			dbpwd)
				DBPWD="${ARR[1]}"
				;;

			requestscheme)
				REQUESTSCHEME="${ARR[1]}"
				;;

			appdir)
				APPDIR="${ARR[1]}"
				;;

			log_requests)
				LOGREQUESTS="${ARR[1]}"
				;;

			log_responses)
				LOGRESPONSES="${ARR[1]}"
				;;

			skip_processing)
				SKIPPROCESSING="${ARR[1]}"
				;;

			client_user)
				LDRCLIENTUSER="${ARR[1]}"
				;;

			client_pwd)
				LDRCLIENTPWD="${ARR[1]}"
				;;

			client_hostname)
				LDRCLIENTHOSTNAME="${ARR[1]}"
				;;
			test_key)
				TESTKEY="${ARR[1]}"
				;;
				test_key)
				TESTKEY="${ARR[1]}"
				;;

			DISABLE_SELECT)
				DISABLE_SELECT="${ARR[1]}"
				;;

			ADPS3BUCKET)
				ADPS3BUCKET="${ARR[1]}"
				;;

			ADPS3CONTENT)
			        ADPS3CONTENT="${ARR[1]}"
				;;

			ADPS3RESULTS)
				ADPS3RESULTS="${ARR[1]}"
				;;

			ADPPASSWORD)
				ADPPASSWORD="${ARR[1]}"
				;;

			ADPSECRET)
				ADPSECRET="${ARR[1]}"
				;;

			ADPUSERNAME)
				ADPUSERNAME="${ARR[1]}"
				;;

			AWSS3PROFILE)
				AWSS3PROFILE="${ARR[1]}"
				;;

			AWSSQSPROFILE)
				AWSSQSPROFILE="${ARR[1]}"
				;;

			AWSSQSQUEUEURL)
			        AWSSQSQUEUEURL="${ARR[1]}"
				;;

			AWSSQSREGION)
				AWSSQSREGION="${ARR[1]}"
				;;

			TESTSTATUSLOCKFILENAME)
				TESTSTATUSLOCKFILENAME="${ARR[1]}"
				;;

			ADPS3RESULTSBUCKET)
				ADPS3RESULTSBUCKET="${ARR[1]}"
				;;

			MYSQLUSER)
				MYSQLUSER="${ARR[1]}"
				;;

			MYSQLPWD)
				MYSQLPWD="${ARR[1]}"
				;;

			ADPHOST)
				ADPHOST="${ARR[1]}"
				;;

			ADPREQUESTSCHEME)
				ADPREQUESTCHEME="${ARR[1]}"
				;;

			ADPRESOURCEPATH)
				ADPRESOURCEPATH="${ARR[1]}"
				;;
			
			DBFIRSTSTUDENT)
				DBFIRSTSTUDENT="${ARR[1]}"
				;;
			
			DBFIRSTTEST)
				DBFIRSTTEST="${ARR[1]}"
				;;
			
			SKIPADPCOMM)
				SKIPADPCOMM="${ARR[1]}"
				;;

			STORE_TEST_FORM_REVISIONS)
				STORE_TEST_FORM_REVISIONS="${ARR[1]}"
				;;
			
			LOG_TEST_STATUS)
				LOG_TEST_STATUS="${ARR[1]}"
				;;
			
			log)
				USELOG="${ARR[1]}"
				;;
		esac
	done
fi

log() {
	COLOR=
	if [ $# -gt 1 ];then
		case "$2" in
			error)
				COLOR="31"
				;;

			warn)
				COLOR="33"
				;;

			ok)
				COLOR="32"
				;;
		esac
	fi

	local MSG=
	if [ -z "$COLOR" ];then
		MSG="$1"
	else
		MSG="\\E[0;""$COLOR""m""$1""\\E[0m"
	fi

	if [ "$USELOG" = true ]; then
		echo -e "$MSG" | tee -a "$LOGFILE"
	else
		echo -e "$MSG"
	fi
}

# Check for required input.
check_required() {
	if [ -z "$1" ];then
		while true;do
			read -p "$2"": "
			if [ ! -z "$REPLY" ];then
				if [ $# -gt 2 ];then
					local MATCH=false
					for (( i=3; i<=$#; i++ ));do
						if [ "${@:i:1}" == "$REPLY" ];then
							MATCH=true;
							break;
						fi
					done

					if [ "$MATCH" = true ];then
						echo "$REPLY"
						break
					fi
				else
					echo "$REPLY"
					break
				fi
			fi
		done
	else
		echo $1
	fi
}

# Executed command and logs
cmd() {
	log "$1"
	local ERR=
	if [ "$USELOG" = true ];then
		local ERR=$((eval $1 >>$LOGFILE) 2>&1)
		if [ $? -gt 0 ];then
			log "$ERR" error
			exit 1
		fi
	else
		eval $1 2>&1
	fi

}

# Gets Status from HTTP response.
status() {
	local STATUS=$(echo "$1" | awk '/^  HTTP/{print $2}')
	echo "$STATUS"
}

OWNER_GROUP=$(check_required "$OWNER_GROUP" "Application directory group owner")
OWNER_USER=$(check_required "$OWNER_USER" "Application directory user owner")
DBHOST=$(check_required "$DBHOST" "LDR database host")
DBNAME=$(check_required "$DBNAME" "LDR database name")
DBUSER=$(check_required "$DBUSER" "LDR database user")
LOGREQUESTS=$(check_required "$LOGREQUESTS" "log requests value")
if [ -z "$DBPWD" ];then
	while true;do
		read -sp "LDR Database password: " PWD
		echo
		if [ ! -z "$PWD" ];then
			read -sp "LDR Database password (again): " AGAIN
			echo
			if [ "$PWD" = "$AGAIN" ];then
				DBPWD="$PWD"
				break
			else
				echo "Please try again"
			fi
		fi
	done
fi

OUTCOMES=("http" "https")
REQUESTSCHEME=$(check_required "$REQUESTSCHEME" "LDR client request scheme [http|https]" ${OUTCOMES[@]})
APPDIR=$(check_required "$APPDIR" "Application directory")
LDRCLIENTUSER=$(check_required "$LDRCLIENTUSER" "LDR client username")
LDRCLIENTPWD=$(check_required "$LDRCLIENTPWD" "LDR client password")

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# modify configuration file
cmd "cp '$DIR/ldr_svcs/default.config.php' '$DIR/ldr_svcs/config.php'"

#ADP and AWS settings

cmd "sed -i 's/define('\''ADP_PASSWORD'\'',.*);/define('\''ADP_PASSWORD'\'', \"'"${ADPPASSWORD}"'\");/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\''LOG_DEBUG'\'',.*);/define('\''STORE_TEST_FORM_REVISIONS'\'', \"'"${STORE_TEST_FORM_REVISIONS}"'\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\''LOG_TEST_STATUS'\'',.*);/define('\''LOG_TEST_STATUS'\'', \"'"${LOG_TEST_STATUS}"'\");/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\''ADP_SECRET'\'',.*);/define('\''ADP_SECRET'\'', \"'"${ADPSECRET}"'\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\''ADP_USERNAME'\'',.*);/define('\''ADP_USERNAME'\'', \"'"${ADPUSERNAME}"'\");/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\''AWS_S3_PROFILE'\'',.*);/define('\''AWS_S3_PROFILE'\'', \"'"${AWSS3PROFILE}"'\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\''AWS_SQS_PROFILE'\'',.*);/define('\''AWS_SQS_PROFILE'\'', \"'"${AWSSQSPROFILE}"'\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\''AWS_SQS_QUEUE URL'\'',.*);/define('\''AWS_SQS_QUEUE URL'\'', \"'"${AWSSQSQUEUEURL}"'\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\''AWS_SQS_REGION'\'',.*);/define('\''AWS_SQS_REGION'\'', \"'"${AWSSQSREGION}"'\");/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\''TEST_STATUS_LOCK_FILENAME'\'',.*);/define('\''TEST_STATUS_LOCK_FILENAME'\'', \"'"${TESTSTATUSLOCKFILENAME}"'\");/g' '$DIR/ldr_svcs/config.php'"

#DB_disable_select
cmd "sed -i 's/define('\''DB_DISABLE_SELECT'\'',.*);/define('\''DB_DISABLE_SELECT'\'', '"${DISABLE_SELECT}"');/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\\''DB_HOSTNAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''DB_HOSTNAME'\\''\\1,\\2\"$DBHOST\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_PRODUCTION'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''DB_PRODUCTION'\\''\\1,\\2\"$DBNAME\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_UNIT_TEST'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''DB_UNIT_TEST'\\''\\1,\\2\"$DBNAMEUNITTEST\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_USERNAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\''DB_USERNAME'\''\\1,\\2\"$DBUSER\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_PASSWORD'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''DB_PASSWORD'\\''\\1,\\2\"$DBPWD\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''TESTKEY'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''TESTKEY'\\''\\1,\\2\"$TESTKEY\");/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\''DB_FIRST_STUDENT_RECORD'\'',.*);/define('\''DB_FIRST_STUDENT_RECORD'\'', '"${DBFIRSTSTUDENT}"');/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\''DB_FIRST_TEST_ASSIGNMENT'\'',.*);/define('\''DB_FIRST_STUDENT_RECORD'\'', '"${DBFIRSTTEST}"');/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\''SKIP_ADP_COMM'\'',.*);/define('\''SKIP_ADP_COMM'\'', '"${SKIPADPCOMM}"');/g' '$DIR/ldr_svcs/config.php'"



cmd "sed -i 's/define('\\''ADP_HOSTNAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''ADP_HOSTNAME'\\''\\1,\\2\"$ADPHOST\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''ADP_REQUEST_SCHEME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''ADP_REQUEST_SCHEME'\\''\\1,\\2\"$ADPREQUESTSCHEME\");/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''ADP_RESOURCE_PATH'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''ADP_RESOURCE_PATH'\\''\\1,\\2\"$ADPRESOURCEPATH\");/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\\''LOG_REQUESTS'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)true);/define('\\''LOG_REQUESTS'\\''\\1,\\2$LOGREQUESTS);/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''LOG_RESPONSES'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)true);/define('\\''LOG_RESPONSES'\\''\\1,\\2$LOGRESPONSES);/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''SKIP_PROCESSING'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)false);/define('\\''SKIP_PROCESSING'\\''\\1,\\2$SKIPPROCESSING);/g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's/define('\\''SKIP_VALIDATION'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''SKIP_VALIDATION'\\''\\1,\\2\"$SKIPVALIDATION\");/g' '$DIR/ldr_svcs/config.php'"

cmd "sed -i 's/define('\\''ROOT_ORGANIZATION_NAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"root\");/define('\\''ROOT_ORGANIZATION_NAME'\\''\\1,\\2\"PARCC\");/g' '$DIR/ldr_svcs/config.php'"

#AWS Parsing
cmd "sed -i 's|define('\\''ADP_S3_CONTENT_BUCKET'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");|define('\\''ADP_S3_CONTENT_BUCKET'\\''\\1,\\2\"$ADPS3BUCKET\");|g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's|define('\\''ADP_S3_CONTENT_ROOT'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");|define('\\''ADP_S3_CONTENT_ROOT'\\''\\1,\\2\"${ADPS3CONTENT}\");|g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's|define('\\''ADP_S3_RESULTS_BUCKET'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");|define('\\''ADP_S3_RESULTS_BUCKET'\\''\\1,\\2\"$ADPS3RESULTSBUCKET\");|g' '$DIR/ldr_svcs/config.php'"
cmd "sed -i 's|define('\\''ADP_S3_RESULTS_ROOT'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");|define('\\''ADP_S3_RESULTS_ROOT'\\''\\1,\\2\"${ADPS3RESULTS}\");|g' '$DIR/ldr_svcs/config.php'"


# modify client configuration file
cmd "cp '$DIR/ldr_client/default.config.php' '$DIR/ldr_client/config.php'"
cmd "sed -i 's/define('\\''REQUEST_SCHEME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"https\");/define('\''REQUEST_SCHEME'\''\\1,\\2\"$REQUESTSCHEME\");/g' '$DIR/ldr_client/config.php'"
cmd "sed -i 's/define('\\''LDR_HOSTNAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"127.0.0.1\");/define('\''LDR_HOSTNAME'\''\\1,\\2\"$LDRCLIENTHOSTNAME\");/g' '$DIR/ldr_client/config.php'"
# test to determine if APPDIR contains objects
if [ -z "$APPDIR" ];then
	cmd "sed -i 's/define('\\''LDR_RESOURCE_PATH'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\/ldr_svcs\/\");/define('\''LDR_RESOURCE_PATH'\''\\1,\\2\"\/ldr_svcs\/\");/g' '$DIR/ldr_client/config.php'"
else
	cmd "sed -i 's/define('\\''LDR_RESOURCE_PATH'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\/ldr_svcs\/\");/define('\''LDR_RESOURCE_PATH'\''\\1,\\2\"\/$APPDIR\/ldr_svcs\/\");/g' '$DIR/ldr_client/config.php'"
fi

# Create unit-test tables, or database update scripts will fail.
cmd "echo '$DBNAME''_unit_test' > '$DIR/ldr_svcs/selected_database.txt'"
cmd "chown -R ""$OWNER_GROUP"":""$OWNER_USER"" -R '$DIR'"
cmd "chmod g+w,u+w '$DIR/ldr_svcs/selected_database.txt'"

REQUEST="wget --server-response --no-cache --no-cookies --no-check-certificate -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/ldr_client/create_all_tables.php"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"
STATUS=$(status "$RESPONSE")
if [ "$STATUS" = 200 ];then
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		SUCCESS=$(echo "$RESPONSE" | grep "Base table or view already exists")
		if [ -z "$SUCCESS" ];then
			log "Failed to create unit-test tables." error
			exit 1
		else
			log "Unit-test tables already exist." warn
		fi
	else
		log "Unit-test tables created." ok
	fi
else
	log "Failed to create unit_test tables." error
	exit 1
fi

# Create LDR tables
cmd "echo '$DBNAME' > '$DIR/ldr_svcs/selected_database.txt'"
cmd "chown -R ""$OWNER_GROUP"":""$OWNER_USER"" -R '$DIR'"
cmd "chmod g+w,u+w '$DIR/ldr_svcs/selected_database.txt'"

REQUEST="wget --server-response --no-cache --no-cookies --no-check-certificate -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/ldr_client/create_all_tables.php"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"
STATUS=$(status "$RESPONSE")
if [ "$STATUS" = 200 ];then
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		SUCCESS=$(echo "$RESPONSE" | grep "Base table or view already exists")
		if [ -z "$SUCCESS" ];then
			log "Failed to create tables." error
			exit 1
		else
			log "Tables already exist." warn
		fi
	else
		log "Tables created." ok
	fi
else
	log "Failed to create tables." error
	exit 1
fi

# Check database version.
rm -f cookies.txt
REQUEST="wget --server-response --no-cache --no-check-certificate --save-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/ldr_client/login_user.php --post-data clientName=""$LDRCLIENTUSER""&password=""$LDRCLIENTPWD"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"

REQUEST="wget --server-response --no-cache --no-check-certificate --keep-session-cookies --load-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/ldr_client/get_database_versions.php"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"
STATUS=$(status "$RESPONSE")
if [ "$STATUS" = 200 ];then
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		log "Failed to get database version." error
	else
		UPDATE=$(echo "$RESPONSE" | grep "<p class=\"status\">DATABASES: OKAY</p>")
		if [ -z "$UPDATE" ];then
			echo "Updating tables..."
			REQUEST="wget --server-response --no-cache --no-check-certificate --keep-session-cookies --load-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/ldr_client/update_databases.php"
			log "$REQUEST"
			RESPONSE=$($REQUEST 2>&1)
			log "$RESPONSE"
			STATUS=$(status "$RESPONSE")
			if [ "$STATUS" = 200 ] || [ "$STATUS" = 302 ];then
				SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
				if [ -z "$SUCCESS" ];then
					log "Failed to update tables." error
					exit 1
				else
					UPDATE=$(echo "$RESPONSE" | grep "<p class=\"status\">DATABASES: OKAY</p>")
					if [ -z "$UPDATE" ];then
						log "Failed to update tables." error
					else
						log "Tables updated." ok
					fi
				fi
			else
				log "Failed to update tables." error
				exit 1
			fi
		else
			log "Tables up to date." ook
		fi
	fi
else
	log "Failed to create tables." error
	exit 1
fi

log "Complete!" ok
exit 0
