#!/usr/bin/env bash
timedatectl set-timezone America/Chicago

dbHost="%"
dbPass="test123"
dbIP="10.0.0.11"
dbPort="6033"

apt-get update >/dev/null 2>&1
apt-get -y upgrade >/dev/null 2>&1
apt-get -y autoremove >/dev/null 2>&1

if [ ! -f /var/log/apachesetup ]; then
    apt-get install -y apache2 apache2-utils >/dev/null 2>&1
    touch /var/log/apachesetup
fi

if [ ! -f /var/log/databasesetup ];
then
    debconf-set-selections <<< 'mysql-server mysql-server/root_password password '$dbPass
    debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password '$dbPass
    apt-get install -y mysql-server

    if [ ! -f /var/log/phpsetup ]; then
        sed -i.bak -e 's/127.0.0.1/'$dbIP'/' /etc/mysql/mysql.conf.d/mysqld.cnf
        sed -i.bak -e 's/3306/'$dbPort'/' /etc/mysql/mysql.conf.d/mysqld.cnf
    else
        dbHost="localhost"
    fi

    mysql -u root -ptest123 -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'"$dbHost"' IDENTIFIED BY '"$dbPass"';"
    mysql -u root -ptest123 -e "FLUSH PRIVILEGES;"

    mysql -u root -ptest123 -e "CREATE DATABASE ldr_tenant1;"
    mysql -u root -ptest123 -e "CREATE DATABASE ldr_tenant2;"
    mysql -u root -ptest123 -e "CREATE DATABASE ldr_test;"

    mysql -u root -ptest123 -e "CREATE USER 'dbTenant1'@'"$dbHost"' IDENTIFIED BY '"$dbPass"';"
    mysql -u root -ptest123 -e "GRANT ALL PRIVILEGES ON ldr_tenant1.* TO 'dbTenant1'@'"$dbHost"';"
    mysql -u root -ptest123 -e "FLUSH PRIVILEGES;"

    mysql -u root -ptest123 -e "CREATE USER 'dbTenant2'@'"$dbHost"' IDENTIFIED BY '"$dbPass"';"
    mysql -u root -ptest123 -e "GRANT ALL PRIVILEGES ON ldr_tenant2.* TO 'dbTenant2'@'"$dbHost"';"
    mysql -u root -ptest123 -e "FLUSH PRIVILEGES;"

    mysql -u root -ptest123 -e "CREATE USER 'dbTest'@'"$dbHost"' IDENTIFIED BY '"$dbPass"';"
    mysql -u root -ptest123 -e "GRANT ALL PRIVILEGES ON ldr_test.* TO 'dbTest'@'"$dbHost"';"
    mysql -u root -ptest123 -e "FLUSH PRIVILEGES;"

    mysql -u root -ptest123 -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'"$dbHost"';"
    mysql -u root -ptest123 -e "FLUSH PRIVILEGES;"

    touch /var/log/databasesetup
fi

if [ ! -f /var/log/phpsetup ]; then
    apt-get install -y php5 libapache2-mod-php5 php5-mysql php5-mcrypt php5-common php5-cli php5-xdebug php5-apcu php5-curl php5-xhprof >/dev/null 2>&1

    touch /var/log/phpsetup
fi

if [ ! -f /var/log/phpmyadmin ]; then
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password '$dbPass
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password '$dbPass
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password '$dbPass
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'

    apt-get -y install phpmyadmin >/dev/null 2>&1
    php5enmod mcrypt

    echo "AuthType Basic" >> /usr/share/phpmyadmin/.htaccess
    echo 'AuthName "Restricted Files"' >> /usr/share/phpmyadmin/.htaccess
    echo "AuthUserFile /etc/phpmyadmin/.htpasswd" >> /usr/share/phpmyadmin/.htaccess
    echo "Require valid-user" >> /usr/share/phpmyadmin/.htaccess
    sed -i.bak '/DirectoryIndex/ a     AllowOverride All' /etc/apache2/conf-available/phpmyadmin.conf

    htpasswd -b -c /etc/phpmyadmin/.htpasswd root "$dbPass"

    touch /var/log/phpmyadmin
fi
