<?php

// REST Frameworks
define('REST_FRAMEWORK_SLIM',       1);
define('REST_FRAMEWORK_PHALCON',    2);

// Database Migration Types
define('MIGRATION_FORWARD',         1);
define('MIGRATION_BACKWARD',        2);

// Redis Status Changes
define('REDIS_STATUS_UPGRADE',      1);
define('REDIS_STATUS_DOWNGRADE',    2);

// Status values for continuous integration (CI) system
define('CI_STATUS_WORD',	'STATUS');
define('CI_STATUS_OKAY',	'OKAY');
define('CI_STATUS_FAIL',	'FAIL');

// Client Types
define('CLIENT_TYPE_DEVELOPMENT',   1);
define('CLIENT_TYPE_PRODUCTION',    2);

// Clients
define('CLIENT_ID_LDR_ROOT',         1);
define('CLIENT_ID_PARCC_ADP',        2);
define('CLIENT_ID_PARCC_CI',         3);
define('CLIENT_ID_PARCC_DEV',        4);
define('CLIENT_ID_PARCC_QA',         5);
define('CLIENT_ID_PARCC_TAP',        6);
define('CLIENT_ID_PARCC_USER',       7);
define('CLIENT_ID_PROD_ADP',         8);
define('CLIENT_ID_PROD_TAP',         9);
define('CLIENT_ID_PROD_USER',       10);

// User Auditing
define('AUDIT_ACTION_CREATE',       0x1);
define('AUDIT_ACTION_UPDATE',       0x2);
define('AUDIT_ACTION_DELETE',       0x4);
define('AUDIT_ACTION_LAST_TYPE',    0x4);
define('AUDIT_ACTION_ALL_TYPES',    0x7);

define('AUDIT_DATA_ORGANIZATION',   0x1);
define('AUDIT_DATA_STUDENT_RECORD', 0x2);
define('AUDIT_DATA_CLASS',          0x4);
define('AUDIT_DATA_CLASS_STUDENT',  0x8);
define('AUDIT_DATA_LAST_TYPE',      0x8);
define('AUDIT_DATA_ALL_TYPES',      0xF);

// Logging definitions
define('LOG_NAME_CALL_ADP',	    '_ldr_call_adp.log');
define('LOG_NAME_DEBUG',	    '_ldr_debug.log');
define('LOG_NAME_ERROR',	    '_ldr_error.log');
define('LOG_NAME_TEST_STATUS',  '_ldr_test_status.log');
define('LOG_NAME_REQUEST',	    '_ldr_request.log');
define('LOG_NAME_RESPONSE',	    '_ldr_response.log');

// Test Assignment Instruction Status
define('INSTRUCTION_STATUS_NONE',   0);
define('INSTRUCTION_STATUS_PRE',    1);
define('INSTRUCTION_STATUS_POST',   2);

// Test Status IDs
define('TEST_STATUS_ID_SCHEDULED',  1);
define('TEST_STATUS_ID_INPROGRESS', 2);
define('TEST_STATUS_ID_PAUSED',     3);
define('MAX_ACTIVE_TEST_STATUS_ID', 3);
define('TEST_STATUS_ID_SUBMITTED',  4);
define('TEST_STATUS_ID_COMPLETED',  5);
define('TEST_STATUS_ID_CANCELED',   6);

// Organization Types
// PARCC_Org_File_Field_Definitions_V1.0.pdf p.10 column A
define('ORG_TYPE_NONE',	        0);
define('ORG_TYPE_ROOT',	        1);
define('ORG_TYPE_STATE',	    2);
define('ORG_TYPE_DISTRICT',     3);
define('ORG_TYPE_SCHOOL',	    4);
define('ORG_TYPE_MAX',          4);

// School Types
define('SCHOOL_TYPE_NONE',			0);     // TODO: Check if these grade categories have changed
define('SCHOOL_TYPE_PRIMARY',		1);		// Grades: 3, 4, 5, 6
define('SCHOOL_TYPE_MIDDLE',		2);		// Grades: 7, 8
define('SCHOOL_TYPE_HIGH',			3);		// Grades: HS
define('SCHOOL_TYPE_MIDDLE_HIGH',	4);		// Grades: 7, 8, HS

$schoolTypeText = ['SCHOOL_TYPE_NONE', 'SCHOOL_TYPE_PRIMARY', 'SCHOOL_TYPE_MIDDLE', 'SCHOOL_TYPE_HIGH',
                   'SCHOOL_TYPE_MIDDLE_HIGH'];

// Session token
define('TOKEN_REQUIRED', 1);	// TODO: Token is required
// define('TOKEN_REQUIRED', 2);    // Token is optional for debugging
define('TOKEN_OPTIONAL', 2);	// Token is optional

define('FIELD_SEPARATOR', '-');

// Data checks and operations
define('DCO_OPTIONAL',      0x0);   // Argument is optional
define('DCO_REQUIRED',      0x1);   // Argument is required
define('DCO_TOLOWER',       0x2);   // Argument will be converted to lower-case
define('DCO_TOUPPER',       0x4);   // Argument will be converted to upper-case
define('DCO_MUST_EXIST',    0x8);   // Argument must exist in database
define('DCO_NO_CHECK',      0x10);  // Argument will be checked by calling service function
define('DCO_TRIM',          0x20);  // Argument will have leading and trailing whitespace removed
define('DCO_URL_DECODE',    0x40);  // Argument will be decoded per RFC-3986

// Data filter types
define('ORGANIZATION_PATH_SEPARATOR',   '_');
define('DFT_ALPHA_MIXED',               'A-Za-z');
define('DFT_ALPHA_MIXED_PUNCT_A',       'A-Za-z_');
define('DFT_ALPHA_MIXED_PUNCT_B',       'A-Za-z\/');
define('DFT_ALPHANUM_MIXED',            '0-9A-Za-z');
define('DFT_ALPHANUM_MIXED_PUNCT_A',    '0-9A-Za-z_');
define('DFT_ALPHANUM_MIXED_PUNCT_B',    '-0-9A-Za-z');
define('DFT_ALPHANUM_MIXED_PUNCT_C',    '-0-9A-Za-z .()\'\/\&\#\+\!\:');
define('DFT_ALPHANUM_MIXED_PUNCT_D',    '-0-9A-Za-z .()\'\/\&\#\+\!\:_');
define('DFT_ALPHANUM_MIXED_PUNCT_E',    '-0-9A-Za-z .()\'\/\&\#\+\!\:\%,');
define('DFT_ALPHANUM_MIXED_PUNCT_F',    '-0-9A-Za-z .\'');
define('DFT_ALPHANUM_MIXED_PUNCT_G',    '-0-9A-Za-z .()\'\/\\\&\+');
define('DFT_ALPHANUM_MIXED_PUNCT_H',    '-0-9A-Za-z .()\'\/\\\&\#\+\!\:_');
define('DFT_ALPHANUM_UPPER',            '0-9A-Z');
define('DFT_DATE',                      '-0-9');
define('DFT_DATETIME',                  '-0-9 :');
define('DFT_GENDER',                    'fFmM');
define('DFT_GUID_UPPER',                '-0-9A-F');
define('DFT_HEXADECIMAL_UPPER',         '0-9A-F');
define('DFT_ITEMS',                     'i0-9A-Za-z');
define('DFT_NATURAL_NUMBER',            '0-9');
define('DFT_NATURAL_NUMBER_LIST',       '0-9,');
define('DFT_NONE',                       null);
define('DFT_REDIS_FIELD',               '0-9A-Za-z_');
define('DFT_REDIS_KEY',                 '0-9A-Za-z_:*');
define('DFT_REDIS_VALUE',               '0-9A-Za-z ');
define('DFT_AWS_S3_KEY',                '-0-9A-Za-z.\/');
define('DFT_UNICODE',                    null); // TODO: IMPLEMENT SQL INJECTION FILTER
define('DFT_YES_NO',                    'yYnN');

// Database Column Fixed and Maximum Lengths
//  [A] = PARCC_Org_File_Field_Definitions_V1.0.pdf
//  [B] = PARCC_Student_Data_File_Field_Definitions_V1.0.pdf
define('LENGTH_GRADE_LEVEL', 2);                // [B] p.14
define('LENGTH_GUID_RFC4122', 36);              // [B] p.13
define('LENGTH_SESSION_TOKEN', 40);
define('LENGTH_STATE_NAME', 2);                 // [B] p.11
define('LENGTH_TEST_KEY', 10);
define('MAXLEN_CALCULATOR_TYPE', 24);
define('MAXLEN_CLASS_IDENTIFIER', 38);          // [B] pp.19
define('MAXLEN_CLIENT_PASSWORD', 128);
define('MAXLEN_CLIENT_TYPE', 1);
define('MAXLEN_CLIENT_URL', 100);
define('MAXLEN_DISABILITY_TYPE', 3);            // [B] p.17
//define('MAXLEN_NAP_USERNAME', 100);
define('MAXLEN_OPTIONAL_INFORMATION', 255);
define('MAXLEN_OPTIONAL_STATE_DATA', 20);       // [B] pp.13,17,18
define('MAXLEN_ORGANIZATION_IDENTIFIER', 15);   // [A] pp.10-11
define('MAXLEN_ORGANIZATION_NAME', 60);         // [A] p.11
define('MAXLEN_QTI_CLASS', 32);
define('MAXLEN_REGISTRATION_KEY', 32);
define('MAXLEN_RESPONSE_CARDINALITY', 12);
define('MAXLEN_SCORING_STATUS', 25);
define('MAXLEN_SECTION_NUMBER', 50);
define('MAXLEN_STUDENT_LOCAL_IDENTIFIER', 30);  // [B] p.13
define('MAXLEN_STUDENT_STATE_IDENTIFIER', 30);  // [B] p.13
define('MAXLEN_STUDENT_FIRST_NAME', 35);        // [B] p.14
define('MAXLEN_STUDENT_LAST_NAME', 35);         // [B] p.14
define('MAXLEN_STUDENT_MIDDLE_NAME', 35);       // [B] p.14
define('MAXLEN_TEST_BATTERY_DESCRIPTION', 4096);
define('MAXLEN_TEST_BATTERY_FORM_NAME', 20);
define('MAXLEN_TEST_BATTERY_GRADE', 16);
define('MAXLEN_TEST_BATTERY_NAME', 100);
define('MAXLEN_TEST_BATTERY_PERMISSIONS', 24);
define('MAXLEN_TEST_BATTERY_PROGRAM', 32);
define('MAXLEN_TEST_BATTERY_SCORING', 24);
define('MAXLEN_TEST_BATTERY_SECURITY', 24);
define('MAXLEN_TEST_BATTERY_SUBJECT', 16);
define('MAXLEN_TEST_FORM_ANY_IDENTIFIER', 60);
define('MAXLEN_TEST_FORM_ANY_SERIAL', 24);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_CLUSTER', 16);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_CONTENT_STANDARD', 16);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_EVIDENCE_STATEMENT', 16);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_DOMAIN', 16);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_FLUENCY_SKILL_AREA', 100);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEM_STRAND', 100);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEMID', 24);
define('MAXLEN_TEST_FORM_ASSESSMENT_ITEM_UIN', 16);
define('MAXLEN_TEST_FORM_ASSESSMENT_SECTION_TITLE', 100);
define('MAXLEN_TEST_FORM_PART_NAVIGATION_MODE', 20);
define('MAXLEN_TEST_FORM_PART_SUBMISSION_MODE', 20);
define('MAXLEN_TEST_FORM_REVISION_IDENTIFIER', 100);
define('MAXLEN_TEST_FORM_REVISION_TITLE', 100);
define('MAXLEN_TEST_FORM_SYSTEM_ITEM_ITEMID', 24);
define('MAXLEN_TEST_RESULTS_TOOLS_USED', 48);
define('MAXLEN_TEST_STATUS', 16);
define('MAXLEN_UPDATE_REASON', 255);

// Miscellaneous Definitions
define('ADMIN_ORGANIZATION_ID', 1);
define('CR', "\n");
define('DEFAULT_DELIVERY_SERVER_ID', '1');
define('DELETE_THIS_VALUE', 'DELETE_THIS_VALUE');
define('EXPORT_DIR', '../export/');
define('MAX_AUDIT_RECORDS', 500);
define('MINLEN_PASSWORD', 8);
define('NOT_PUBLISHED', 'unpublished');
define('REDIS_COMMAND_FAILED', 'command failed');
define('ROOT_CLIENT_USER_ID', '1');
define('ROOT_ORGANIZATION_ID', '1');
define('ROOT_PARENT_ORGANIZATION_ID', '0');
define('SECONDS_PER_DAY', 86400);
define('SECONDS_PER_HOUR', 3600);
define('SUCCESS', 'success');

// HAL Severity Levels
define('HAL_SEVERITY_EMERGENCY', '0');
define('HAL_SEVERITY_ALERT', '1');
define('HAL_SEVERITY_CRITICAL', '2');
define('HAL_SEVERITY_ERROR', '3');
define('HAL_SEVERITY_WARNING', '4');
define('HAL_SEVERITY_NOTICE', '5');
define('HAL_SEVERITY_INFO', '6');
define('HAL_SEVERITY_DEBUG', '7 ');

// HAL Event Types
define('HAL_LDR_PASSWORD_HASH_REQUESTED', '5300');

//
// LDR Error Codes
//
// Next Available: refer to https://breaktech.atlassian.net/wiki/display/ADS/Error+Codes
define('LDR_EC_ADP_ERROR_RESPONSE', '2073');
define('LDR_EC_ADP_NO_TEST_ASSIGNMENT_ID', '2072');
define('LDR_EC_ADP_PING_FAILED', '2090');
define('LDR_EC_ADP_RESPONSE_DECODE_FAILED', '2096');
define('LDR_ED_ADP_RESPONSE_DECODE_FAILED', 'Unable to decode this ADP response: ');
define('LDR_EC_ADP_RESULTS_NO_RESULTS_ID', '2105');
define('LDR_EC_ADP_RESULTS_NO_REVISION_ID', '2106');
define('LDR_EC_ADP_STATUS_CHANGE_IGNORED', '2112');
define('LDR_EC_ADP_STATUS_CHANGE_INVALID', '2110');
define('LDR_EC_ADP_STATUS_CHANGE_UNKNOWN', '2109');
define('LDR_EC_ADP_STATUS_INVALID', '2111');
define('LDR_EC_ARG_INVALID_CHARACTERS', '2016');
define('LDR_EC_ARG_LENGTH_TOO_LONG', '2035');
define('LDR_EC_ARG_NOT_PROVIDED', '2014');
define('LDR_EC_ARG_VALUE_IS_UNDEFINED', '2065');
define('LDR_EC_AUDIT_ACTION_TYPE_UNKNOWN', '2049');
define('LDR_EC_AUDIT_DATA_TYPE_UNKNOWN', '2048');
define('LDR_EC_AWS_ERROR_RESPONSE', '2095');
define('LDR_EC_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS', '2153');
define('LDR_ED_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS', 'Unable to automatically create test assignments');
define('LDR_EC_CANNOT_DELETE_TEST_ASSIGNMENTS', '2160');
define('LDR_ED_CANNOT_DELETE_TEST_ASSIGNMENTS', 'Unable to delete test assignments for studentRecordId: ');
define('LDR_EC_CANNOT_FIND_DATABASE_MIGRATION_FILE', '2186');
define('LDR_ED_CANNOT_FIND_DATABASE_MIGRATION_FILE', 'Cannot find database migration file: ');
define('LDR_EC_CANNOT_SOFT_DELETE_TEST_ASSIGNMENTS', '2161');
define('LDR_ED_CANNOT_SOFT_DELETE_TEST_ASSIGNMENTS', 'Test assignments were successfully deleted from ADP but were not soft deleted from LDR. testAssignmentIds: ');
define('LDR_EC_CLASS_HAS_STUDENTS', '2058');
define('LDR_EC_CLASS_ID_NOT_FOUND', '2040');
define('LDR_ED_CLASS_ID_NOT_FOUND', 'Class not found for classId ');
define('LDR_EC_CLASS_IDENTIFIER_DUP', '2045');
define('LDR_EC_CLASS_NOT_FOUND', '2093');
define('LDR_EC_CLASS_SECTION_DUP', '2041');
define('LDR_EC_CLASS_STUDENT_DUP', '2044');
define('LDR_EC_CLASSES_HAVE_NO_STUDENTS', '2086');
define('LDR_EC_CLIENT_NOT_AUTHORIZED', '2064');
define('LDR_ED_CLIENT_NOT_AUTHORIZED', 'This client is not authorized to run the service: ');
define('LDR_EC_CREDENTIALS_NOT_VALID', '2004');
define('LDR_ED_CREDENTIALS_NOT_VALID', 'Client authentication credentials are not valid');
define('LDR_EC_CURL_ERROR_RESPONSE', '2074');
define('LDR_EC_COULDNOT_ADJUST_STUDENT_COUNT', '2156');
define('LDR_ED_COULDNOT_ADJUST_STUDENT_COUNT', 'Unable to adjust studentCount');
define('LDR_EC_DATABASE_CONNECT_FAILED', '2006');
define('LDR_EC_DATABASE_RECORD_NOT_FOUND', '2023');
define('LDR_ED_DATABASE_RECORD_NOT_FOUND', 'Database record not found for ');
define('LDR_EC_DATABASE_SELECTION_DISABLED', '2001');
define('LDR_EC_DATABASE_SELECTION_ENABLED', '2116');
define('LDR_EC_DATABASE_SELECTION_FAILED', '2059');
define('LDR_ED_DATABASE_SELECTION_FAILED', 'Database selection failed');
define('LDR_EC_DATABASE_SET_ATTRIBUTE_FAILED', '2015');
define('LDR_EC_DATABASE_TABLE_UNKNOWN_TABLE', '2017');
define('LDR_ED_DATABASE_TABLE_UNKNOWN_TABLE', 'This database table is unknown: ');
define('LDR_EC_DATA_IS_EMPTY', '2165');
define('LDR_ED_DATA_IS_EMPTY', 'Input array is empty');
define('LDR_EC_DATA_IS_NOT_AN_ARRAY', '2164');
define('LDR_ED_DATA_IS_NOT_AN_ARRAY', 'Input data is not an array');
define('LDR_EC_DATE_CANNOT_BE_FUTURE_DATE', '2174');
define('LDR_EC_DATE_CANNOT_BE_TODAY_DATE', '2175');
define('LDR_EC_DATE_RANGE_EXCEEDS_MAX', '2177');
define('LDR_EC_DATE_INVALID_FORMAT', '2036');
define('LDR_EC_DATE_INVALID_DATE', '2037');
define('LDR_EC_DATETIME_INVALID_FORMAT', '2050');
define('LDR_EC_DISABILITY_TYPE_UNKNOWN', '2038');
define('LDR_EC_DISABILITY_ARGS_ARE_INCONSISTENT', '2039');
define('LDR_EC_DUPLICATE_VALUE', '2191');
define('LDR_EC_EMBEDDED_SCORE_REPORT_NOT_FOUND', '2100');
define('LDR_EC_END_DATE_BEFORE_START_DATE', '2176');
define('LDR_ED_END_DATE_BEFORE_START_DATE', 'The endDate is earlier than the startDate');
define('LDR_EC_END_DATETIME_BEFORE_START_DATETIME', '2055');
define('LDR_ED_END_DATETIME_BEFORE_START_DATETIME', 'The endDateTime is earlier than the startDateTime');
define('LDR_EC_FAILED_TO_UPDATE_TEST_ASSIGNMENT', '2155');
define('LDR_EC_GRADE_LEVEL_UNKNOWN', '2024');
define('LDR_EC_HAL_LOG_REQUEST_FAILED', '2025');
define('LDR_EC_HAL_RESPONSE_DECODE_FAILED', '2097');
define('LDR_ED_HAL_RESPONSE_DECODE_FAILED', 'Unable to decode this HAL response: ');
define('LDR_EC_INSTRUCTION_STATUS_UNKNOWN', '2078');
define('LDR_EC_INVALID_CSV_DELIM', '2167');
define('LDR_ED_INVALID_CSV_DELIM', 'Invalid CSV delim character: ');
define('LDR_EC_INVALID_CSV_ESCAPE_CHARACTER', '2166');
define('LDR_ED_INVALID_CSV_ESCAPE_CHARACTER', 'Invalid CSV escape character: ');
define('LDR_EC_INVALID_CSV_NEWLINE', '2168');
define('LDR_ED_INVALID_CSV_NEWLINE', 'Invalid CSV newline');
define('LDR_EC_INVALID_DATE_FORMAT', '2172');
define('LDR_ED_INVALID_DATE_FORMAT', 'Invalid date format: ');
define('LDR_EC_INVALID_S3_OBJECT_KEY', '2151');
define('LDR_ED_INVALID_S3_OBJECT_KEY', 'Invalid S3 Object Key. S3 Object Key: ');
define('LDR_EC_MISSING_END_DATE', '2185');
define('LDR_ED_MISSING_END_DATE', 'Missing end date');
define('LDR_EC_MISSING_INPUT_DATA', '2190');
define('LDR_EC_MISSING_INSERT_DATA', '2189');
define('LDR_ED_MISSING_INSERT_DATA', 'Missing insert statement values');
define('LDR_EC_MISSING_ITEMS_EXPORT_DATA', '2178');
define('LDR_ED_MISSING_ITEMS_EXPORT_DATA', 'Missing expected items from the data export');
define('LDR_EC_MISSING_ITEM_ELEMENTS_EXPORT_DATA', '2179');
define('LDR_ED_MISSING_ITEM_ELEMENTS_EXPORT_DATA', 'Missing expected item elements from the data export');
define('LDR_EC_MISSING_ITEM_ELEMENT_RESPONSES_EXPORT_DATA', '2181');
define('LDR_ED_MISSING_ITEM_ELEMENT_RESPONSES_EXPORT_DATA', 'Missing expected item element responses from the data export');
define('LDR_EC_MISSING_ITEM_RESULTS_EXPORT_DATA', '2180');
define('LDR_ED_MISSING_ITEM_RESULTS_EXPORT_DATA', 'Missing expected item results from the data export');
define('LDR_EC_MISSING_FORMS_EXPORT_DATA', '2171');
define('LDR_ED_MISSING_FORMS_EXPORT_DATA', 'Missing expected forms from the data export');
define('LDR_EC_MISSING_ITEMS_MAX_POINTS', '2192');
define('LDR_ED_MISSING_ITEMS_MAX_POINTS', 'Missing max points for Items');
define('LDR_EC_MISSING_OR_MULTIPLE_ARGS', '2085');
define('LDR_EC_MISSING_ORGANIZATIONS_EXPORT_DATA', '2170');
define('LDR_ED_MISSING_ORGANIZATIONS_EXPORT_DATA', 'Missing expected organizations from the data export');
define('LDR_EC_MISSING_START_DATE', '2184');
define('LDR_ED_MISSING_START_DATE', 'Missing start date');
define('LDR_EC_MISSING_STUDENTS_EXPORT_DATA', '2169');
define('LDR_ED_MISSING_STUDENTS_EXPORT_DATA', 'Missing expected students from the data export');
define('LDR_EC_MISSING_TABLE_NAME', '2188');
define('LDR_ED_MISSING_TABLE_NAME', 'Missing table name');
define('LDR_EC_NESTED_SETS_ARE_DISABLED', '2122');
define('LDR_ED_NESTED_SETS_ARE_DISABLED', 'Nested sets are disabled');
define('LDR_EC_NO_TEST_BATTERY_FOR_GRADE', '2158');
define('LDR_ED_NO_TEST_BATTERY_FOR_GRADE', 'There is no test battery available for the requested grade: ');
define('LDR_EC_NO_TEST_BATTERY_FORM_FOUND', '2150');
define('LDR_ED_NO_TEST_BATTERY_FORM_FOUND', 'There are no test battery forms in the system yet');
define('LDR_EC_ORGANIZATION_CANNOT_BE_MODIFIED', '2088');
define('LDR_EC_ORGANIZATION_CANNOT_ENROLL_STUDENTS', '2028');
define('LDR_ED_ORGANIZATION_CANNOT_ENROLL_STUDENTS', 'This organization cannot enroll students - organizationId: ');
define('LDR_EC_ORGANIZATION_CANNOT_HAVE_CLASSES', '2042');
define('LDR_EC_ORGANIZATION_HAS_CLASSES', '2054');
define('LDR_EC_ORGANIZATION_HAS_NO_STUDENTS', '2087');
define('LDR_EC_ORGANIZATION_HAS_STUDENTS', '2053');
define('LDR_EC_ORGANIZATION_ID_NOT_FOUND', '2019');
define('LDR_ED_ORGANIZATION_ID_NOT_FOUND', 'Organization not found for organizationId ');
define('LDR_EC_ORGANIZATION_IS_PARENT', '2052');
define('LDR_EC_ORGANIZATION_TYPE_IS_NOT_SCHOOL', '2152');
define('LDR_EC_ORGANIZATION_TYPE_UNKNOWN', '2027');
define('LDR_EC_PARENT_ORGANIZATION_TYPE_MISMATCH', '2092');
define('LDR_EC_PEER_ORGANIZATION_CONFLICT', '2018');
define('LDR_ED_PEER_ORGANIZATION_CONFLICT', 'A peer organization already has the identifier ');
define('LDR_EC_REDIS_COMMMAND_FAILED', '2062');
define('LDR_EC_REDIS_COMMMAND_UNKNOWN', '2063');
define('LDR_EC_REDIS_CONNECT_FAILED', '2061');
define('LDR_EC_REQUEST_BODY_DECODE_FAILED', '2011');
define('LDR_ED_REQUEST_BODY_DECODE_FAILED', 'Unable to decode this request body: ');
define('LDR_EC_REQUEST_BODY_NOT_PROVIDED', '2010');
define('LDR_ED_REQUEST_BODY_NOT_PROVIDED', 'Required request body data not provided');
define('LDR_EC_QUERY_EXCEPTION', '2002');
define('LDR_EC_SCORE_REPORT_DECODE_FAILED', '2099');
define('LDR_ED_SCORE_REPORT_DECODE_FAILED', 'Unable to decode score report');
define('LDR_EC_STATE_CODE_UNKNOWN', '2026');
//define('LDR_EC_STATE_ORG_PARENT_NOT_ROOT_ORG', '2029');
//define('LDR_ED_STATE_ORG_PARENT_NOT_ROOT_ORG', 'State organization parent must be PARCC organization');
define('LDR_EC_STATE_ORGANIZATION_CONFLICT', '2030');
define('LDR_EC_STATE_ORGANIZATION_NOT_FOUND', '2091');
define('LDR_ED_STATE_ORGANIZATION_CONFLICT', 'A state organization already has the code ');
define('LDR_EC_STUDENT_CLASS_ORG_MISMATCH', '2043');
define('LDR_EC_STUDENT_COUNT_MISMATCH', '2159');
define('LDR_EC_STUDENT_GUID_DATA_NOT_VALID', '2033');
define('LDR_EC_STUDENT_GUID_NOT_FOUND', '2032');
define('LDR_EC_STUDENT_HAS_ACTIVE_TEST_ASSIGNMENT', '2082');
define('LDR_EC_STUDENT_HAS_CLASSES', '2056');
define('LDR_EC_STUDENT_HAS_TEST_ASSIGNMENTS', '2094');
define('LDR_EC_STUDENT_NOT_IN_CLASS', '2057');
define('LDR_EC_STUDENT_RECORD_ID_NOT_FOUND', '2034');
define('LDR_ED_STUDENT_RECORD_ID_NOT_FOUND', 'Student record not found for studentRecordId ');
define('LDR_EC_STUDENT_STATE_IDENTIFIER_DUP', '2031');
define('LDR_EC_TEST_ASSIGNMENT_CANNOT_BE_CANCELED', '2104');
define('LDR_EC_TEST_ASSIGNMENT_CANNOT_BE_UNLOCKED', '2103');
define('LDR_EC_TEST_ASSIGNMENT_IS_ACTIVE', '2077');
define('LDR_ED_TEST_ASSIGNMENT_IS_ACTIVE', 'Test assignment is still active for testAssignmentId ');
define('LDR_EC_TEST_ASSIGNMENT_IS_INCORRECT', '2154');
define('LDR_EC_TEST_ASSIGNMENT_RECORD_ID_NOT_FOUND', '2076');
define('LDR_ED_TEST_ASSIGNMENT_RECORD_ID_NOT_FOUND', 'Test assignment record not found for testAssignmentId ');
define('LDR_EC_TEST_BATTERY_DUP', '2070');
define('LDR_EC_TEST_BATTERY_FORM_DUP', '2071');
define('LDR_EC_TEST_BATTERY_FORM_ID_CONFLICT', '2068');
define('LDR_ED_TEST_BATTERY_FORM_ID_CONFLICT', 'An existing test battery form already has the ID ');
define('LDR_EC_TEST_BATTERY_FORM_ID_NOT_FOUND', '2067');
define('LDR_ED_TEST_BATTERY_FORM_ID_NOT_FOUND', 'Test battery form not found for testBatteryFormId ');
define('LDR_EC_TEST_BATTERY_ID_MISMATCH', '2069');
define('LDR_EC_TEST_BATTERY_ID_NOT_FOUND', '2066');
define('LDR_ED_TEST_BATTERY_ID_NOT_FOUND', 'Test battery not found for testBatteryId ');
define('LDR_EC_TEST_FORM_REVISION_ASSESSMENT_ITEMS_NOT_FOUND', '2119');
define('LDR_ED_TEST_FORM_REVISION_ASSESSMENT_ITEMS_NOT_FOUND', 'Assessment items not found');
define('LDR_EC_TEST_FORM_REVISION_ASSESSMENT_SECTIONS_NOT_FOUND', '2118');
define('LDR_ED_TEST_FORM_REVISION_ASSESSMENT_SECTIONS_NOT_FOUND', 'Assessment sections not found');
define('LDR_EC_TEST_FORM_REVISION_DECODE_FAILED', '2108');
define('LDR_ED_TEST_FORM_REVISION_DECODE_FAILED', 'Unable to decode this test form revision file: ');
define('LDR_EC_TEST_FORM_REVISION_HAS_CORRECT_RESPONSES', '2182');
define('LDR_ED_TEST_FORM_REVISION_HAS_CORRECT_RESPONSES', 'Test form revision already contains the correct responses. testFormRevisionId: ');
define('LDR_EC_TEST_FORM_REVISION_ID_NOT_FOUND', '2107');
define('LDR_ED_TEST_FORM_REVISION_ID_NOT_FOUND', 'Test form revision not found for testFormRevisionId ');
define('LDR_EC_TEST_FORM_REVISION_ITEM_BODY_NOT_FOUND', '2126');
define('LDR_ED_TEST_FORM_REVISION_ITEM_BODY_NOT_FOUND', 'Assessment item body not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_CHOICE_IDENTIFIER_NOT_FOUND', '2137');
define('LDR_ED_TEST_FORM_REVISION_ITEM_CHOICE_IDENTIFIER_NOT_FOUND', 'Assessment item element choice identifier not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_CONTENT_NOT_FOUND', '2120');
define('LDR_ED_TEST_FORM_REVISION_ITEM_CONTENT_NOT_FOUND', 'Assessment item content not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_CORRECT_NO_MATCHING_CHOICE', '2138');
define('LDR_ED_TEST_FORM_REVISION_ITEM_CORRECT_NO_MATCHING_CHOICE',
       'Assessment item element correct response has no matching choice');
define('LDR_EC_TEST_FORM_REVISION_ITEM_DATA_NOT_FOUND', '2121');
define('LDR_ED_TEST_FORM_REVISION_ITEM_DATA_NOT_FOUND', 'Assessment item data not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_ATTR_NOT_FOUND', '2129');
define('LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_ATTR_NOT_FOUND', 'Assessment item element attributes not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_CHOICES_NOT_FOUND', '2136');
define('LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_CHOICES_NOT_FOUND', 'Assessment item element choices not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_NO_MATCHING_RESPONSE', '2135');
define('LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_NO_MATCHING_RESPONSE', 'Assessment item element has no matching response');
define('LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_QTI_CLASS_NOT_FOUND', '2128');
define('LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_QTI_CLASS_NOT_FOUND', 'Assessment item element qtiClass not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_DUP', '2132');
define('LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_DUP',
       'This assessment item element responseIdentifier is a duplicate: ');
define('LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND', '2131');
define('LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND', 'Assessment item element responseIdentifier not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENTS_NOT_FOUND', '2127');
define('LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENTS_NOT_FOUND', 'Assessment item elements not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_INTERACTION_NOT_FOUND', '2130');
define('LDR_ED_TEST_FORM_REVISION_ITEM_INTERACTION_NOT_FOUND', 'Assessment item interaction not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_ATTR_NOT_FOUND', '2124');
define('LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_ATTR_NOT_FOUND', 'Assessment item response attributes not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND', '2125');
define('LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND', 'Assessment item response correctResponses not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_IDENTIFIER_NOT_FOUND', '2133');
define('LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_IDENTIFIER_NOT_FOUND', 'Assessment item response identifier not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_NO_MATCHING_ELEMENT', '2134');
define('LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_NO_MATCHING_ELEMENT', 'Assessment item response has no matching element');
define('LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSES_NOT_FOUND', '2123');
define('LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSES_NOT_FOUND', 'Assessment item responses not found');
define('LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_SERIAL_NOT_FOUND', '2183');
define('LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_SERIAL_NOT_FOUND', 'Assessment item response serial not found');
define('LDR_EC_TEST_FORM_REVISION_TEST_PARTS_NOT_FOUND', '2117');
define('LDR_ED_TEST_FORM_REVISION_TEST_PARTS_NOT_FOUND', 'Test parts not found');
define('LDR_EC_TEST_RESULTS_ASSESSMENT_ITEMS_NOT_FOUND', '2142');
define('LDR_ED_TEST_RESULTS_ASSESSMENT_ITEMS_NOT_FOUND', 'Test results assessment items not found');
define('LDR_EC_TEST_RESULTS_ASSESSMENT_SECTIONS_NOT_FOUND', '2141');
define('LDR_ED_TEST_RESULTS_ASSESSMENT_SECTIONS_NOT_FOUND', 'Test results assessment sections not found');
define('LDR_EC_TEST_RESULTS_DECODE_FAILED', '2098');
define('LDR_ED_TEST_RESULTS_DECODE_FAILED', 'Unable to decode test results file');
define('LDR_EC_TEST_RESULTS_ITEM_DYNAMIC_NOT_FOUND', '2143');
define('LDR_ED_TEST_RESULTS_ITEM_DYNAMIC_NOT_FOUND', 'Test results assessment item dynamic not found');
define('LDR_EC_TEST_RESULTS_ITEM_RESULTS_NOT_FOUND', '2144');
define('LDR_ED_TEST_RESULTS_ITEM_RESULTS_NOT_FOUND', 'Test results assessment item results not found');
define('LDR_EC_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_MISMATCH', '2146');
define('LDR_ED_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_MISMATCH', 'Test results item results identifier mismatch');
define('LDR_EC_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_NOT_FOUND', '2145');
define('LDR_ED_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_NOT_FOUND', 'Test results item results identifier not found');
define('LDR_EC_TEST_RESULTS_ITEM_RESULTS_DIRECTED_PAIR', '2187');
define('LDR_ED_TEST_RESULTS_ITEM_RESULTS_DIRECTED_PAIR', 'Test results item results directed pair element is invalid');
define('LDR_EC_TEST_RESULTS_ITEM_RESULTS_RESPONSE_UNKNOWN', '2148');
define('LDR_ED_TEST_RESULTS_ITEM_RESULTS_RESPONSE_UNKNOWN', 'Test results item results response unknown');
define('LDR_ED_TEST_RESULTS_TEST_LEVEL_RESULTS_NOT_FOUND', 'Test-level results not found');
define('LDR_EC_TEST_RESULTS_TEST_LEVEL_RESULTS_NOT_FOUND', '2139');
define('LDR_EC_TEST_RESULTS_TEST_PARTS_NOT_FOUND', '2140');
define('LDR_ED_TEST_RESULTS_TEST_PARTS_NOT_FOUND', 'Test results parts not found');
define('LDR_EC_TEST_STATUS_CONTEXT_INVALID', '2102');
define('LDR_EC_TEST_STATUS_PROCESSING_LOCK_FAILED', '2114');
define('LDR_EC_TEST_STATUS_PROCESSING_LOCKED', '2113');
define('LDR_EC_TEST_STATUS_PROCESSING_UNLOCK_FAILED', '2115');
define('LDR_EC_TEST_STATUS_UNKNOWN', '2101');
define('LDR_ED_TEST_STATUS_UNKNOWN', 'This testAssignmentStatus is unknown: ');
define('LDR_EC_TIME_INVALID_FORMAT', '2046');
define('LDR_EC_TIME_INVALID_TIME', '2047');
define('LDR_EC_TOKEN_CREATION_FAILED', '2005');
define('LDR_ED_TOKEN_CREATION_FAILED', 'Creation of the session token failed');
define('LDR_EC_TOKEN_HAS_EXPIRED', '2009');
define('LDR_ED_TOKEN_HAS_EXPIRED', 'Session token has expired');
define('LDR_EC_TOKEN_INVALID_CHARACTERS', '2013');
define('LDR_ED_TOKEN_INVALID_CHARACTERS', 'This session token has invalid characters: ');
define('LDR_EC_TOKEN_LENGTH_INVALID', '2012');
define('LDR_ED_TOKEN_LENGTH_INVALID', 'Session token length is incorrect');
define('LDR_EC_TOKEN_NOT_FOUND', '2008');
define('LDR_ED_TOKEN_NOT_FOUND', 'Session token not found');
define('LDR_EC_TOKEN_NOT_PROVIDED', '2007');
define('LDR_EC_UNABLE_TO_DELETE_CLASS_ASSIGNMENT', '2163');
define('LDR_ED_UNABLE_TO_DELETE_CLASS_ASSIGNMENT', 'Unable to delete all class assignments for studentRecordId: ');
define('LDR_EC_UNABLE_TO_RETRIEVE_CLASS_INFO', '2173');
define('LDR_ED_UNABLE_TO_RETRIEVE_CLASS_INFO', 	'Unable to retrieve class info for classId: ');
define('LDR_EC_UNEXPECTED_ADP_RESPONSE', '2162');
define('LDR_ED_UNEXPECTED_ADP_RESPONSE', 'Unexpected response received from ADP: ');
define('LDR_EC_UPDATE_HAS_NO_CHANGES', '2051');
define('LDR_ED_UPDATE_HAS_NO_CHANGES', 'Update arguments are missing or match existing values - no changes were made');
define('LDR_EC_USE_DATABASE_FAILED', '2003');
define('LDR_EC_UNAUTHORIZED_TENANT', '2157');
define('LDR_ED_UNAUTHORIZED_TENANT', "Unauthorized tenant attempted to communicate with LDR. Unauthorized Tenant's URL: ");

define('LDR_EC_INTERNAL_ERROR_001', '3001');
define('LDR_EC_INTERNAL_ERROR_003', '3003');
define('LDR_EC_INTERNAL_ERROR_009', '3009');
define('LDR_EC_INTERNAL_ERROR_010', '3010');
define('LDR_EC_INTERNAL_ERROR_028', '3028');
define('LDR_EC_INTERNAL_ERROR_045', '3045');
define('LDR_EC_INTERNAL_ERROR_046', '3046');
define('LDR_EC_INTERNAL_ERROR_047', '3047');
define('LDR_EC_INTERNAL_ERROR_048', '3048');
define('LDR_EC_INTERNAL_ERROR_049', '3049');
define('LDR_EC_INTERNAL_ERROR_050', '3050');
define('LDR_EC_INTERNAL_ERROR_051', '3051');
define('LDR_EC_INTERNAL_ERROR_052', '3052');

//
// CLASS DEFINITIONS
//
class ErrorLDR
{
	private $code;
	private $detail;
	private $logId;

	public function __construct($logId, $code, $detail = '')
	{
		$this->code = $code;
		$this->detail = $detail;
		$this->logId = $logId;
	}

    public function getErrorArray()
    {
        $error = [];
        $error['error'] = $this->code;
        $error['logid'] = $this->logId;
        $error['detail'] = $this->detail;
        return $error;
    }

    public function getErrorCode()
    {
        return $this->code;
    }

    public function getErrorDetail()
    {
        return $this->detail;
    }

    public function returnErrorJSON($reporter)
	{
		// Return the error to the client
        $error = $this->getErrorArray();
		echo json_encode($error);

		// Log the error
		$this->logError($reporter);
	}

	public function logError($reporter)
	{
		logError($this->logId, $this->code . ' [' . $reporter . '] ' . $this->detail);
	}

	public function logMessage($reporter, $message)
	{
        logError($this->logId, $this->code . ' [' . $reporter . '] ' . $message);
	}
}

class Organization
{
	public $organizationId;
	public $level;
	public $name;

	public function __construct($organizationId, $level, $name)
	{
		$this->organizationId = $organizationId;
		$this->level = $level;
		$this->name = $name;
	}
}

class Performance
{
    public $serviceCount;
    public $totalElapsedTime;
    public $averageElapsedTime;
    public $maximumElapsedTime;
    public $minimumElapsedTime;

    public function __construct()
    {
        $this->serviceCount = 0;
        $this->totalElapsedTime = 0;
        $this->averageElapsedTime = 0;
        $this->maximumElapsedTime = 0;
        $this->minimumElapsedTime = PHP_INT_MAX;
    }
}

class QueueProcessing
{
    public $totalMessagesProcessed;
    public $sqsCreateClientElapsedTime;
    public $sqsReceiveMessageElapsedTime;
    public $sqsDeleteMessageElapsedTime;
    public $ldrGetTestAssignmentIdElapsedTime;
    public $ldrGetTestAssignmentElapsedTime;
    public $ldrUpdateLastQueueEventTime;
    public $ldrUpdateTestStatusElapsedTime;
    public $ldrStoreTestResultsElapsedTime;
    public $adpUpdateTestStatusElapsedTime;
    public $halLoggingElapsedTime;
    public $submittedMessagesProcessed;
    public $submittedMessagesIgnored;
    public $submittedMessagesInvalid;
    public $submittedMessagesFailed;
    public $submittedElapsedTime;
    public $startedMessagesProcessed;
    public $startedMessagesIgnored;
    public $startedMessagesInvalid;
    public $startedMessagesFailed;
    public $startedElapsedTime;
    public $pausedMessagesProcessed;
    public $pausedMessagesIgnored;
    public $pausedMessagesInvalid;
    public $pausedMessagesFailed;
    public $pausedElapsedTime;
    public $resumedMessagesProcessed;
    public $resumedMessagesIgnored;
    public $resumedMessagesInvalid;
    public $resumedMessagesFailed;
    public $resumedElapsedTime;
    public $unknownStatusChangesProcessed;
    public $unknownMessagesProcessed;
    public $unknownElapsedTime;

    public function __construct()
    {
        $this->totalMessagesProcessed = 0;
        $this->sqsCreateClientElapsedTime = 0.0;
        $this->sqsReceiveMessageElapsedTime = 0.0;
        $this->sqsDeleteMessageElapsedTime = 0.0;
        $this->ldrGetTestAssignmentIdElapsedTime = 0.0;
        $this->ldrGetTestAssignmentElapsedTime = 0.0;
        $this->ldrUpdateLastQueueEventTime = 0.0;
        $this->ldrUpdateTestStatusElapsedTime = 0.0;
        $this->ldrStoreTestResultsElapsedTime = 0.0;
        $this->adpUpdateTestStatusElapsedTime = 0.0;
        $this->halLoggingElapsedTime = 0.0;
        $this->submittedMessagesProcessed = 0;
        $this->submittedMessagesIgnored = 0;
        $this->submittedMessagesInvalid = 0;
        $this->submittedMessagesFailed = 0;
        $this->submittedElapsedTime = 0.0;
        $this->startedMessagesProcessed = 0;
        $this->startedMessagesIgnored = 0;
        $this->startedMessagesInvalid = 0;
        $this->startedMessagesFailed = 0;
        $this->startedElapsedTime = 0.0;
        $this->pausedMessagesProcessed = 0;
        $this->pausedMessagesIgnored = 0;
        $this->pausedMessagesInvalid = 0;
        $this->pausedMessagesFailed = 0;
        $this->pausedElapsedTime = 0.0;
        $this->resumedMessagesProcessed = 0;
        $this->resumedMessagesIgnored = 0;
        $this->resumedMessagesInvalid = 0;
        $this->resumedMessagesFailed = 0;
        $this->resumedElapsedTime = 0.0;
        $this->unknownStatusChangesProcessed = 0;
        $this->unknownMessagesProcessed = 0;
        $this->unknownElapsedTime = 0.0;
    }
}

class School
{
	public $organizationId;
	public $schoolIdentifier;
	public $schoolName;

	public function __construct()
	{
		$this->organizationId = '';
		$this->schoolIdentifier = '';
		$this->schoolName = '';
	}
}

class AssignedTest
{
    public $testBatteryId;
    public $testBatteryName;
    public $testBatterySubject;
    public $testBatteryGrade;
    public $countAssigned;
    public $countScheduled;
    public $percentScheduled;
    public $countInProgress;
    public $countPaused;
    public $countSubmitted;
    public $percentSubmitted;
    public $countCanceled;
    public $percentCanceled;
    public $percentSubmittedOrCanceled;

    public function __construct($testBatteryId)
    {
        $this->testBatteryId = $testBatteryId;
        $this->testBatteryName = '';
        $this->testBatterySubject = '';
        $this->testBatteryGrade = '';
        $this->countAssigned = 0;
        $this->countScheduled = 0;
        $this->percentScheduled = 0;
        $this->countInProgress = 0;
        $this->countPaused = 0;
        $this->countSubmitted = 0;
        $this->percentSubmitted = 0;
        $this->countCanceled = 0;
        $this->percentCanceled = 0;
        $this->percentSubmittedOrCanceled = 0;
    }
}

class ClassAssignedTest
{
    public $testBatteryId;
    public $testBatteryName;
    public $testBatterySubject;
    public $testBatteryGrade;
    public $instructionStatusList;

    public function __construct($testBatteryId)
    {
        $this->testBatteryId = $testBatteryId;
        $this->testBatteryName = '';
        $this->testBatterySubject = '';
        $this->testBatteryGrade = '';
        $this->instructionStatusList = [];
    }
}

class InstructionStatus
{
    public $instructionStatus;
    public $statusScheduled;
    public $statusInProgress;
    public $statusSubmitted;

    public function __construct($instructionStatus)
    {
        $this->instructionStatus = $instructionStatus;
        $this->statusScheduled = [];
        $this->statusScheduled['testCount'] = 0;
        $this->statusScheduled['students'] = [];
        $this->statusInProgress = [];
        $this->statusInProgress['testCount'] = 0;
        $this->statusInProgress['students'] = [];
        $this->statusSubmitted = [];
        $this->statusSubmitted['testCount'] = 0;
        $this->statusSubmitted['students'] = [];
    }
}
