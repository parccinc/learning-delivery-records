#!/bin/bash

# LDR unit test execution
# Run as root
# Usage bash unittests.sh owner_group=group owner_user=user requestscheme=scheme ldrclientuser=user ldrclientpwd=password rootdir=dir appdir=dir dbname=name log=false
# owner_group is group that owns directory where application is deployed (ex.: apache)
# owner_user is group that owns directory where application is deployed (ex.: apache)
# requestscheme is LDR client request scheme (http or https. If not specified, script will prompt for scheme.
# ldrclientuser is LDR client user.
# ldrclientpwd is LDR client password.
# rootdir is Apache server application root directory.
# appdir is directory on Apache server where code is deployed.
# dbname is the name of LDR database. This script will configure append "_unit_test" to dbname.
# log enables this script log file (true or false). Default is false.

USELOG=false
LOGFILE=unittests.sh.log
echo `date` >$LOGFILE
OWNER_GROUP=
OWNER_USER=
REQUESTSCHEME=
LDRCLIENTUSER=
LDRCLIENTPWD=
ROOTDIR=
APPDIR=
DBNAME=
DBNAMEUNITTEST=

# Process script parameters
if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			owner_group)
				OWNER_GROUP="${ARR[1]}"
				;;
				
			owner_user)
				OWNER_USER="${ARR[1]}"
				;;

			requestscheme)
				REQUESTSCHEME="${ARR[1]}"
				;;
				
			ldrclientuser)
				LDRCLIENTUSER="${ARR[1]}"
				;;
				
			ldrclientpwd)
				LDRCLIENTPWD="${ARR[1]}"
				;;
				
			rootdir)
				ROOTDIR="${ARR[1]}"
				;;
				
			appdir)
				APPDIR="${ARR[1]}"
				;;
				
			dbname)
				DBNAME="${ARR[1]}"
				DBNAMEUNITTEST="$DBNAME""_unit_test"
				;;
				
			log)
				USELOG="${ARR[1]}"
				;;
		esac
	done
fi

log() {
	COLOR=
	if [ $# -gt 1 ];then
		case "$2" in
			error)
				COLOR="31"
				;;

			warn)
				COLOR="33"
				;;
					
			ok)
				COLOR="32"
				;;
		esac
	fi

	local MSG=
	if [ -z "$COLOR" ];then
		MSG="$1"
	else
		MSG="\\E[0;""$COLOR""m""$1""\\E[0m"
	fi

	if [ "$USELOG" = true ]; then
		echo -e "$MSG" | tee -a "$LOGFILE"
	else
		echo -e "$MSG"
	fi
}

# Gets Status from HTTP response.
status() {
	local STATUS=$(echo "$1" | awk '/^  HTTP/{print $2}')
	echo "$STATUS"
}

revertDbName() {
	echo "$DBNAME" > "$ROOTDIR""/""$APPDIR""/ldr_svcs/selected_database.txt"
	chown "$OWNER_GROUP":"$OWNER_USER" "$ROOTDIR""/""$APPDIR""/ldr_svcs/selected_database.txt"
	chmod g+w,u+w "$ROOTDIR""/""$APPDIR""/ldr_svcs/selected_database.txt"
}

# Switch to unit-test database.
echo "$DBNAMEUNITTEST" > "$ROOTDIR""/""$APPDIR""/ldr_svcs/selected_database.txt"
chown "$OWNER_GROUP":"$OWNER_USER" "$ROOTDIR""/""$APPDIR""/ldr_svcs/selected_database.txt"
chmod g+w,u+w "$ROOTDIR""/""$APPDIR""/ldr_svcs/selected_database.txt"

rm -f cookies.txt
REQUEST="wget --server-response --no-cache --no-check-certificate --save-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/ldr_client/login_user.php --post-data clientName=""$LDRCLIENTUSER""&password=""$LDRCLIENTPWD"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"

REQUEST="wget --server-response --no-cache --no-check-certificate --keep-session-cookies --load-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/ldr_client/process_service_list.php?filename=run_unit_tests.json"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"
STATUS=$(status "$RESPONSE")
if [ "$STATUS" = 200 ];then
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		log "Unit tests failed." error
		$(revertDbName)
		exit 1
	else
		log "Unit tests passed." ok
	fi
else
	log "Unit tests failed." error
	$(revertDbName)
	exit 1
fi

log "Complete!" ok
$(revertDbName)
exit 0