#!/usr/bin/env bash
timedatectl set-timezone America/Chicago

apt-get update >/dev/null 2>&1
apt-get -y upgrade >/dev/null 2>&1
apt-get -y autoremove >/dev/null 2>&1

if [ ! -f /var/log/apachesetup ]; then
    apt-get install -y apache2 apache2-utils >/dev/null 2>&1
    touch /var/log/apachesetup
fi

if ! [ -L /var/www/html ]; then
    rm -rf /var/www/html
    ln -fs /adsldr /var/www/html

    chown vagrant:www-data /ads-ldr
fi

if [ ! -f /var/log/sslconfig ]; then
    mkdir -m 0700 /etc/ssl /etc/ssl/certs /etc/ssl/private
    a2enmod ssl

    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj "/C=US/ST=Illinois/L=Chicago/O=Dis/CN=www.devtools.com" -keyout /etc/ssl/private/devtools.key -out /etc/ssl/certs/devtools.crt >/dev/null 2>&1

    sed -i.bak -e 's/ssl-cert-snakeoil.pem/devtools.crt/' /etc/apache2/sites-available/default-ssl.conf
    sed -i.bak -e 's/ssl-cert-snakeoil.key/devtools.key/' /etc/apache2/sites-available/default-ssl.conf

    a2ensite default-ssl.conf

    touch /var/log/sslconfig
fi

if [ ! -f /var/log/phpsetup ]; then
    # add-apt-repository ppa:ondrej/php5-5.6
    # apt-get -y update >/dev/null 2>&1

    apt-get install -y php5 libapache2-mod-php5 php5-mysql php5-mcrypt php5-common php5-cli php5-xdebug php5-apcu php5-curl php5-xhprof >/dev/null 2>&1

    touch /var/log/phpsetup
fi

if [ ! -h /var/www ]; then
    echo '' >> /etc/php5/mods-available/xdebug.ini
    echo ';;;;;;;;;;;;;;;;;;;;;;;;;;' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.idekey = "vagrant"' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_enable = 1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_autostart = 1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_port = 9000' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_handler=dbgp' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_log="/var/log/xdebug/xdebug.log"' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_host=10.0.2.2 ; IDE-Environments IP, from vagrant box.' >> /etc/php5/mods-available/xdebug.ini
    echo '; xdebug.remote_host=127.0.0.1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_mode=req' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.remote_connect_back = 1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.max_nesting_level = 1000' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.dump_globals = 1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.show_exception_trace = 1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.var_display_max_children = -1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.var_display_max_data = -1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.var_display_max_depth = -1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.default_enable=0' >> /etc/php5/mods-available/xdebug.ini

    echo 'xdebug.profiler_enable = 1' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.profiler_output_dir = /var/log/xdebug' >> /etc/php5/mods-available/xdebug.ini
    echo 'xdebug.profiler_output_name = "callgrind.%R.%t"' >> /etc/php5/mods-available/xdebug.ini

    echo "apc.enabled=1" >> /etc/php5/mods-available/apcu.ini
    echo "apc.shm_segments=1" >> /etc/php5/mods-available/apcu.ini
    echo "apc.shm_size=32M" >> /etc/php5/mods-available/apcu.ini
    echo "apc.ttl=5" >> /etc/php5/mods-available/apcu.ini
    echo "apc.user_ttl=5" >> /etc/php5/mods-available/apcu.ini
    echo "apc.gc_ttl=5" >> /etc/php5/mods-available/apcu.ini

    mkdir -m 0777 /var/log/xhprof
    echo 'xhprof.output_dir = "/var/log/xhprof"' >> /etc/php5/mods-available/xhprof.ini

    ln -s /etc/php5/mods-available/xhprof.ini /etc/php5/apache2/conf.d/20-xhprof.ini
    ln -s /etc/php5/mods-available/xhprof.ini /etc/php5/cli/conf.d/20-xhprof.ini

    a2dissite 000-default.conf
    a2enmod rewrite

    sed -i.bak 's/<\/VirtualHost>//' /etc/apache2/sites-available/000-default.conf
    echo -e '    ServerName example' >> /etc/apache2/sites-available/000-default.conf
    echo -e '    <Directory "/var/www/html">' >> /etc/apache2/sites-available/000-default.conf
    echo -e '       Options -Indexes' >> /etc/apache2/sites-available/000-default.conf
    echo -e '       DirectoryIndex index.php' >> /etc/apache2/sites-available/000-default.conf
    echo -e '       AllowOverride All' >> /etc/apache2/sites-available/000-default.conf
    echo -e '       Order deny,allow' >> /etc/apache2/sites-available/000-default.conf
    echo -e '       Allow from All' >> /etc/apache2/sites-available/000-default.conf
    echo -e '   </Directory>' >> /etc/apache2/sites-available/000-default.conf
    echo '</VirtualHost>' >> /etc/apache2/sites-available/000-default.conf

    cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/ldr-tenant1.conf
    cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/ldr-tenant2.conf
    cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/ldr-test.conf

    sed -i.bak 's/example/vagrant-ldr-tenant1/' /etc/apache2/sites-available/ldr-tenant1.conf
    sed -i.bak 's/example/vagrant-ldr-tenant2/' /etc/apache2/sites-available/ldr-tenant2.conf
    sed -i.bak 's/example/vagrant-ldr-test/' /etc/apache2/sites-available/ldr-test.conf

    echo -e '127.0.0.1 vagrant-ldr-tenant1' >> /etc/hosts
    echo -e '127.0.0.1 vagrant-ldr-tenant2' >> /etc/hosts
    echo -e '127.0.0.1 vagrant-ldr-test' >> /etc/hosts

    a2ensite ldr-tenant1.conf
    a2ensite ldr-tenant2.conf
    a2ensite ldr-test.conf
fi

# if [ ! -f /var/log/awssetup ]; then
#     mkdir -m 0755 /.aws
#     touch /.aws/credentials
#     chmod 0755 /.aws/credentials

#     touch /var/log/awsetup
# fi

if [ ! -f /var/log/grunt ]; then
    apt-get -y install nodejs npm >/dev/null 2>&1
    npm install -g grunt-cli >/dev/null 2>&1

    # BUG FIX for /usr/bin/env: node: No such file or directory
    ln -s /usr/bin/nodejs /usr/bin/node

    touch /var/log/grunt
fi

service apache2 restart
