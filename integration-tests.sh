#!/bin/bash

# LDR integration tests
# Usage bash integration-tests.sh url
# url is URL to LDR server root.

URL=
if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			url)
				URL="${ARR[1]}"
				;;
		esac
	done
fi

if [ -z "$URL" ]; then
	echo LDR root URL is required.
	exit 1
fi


# Check that the LDR server is responding
RESPONSE=$( curl -i -j -k $URL/ldr_svcs/version 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo LDR server is not responding.
	exit 1
else
	echo LDR server is responding.
fi

# Check that the LDR client is responding
RESPONSE=$( curl -i -j -k $URL/ldr_client/index.php 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo LDR client is not responding.
	exit 1
else
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		echo LDR client is not responding.
		exit 1
	else
		echo LDR client is responding.
	fi
fi

# Check that the LDR client can communicate with the LDR server
RESPONSE=$( curl -i -j -k $URL/ldr_client/get_version.php 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo LDR client cannot communicate with the LDR server.
	exit 1
else
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		echo LDR client cannot communicate with the LDR server.
		exit 1
	else
		echo LDR client can communicate with the LDR server.
	fi
fi

# Check that the LDR server can connect to the LDR database
RESPONSE=$( curl -i -j -k $URL/ldr_client/get_table_names.php 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo LDR server cannot connect to the LDR database.
	exit 1
else
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		echo LDR server cannot connect to the LDR database.
		exit 1
	else
		echo LDR server can connect to the LDR database.
	fi
fi

# Check that the LDR server can log messages to the HAL server by causing a message to be logged
RESPONSE=$( curl -i -j -k $URL/ldr_svcs/password-hash/password 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo LDR server cannot log messages to the HAL server by causing a message to be logged.
	exit 1
else
	REGEX='.+\{"password":"password","hash":"([0-9A-F]+)","length":([0-9]+)\}'
	if [[ $RESPONSE =~ $REGEX ]];then
		echo LDR server can log messages to the HAL server by causing a message to be logged.
	else
		echo LDR server cannot log messages to the HAL server by causing a message to be logged.
		exit 1
	fi
fi

echo Integration tests passed.
exit 0