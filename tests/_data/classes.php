<?php
$classes = [];

foreach ($schools as $schoolId => $school) {
    for ($c = 0; $c < $classesCount; $c++) {
        $class = [
            'organizationId' => $schoolId
            , 'classIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_CLASS_IDENTIFIER.'}'))
            , 'sectionNumber' => strtoupper($I->fake->regexify('[A-Z\s0-9]{16}'))
            , 'gradeLevel' => $I->fake->randomElement($array = ['K', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        ];

        $I->haveInDatabase('class', $class);
        $class['classId'] = $I->grabFromDatabase('class', 'classId', $class);
        $classes[$class['classId']] = $class;
    }
}
