<?php
$students = [];

foreach ($schools as $schoolId => $school) {
    for ($st = 0; $st < $studentsCount; $st++) {
        // Add to student
        $student = [
            // student attributes
            'lastName' => $I->fake->lastName
            , 'firstName' => $I->fake->firstName
            , 'middleName' => $I->fake->firstName
            , 'dateOfBirth' => $I->fake->dateTimeBetween($startDate = '-10 years', $endDate = '-4 years')->format("Y-m-d")
            , 'gender' => $I->fake->randomElement($array = ['M', 'F'])
            , 'globalUniqueIdentifier' => $I->generateGUID_RFC4122v4()
        ];
        $I->haveInDatabase('student', $student);
        $studentId = $I->grabFromDatabase('student', 'studentId', $student);
        $student['studentId'] = $studentId;

        // Add to student_record
        $studentRecord = [
            'studentId' => $studentId
            // organization identifiers/IDs
            , 'stateOrgId' => current($states)['organizationId']
            , 'enrollOrgId' => current($schools)['organizationId']
            // student / student_record identifiers
            , 'stateIdentifier' => $I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_STUDENT_STATE_IDENTIFIER.'}')
            , 'localIdentifier' => ''
            // student_record attributes
            , 'optionalStateData1' => null
            , 'optionalStateData2' => null
            , 'optionalStateData3' => null
            , 'optionalStateData4' => null
            , 'optionalStateData5' => null
            , 'optionalStateData6' => null
            , 'optionalStateData7' => null
            , 'optionalStateData8' => null
            , 'gradeLevel' => $I->fake->randomElement($array = ['K', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            , 'raceAA' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceAN' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceAS' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceHL' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceHP' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceWH' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusDIS' => 'Y'
            , 'statusECO' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusELL' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusGAT' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusLEP' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusMIG' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'disabilityType' => $I->fake->randomElement($array = ['TBI', 'AUT', 'ID'])
        ];
        $I->haveInDatabase('student_record', $studentRecord);
        $studentRecordId = $I->grabFromDatabase('student_record', 'studentRecordId', $studentRecord);
        $student['studentRecordId'] = $studentRecordId;
        $student = array_merge($student, $studentRecord);

        // Add to student_class
        foreach ($classes as $classId => $class) {
            $studentClass = [
                'studentRecordId' => $studentRecordId
                , 'classId' => $classId
            ];
            $I->haveInDatabase('student_class', $studentClass);
        }

        $students[$studentId] = $student;
    }
}
