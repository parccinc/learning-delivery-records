<?php
$testBatteries = [];
$testBatteryForms = [];

$grades = array_keys($I->baseData['test_battery_grade']);
$gradeCount = count($grades);
for ($g = 0; $g < $gradeCount; $g++) {
    for ($tb = 1; $tb <= $testBatteriesCount; $tb++) {
        $testBattery = [
            'testBatteryId' => (int)($tb + $g*$testBatteriesCount)
            , 'testBatteryName' => $I->fake->text($maxNoChars = MAXLEN_TEST_BATTERY_NAME)
            , 'testBatterySubjectId' => $I->fake->randomElement($array = array_keys($I->baseData['test_battery_subject']))
            , 'testBatteryGradeId' => $grades[$g]
            , 'testBatteryProgramId' => $I->fake->randomElement($array = array_keys($I->baseData['test_battery_program']))
            , 'testBatterySecurityId' => $I->fake->randomElement($array = array_keys($I->baseData['test_battery_security']))
            , 'testBatteryPermissionsId' => $I->fake->randomElement($array = array_keys($I->baseData['test_battery_permissions']))
            , 'testBatteryScoringId' => $I->fake->randomElement($array = array_keys($I->baseData['test_battery_scoring']))
            , 'testBatteryDescription' => $I->fake->text($maxNoChars = MAXLEN_TEST_BATTERY_DESCRIPTION/100) // Needs to be much lower than the 4096 limit that it is set to now
        ];
        $I->haveInDatabase('test_battery', $testBattery);
        $testBatteries[] = $testBattery;

        for ($tbf = 1; $tbf <= $testBatteryFormsCount; $tbf++) {
            $testBatteryForm = [
                'testBatteryId' => $testBattery['testBatteryId']
                , 'testBatteryFormId' => (int)($tbf + ($testBatteryFormsCount*($tb - 1)) + $g*$testBatteriesCount*$testBatteryFormsCount)
                , 'testBatteryFormName' => $I->fake->text($maxNoChars = MAXLEN_TEST_BATTERY_FORM_NAME)
            ];

            $I->haveInDatabase('test_battery_form', $testBatteryForm);
            $testBatteryForms[] = $testBatteryForm;
        }
    }
}
