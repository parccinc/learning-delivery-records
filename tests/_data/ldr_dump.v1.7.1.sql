# ************************************************************
# Sequel Pro SQL dump
# Version 4500
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.28-0ubuntu0.15.04.1)
# Database: ldr_test
# Generation Time: 2016-02-15 17:02:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table calculator_type
# ------------------------------------------------------------

CREATE TABLE `calculator_type` (
  `calculatorTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `calculatorType` varchar(24) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`calculatorTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `calculator_type` WRITE;
/*!40000 ALTER TABLE `calculator_type` DISABLE KEYS */;

INSERT INTO `calculator_type` (`calculatorTypeId`, `calculatorType`, `deleted`)
VALUES
	(1,X'4E6F2043616C63756C61746F72',X'4E'),
	(2,X'536369656E74696669632043616C63756C61746F72',X'4E');

/*!40000 ALTER TABLE `calculator_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table class
# ------------------------------------------------------------

CREATE TABLE `class` (
  `classId` int(11) NOT NULL AUTO_INCREMENT,
  `organizationId` int(11) NOT NULL,
  `classIdentifier` varchar(38) COLLATE utf8_bin NOT NULL,
  `sectionNumber` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `gradeLevel` char(2) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`classId`),
  KEY `classIdentifier` (`classIdentifier`),
  KEY `organizationId` (`organizationId`,`sectionNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table client
# ------------------------------------------------------------

CREATE TABLE `client` (
  `clientId` int(11) NOT NULL AUTO_INCREMENT,
  `clientType` int(11) NOT NULL DEFAULT '1',
  `clientName` varchar(40) COLLATE utf8_bin NOT NULL,
  `clientPassword` char(128) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clientId`),
  UNIQUE KEY `clientName` (`clientName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;

INSERT INTO `client` (`clientId`, `clientType`, `clientName`, `clientPassword`)
VALUES
	(1,1,X'6C64725F726F6F74',X'4246354235303037363846463233443046343842453837333436374239343136314342454546463442433046434138424446414545423637433341363831324133443037343437354532423234343545333545354631394531324334323137364539413943444331454335313432343738393836434338303743303237463639'),
	(2,1,X'70617263635F616470',X'3942313946424546334238333642333342374543393632374637364337463044303035364133304631384244364531374531334242333737343841314445353444433542413131334534354231364135373132314134424431323341424544463746463430373331464137383539453836333630333133413434374244303932'),
	(3,1,X'70617263635F6369',X'4636433342433933463944373833353143444634303430393830323145354341433846303031393734393534413630393543413842383832334236424241333139414245454542333134453441324538353337333237354430353830304234383639304439394542354336323632413737384537394443463235303235313238'),
	(4,1,X'70617263635F646576',X'3934363933433246443746423632373937323938454143353441304646384443323946353638454436373938394330334237423433323239324131434331304133303944433339383732394630313542434441434242363141353737433134344533443531453332354133444436344436454245373330433841374632313631'),
	(5,1,X'70617263635F7161',X'3831443043303138394139443641343934453845423533353938443034343437444138393930374536304642333342414135343542414237343444453738414534453445333043304341453237384634353437444346464436304437383230454443433944393432413731314641324637313532303035383332324631364335'),
	(6,1,X'70617263635F746170',X'3932374145343030314542443135303236383331383339353336323630373435453243304638434434313737303242423437423536354146454432413338463042354341373837444330303441364539414641434231313133414239343934353934334439354243433144333742434136363137323331414139334435433136'),
	(7,1,X'70617263635F75736572',X'4137453031324438313942304544443645364135324631303739364639334644433830453946374637423836413138313130453242444435343934443144393434324638383546433834353730314434384643453546354338343332433130324234383137433936423938463439394541393344443645443838333634423636'),
	(8,2,X'70726F645F616470',X'4544433845363945443334423538333939383135463943353845353644434630383545454634433646444431343944463743464230333645463130453839343835303532463131373944413830363937463946413937433436384142453835374235373834454246383546354632394241333046443144323134383135454239'),
	(9,2,X'70726F645F746170',X'4443433142353245323932433035414536463137444631323139423044463137323832303842453934374339433132463539353336363838394237303143424238424338303339303137384144334646364146393633463133383733463336383633454438303231423845464242414235363730424132463941364146354531'),
	(10,2,X'70726F645F75736572',X'3241394635353731333038423745354543383334424143463443363033333743444546373437333044463545423431464642443933324539464536314441383731433044363034443244413634374232424131423542343135413430313235424246314130334632443835434130393441363836414546303234393338413439');

/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table embedded_score_report
# ------------------------------------------------------------

CREATE TABLE `embedded_score_report` (
  `embeddedScoreReportId` int(11) NOT NULL AUTO_INCREMENT,
  `embeddedScoreReport` text COLLATE utf8_bin,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`embeddedScoreReportId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table organization
# ------------------------------------------------------------

CREATE TABLE `organization` (
  `organizationId` int(11) NOT NULL AUTO_INCREMENT,
  `organizationType` tinyint(4) NOT NULL,
  `stateOrganizationId` int(11) NOT NULL,
  `parentOrganizationId` int(11) NOT NULL,
  `organizationIdentifier` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT '',
  `organizationName` varchar(60) COLLATE utf8_bin NOT NULL,
  `nestedSetLeft` int(11) NOT NULL,
  `nestedSetRight` int(11) NOT NULL,
  `studentCount` int(11) NOT NULL DEFAULT '0',
  `childCount` int(11) NOT NULL DEFAULT '0',
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `fake` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`organizationId`),
  KEY `stateOrganizationId` (`stateOrganizationId`),
  KEY `parentOrganizationId` (`parentOrganizationId`),
  KEY `nestedSetLeft` (`nestedSetLeft`),
  KEY `nestedSetRight` (`nestedSetRight`),
  KEY `organizationIdentifier` (`organizationIdentifier`),
  KEY `organizationName` (`organizationName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `organization` WRITE;
/*!40000 ALTER TABLE `organization` DISABLE KEYS */;

INSERT INTO `organization` (`organizationId`, `organizationType`, `stateOrganizationId`, `parentOrganizationId`, `organizationIdentifier`, `organizationName`, `nestedSetLeft`, `nestedSetRight`, `studentCount`, `childCount`, `deleted`, `fake`)
VALUES
	(1,1,0,0,X'',X'5041524343',0,9872,1545,54,X'4E',X'4E');

/*!40000 ALTER TABLE `organization` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table performance
# ------------------------------------------------------------

CREATE TABLE `performance` (
  `performanceId` int(11) NOT NULL AUTO_INCREMENT,
  `statisticsDate` date DEFAULT NULL,
  `serviceName` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `serviceCount` int(11) DEFAULT NULL,
  `totalElapsedTime` double DEFAULT NULL,
  `maximumElapsedTime` double DEFAULT NULL,
  `minimumElapsedTime` double DEFAULT NULL,
  PRIMARY KEY (`performanceId`),
  KEY `statisticsDate` (`statisticsDate`,`serviceName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table qti_class
# ------------------------------------------------------------

CREATE TABLE `qti_class` (
  `qtiClassId` int(11) NOT NULL AUTO_INCREMENT,
  `qtiClass` varchar(32) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`qtiClassId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `qti_class` WRITE;
/*!40000 ALTER TABLE `qti_class` DISABLE KEYS */;

INSERT INTO `qti_class` (`qtiClassId`, `qtiClass`, `deleted`)
VALUES
	(1,X'6173736573736D656E744974656D',X'4E'),
	(2,X'63686F696365496E746572616374696F6E',X'4E'),
	(3,X'73696D706C6543686F696365',X'4E'),
	(4,X'726573706F6E73654465636C61726174696F6E',X'4E');

/*!40000 ALTER TABLE `qti_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table queue_processing
# ------------------------------------------------------------

CREATE TABLE `queue_processing` (
  `queueProcessingId` int(11) NOT NULL AUTO_INCREMENT,
  `statisticsDate` date DEFAULT NULL,
  `hostname` text COLLATE utf8_bin NOT NULL,
  `totalMessagesProcessed` int(11) NOT NULL,
  `sqsCreateClientElapsedTime` double DEFAULT NULL,
  `sqsReceiveMessageElapsedTime` double DEFAULT NULL,
  `sqsDeleteMessageElapsedTime` double DEFAULT NULL,
  `ldrGetTestAssignmentIdElapsedTime` double DEFAULT NULL,
  `ldrGetTestAssignmentElapsedTime` double DEFAULT NULL,
  `ldrUpdateLastQueueEventTime` double DEFAULT '0',
  `ldrUpdateTestStatusElapsedTime` double DEFAULT NULL,
  `ldrStoreTestResultsElapsedTime` double DEFAULT NULL,
  `adpUpdateTestStatusElapsedTime` double DEFAULT NULL,
  `halLoggingElapsedTime` double DEFAULT NULL,
  `submittedMessagesProcessed` int(11) NOT NULL,
  `submittedMessagesIgnored` int(11) NOT NULL,
  `submittedMessagesInvalid` int(11) NOT NULL,
  `submittedMessagesFailed` int(11) NOT NULL,
  `submittedElapsedTime` double DEFAULT NULL,
  `startedMessagesProcessed` int(11) NOT NULL,
  `startedMessagesIgnored` int(11) NOT NULL,
  `startedMessagesInvalid` int(11) NOT NULL,
  `startedMessagesFailed` int(11) NOT NULL,
  `startedElapsedTime` double DEFAULT NULL,
  `pausedMessagesProcessed` int(11) NOT NULL,
  `pausedMessagesIgnored` int(11) NOT NULL,
  `pausedMessagesInvalid` int(11) NOT NULL,
  `pausedMessagesFailed` int(11) NOT NULL,
  `pausedElapsedTime` double DEFAULT NULL,
  `resumedMessagesProcessed` int(11) NOT NULL,
  `resumedMessagesIgnored` int(11) NOT NULL,
  `resumedMessagesInvalid` int(11) NOT NULL,
  `resumedMessagesFailed` int(11) NOT NULL,
  `resumedElapsedTime` double DEFAULT NULL,
  `unknownStatusChangesProcessed` int(11) NOT NULL,
  `unknownMessagesProcessed` int(11) NOT NULL,
  `unknownElapsedTime` double DEFAULT NULL,
  PRIMARY KEY (`queueProcessingId`),
  KEY `statisticsDate` (`statisticsDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table response_cardinality
# ------------------------------------------------------------

CREATE TABLE `response_cardinality` (
  `responseCardinalityId` int(11) NOT NULL AUTO_INCREMENT,
  `responseCardinality` varchar(12) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`responseCardinalityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `response_cardinality` WRITE;
/*!40000 ALTER TABLE `response_cardinality` DISABLE KEYS */;

INSERT INTO `response_cardinality` (`responseCardinalityId`, `responseCardinality`, `deleted`)
VALUES
	(1,X'73696E676C65',X'4E'),
	(2,X'6D756C7469706C65',X'4E');

/*!40000 ALTER TABLE `response_cardinality` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table session_token
# ------------------------------------------------------------

CREATE TABLE `session_token` (
  `sessionTokenId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL,
  `token` char(40) COLLATE utf8_bin NOT NULL,
  `dateTime` datetime NOT NULL,
  PRIMARY KEY (`sessionTokenId`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table student
# ------------------------------------------------------------

CREATE TABLE `student` (
  `studentId` int(11) NOT NULL AUTO_INCREMENT,
  `globalUniqueIdentifier` char(36) COLLATE utf8_bin NOT NULL,
  `lastName` varchar(35) COLLATE utf8_bin NOT NULL,
  `firstName` varchar(35) COLLATE utf8_bin NOT NULL,
  `middleName` varchar(35) COLLATE utf8_bin NOT NULL,
  `dateOfBirth` date NOT NULL,
  `gender` enum('F','M') COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`studentId`),
  UNIQUE KEY `globalUniqueIdentifier` (`globalUniqueIdentifier`),
  KEY `firstName` (`firstName`),
  KEY `lastName` (`lastName`),
  KEY `dateOfBirth` (`dateOfBirth`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table student_class
# ------------------------------------------------------------

CREATE TABLE `student_class` (
  `studentClassId` int(11) NOT NULL AUTO_INCREMENT,
  `studentRecordId` int(11) NOT NULL,
  `classId` int(11) NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`studentClassId`),
  KEY `studentRecordId` (`studentRecordId`),
  KEY `classId` (`classId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table student_record
# ------------------------------------------------------------

CREATE TABLE `student_record` (
  `studentRecordId` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `enrollOrgId` int(11) NOT NULL,
  `stateOrgId` int(11) NOT NULL,
  `stateIdentifier` varchar(30) COLLATE utf8_bin NOT NULL,
  `localIdentifier` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData1` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `gradeLevel` char(2) COLLATE utf8_bin NOT NULL,
  `raceAA` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `raceAN` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `raceAS` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `raceHL` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `raceHP` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `raceWH` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `statusDIS` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `statusECO` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `statusELL` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `statusGAT` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `statusLEP` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `statusMIG` enum('Y','N') COLLATE utf8_bin DEFAULT NULL,
  `disabilityType` varchar(3) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData2` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData3` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData4` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData5` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData6` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData7` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `optionalStateData8` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`studentRecordId`),
  KEY `studentId` (`studentId`),
  KEY `enrollOrgId` (`enrollOrgId`),
  KEY `stateOrgId` (`stateOrgId`),
  KEY `stateIdentifier` (`stateIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table test_assignment
# ------------------------------------------------------------

CREATE TABLE `test_assignment` (
  `testAssignmentId` int(11) NOT NULL AUTO_INCREMENT,
  `adpTestAssignmentId` int(11) DEFAULT NULL,
  `studentRecordId` int(11) NOT NULL,
  `testBatteryFormId` int(11) NOT NULL,
  `testBatteryId` int(11) NOT NULL,
  `testStatusId` int(11) NOT NULL,
  `lastQueueEventTime` int(11) NOT NULL DEFAULT '0',
  `embeddedScoreReportId` int(11) NOT NULL DEFAULT '0',
  `testKey` char(10) COLLATE utf8_bin NOT NULL,
  `instructionStatus` int(11) NOT NULL DEFAULT '0',
  `enableLineReader` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `enableTextToSpeech` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testAssignmentId`),
  UNIQUE KEY `testKey` (`testKey`),
  KEY `studentRecordId` (`studentRecordId`),
  CONSTRAINT `test_assignment_ibfk_1` FOREIGN KEY (`studentRecordId`) REFERENCES `student_record` (`studentRecordId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table test_battery
# ------------------------------------------------------------

CREATE TABLE `test_battery` (
  `testBatteryId` int(11) NOT NULL,
  `testBatteryName` varchar(100) NOT NULL,
  `testBatterySubjectId` int(11) NOT NULL,
  `testBatteryGradeId` int(11) NOT NULL,
  `testBatteryProgramId` int(11) NOT NULL,
  `testBatterySecurityId` int(11) NOT NULL,
  `testBatteryPermissionsId` int(11) NOT NULL,
  `testBatteryScoringId` int(11) NOT NULL,
  `testBatteryDescription` varchar(4096) NOT NULL,
  `deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatteryId`),
  KEY `testBatteryName` (`testBatteryName`),
  KEY `testBatterySubjectId` (`testBatterySubjectId`),
  KEY `testBatteryGradeId` (`testBatteryGradeId`),
  KEY `testBatteryProgramId` (`testBatteryProgramId`),
  KEY `testBatterySecurityId` (`testBatterySecurityId`),
  KEY `testBatteryPermissionsId` (`testBatteryPermissionsId`),
  KEY `testBatteryScoringId` (`testBatteryScoringId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table test_battery_form
# ------------------------------------------------------------

CREATE TABLE `test_battery_form` (
  `testBatteryId` int(11) NOT NULL,
  `testBatteryFormId` int(11) NOT NULL,
  `testBatteryFormName` varchar(20) NOT NULL,
  `deleted` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatteryFormId`),
  KEY `testBatteryFormName` (`testBatteryFormName`),
  KEY `testBatteryId` (`testBatteryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table test_battery_grade
# ------------------------------------------------------------

CREATE TABLE `test_battery_grade` (
  `testBatteryGradeId` int(11) NOT NULL AUTO_INCREMENT,
  `testBatteryGrade` varchar(16) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatteryGradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `test_battery_grade` WRITE;
/*!40000 ALTER TABLE `test_battery_grade` DISABLE KEYS */;

INSERT INTO `test_battery_grade` (`testBatteryGradeId`, `testBatteryGrade`, `deleted`)
VALUES
	(1,X'4B',X'4E'),
	(2,X'31',X'4E'),
	(3,X'32',X'4E'),
	(4,X'33',X'4E'),
	(5,X'34',X'4E'),
	(6,X'35',X'4E'),
	(7,X'36',X'4E'),
	(8,X'37',X'4E'),
	(9,X'38',X'4E'),
	(10,X'39',X'4E'),
	(11,X'3130',X'4E'),
	(12,X'3131',X'4E'),
	(13,X'3132',X'4E'),
	(14,X'4D756C74692D6C6576656C',X'4E');

/*!40000 ALTER TABLE `test_battery_grade` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test_battery_permissions
# ------------------------------------------------------------

CREATE TABLE `test_battery_permissions` (
  `testBatteryPermissionsId` int(11) NOT NULL AUTO_INCREMENT,
  `testBatteryPermissions` varchar(24) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatteryPermissionsId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `test_battery_permissions` WRITE;
/*!40000 ALTER TABLE `test_battery_permissions` DISABLE KEYS */;

INSERT INTO `test_battery_permissions` (`testBatteryPermissionsId`, `testBatteryPermissions`, `deleted`)
VALUES
	(1,X'4E6F6E2D52657374726963746564',X'4E'),
	(2,X'52657374726963746564',X'4E');

/*!40000 ALTER TABLE `test_battery_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test_battery_program
# ------------------------------------------------------------

CREATE TABLE `test_battery_program` (
  `testBatteryProgramId` int(11) NOT NULL AUTO_INCREMENT,
  `testBatteryProgram` varchar(32) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatteryProgramId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `test_battery_program` WRITE;
/*!40000 ALTER TABLE `test_battery_program` DISABLE KEYS */;

INSERT INTO `test_battery_program` (`testBatteryProgramId`, `testBatteryProgram`, `deleted`)
VALUES
	(1,X'446961676E6F73746963204173736573736D656E74',X'4E'),
	(2,X'4B3220466F726D6174697665',X'4E'),
	(3,X'4D69642D596561722F496E746572696D',X'4E'),
	(4,X'50726163746963652054657374',X'4E'),
	(5,X'537065616B696E672026204C697374656E696E67',X'4E');

/*!40000 ALTER TABLE `test_battery_program` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test_battery_scoring
# ------------------------------------------------------------

CREATE TABLE `test_battery_scoring` (
  `testBatteryScoringId` int(11) NOT NULL AUTO_INCREMENT,
  `testBatteryScoring` varchar(24) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatteryScoringId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `test_battery_scoring` WRITE;
/*!40000 ALTER TABLE `test_battery_scoring` DISABLE KEYS */;

INSERT INTO `test_battery_scoring` (`testBatteryScoringId`, `testBatteryScoring`, `deleted`)
VALUES
	(1,X'496D6D656469617465',X'4E'),
	(2,X'44656C61796564',X'4E');

/*!40000 ALTER TABLE `test_battery_scoring` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test_battery_security
# ------------------------------------------------------------

CREATE TABLE `test_battery_security` (
  `testBatterySecurityId` int(11) NOT NULL AUTO_INCREMENT,
  `testBatterySecurity` varchar(24) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatterySecurityId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `test_battery_security` WRITE;
/*!40000 ALTER TABLE `test_battery_security` DISABLE KEYS */;

INSERT INTO `test_battery_security` (`testBatterySecurityId`, `testBatterySecurity`, `deleted`)
VALUES
	(1,X'4E6F6E2D536563757265',X'4E'),
	(2,X'536563757265',X'4E');

/*!40000 ALTER TABLE `test_battery_security` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test_battery_subject
# ------------------------------------------------------------

CREATE TABLE `test_battery_subject` (
  `testBatterySubjectId` int(11) NOT NULL AUTO_INCREMENT,
  `testBatterySubject` varchar(16) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testBatterySubjectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `test_battery_subject` WRITE;
/*!40000 ALTER TABLE `test_battery_subject` DISABLE KEYS */;

INSERT INTO `test_battery_subject` (`testBatterySubjectId`, `testBatterySubject`, `deleted`)
VALUES
	(1,X'454C41',X'4E'),
	(2,X'4D617468',X'4E'),
	(3,X'4E2F41',X'4E');

/*!40000 ALTER TABLE `test_battery_subject` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table test_form_assessment_item
# ------------------------------------------------------------

CREATE TABLE `test_form_assessment_item` (
  `testFormRevisionId` int(11) NOT NULL,
  `testFormAssessmentSectionId` int(11) NOT NULL,
  `testFormAssessmentItemId` int(11) NOT NULL AUTO_INCREMENT,
  `uin` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `itemId` varchar(24) COLLATE utf8_bin DEFAULT NULL,
  `identifier` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `serial` varchar(24) COLLATE utf8_bin DEFAULT NULL,
  `domain` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `cluster` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `contentStandard` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `evidenceStatement` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  `itemGrade` char(2) COLLATE utf8_bin DEFAULT NULL,
  `maxScorePoints` int(11) DEFAULT NULL,
  `calculatorTypeId` int(11) DEFAULT NULL,
  `qtiClassId` int(11) DEFAULT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testFormAssessmentItemId`),
  KEY `testFormRevisionId` (`testFormRevisionId`),
  KEY `testFormAssessmentSectionId` (`testFormAssessmentSectionId`),
  CONSTRAINT `test_form_assessment_item_ibfk_1` FOREIGN KEY (`testFormRevisionId`) REFERENCES `test_form_revision` (`testFormRevisionId`),
  CONSTRAINT `test_form_assessment_item_ibfk_2` FOREIGN KEY (`testFormAssessmentSectionId`) REFERENCES `test_form_assessment_section` (`testFormAssessmentSectionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table test_form_assessment_section
# ------------------------------------------------------------

CREATE TABLE `test_form_assessment_section` (
  `testFormRevisionId` int(11) NOT NULL,
  `testFormPartId` int(11) NOT NULL,
  `testFormAssessmentSectionId` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testFormAssessmentSectionId`),
  KEY `testFormRevisionId` (`testFormRevisionId`),
  KEY `testFormPartId` (`testFormPartId`),
  CONSTRAINT `test_form_assessment_section_ibfk_1` FOREIGN KEY (`testFormRevisionId`) REFERENCES `test_form_revision` (`testFormRevisionId`),
  CONSTRAINT `test_form_assessment_section_ibfk_2` FOREIGN KEY (`testFormPartId`) REFERENCES `test_form_part` (`testFormPartId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table test_form_part
# ------------------------------------------------------------

CREATE TABLE `test_form_part` (
  `testFormRevisionId` int(11) NOT NULL,
  `testFormPartId` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `navigationMode` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `submissionMode` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testFormPartId`),
  KEY `testFormRevisionId` (`testFormRevisionId`),
  CONSTRAINT `test_form_part_ibfk_1` FOREIGN KEY (`testFormRevisionId`) REFERENCES `test_form_revision` (`testFormRevisionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table test_form_revision
# ------------------------------------------------------------

CREATE TABLE `test_form_revision` (
  `testFormRevisionId` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `identifier` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testFormRevisionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table test_form_system_item
# ------------------------------------------------------------

CREATE TABLE `test_form_system_item` (
  `testFormRevisionId` int(11) NOT NULL,
  `testFormAssessmentSectionId` int(11) NOT NULL,
  `testFormSystemItemId` int(11) NOT NULL AUTO_INCREMENT,
  `itemId` varchar(24) COLLATE utf8_bin DEFAULT NULL,
  `identifier` varchar(60) COLLATE utf8_bin DEFAULT NULL,
  `serial` varchar(24) COLLATE utf8_bin DEFAULT NULL,
  `qtiClassId` int(11) DEFAULT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testFormSystemItemId`),
  KEY `testFormRevisionId` (`testFormRevisionId`),
  KEY `testFormAssessmentSectionId` (`testFormAssessmentSectionId`),
  CONSTRAINT `test_form_system_item_ibfk_1` FOREIGN KEY (`testFormRevisionId`) REFERENCES `test_form_revision` (`testFormRevisionId`),
  CONSTRAINT `test_form_system_item_ibfk_2` FOREIGN KEY (`testFormAssessmentSectionId`) REFERENCES `test_form_assessment_section` (`testFormAssessmentSectionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table test_status
# ------------------------------------------------------------

CREATE TABLE `test_status` (
  `testStatusId` int(11) NOT NULL AUTO_INCREMENT,
  `testStatus` varchar(16) COLLATE utf8_bin NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`testStatusId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `test_status` WRITE;
/*!40000 ALTER TABLE `test_status` DISABLE KEYS */;

INSERT INTO `test_status` (`testStatusId`, `testStatus`, `deleted`)
VALUES
	(1,X'5363686564756C6564',X'4E'),
	(2,X'496E50726F6772657373',X'4E'),
	(3,X'506175736564',X'4E'),
	(4,X'5375626D6974746564',X'4E'),
	(5,X'436F6D706C65746564',X'4E'),
	(6,X'43616E63656C6564',X'4E');

/*!40000 ALTER TABLE `test_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `organizationId` int(11) NOT NULL,
  `clientUserId` int(11) NOT NULL,
  `deleted` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `organizationId` (`organizationId`,`clientUserId`),
  KEY `clientUserId` (`clientUserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`userId`, `organizationId`, `clientUserId`, `deleted`)
VALUES
	(1,1,1,X'4E');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_audit
# ------------------------------------------------------------

CREATE TABLE `user_audit` (
  `userAuditId` int(11) NOT NULL AUTO_INCREMENT,
  `dateTime` datetime NOT NULL,
  `clientUserId` int(11) NOT NULL,
  `actionType` int(11) NOT NULL,
  `dataType` int(11) NOT NULL,
  `primaryId` int(11) NOT NULL,
  `dataChanges` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`userAuditId`),
  KEY `clientUserId` (`clientUserId`),
  KEY `actionType` (`actionType`),
  KEY `dataType` (`dataType`,`primaryId`),
  KEY `dateTime` (`dateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



# Dump of table version_history
# ------------------------------------------------------------

CREATE TABLE `version_history` (
  `versionHistoryId` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(8) COLLATE utf8_bin NOT NULL,
  `dateTime` datetime NOT NULL,
  PRIMARY KEY (`versionHistoryId`),
  KEY `version` (`version`),
  KEY `dateTime` (`dateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

LOCK TABLES `version_history` WRITE;
/*!40000 ALTER TABLE `version_history` DISABLE KEYS */;

INSERT INTO `version_history` (`version`, `dateTime`)
VALUES
  ('1.7.1',NOW());

/*!40000 ALTER TABLE `version_history` ENABLE KEYS */;
UNLOCK TABLES;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
