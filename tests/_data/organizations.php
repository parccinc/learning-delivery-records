<?php
$states = [];
$districts = [];
$schools = [];

for ($st = 0; $st < $statesCount; $st++) {
    $state = [];
    $state = [
        'organizationType' => 2
        , 'parentOrganizationId' => 1
        , 'organizationIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_ORGANIZATION_IDENTIFIER.'}'))
        , 'organizationName' => $I->fake->stateAbbr
        , 'childCount' => $districtsCount*$schoolsCount
        , 'studentCount' => 0
    ];
    $I->haveInDatabase('organization', $state);
    $state['organizationId'] = $I->grabFromDatabase(
        'organization'
        , 'organizationId'
        , [
            'organizationIdentifier' => $state['organizationIdentifier']
            , 'organizationName' => $state['organizationName']
        ]
    );
    $states[$state['organizationId']] = $state;

    for ($d = 0; $d < $districtsCount; $d++) {
        $district = [];
        $district = [
            'organizationType' => 3
            , 'stateOrganizationId' => $state['organizationId']
            , 'parentOrganizationId' => $state['organizationId']
            , 'organizationIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_ORGANIZATION_IDENTIFIER.'}'))
            , 'organizationName' => "District".$d." of ".$state['organizationName']
            , 'childCount' => $schoolsCount
            , 'studentCount' => 0
        ];

        $I->haveInDatabase('organization', $district);
        $district['organizationId'] = $I->grabFromDatabase(
            'organization'
            , 'organizationId'
            , [
                'organizationName' => $district['organizationName']
                , 'organizationIdentifier' => $district['organizationIdentifier']
            ]
        );
        $districts[$district['organizationId']] = $district;

        for ($sc = 0; $sc < $schoolsCount; $sc++) {
            $school = [
                'organizationType' => 4
                , 'stateOrganizationId' => $state['organizationId']
                , 'parentOrganizationId' => $district['organizationId']
                , 'organizationIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_ORGANIZATION_IDENTIFIER.'}'))
                , 'organizationName' => "School".$sc." of ".$state['organizationName']
                , 'childCount' => 0
                , 'studentCount' => 0
            ];

            $I->haveInDatabase('organization', $school);
            $school['organizationId'] = $I->grabFromDatabase(
                'organization'
                , 'organizationId'
                , [
                    'organizationName' => $school['organizationName']
                    , 'organizationIdentifier' => $school['organizationIdentifier']
                ]
            );
            $schools[$school['organizationId']] = $school;
        }
    }
}
