<?php
/**
 * Loading all mandatory data into the database
 * 1. CLIENT
 * 2. CALCULATOR_TYPE
 * 3. QTI_CLASS
 * 4. RESPONSE_CARDINALITY
 * 5. TEST_BATTERY_GRADE
 * 6. TEST_BATTERY_PERMISSIONS
 * 7. TEST_BATTERY_PROGRAM
 * 8. TEST_BATTERY_SCORING
 * 9. TEST_BATTERY_SECURITY
 * 10. TEST_BATTERY_SUBJECT
 */
$I->baseData = [
    'client' => [
        1 => ['clientName' => 'test', 'clientPassword' => 'BA1C548D8025B4770328CEBB8FA4B06388630809474010D08807046D044FDF86C0793C74C8E005F8412A2150A1047380EFB56836C0F02DFB738617BE5FDEBAC6']
    ] , 'calculator_type' => [
        1 => ['calculatorType' => 'No Calculator']
        , 2 => ['calculatorType' => 'Scientific Calculator']
    ] , 'qti_class' => [
        1 => ['qtiClass' => 'assessmentItem']
        , 2 => ['qtiClass' => 'choiceInteraction']
        , 3 => ['qtiClass' => 'simpleChoice']
        , 4 => ['qtiClass' => 'responseDeclaration']
        , 5 => ['qtiClass' => 'textEntryInteraction']
    ], 'response_cardinality' => [
        1 => ['responseCardinality' => 'single']
        , 2 => ['responseCardinality' => 'multiple']
    ], 'test_battery_grade' => [
        1 => 'K'
        , 2 => '1'
        , 3 => '2'
        , 4 => '3'
        , 5 => '4'
        , 6 => '5'
        , 7 => '6'
        , 8 => '7'
        , 9 => '8'
        , 10 => '9'
        , 11 => '10'
        , 12 => '11'
        , 13 => '12'
        , 14 => 'Multi-level'
    ], 'test_battery_permissions' => [
        1 => ['testBatteryPermissions' => 'Non-Restricted']
        , 2 => ['testBatteryPermissions' => 'Restricted']
    ], 'test_battery_program' => [
        1 => ['testBatteryProgram' => 'Diagnostic Assessment']
        , 2 => ['testBatteryProgram' => 'K2 Formative']
        , 3 => ['testBatteryProgram' => 'Mid-Year/Interim']
        , 4 => ['testBatteryProgram' => 'Practice Test']
        , 5 => ['testBatteryProgram' => 'Speaking & Listening']
        , 6 => ['testBatteryProgram' => 'Summative']
    ], 'test_battery_scoring' => [
        1 => ['testBatteryScoring' => 'Immediate']
        , 2 => ['testBatteryScoring' => 'Delayed']
    ], 'test_battery_security' => [
        1 => ['testBatterySecurity' => 'Non-Secure']
        , 2 => ['testBatterySecurity' => 'Secure']
    ], 'test_battery_subject' => [
        1 => ['testBatterySubject' => 'ELA']
        , 2 => ['testBatterySubject' => 'Math']
        , 3 => ['testBatterySubject' => 'N/A']
        // , 4 => ['testBatterySubject' => 'Science']
    ], 'test_status' => [
        1 => ['testStatus' => 'Scheduled']
        , 2 => ['testStatus' => 'InProgress']
        , 3 => ['testStatus' => 'Paused']
        , 3 => ['testStatus' => 'Submitted']
        , 5 => ['testStatus' => 'Completed']
        , 6 => ['testStatus' => 'Canceled']
    ], 'user' => [
        1 => ['organizationId' => 1, 'clientUserId' => 1]
    ]
];
