<?php
/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
*/

class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

   /**
    * Define custom actions here
    */

    public $token = null;

    public $baseData = null;

    public function preTest()
    {
        $I = $this;
        include MOCKUPS_DIR."/base.php";

        $I->login2LDR();
        $I->dropTables();
        $I->createTables();

        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function initFaker()
    {
        $I = $this;
        $I->fake = Faker\Factory::create();
    }

    public function sendHttpRequestToLDR($method = 'GET', $serviceCall, $params = [], $includeToken = false)
    {
        $I = $this;
        $I->haveHttpHeader('Content-Type', 'application/json');
        if ($includeToken === true) {
            $I->haveHttpHeader('Token', $I->token);
        }

        $url = '/'.SERVICE_PREFIX.$serviceCall;
        switch (strtoupper($method)) {
            case 'GET':
                $I->sendGET($url, $params);
                break;
            case 'POST':
                $I->sendPOST($url, $params);
                break;
            case 'PUT':
                $I->sendPUT($url, $params);
                break;
            case 'DELETE':
                $I->sendDELETE($url, $params);
                break;
            default:
                throw new \Exception('Unkown method: '.$method.'. Please verify the method type and try again');
                break;
        }

        return true;
    }

    public function dropTables()
    {
        $I = $this;
        $I->sendHttpRequestToLDR('DELETE', 'tables', [], true);
        $response = $I->getJsonResponse();

        return true;
    }

    public function createTables()
    {
        $I = $this;
        $I->sendHttpRequestToLDR('POST', 'tables', []);
        $response = $I->getJsonResponse();

        return true;
    }

    public function login2LDR()
    {
        $I = $this;
        $I->sendHttpRequestToLDR('POST', 'session-token', ['clientName' => LDR_USER, 'password' => LDR_PASS]);
        $response = $I->getJsonResponse();
        $I->assertRegExp('/[A-Z0-9]{40}/', $response->token, 'Check if I got a valid token');
        $this->token = (string)$response->token;

        return true;
    }

    public function logoutFromLDR($token = null)
    {
        $I = $this;
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendHttpRequestToLDR('DELETE', 'session-token', [], true);
        $response = $I->getSuccessMessage();

        return true;
    }

    public function missingToken($serviceName, $requestType = POST)
    {
        $I = $this;
        $I->wantTo('Attempt to test Service Call with missing token');
        if ($requestType === POST) {
            $I->sendHttpRequestToLDR('POST', $serviceName, []);
        } elseif ($requestType === GET) {
            $I->sendHttpRequestToLDR('GET', $serviceName, []);
        }
        $response = $I->getJsonResponse();
        $I->assertErrorMessageAndDetail($response, "TOKEN_NOT_PROVIDED");
    }

    public function wrongToken($serviceName, $requestType = POST)
    {
        $I = $this;
        if ($requestType === POST) {
            $I->sendHttpRequestToLDR('POST', $serviceName, ['token' => substr($I->token, -1).'AA']);
        } elseif ($requestType === GET) {
            $I->sendHttpRequestToLDR('GET', $serviceName, ['token' => substr($I->token, -1).'AA']);
        }
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->assertErrorMessageAndDetail($I->getJsonResponse(), "TOKEN_DOES_NOT_EXIST");
    }

    public function getJsonResponse()
    {
        $I = $this;
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = json_decode($I->grabResponse());
        $I->doNotSeeAnError($response);
        return $response;
    }

    public function assertErrorMessageAndDetail($response, $error_code, $detail_suffix = NULL)
    {
        $I = $this;
        $I->assertEquals(true, isset($response->error));
        $I->assertEquals(constant("LDR_EC_" . $error_code), (string)$response->error, 'Unexpected error message');
        $I->assertEquals(constant("LDR_ED_" . $error_code) . $detail_suffix, (string)$response->detail, 'Unexpected error detail');
    }

    public function doNotSeeAnError($response)
    {
        $I = $this;
        $I->assertFalse(isset($response->error), "No error returned from LDR");
    }

    public function getSuccessMessage()
    {
        $I = $this;
        $response = $I->getJsonResponse();
        $I->assertTrue(isset($response->result), 'Got result from LDR');
        $I->assertEquals(SUCCESS, $response->result, 'Got successful result from LDR');

        return $response;
    }

    public function updateDatabase()
    {
        $I = $this;
        $I->sendHttpRequestToLDR('PUT', 'databases', [], true);
        $I->getSuccessMessage();
    }

    public function generateOrganizations($statesCount = TEST_STATE_COUNT, $districtsCount = TEST_DISTRICT_COUNT, $schoolsCount = TEST_SCHOOL_COUNT)
    {
        $I = $this;
        include MOCKUPS_DIR."/organizations.php";

        return [$states, $districts, $schools];
    }

    public function generateClasses($classesCount = TEST_CLASS_COUNT)
    {
        $I = $this;
        list($states, $districts, $schools) = $I->generateOrganizations($classesCount, $classesCount, $classesCount);

        include MOCKUPS_DIR."/classes.php";

        return [$states, $districts, $schools, $classes];
    }

    public function generateStudents($studentsCount = TEST_STUDENT_COUNT)
    {
        $I = $this;
        list($states, $districts, $schools, $classes) = $I->generateClasses($studentsCount);

        include MOCKUPS_DIR."/students.php";

        return [$states, $districts, $schools, $classes, $students];
    }

    public function generateTestBatteries($testBatteriesCount = TEST_BATTERIES_COUNT, $testBatteryFormsCount = TEST_BATTERY_FORMS_COUNT)
    {
        $I = $this;

        include MOCKUPS_DIR."/test_batteries.php";

        return [$testBatteries, $testBatteryForms];
    }

    public function generateGUID_RFC4122v4()
    {
        $uuidCharSet = '0123456789ABCDEF';
        $nibbleCharSet = '89AB';

        $uuid = '';
        for ($idx = 0; $idx < LENGTH_GUID_RFC4122; $idx++)
        {
            switch ($idx)
            {
                case 14:
                    $uuid .= '4'; // RFC4122 version 4
                    break;
                case 19:
                    $uuid .= substr($nibbleCharSet, mt_rand(0, 3), 1);
                    break;
                case 8:
                case 13:
                case 18:
                case 23:
                $uuid .= '-'; // RFC4122 version 4
                    break;
                default:
                    $uuid .= substr($uuidCharSet, mt_rand(0, 15), 1);
                    break;
            }
        }

        return $uuid;
    }
}
