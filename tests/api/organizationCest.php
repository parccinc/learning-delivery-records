<?php
class organizationCest
{
    private $_serviceCall = "organization";

    public function _before(ApiTester $I)
    {
        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group organization
     */
    public function getOrganization(ApiTester $I)
    {
        $I->wantTo("Get organization info for a given organization ID");

        list($states, $districts, $schools) = $I->generateOrganizations(1, 1, 1);

        // $orgs[] = current($states);
        $orgs[] = current($districts);
        $orgs[] = current($schools);

        foreach ($orgs as $org) {
            $I->sendHttpRequestToLDR('GET', $this->_serviceCall."/".$org['organizationId'], [], true);
            $response = $I->getJsonResponse();

            $params = [
                'stateCode' => current($states)['organizationName']
                , 'organizationIdentifier' => $org['organizationIdentifier']
            ];
            $I->sendHttpRequestToLDR('GET', $this->_serviceCall, $params, true);
            $response = $I->getJsonResponse();
            $I->assertEquals($org['organizationId'], $response->organizationId, 'Check organizationId value');
            $I->assertEquals($org['organizationType'], $response->organizationType, 'Check organizationType value');
            $I->assertEquals($org['stateOrganizationId'], $response->stateOrganizationId, 'Check stateOrganizationId value');
            $I->assertEquals($org['parentOrganizationId'], $response->parentOrganizationId, 'Check parentOrganizationId value');
            $I->assertEquals($org['organizationIdentifier'], $response->organizationIdentifier, 'Check organizationIdentifier value');
            $I->assertEquals($org['organizationName'], $response->organizationName, 'Check organizationName value');
            $I->assertEquals($org['studentCount'], $response->studentCount, 'Check studentCount value');
            $I->assertEquals($org['childCount'], $response->childCount, 'Check childCount value');
            $I->assertEquals('N', $response->fake, 'Check class Id value');
        }
    }

    /**
     * @group organization
     */
    public function createOrganization(ApiTester $I)
    {
        $orgs = [];
        $orgTypes = [3 => 'Dsitrict', 4 => 'School'];

        for ($i = 0; $i < 1; $i++) {
            $state = $I->fake->stateAbbr;

            $I->wantTo("Add organization for a parent organization using parent organization ID");
            $I->expectTo("I expect to succeed in adding the new organization");

            $params = [
                'clientUserId' => 1
                , 'organizationName' => $state
                , 'organizationType' => 2
                , 'organizationIdentifier' => $state
                , 'parentOrganizationId' => 1
            ];
            $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
            $response = $I->getJsonResponse();
            $orgId = $response->organizationId;
            $stateOrgId = $orgId;
            $orgs[$orgId] = [
                'state' => $state
                , 'organizationName' => $params['organizationName']
                , 'organizationIdentifier' => $params['organizationIdentifier']
            ];

            foreach ($orgTypes as $orgType => $orgTypeName) {
                $parentOrgId = $orgId;
                $params = [
                    'clientUserId' => 1
                    , 'organizationName' => $orgTypeName." of ".$state
                    , 'organizationType' => $orgType
                    , 'organizationIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_ORGANIZATION_IDENTIFIER.'}'))
                    , 'parentOrganizationId' => $parentOrgId
                ];
                $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
                $response = $I->getJsonResponse();
                $orgId = $response->organizationId;

                $orgs[$orgId] = [
                    'state' => $state
                    , 'organizationName' => $params['organizationName']
                    , 'organizationIdentifier' => $params['organizationIdentifier']
                ];

                $I->seeInDatabase(
                    'organization'
                    , [
                        'organizationId' => $orgId
                        , 'organizationType' => $orgType
                        , 'stateOrganizationId' => $stateOrgId
                        , 'parentOrganizationId' => $parentOrgId
                        , 'organizationIdentifier' => $params['organizationIdentifier']
                        , 'organizationName' => $params['organizationName']
                    ]
                );
            }
        }

        return $orgs;
    }

    /**
     * @group organization
     */
    public function updateOrganization(ApiTester $I)
    {
        $I->wantTo('Update organization info using organization ID');
        $I->expectTo('Successfully update organization info');

        list($states, $districts, $schools) = $I->generateOrganizations(1, 1, 1);

        $orgs[] = current($districts);
        $orgs[] = current($schools);

        foreach ($orgs as $org) {
            $params = [
                'clientUserId' => 1
                , 'organizationName' => $org['organizationName']." ADD"
                , 'organizationIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_ORGANIZATION_IDENTIFIER.'}'))
            ];
            $I->sendHttpRequestToLDR('PUT', $this->_serviceCall."/".$org['organizationId'], $params, true);
            $I->getSuccessMessage();

            $I->seeInDatabase(
                'organization'
                , [
                    'organizationId' => $org['organizationId']
                    , 'organizationName' => $params['organizationName']
                    , 'organizationIdentifier' => $params['organizationIdentifier']
                ]
            );
        }
    }

    /**
     * @group organization
     */
    public function deleteOrganization(ApiTester $I)
    {
        $I->wantTo('Delete organization using organization ID');
        $I->expectTo('Successfully delete organization');

        list($states, $districts, $schools) = $I->generateOrganizations(1, 1, 1);

        $orgs[] = current($schools);
        $orgs[] = current($districts);
        $orgs[] = current($states);

        foreach ($orgs as $org) {
            $params = ['clientUserId' => 1];
            $I->sendHttpRequestToLDR('DELETE', $this->_serviceCall."/".$org['organizationId'], $params, true);
            $I->getSuccessMessage();

            $I->assertEquals(
                SOFT_DELETE
                , $I->grabFromDatabase('organization', 'deleted', ['organizationId' => $org['organizationId']])
            );
        }
    }
}
