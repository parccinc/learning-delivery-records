<?php
class student_recordCest
{
    private $_serviceCall = "student-record";

    public function _before(ApiTester $I)
    {
        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group student
     */
    public function createStudentRecord(ApiTester $I)
    {
        $I->wantTo("Add student to a school with one class assignment");
        $I->expectTo("Successfully add a student to a school and assign it to one class");

        list($states, $districts, $schools, $classes) = $I->generateClasses(2);
        $test = $I->fake->dateTimeBetween($startDate = '-10 years', $endDate = '-4 years');
        reset($schools);

        $params = [
            'clientUserId' => 1
            // organization identifiers/IDs
            , 'enrollStateCode' => current($states)['organizationName']
            , 'enrollSchoolIdentifier' => current($schools)['organizationIdentifier']
            // student / student_record identifiers
            , 'stateIdentifier' => $I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_STUDENT_STATE_IDENTIFIER.'}')
            , 'localIdentifier' => ''
            // student attributes
            , 'lastName' => $I->fake->lastName
            , 'firstName' => $I->fake->firstName
            , 'middleName' => $I->fake->firstName
            , 'dateOfBirth' => $I->fake->dateTimeBetween($startDate = '-10 years', $endDate = '-4 years')->format("Y-m-d")
            , 'gender' => $I->fake->randomElement($array = ['M', 'F'])
            // student_record attributes
            , 'optionalStateData1' => ''
            , 'gradeLevel' => $I->fake->randomElement($array = ['K', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            , 'raceAA' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceAN' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceAS' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceHL' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceHP' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'raceWH' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusDIS' => 'Y'
            , 'statusECO' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusELL' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusGAT' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusLEP' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'statusMIG' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'disabilityType' => $I->fake->randomElement($array = ['TBI', 'AUT', 'ID'])
            // class assignments
            , 'classAssignment1' => $classes[1]['sectionNumber']
            , 'classAssignment2' => $classes[2]['sectionNumber']
            , 'isDryRun' => 'N'
        ];
        $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->seeInDatabase(
            'student_record'
            , [
                'studentRecordId' => $response->studentRecordId
                , 'enrollOrgId' => current($schools)['organizationId']
                , 'stateOrgId' => current($states)['organizationId']
            ]
        );

        $I->seeInDatabase(
            'student_record'
            , [
                'stateIdentifier' => strtoupper($params['stateIdentifier'])
                , 'enrollOrgId' => current($schools)['organizationId']
                , 'stateOrgId' => current($states)['organizationId']
                // , 'optionalStateData1' => (isset($params['optionalStateData1']) ? $params['optionalStateData1']:null)
                // , 'optionalStateData2' => (isset($params['optionalStateData2']) ? $params['optionalStateData2']:null)
                // , 'optionalStateData3' => (isset($params['optionalStateData3']) ? $params['optionalStateData3']:null)
                // , 'optionalStateData4' => (isset($params['optionalStateData4']) ? $params['optionalStateData4']:null)
                // , 'optionalStateData5' => (isset($params['optionalStateData5']) ? $params['optionalStateData5']:null)
                // , 'optionalStateData6' => (isset($params['optionalStateData6']) ? $params['optionalStateData6']:null)
                // , 'optionalStateData7' => (isset($params['optionalStateData7']) ? $params['optionalStateData7']:null)
                // , 'optionalStateData8' => (isset($params['optionalStateData8']) ? $params['optionalStateData8']:null)
                , 'gradeLevel' => $params['gradeLevel']
                , 'raceAA' => $params['raceAA']
                , 'raceAN' => $params['raceAN']
                , 'raceAS' => $params['raceAS']
                , 'raceHL' => $params['raceHL']
                , 'raceHP' => $params['raceHP']
                , 'raceWH' => $params['raceWH']
                , 'statusDIS' => 'Y'
                , 'statusECO' => $params['statusECO']
                , 'statusELL' => $params['statusELL']
                , 'statusGAT' => $params['statusGAT']
                , 'statusLEP' => $params['statusLEP']
                , 'statusMIG' => $params['statusMIG']
                , 'disabilityType' => $params['disabilityType']
            ]
        );

        $studentId = $I->grabFromDatabase('student_record', 'studentId', [
                'stateIdentifier' => strtoupper($params['stateIdentifier'])
                , 'enrollOrgId' => current($schools)['organizationId']
                , 'stateOrgId' => current($states)['organizationId']
                , 'gradeLevel' => $params['gradeLevel']
            ]
        );

        $studentRecordId = $I->grabFromDatabase('student_record', 'studentRecordId', [
                'stateIdentifier' => strtoupper($params['stateIdentifier'])
                , 'enrollOrgId' => current($schools)['organizationId']
                , 'stateOrgId' => current($states)['organizationId']
                , 'gradeLevel' => $params['gradeLevel']
            ]
        );

        $I->seeInDatabase('student'
            , [
                'lastName' => $params['lastName']
                , 'firstName' => $params['firstName']
                , 'middleName' => $params['middleName']
                , 'dateOfBirth' => $params['dateOfBirth']
                , 'gender' => $params['gender']
            ]
        );

        $I->seeInDatabase('organization', ['studentCount' => 1]);

        $I->seeInDatabase('student_class', ['studentRecordId' => $studentRecordId, 'classId' => $classes[1]['classId']]);
        $I->seeInDatabase('student_class', ['studentRecordId' => $studentRecordId, 'classId' => $classes[2]['classId']]);

        /**
         * @todo Add the AUTO TEST ASSIGNMENT LOGIC
         */
        // if (AUTO_TEST_ASSIGNMENT) {
        //     // Get all active test batteries
        //     $testBatteries = $I->grabFromDatabase('test_battery', 'testBatteryId', ['testBattery']);
        // }
    }
}
