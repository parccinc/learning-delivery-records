<?php
class organization_idCest
{
    private $_serviceCall = "organization-id";

    public function _before(ApiTester $I)
    {
        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group organization
     */
    public function getOrganizationId(ApiTester $I)
    {
        list($states, $districts, $schools) = $I->generateOrganizations(1, 1, 1);
        $state = current($states);
        $district = current($districts);
        $school = current($schools);

        $I->wantTo("Get organizationId using stateCode and organizationIdentifier");
        $params = [
            'stateCode' => $state['organizationName']
            , 'organizationIdentifier' => $school['organizationIdentifier']
        ];
        $I->sendHttpRequestToLDR('GET', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->assertEquals($school['organizationId'], $response->organizationId, 'Check the organizationiId');

        $I->wantTo("Get organizationId using stateCode and organizationIdentifier");
        $params = [
            'parentOrganizationId' => $district['organizationId']
            , 'organizationName' => $school['organizationName']
        ];
        $I->sendHttpRequestToLDR('GET', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->assertEquals($school['organizationId'], $response->organizationId, 'Check the organizationiId');

        $I->wantTo("Get organizationId using stateCode and organizationIdentifier");
        $params = [
            'organizationPath' => implode(
                '_'
                , [ROOT_ORG, $state['organizationName'], $district['organizationName'], $school['organizationName']]
            )
        ];
        $I->sendHttpRequestToLDR('GET', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->assertEquals($school['organizationId'], $response->organizationId, 'Check the organizationiId');
    }
}
