<?php
class organizationsCest
{
    private $_serviceCall = "organizations";

    public function _before(ApiTester $I)
    {
        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group organization
     */
    public function getChildOrganizations(ApiTester $I)
    {
        $numStates = 1;
        $numDistricts = 2;
        $numSchools = 3;

        list($states, $districts, $schools) = $I->generateOrganizations($numStates, $numDistricts, $numSchools);
        $state = current($states);
        $district = current($districts);

        $I->wantTo("Get child organizations for an organizationId");
        $I->sendHttpRequestToLDR('GET', $this->_serviceCall."/".$state['organizationId'], [], true);
        $response = $I->getJsonResponse();
        $I->assertEquals($numDistricts, $response->totalCount, 'Child counts should match');
        for ($d = 0; $d < $numDistricts; $d++) {
            $org = (array)$response->organizations[$d];

            foreach ($org as $key => $value) {
                $I->assertEquals($districts[$org['organizationId']][$key], $value, "Organization info: '".$key."' should match");
            }
        }

        $I->wantTo("Get child organizations for an organizationId");
        $I->sendHttpRequestToLDR('GET', $this->_serviceCall."/".$district['organizationId'], [], true);
        $response = $I->getJsonResponse();
        $I->assertEquals($numSchools, $response->totalCount, 'Child counts should match');
        for ($sc = 0; $sc < $numDistricts; $sc++) {
            $org = (array)$response->organizations[$sc];

            foreach ($org as $key => $value) {
                $I->assertEquals($schools[$org['organizationId']][$key], $value, "Organization info: '".$key."' should match");
            }
        }
    }
}
