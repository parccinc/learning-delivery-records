<?php
class classCest
{
    private $_serviceCall = "class";

    public function _before(ApiTester $I)
    {
        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group organization
     */
    public function getClass(ApiTester $I)
    {
        $I->wantTo("Get classes info for a given class ID");

        list($states, $districts, $schools, $classes) = $I->generateClasses(1);
        $school = current($schools);
        $class = current($classes);

        $I->sendHttpRequestToLDR('GET', $this->_serviceCall."/".$class['classId'], [], true);
        $response = $I->getJsonResponse();
        $I->assertEquals($class['classId'], $response->classId, 'Check class Id value');
        $I->assertEquals($class['organizationId'], $response->organizationId, 'Check organization Id value');
        $I->assertEquals($class['classIdentifier'], $response->classIdentifier, 'Check class identifier value');
        $I->assertEquals($class['sectionNumber'], $response->sectionNumber, 'Check section number value');
        $I->assertEquals($class['gradeLevel'], $response->gradeLevel, 'Check grade level value');
    }

    /**
     * @group organization
     */
    public function getClasses(ApiTester $I)
    {
        $I->wantTo('Get classes for a given school ID');

        $numClass = 2;
        list($states, $districts, $schools, $classes) = $I->generateClasses($numClass);
        $schools = array_slice($schools, 0, 1, true);
        $classes = array_slice($classes, 0, $numClass, true);

        foreach ($schools as $school) {
            $I->sendHttpRequestToLDR('GET', "classes/".$school['organizationId'], [], true);
            $response = $I->getJsonResponse();
            $I->assertEquals($numClass, $response->totalCount, 'The number of classes added during test');

            for ($i = 0; $i < $numClass; $i++) {
                $class = (array)$response->classes[$i];
                $classId = $class['classId'];

                $I->assertTrue(isset($classes[$classId]), 'Check that the returned class is actually generated during test');
                $I->assertEquals($classes[$classId]['classId'], $class['classId'], 'Check classId value');
                $I->assertEquals($classes[$classId]['organizationId'], $class['organizationId'], 'Check organizationId value');
                $I->assertEquals($classes[$classId]['classIdentifier'], $class['classIdentifier'], 'Check classIdentifier value');
                $I->assertEquals($classes[$classId]['sectionNumber'], $class['sectionNumber'], 'Check sectionNumber value');
                $I->assertEquals($classes[$classId]['gradeLevel'], $class['gradeLevel'], 'Check gradeLevel value');
            }
        }
    }

    /**
     * @group organization
     */
    public function createClass(ApiTester $I)
    {
        $I->wantTo("Add class for a school using school's organization ID");
        $I->expectTo("Successfully add a new class under a school");

        list($states, $districts, $schools) = $I->generateOrganizations(1, 1, 1);

        $params = [
            'clientUserId' => 1
            , 'classIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_CLASS_IDENTIFIER.'}'))
            , 'gradeLevel' => $I->fake->randomElement($array = ['K', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            , 'sectionNumber' => strtoupper($I->fake->regexify('[A-Z\s0-9]{16}'))
            , 'organizationId' => current($schools)['organizationId']
        ];
        $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->seeInDatabase(
            'class'
            , [
                'classId' => $response->classId
                , 'classIdentifier' => $params['classIdentifier']
                , 'sectionNumber' => $params['sectionNumber']
                , 'organizationId' => $params['organizationId']
            ]
        );

        $params = [
            'clientUserId' => 1
            , 'classIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_CLASS_IDENTIFIER.'}'))
            , 'gradeLevel' => $I->fake->randomElement($array = ['K', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
            , 'sectionNumber' => strtoupper($I->fake->regexify('[A-Z\s0-9]{16}'))
            , 'stateCode' => current($states)['organizationName']
            , 'organizationIdentifier' => current($schools)['organizationIdentifier']
        ];
        $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->seeInDatabase(
            'class'
            , [
                'classId' => $response->classId
                , 'classIdentifier' => $params['classIdentifier']
                , 'sectionNumber' => $params['sectionNumber']
                , 'organizationId' => current($schools)['organizationId']
            ]
        );
    }

    /**
     * @group organization
     */
    public function updateClass(ApiTester $I)
    {
        $I->wantTo('Update class info using class Id');

        $numClass = 1;
        list($states, $districts, $schools, $classes) = $I->generateClasses($numClass);
        $classes = array_slice($classes, 0, $numClass, true);
        $class = current($classes);

        $params = [
            'clientUserId' => 1
            , 'classIdentifier' => strtoupper($I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_CLASS_IDENTIFIER.'}'))
        ];
        $I->sendHttpRequestToLDR('PUT', $this->_serviceCall."/".$class['classId'], $params, true);
        $I->getSuccessMessage();
        $I->assertEquals(
            $params['classIdentifier']
            , $I->grabFromDatabase('class', 'classIdentifier', ['classId' => $class['classId']])
        );

        $params = [
            'clientUserId' => 1
            , 'gradeLevel' => $I->fake->randomElement($array = ['K', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
        ];
        $I->sendHttpRequestToLDR('PUT', $this->_serviceCall."/".$class['classId'], $params, true);
        $I->getSuccessMessage();
        $I->assertEquals(
            $params['gradeLevel']
            , $I->grabFromDatabase('class', 'gradeLevel', ['classId' => $class['classId']])
        );

        $params = [
            'clientUserId' => 1
            , 'sectionNumber' => strtoupper($I->fake->regexify('[A-Z\s0-9]{16}'))
        ];
        $I->sendHttpRequestToLDR('PUT', $this->_serviceCall."/".$class['classId'], $params, true);
        $I->getSuccessMessage();
        $I->assertEquals(
            $params['sectionNumber']
            , $I->grabFromDatabase('class', 'sectionNumber', ['classId' => $class['classId']])
        );
    }

    /**
     * @group organization
     */
    public function deleteClass(ApiTester $I)
    {
        $I->wantTo('Delete class using class Id');

        $numClass = 1;
        list($states, $districts, $schools, $classes) = $I->generateClasses($numClass);
        $classes = array_slice($classes, 0, $numClass, true);
        $class = current($classes);

        $params = ['clientUserId' => 1];
        $I->sendHttpRequestToLDR('DELETE', $this->_serviceCall."/".$class['classId'], $params, true);
        $I->getSuccessMessage();

        $I->assertEquals(
            SOFT_DELETE
            , $I->grabFromDatabase('class', 'deleted', ['classId' => $class['classId']])
        );
    }
}
