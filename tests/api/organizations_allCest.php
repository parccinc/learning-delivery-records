<?php
class organizations_allCest
{
    private $_serviceCall = "organizations-all";

    public function _before(ApiTester $I)
    {
        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group organization
     */
    public function getAllOrganizations(ApiTester $I)
    {
        $numStates = 2;
        $numDistricts = 2;
        $numSchools = 3;
        $totalCount = $numStates*$numDistricts*$numSchools+$numStates+$numDistricts+$numSchools;

        list($states, $districts, $schools) = $I->generateOrganizations($numStates, $numDistricts, $numSchools);
        $orgs = $states + $districts + $schools;

        $I->wantTo("Get all child organizations for an organizationId (ROOT ORG in the current testing)");
        $I->sendHttpRequestToLDR('GET', $this->_serviceCall, [], true);
        $response = $I->getJsonResponse();
        codecept_debug($response);
        codecept_debug($orgs);
        $I->assertEquals($totalCount, intval($response->totalCount), 'Child counts should match');

        /**
         * Count starts form 1 to skip checking for the root org
         */
        for ($d = 1; $d < $totalCount; $d++) {
            $org = (array)$response->organizations[$d];

            foreach ($org as $key => $value) {
                if ($key !== 'fake') {
                    $I->assertEquals($orgs[$org['organizationId']][$key], $value, "Organization info: '".$key."' should match");
                } else {
                    $I->assertEquals('N', $value, "Organization info: 'fake' should match");
                }
            }
        }
    }
}
