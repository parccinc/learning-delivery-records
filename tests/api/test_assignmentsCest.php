<?php
class test_assignmentsCest
{
    private $_serviceCall = "test-assignments";

    public function _before(ApiTester $I)
    {
        $I->preTest();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group test_services
     */
    public function createTestAssignments(ApiTester $I)
    {
        $I->wantTo("Create Test Assignments for all class' student using Test Battery Id and class Id");
        $I->expectTo("Successfully create test assignment for all students within a class");

        list($states, $districts, $schools, $classes, $students) = $I->generateStudents(TEST_STUDENT_COUNT);
        list($testBatteries) = $I->generateTestBatteries(TEST_BATTERIES_COUNT);
        reset($classes);
        $I->seeInDatabase('student_record', ['studentRecordId' => 1]);

        $gradeLevel = current($classes)['gradeLevel'];
        $gradeLevel = array_keys($I->baseData['test_battery_grade'], $gradeLevel)[0];
        $testBatteryIdsForGrade = array_column($testBatteries, 'testBatteryGradeId', 'testBatteryId');
        $testBatteryId = array_keys($testBatteryIdsForGrade, $gradeLevel);
        $params = [
            'clientUserId' => 1
            , 'classId' => current($classes)['classId']
            , 'testBatteryId' => current($testBatteryId)
            , 'instructionStatus' => 0
            , 'allOrNone' => 'N'
        ];

        $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->assertGreaterThanOrEqual(TEST_STUDENT_COUNT, $response->testsAssigned);
    }
}
