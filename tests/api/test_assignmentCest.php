<?php
class test_assignmentCest
{
    private $_serviceCall = "test-assignment";

    public function _before(ApiTester $I)
    {
        $I->preTest();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group test_servicess
     */
    public function createTestAssignment(ApiTester $I)
    {
        $I->wantTo("Create Test Assignment for a student using Test Battery Id and Student Record Id");
        $I->expectTo("Successfully create test assignment for student");

        list($states, $districts, $schools, $classes, $students) = $I->generateStudents(1);
        list($testBatteries) = $I->generateTestBatteries(1);
        $I->seeInDatabase('student_record', ['studentRecordId' => 1]);

        $params = [
            'clientUserId' => 1
            , 'studentRecordId' => current($students)['studentRecordId']
            , 'testBatteryId' => current($testBatteries)['testBatteryId']
            , 'instructionStatus' => 0
            , 'enableLineReader' => $I->fake->randomElement($array = ['Y', 'N'])
            , 'enableTextToSpeech' => $I->fake->randomElement($array = ['Y', 'N'])
        ];

        $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        codecept_debug($response);
    }
}
