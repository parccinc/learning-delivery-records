<?php
class student_recordsCest
{
    private $_serviceCall = "student-records";

    public function _before(ApiTester $I)
    {
        $I->login2LDR();
        $I->updateDatabase();
        $I->initFaker();
    }

    public function _after(ApiTester $I)
    {
        $I->logoutFromLDR();
    }

    /**
     * @group student
     */
    public function createStudentRecords(ApiTester $I)
    {
        $I->wantTo("Add ".TEST_STUDENT_COUNT." students to a school with one class assignment");
        $I->expectTo("Successfully add a student to a school and assign it to one class");

        list($states, $districts, $schools, $classes) = $I->generateClasses(2);
        $test = $I->fake->dateTimeBetween($startDate = '-10 years', $endDate = '-4 years');
        reset($schools);
        // codecept_debug($classes);
        // exit();

        $students = [];
        for ($i = 1; $i <= TEST_STUDENT_COUNT; $i++) {
            $tmpStudent = [
                'clientUserId' => 1
                // organization identifiers/IDs
                , 'enrollStateCode' => current($states)['organizationName']
                , 'enrollSchoolIdentifier' => current($schools)['organizationIdentifier']
                // student / student_record identifiers
                , 'stateIdentifier' => $I->fake->regexify('[A-Za-z0-9]{'.MAXLEN_STUDENT_STATE_IDENTIFIER.'}')
                , 'localIdentifier' => ''
                // student attributes
                , 'lastName' => $I->fake->lastName
                , 'firstName' => $I->fake->firstName
                , 'middleName' => $I->fake->firstName
                , 'dateOfBirth' => $I->fake->dateTimeBetween($startDate = '-10 years', $endDate = '-4 years')->format("Y-m-d")
                , 'gender' => $I->fake->randomElement($array = ['M', 'F'])
                // student_record attributes
                , 'optionalStateData1' => ''
                , 'gradeLevel' => $I->fake->randomElement($array = ['K', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
                , 'raceAA' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'raceAN' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'raceAS' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'raceHL' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'raceHP' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'raceWH' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'statusDIS' => 'Y'
                , 'statusECO' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'statusELL' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'statusGAT' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'statusLEP' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'statusMIG' => $I->fake->randomElement($array = ['Y', 'N'])
                , 'disabilityType' => $I->fake->randomElement($array = ['TBI', 'AUT', 'ID'])
                // class assignments
                , 'classAssignment1' => $classes[1]['sectionNumber']
                , 'classAssignment2' => $classes[2]['sectionNumber']
                , 'isDryRun' => 'N'
            ];
            $students["$i"] = $tmpStudent;
        }

        $params = ['clientUserId' => 1, 'students' => $students];
        $I->sendHttpRequestToLDR('POST', $this->_serviceCall, $params, true);
        $response = $I->getJsonResponse();
        $I->assertEquals(TEST_STUDENT_COUNT, $response->studentsAdded, 'Making sure LDR returns the expected new students');

        foreach ($students as $student) {
            $I->seeInDatabase(
                'student_record'
                , [
                    'stateIdentifier' => strtoupper($student['stateIdentifier'])
                    , 'enrollOrgId' => current($schools)['organizationId']
                    , 'stateOrgId' => current($states)['organizationId']
                    // , 'optionalStateData1' => (isset($student['optionalStateData1']) ? $student['optionalStateData1']:null)
                    // , 'optionalStateData2' => (isset($student['optionalStateData2']) ? $student['optionalStateData2']:null)
                    // , 'optionalStateData3' => (isset($student['optionalStateData3']) ? $student['optionalStateData3']:null)
                    // , 'optionalStateData4' => (isset($student['optionalStateData4']) ? $student['optionalStateData4']:null)
                    // , 'optionalStateData5' => (isset($student['optionalStateData5']) ? $student['optionalStateData5']:null)
                    // , 'optionalStateData6' => (isset($student['optionalStateData6']) ? $student['optionalStateData6']:null)
                    // , 'optionalStateData7' => (isset($student['optionalStateData7']) ? $student['optionalStateData7']:null)
                    // , 'optionalStateData8' => (isset($student['optionalStateData8']) ? $student['optionalStateData8']:null)
                    , 'gradeLevel' => $student['gradeLevel']
                    , 'raceAA' => $student['raceAA']
                    , 'raceAN' => $student['raceAN']
                    , 'raceAS' => $student['raceAS']
                    , 'raceHL' => $student['raceHL']
                    , 'raceHP' => $student['raceHP']
                    , 'raceWH' => $student['raceWH']
                    , 'statusDIS' => 'Y'
                    , 'statusECO' => $student['statusECO']
                    , 'statusELL' => $student['statusELL']
                    , 'statusGAT' => $student['statusGAT']
                    , 'statusLEP' => $student['statusLEP']
                    , 'statusMIG' => $student['statusMIG']
                    , 'disabilityType' => $student['disabilityType']
                ]
            );

            $studentId = $I->grabFromDatabase('student_record', 'studentId', [
                    'stateIdentifier' => strtoupper($student['stateIdentifier'])
                    , 'enrollOrgId' => current($schools)['organizationId']
                    , 'stateOrgId' => current($states)['organizationId']
                    , 'gradeLevel' => $student['gradeLevel']
                ]
            );

            $studentRecordId = $I->grabFromDatabase('student_record', 'studentRecordId', [
                    'stateIdentifier' => strtoupper($student['stateIdentifier'])
                    , 'enrollOrgId' => current($schools)['organizationId']
                    , 'stateOrgId' => current($states)['organizationId']
                    , 'gradeLevel' => $student['gradeLevel']
                ]
            );

            $I->seeInDatabase('student'
                , [
                    'lastName' => $student['lastName']
                    , 'firstName' => $student['firstName']
                    , 'middleName' => $student['middleName']
                    , 'dateOfBirth' => $student['dateOfBirth']
                    , 'gender' => $student['gender']
                ]
            );

            $I->seeInDatabase('organization', ['studentCount' => TEST_STUDENT_COUNT]);

            $I->seeInDatabase('student_class', ['studentRecordId' => $studentRecordId, 'classId' => $classes[1]['classId']]);
            $I->seeInDatabase('student_class', ['studentRecordId' => $studentRecordId, 'classId' => $classes[2]['classId']]);

            /**
             * @todo Add the AUTO TEST ASSIGNMENT LOGIC
             */
            // if (AUTO_TEST_ASSIGNMENT) {
            //
            // }
        }
    }
}
