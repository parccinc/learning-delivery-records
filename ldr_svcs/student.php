<?php
//
// LDR student services
//
// createClassStudents()                create_class_students.php
// createStudentRecord()                create_student_record.php
// createStudentRecords()
// deleteAllClassStudents()             delete_all_class_students.php
// deleteClassStudents()                delete_class_students.php
// deleteStudentRecord()                delete_student_record.php
// deleteStudentRecords()               delete_student_records.php
// exportStudents()
// getClassStudents()                   get_class_students.php
// getStudentClasses()                  get_student_classes.php
// getStudentRecord()                   get_student_record.php
// getStudentRecords()                  get_student_records.php
// updateStudentRecord()                update_student_record.php
//
// LDR student support functions
//
// buildStudentAttributesQueryClause()
// dbCheckStudentCanBeDeleted()
// checkStudentIdentifierAvailable()
// dbCreateStudentClass()
// dbCreateStudentRecord()
// dbDeleteStudentClass()
// dbDeleteStudentRecord()
// dbExportStudents()
// dbGetClassStudentCount()
// dbGetClassStudentRecordIds()
// dbGetClassStudents()
// dbGetStudent_ByGUID()
// dbGetStudentClassCount()
// dbGetStudentClassId()
// dbGetStudentClassIds_ByClassId()
// dbGetStudentClassIds_ByOrganizationId()
// dbGetStudentClasses()
// dbGetStudentRecord()
// dbGetStudentRecordId_ByStateOrgId_ByStateIdentifier()
// dbGetStudentRecordIds()
// dbGetStudentRecords()
// dbUpdateStudentRecord()
//

// createClassStudents()
//   Creates one or more student class assignments
//
function createClassStudents($classId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'studentRecordIds' => DCO_REQUIRED | DCO_NO_CHECK
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $studentRecordIds) = $args;

    // Validate the classId
    $argName = 'classId';
    $classId = validateArg($logId, $db, [$argName => $classId], $argName, $argFilterMap[$argName],
        DCO_REQUIRED | DCO_MUST_EXIST);
    if ($classId instanceof ErrorLDR)
    {
        $classId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Initialize the response
    $result = new stdClass();
    $result->studentsAdded = 0;
    $result->errors = [];

    // Add the students
    foreach ($studentRecordIds as $studentRecordId)
    {
        // Check that there is a studentRecordId value
        if (strlen($studentRecordId) < 1)
            continue;

        // Validate the studentRecordId
        $argName = 'studentRecordId';
        $validatedStudentRecordId = validateArg($logId, $db, [$argName => $studentRecordId], $argName,
            $argFilterMap[$argName], DCO_REQUIRED);
        if ($validatedStudentRecordId instanceof ErrorLDR)
        {
            $result->errors[$studentRecordId] = $validatedStudentRecordId->getErrorArray();
            continue;
        }

        $studentClassResult = dbCreateStudentClass($logId, $db, $clientUserId, $classId, $validatedStudentRecordId, true);
        if ($studentClassResult instanceof ErrorLDR) {
            $result->errors[$studentRecordId] = $studentClassResult->getErrorArray();
            continue;
        }

        if (AUTO_TEST_ASSIGNMENT) {
            $testAssignments = $studentClassResult['testAssignments'];
            if ($testAssignments->expectedCount != count($testAssignments->testAssignmentIds)) {
                $errorLDR = new ErrorLDR(LDR_EC_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS, LDR_ED_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS);
                $result->errors[$studentRecordId] = $errorLDR->getErrorArray();
                continue;
            }
        }
        $result->studentsAdded++;
    }

    // Return the result
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createStudentRecord()
//   Creates a new student record
//
function createStudentRecord($logId = null, $requestData = null)
{
    if ($logId === null)
    {
        // Log the service call
        $startTime = microtime(true);
        $logId = logRequest();
    }
    else
        $startTime = null;

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,

            // organization identifiers/IDs
            'enrollStateCode' => DCO_OPTIONAL | DCO_TOUPPER,
            'enrollSchoolIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,
            'enrollOrgId' => DCO_OPTIONAL | DCO_MUST_EXIST,

            // student / student_record identifiers
            'stateIdentifier' => DCO_REQUIRED | DCO_TOUPPER,
            'localIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,

            // student attributes
            'lastName' => DCO_REQUIRED,
            'firstName' => DCO_REQUIRED,
            'middleName' => DCO_OPTIONAL,
            'dateOfBirth' => DCO_REQUIRED,
            'gender' => DCO_REQUIRED | DCO_TOUPPER,

            // student_record attributes
            'optionalStateData1' => DCO_OPTIONAL,
            'gradeLevel' => DCO_REQUIRED | DCO_TOUPPER,
            'raceAA' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAN' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAS' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceHL' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceHP' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceWH' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusDIS' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusECO' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusELL' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusGAT' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusLEP' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusMIG' => DCO_OPTIONAL | DCO_TOUPPER,
            'disabilityType' => DCO_OPTIONAL | DCO_TOUPPER,
            'optionalStateData2' => DCO_OPTIONAL,
            'optionalStateData3' => DCO_OPTIONAL,
            'optionalStateData4' => DCO_OPTIONAL,
            'optionalStateData5' => DCO_OPTIONAL,
            'optionalStateData6' => DCO_OPTIONAL,
            'optionalStateData7' => DCO_OPTIONAL,
            'optionalStateData8' => DCO_OPTIONAL,

            // class assignments
            'classAssignment1' => DCO_OPTIONAL,
            'classAssignment2' => DCO_OPTIONAL,
            'classAssignment3' => DCO_OPTIONAL,
            'classAssignment4' => DCO_OPTIONAL,
            'classAssignment5' => DCO_OPTIONAL,
            'classAssignment6' => DCO_OPTIONAL,
            'classAssignment7' => DCO_OPTIONAL,
            'classAssignment8' => DCO_OPTIONAL,
            'classAssignment9' => DCO_OPTIONAL,
            'classAssignment10' => DCO_OPTIONAL,

            'isDryRun' => DCO_OPTIONAL | DCO_TOUPPER
        ), $requestData);
    if ($args instanceof ErrorLDR)
    {
        if ($requestData === null)
            $args->returnErrorJSON(__FUNCTION__);
        return $args;
    }
    list($db, $clientUserId, $enrollStateCode, $enrollSchoolIdentifier, $enrollOrgId, $stateIdentifier, $localIdentifier,
         $lastName, $firstName, $middleName, $dateOfBirth, $gender, $optionalStateData1, $gradeLevel, $raceAA, $raceAN,
         $raceAS, $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG,
         $disabilityType, $optionalStateData2, $optionalStateData3, $optionalStateData4, $optionalStateData5,
         $optionalStateData6, $optionalStateData7, $optionalStateData8, $classAssignment1, $classAssignment2,
         $classAssignment3, $classAssignment4, $classAssignment5, $classAssignment6, $classAssignment7,
         $classAssignment8, $classAssignment9, $classAssignment10, $isDryRun) = $args;

    // Get the student enrollment organization
    if (strlen($enrollOrgId) == 0)
    {
        // organizationId was not passed so use the stateCode and organizationIdentifier
        if (strlen($enrollStateCode) == 0 || strlen($enrollSchoolIdentifier) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED,
                                     'One or more enrollment organization values not provided');
            if ($requestData === null)
                $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return $errorLDR;
        }
        $stateOrganizationId = dbGetStateOrganizationId($logId, $db, $enrollStateCode);
        if ($stateOrganizationId instanceof ErrorLDR)
        {
            if ($requestData === null)
                $stateOrganizationId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return $stateOrganizationId;
        }
        $enrollOrgId =
            dbGetOrgId_ByStateOrgId_ByIdentifier($logId, $db, $stateOrganizationId, $enrollSchoolIdentifier);
        if ($enrollOrgId instanceof ErrorLDR)
        {
            if ($requestData === null)
                $enrollOrgId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return $enrollOrgId;
        }
    }

    // Get the enrollment organization
    $organization = dbGetOrganization($logId, $db, $enrollOrgId);
    if ($organization instanceof ErrorLDR)
    {
        if ($requestData === null)
            $organization->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $organization;
    }

    // Check that the enrollment organization can enroll students
    if ($organization['organizationType'] != ORG_TYPE_SCHOOL)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_CANNOT_ENROLL_STUDENTS,
            LDR_ED_ORGANIZATION_CANNOT_ENROLL_STUDENTS . $enrollOrgId);
        if ($requestData === null)
            $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $errorLDR;
    }

    // Check that the stateIdentifier is available
    $result = checkStudentIdentifierAvailable($logId, $db, $organization['stateOrganizationId'], $stateIdentifier);
    if ($result instanceof ErrorLDR)
    {
        if ($requestData === null)
            $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $result;
    }

    // Validate the dateOfBirth
    $dateOfBirth = validateDate($logId, $dateOfBirth, 'dateOfBirth');
    if ($dateOfBirth instanceof ErrorLDR)
    {
        if ($requestData === null)
            $dateOfBirth->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $dateOfBirth;
    }

    // Validate the gradeLevel
    $gradeLevel = validateGradeLevel($logId, $gradeLevel);
    if ($gradeLevel instanceof ErrorLDR)
    {
        if ($requestData === null)
            $gradeLevel->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $gradeLevel;
    }

    // Validate the disabilityType
    if (strlen($disabilityType) > 0)
    {
        $disabilityType = validateDisabilityType($logId, $disabilityType);
        if ($disabilityType instanceof ErrorLDR)
        {
            if ($requestData === null)
                $disabilityType->returnErrorJSON(__FUNCTION__);
            $db = null;
            return $disabilityType;
        }
    }

    // Check that the statusDIS / disabilityType values are consistent
    if ($statusDIS == 'Y' && strlen($disabilityType) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DISABILITY_ARGS_ARE_INCONSISTENT,
                                 'statusDIS argument is Y but disabilityType argument is missing');
        if ($requestData === null)
            $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $errorLDR;
    }
    if ($statusDIS != 'Y' && strlen($disabilityType) > 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DISABILITY_ARGS_ARE_INCONSISTENT, 'disabilityType argument is ' .
                                 $disabilityType . ' but statusDIS argument is not Y');
        if ($requestData === null)
            $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $errorLDR;
    }

    // Validate the class assignments
    $classAssignments =
    [
        [$classAssignment1, 0], [$classAssignment2, 0], [$classAssignment3, 0], [$classAssignment4, 0],
        [$classAssignment5, 0], [$classAssignment6, 0], [$classAssignment7, 0], [$classAssignment8, 0],
        [$classAssignment9, 0], [$classAssignment10, 0]
    ];
    foreach ($classAssignments as &$classAssignment)
    {
        // Check if there is a class assignment value
        if (strlen($classAssignment[0]) > 0)
        {
            // Get the classId
            $classId = dbGetClassId_By_SectionNumber($logId, $db, $enrollOrgId, $classAssignment[0]);
            if ($classId instanceof ErrorLDR)
            {
                if ($requestData === null)
                    $classId->returnErrorJSON(__FUNCTION__);
                $db = null;
                return $classId;
            }
            if ($classId == 0)
            {
                $classId = dbGetClassId_By_ClassIdentifier($logId, $db, $enrollOrgId, $classAssignment[0]);
                if ($classId instanceof ErrorLDR)
                {
                    if ($requestData === null)
                        $classId->returnErrorJSON(__FUNCTION__);
                    $db = null;
                    return $classId;
                }
            }
            if ($classId == 0)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_NOT_FOUND, 'Class ' . $classAssignment[0] .
                    ' for student ' . $firstName . ' ' . $lastName . ' (' . $stateIdentifier . ') not found');
                if ($requestData === null)
                    $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return $errorLDR;
            }

            // Save the classId
            $classAssignment[1] = $classId;
        }
    }

    // Check if this is a dry run
    if ($isDryRun == 'Y')
    {
        $result = ['studentRecordId' => '0'];
        if ($requestData === null)
            finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
        return $result;
    }

    // Create the student record(s)
    $studentRecordResult = dbCreateStudentRecord($logId, $db, $clientUserId, $enrollOrgId,
        $organization['stateOrganizationId'], $stateIdentifier, $localIdentifier, '', $lastName, $firstName,
        $middleName, $dateOfBirth, $gender, $optionalStateData1, $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL,
        $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType,
        $optionalStateData2, $optionalStateData3, $optionalStateData4, $optionalStateData5, $optionalStateData6,
        $optionalStateData7, $optionalStateData8, $classAssignments);
    if ($studentRecordResult instanceof ErrorLDR) {
        if ($requestData === null)
            $studentRecordResult->returnErrorJSON(__FUNCTION__);
        $db = null;
        return $studentRecordResult;
    }

    if (AUTO_TEST_ASSIGNMENT) {
        $testAssignments = $studentRecordResult['testAssignments'];
        if ($testAssignments != SUCCESS) {
            $errorLDR = new ErrorLDR(LDR_EC_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS, LDR_ED_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS);
            if ($requestData === null)
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return $errorLDR;
        }
    }

    $studentRecordId = $studentRecordResult['studentRecordId'];
    // Return the student record ID
    $result = ['studentRecordId' => $studentRecordId];
    if ($requestData === null)
        finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
    return $result;
}

// createStudentRecords()
//   Creates one or more student records
//
function createStudentRecords()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'isDryRun' => DCO_OPTIONAL | DCO_TOUPPER,
            'students' => DCO_REQUIRED | DCO_NO_CHECK
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $isDryRun, $students) = $args;

    // Initialize the response
    $result = new stdClass();
    $result->studentsAdded = 0;
    $result->errors = [];

    // Add the students
    foreach ($students as $key => $student)
    {
        $student['isDryRun'] = $isDryRun;
        $student['clientUserId'] = $clientUserId;
        $studentRecordId = createStudentRecord($logId, $student);
        if ($studentRecordId instanceof ErrorLDR)
        {
            $result->errors[$key] = $studentRecordId->getErrorArray();
            continue;
        }

        if ($isDryRun != 'Y')
            $result->studentsAdded++;
    }

    // Return the result
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createStudentRecordsBatched()
//   Creates one or more student records
//
function createStudentRecordsBatched()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'isDryRun' => DCO_OPTIONAL | DCO_TOUPPER,
            'students' => DCO_REQUIRED | DCO_NO_CHECK
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $isDryRun, $studentRecords) = $args;

    // Initialize the response
    $result = new stdClass();
    $result->studentsAdded = 0;
    $result->errors = [];
    $students = [];
    $enrollOrgIds = [];
    $stateCodeOrgIdentifier = [];
    $stateCodeStateIdentifier = [];
    $classAssignments = [];
    $schoolOrgIdClassIdentifier = [];

    foreach ($studentRecords as $key => $studentRecord) {
        $validationResult = validateStudentRecord($logId, $db, $studentRecord);
        if ($validationResult instanceof ErrorLDR) {
            $result->errors[$key] = $validationResult->getErrorArray();
            continue;
        }

        list($students[$key], $enrollOrgIds[$key], $stateCodeOrgIdentifier[$key], $classAssignments[$key]) = $validationResult;

        // $uniqueClasses = $uniqueClasses + array_unique($classAssignments);
    }

    if (count($students) > 0) {
        /**
         * Get orgIds if statecode and OrgIdentifier is given
         */
        if (count($stateCodeOrgIdentifier) > 0) {
            $schoolOrgIds = dbGetOrgId_ByStateCode_ByOrgIdentifier($logId, $db, $stateCodeOrgIdentifier);
            foreach ($schoolOrgIds as $identifier => $orgId) {
                $studentsKey = array_keys($stateCodeOrgIdentifier, $identifier);
                foreach ($studentsKey as $key) {
                    $students[$key]['enrollOrgId'] = $orgId;
                    $enrollOrgIds[$key] = $orgId;
                }
            }
        }

        /**
        * Check if there are students with no orgid
        */
        $enrollOrgIds = array_combine(array_keys($students), array_column($students, 'enrollOrgId'));
        $studentsWithoutOrg = array_keys($enrollOrgIds, 0);
        if (count($studentsWithoutOrg) > 0) {
            foreach ($studentsWithoutOrg as $studentWithoutOrg) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND
                    , 'Organization not found for '.$students[$studentWithoutOrg]['enrollStateCode'].' organization identifier '.$students[$studentWithoutOrg]['enrollSchoolIdentifier']);
                $result->errors[$studentWithoutOrg] = $errorLDR->getErrorArray();
                unset($students[$studentWithoutOrg]);
            }
            $enrollOrgIds = array_intersect_key($enrollOrgIds, $students);
        }

        $orgsInfo = dbGetOrganization($logId, $db, $enrollOrgIds);
        foreach ($enrollOrgIds as $studentKey => $enrollOrgId) {
            if ($orgsInfo[$enrollOrgId]['organizationType'] != ORG_TYPE_SCHOOL) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_CANNOT_ENROLL_STUDENTS,
                    LDR_ED_ORGANIZATION_CANNOT_ENROLL_STUDENTS . $orgId);
                $result->errors[$studentKey] = $errorLDR->getErrorArray();
                unset($students[$studentKey]);
            } else {
                $classAssignments[$studentKey] = array_filter($classAssignments[$studentKey]);
                $schoolOrgIdClassIdentifier[$studentKey] = $enrollOrgId.FIELD_SEPARATOR.implode("', '".$enrollOrgId.FIELD_SEPARATOR, $classAssignments[$studentKey]);
            }
        }
    }

    if (count($students) > 0) {
        /**
         * Check student identifier
         */
        $stateOrgIdStateIdentifier = [];
        foreach ($students as $key => $student) {
            $stateOrgIdStateIdentifier[$key] = $orgsInfo[$student['enrollOrgId']]['stateOrganizationId'].FIELD_SEPARATOR.$student['stateIdentifier'];
            $students[$key]['stateOrgId'] = $orgsInfo[$student['enrollOrgId']]['stateOrganizationId'];
        }

        $uniqueStateOrgIdStateIdentifier = array_unique($stateOrgIdStateIdentifier);
        if (count($uniqueStateOrgIdStateIdentifier) != count($stateOrgIdStateIdentifier)) {
            $duplicateStateOrgIdStateIdentifiers = array_diff_key($stateOrgIdStateIdentifier, $uniqueStateOrgIdStateIdentifier);
            foreach ($duplicateStateOrgIdStateIdentifiers as $key => $duplicateStateOrgIdStateIdentifier) {
                $duplicateStateIdentifier = explode(FIELD_SEPARATOR, $duplicateStateOrgIdStateIdentifier);
                $duplicateStateIdentifier = implode('', array_slice($duplicateStateIdentifier, 1));
                $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_STATE_IDENTIFIER_DUP
                    , 'Another student within the uploaded list already using state identifier '.$duplicateStateIdentifier);
                $result->errors[$key] = $errorLDR->getErrorArray();
                unset($students[$key]);
                unset($stateOrgIdStateIdentifier[$key]);
            }
        }

        $studentsIdentifier = dbGetStudentsRecordId_ByStateOrgId_ByStateIdentifier($logId, $db, $stateOrgIdStateIdentifier);
        if (count($studentsIdentifier) > 0) {
            foreach ($studentsIdentifier as $studentIdentifier => $studentRecordId) {
                $studentsKey = array_keys($stateOrgIdStateIdentifier, $studentIdentifier);
                foreach ($studentsKey as $studentKey) {
                    $stateInfo = '';
                    if (strlen($students[$studentKey]['enrollStateCode']) > 0) {
                        $stateInfo = $students[$studentKey]['enrollStateCode'];
                    } else {
                        $stateInfo = 'StateOrgId: '.$orgsInfo[$students[$studentKey]['enrollOrgId']]['stateOrganizationId'];
                    }
                    $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_STATE_IDENTIFIER_DUP,
                        'A student in '.$stateInfo.' with state identifier '.$students[$studentKey]['stateIdentifier'].' already exists');
                    $result->errors[$studentKey] = $errorLDR->getErrorArray();
                    unset($students[$studentKey]);
                }
            }
        }
    }

    $classIds = [];

    if (count($students) > 0) {
        /**
        * Get class assignment
        */
        $classIdsByClassIdentifier = [];
        $classIdsBySectionNumber = dbGetClassId_ByOrgId_BySectionNumber($logId, $db, $schoolOrgIdClassIdentifier);
        if (count($classIdsBySectionNumber) >= 0 && count($classIdsBySectionNumber) < count($schoolOrgIdClassIdentifier)) {
            $classIdsByClassIdentifier = dbGetClassId_ByOrgId_ByClassIdentifier($logId, $db, $schoolOrgIdClassIdentifier);
        }

        $classAssignments = array_intersect_key($classAssignments, $students);
        foreach ($classAssignments as $key => $classes) {
            foreach ($classes as $cKey => $classAssignment) {
                if (isset($classAssignment) === true) {
                    $classId = 0;
                    $key2search = $enrollOrgIds[$key].FIELD_SEPARATOR.$classAssignment;
                    if (array_key_exists($key2search, $classIdsBySectionNumber) === true) {
                        $classId = $classIdsBySectionNumber[$key2search];
                    } elseif (array_key_exists($key2search, $classIdsByClassIdentifier) === true) {
                        $classId = $classIdsByClassIdentifier[$key2search];
                    } else {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_NOT_FOUND, 'Class ' . $classAssignment .
                            ' for student ' . $students[$key]['firstName'] . ' ' . $students[$key]['lastName'] . ' (' . $students[$key]['stateIdentifier'] . ') not found');
                        $result->errors[$key] = $errorLDR->getErrorArray();
                        unset($students[$key]);
                        continue;
                    }

                    $students[$key]['classAssignments'][] = $classId;
                    $classIds[] = $classId;
                }
            }
        }
    }

    if (count($students) > 0) {
        if ($isDryRun != 'Y') {
            /**
            * Add the new student record
            */
            $studentsIdStudentRecordsId = dbCreateStudentRecords($logId, $db, $clientUserId, $students);
            if ($studentsIdStudentRecordsId instanceof ErrorLDR) {
                $studentsIdStudentRecordsId->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            $allEnrollOrgIds = array_unique(array_column($students, 'enrollOrgId'));
            $adjustments = array_fill_keys($allEnrollOrgIds, 0);
            foreach ($students as $key => $student) {
                $adjustments[$student['enrollOrgId']] = $adjustments[$student['enrollOrgId']] + 1;
            }
            $adjustmentResult = dbAdjustOrganizationsStudentCount($logId, $db, $adjustments);
            if ($adjustmentResult instanceof ErrorLDR) {
                $adjustmentResult->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            $result->studentsAdded = count($students);

            if (AUTO_TEST_ASSIGNMENT && !empty($classIds) && count($classIds) > 0) {
                $resultTestAssignments = new stdClass();
                $resultTestAssignments->testsAssigned = 0;
                $resultTestAssignments->testBatteriesCount = 0;
                $resultTestAssignments->classCount = 0;
                $resultTestAssignments->errors = [];

                $classIds = array_unique($classIds);
                $classesInfo = dbGetClass($logId, $db, $classIds);
                if ($classesInfo instanceof ErrorLDR) {
                    $classesInfo->returnErrorJSON(__FUNCTION__);
                    $db = null;
                    return;
                }
                if (count($classesInfo) !== count($classIds)) {
                    $missingClasses = array_diff_key(array_flip($classIds), $classesInfo);
                    foreach ($missingClasses as $missingClassId => $missingClass) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_UNABLE_TO_RETRIEVE_CLASS_INFO
                                                    , LDR_ED_UNABLE_TO_RETRIEVE_CLASS_INFO.$missingClassId);
                        $resultTestAssignments->errors[$missingClassId] = $errorLDR->getErrorArray();
                        $errorLDR->logError(__FUNCTION__);
                    }
                }
                $resultTestAssignments->classCount = count($classesInfo);

                if (count($classesInfo) > 0) {
                    // Get all test batteries
                    $testBatteries = dbGetTestBatteries($logId, $db, null, null, null, null, null, null);
                    if ($testBatteries instanceof ErrorLDR) {
                        $testBatteries->returnErrorJSON(__FUNCTION__);
                        $db = null;
                        return;
                    }
                    $resultTestAssignments->testBatteriesCount = count($testBatteries);
                    $testBatteryIdGradeLevel = array_column($testBatteries, 'testBatteryGrade', 'testBatteryId');

                    set_time_limit(BULK_ASSIGNMENT_TIMEOUT);

                    foreach ($classesInfo as $classId => $classInfo) {
                        $testBatteryForGradeLevel = array_keys($testBatteryIdGradeLevel, $classInfo['gradeLevel']);
                        if (count($testBatteryForGradeLevel) === 0) {
                            $errorLDR = new ErrorLDR($logId, LDR_EC_NO_TEST_BATTERY_FOR_GRADE, LDR_ED_NO_TEST_BATTERY_FOR_GRADE.$classInfo['gradeLevel']);
                            $resultTestAssignments->errors[$classId] = $errorLDR->getErrorArray();
                            $errorLDR->logError(__FUNCTION__);
                            continue;
                        }

                        foreach ($testBatteryForGradeLevel as $testBatteryId) {
                            // USE THE BATCH TEST ASSIGNMENTS INSTEAD OF DOING IT ONE BY ONE
                            $countOfNewTestAssignments = createNewTestAssignments($logId, $db, 1, $classId, $testBatteryId, 0, 'N');
                            if ($countOfNewTestAssignments instanceof ErrorLDR) {
                                if ($countOfNewTestAssignments->getErrorCode() != LDR_EC_STUDENT_HAS_ACTIVE_TEST_ASSIGNMENT) {
                                    $resultTestAssignments->errors[$classId][$testBatteryId] = $countOfNewTestAssignments->getErrorArray();
                                    $countOfNewTestAssignments->logError(__FUNCTION__);
                                }
                                continue;
                            }

                            $resultTestAssignments->testsAssigned += $countOfNewTestAssignments;
                        }
                    }
                }
            }
        }
    }

    // Return the result
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteAllClassStudents()
//   Deletes all student class assignments for a class or all classes in or under an organization
//
function deleteAllClassStudents()
{
    global $sessionClientId;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'organizationId' => DCO_OPTIONAL | DCO_MUST_EXIST,
            'classId' => DCO_OPTIONAL | DCO_MUST_EXIST
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $organizationId, $classId) = $args;

    // Check the session client ID
    if ($sessionClientId != CLIENT_ID_LDR_ROOT && $sessionClientId != CLIENT_ID_PARCC_QA)
    {
        $clientName = dbGetClientName_By_ClientId($logId, $db, $sessionClientId);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CLIENT_NOT_AUTHORIZED, LDR_ED_CLIENT_NOT_AUTHORIZED . $clientName);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that there is a classId or organizationID
    if (strlen($classId) == 0 && strlen($organizationId) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED, 'No classId or organizationId value was provided');
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the student_class IDs
    if (strlen($classId) > 0)
        $studentClassIds = dbGetStudentClassIds_ByClassId($logId, $db, $classId);
    else
        $studentClassIds = dbGetStudentClassIds_ByOrganizationId($logId, $db, $organizationId);

    // Delete the student class assignments
    foreach ($studentClassIds as $studentClassId)
    {
        $deleteResult = dbDeleteStudentClass($logId, $db, $clientUserId, $studentClassId, $studentClassId);
        if ($deleteResult instanceof ErrorLDR)
        {
            $deleteResult->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Return the result
    $response = ['result' => SUCCESS, 'countDeleted' => sizeof($studentClassIds)];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteClassStudents()
//   Deletes one or more student class assignments
//
function deleteClassStudents($classId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'studentRecordIds' => DCO_REQUIRED | DCO_NO_CHECK
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $studentRecordIds) = $args;

    // Validate the classId
    $argName = 'classId';
    $classId = validateArg($logId, $db, [$argName => $classId], $argName, $argFilterMap[$argName],
                           DCO_REQUIRED | DCO_MUST_EXIST);
    if ($classId instanceof ErrorLDR)
    {
        $classId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Initialize the response
    $result = new stdClass();
    $result->studentsDeleted = 0;
    $result->errors = [];

    // Delete the student class assignments
    foreach ($studentRecordIds as $studentRecordId)
    {
        // Check that there is a studentRecordId value
        if (strlen($studentRecordId) < 1)
            continue;

        // Validate the studentRecordId
        $argName = 'studentRecordId';
        $validatedStudentRecordId = validateArg($logId, $db, [$argName => $studentRecordId], $argName,
                                                $argFilterMap[$argName], DCO_REQUIRED | DCO_MUST_EXIST);
        if ($validatedStudentRecordId instanceof ErrorLDR)
        {
            $result->errors[$studentRecordId] = $validatedStudentRecordId->getErrorArray();
            continue;
        }

        // Get the student_class ID (the class assignment primary key)
        $studentClassId = dbGetStudentClassId($logId, $db, $studentRecordId, $classId);
        if ($studentClassId instanceof ErrorLDR)
        {
            $result->errors[$studentRecordId] = $studentClassId->getErrorArray();
            continue;
        }
        if ($studentClassId == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_NOT_IN_CLASS, 'Student with studentRecordId ' .
                                     $studentRecordId . ' is not assigned to the class that has classId ' . $classId);
            $result->errors[$studentRecordId] = $errorLDR->getErrorArray();
            continue;
        }

        // Delete the student class assignment
        $deleteResult = dbDeleteStudentClass($logId, $db, $clientUserId, $studentRecordId, $studentClassId);
        if ($deleteResult instanceof ErrorLDR)
        {
            $result->errors[$studentRecordId] = $deleteResult->getErrorArray();
            continue;
        }

        $result->studentsDeleted++;
    }

    // Return the result
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteStudentRecord()
//   Deletes a student record
//
function deleteStudentRecord($studentRecordId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId) = $args;

    // Validate the studentRecordId
    $argName = 'studentRecordId';
    $studentRecordId = validateArg($logId, $db, [$argName => $studentRecordId], $argName, $argFilterMap[$argName],
                                   DCO_REQUIRED | DCO_MUST_EXIST);
    if ($studentRecordId instanceof ErrorLDR)
    {
        $studentRecordId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check if the student record can be deleted
    $result = dbCheckStudentCanBeDeleted($logId, $db, $studentRecordId);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Delete the student record
    $result = dbDeleteStudentRecord($logId, $db, $clientUserId, $studentRecordId);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    $response = ['result' => $result];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteStudentRecords()
//   Deletes student records
//
function deleteStudentRecords($organizationId)
{
    global $argFilterMap, $sessionClientId;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId) = $args;

    // Check the session client ID
    if ($sessionClientId != CLIENT_ID_LDR_ROOT && $sessionClientId != CLIENT_ID_PARCC_QA)
    {
        $clientName = dbGetClientName_By_ClientId($logId, $db, $sessionClientId);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CLIENT_NOT_AUTHORIZED, LDR_ED_CLIENT_NOT_AUTHORIZED . $clientName);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Validate the organizationId
    $argName = 'organizationId';
    $organizationId = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName],
                                  DCO_REQUIRED | DCO_MUST_EXIST);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the student record IDs
    $studentRecordIds = dbGetStudentRecordIds($logId, $db, $organizationId);
    if ($studentRecordIds instanceof ErrorLDR)
    {
        $studentRecordIds->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that all of the student records can be deleted
    foreach ($studentRecordIds as $studentRecordId)
    {
        $result = dbCheckStudentCanBeDeleted($logId, $db, $studentRecordId);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Delete the student records
    foreach ($studentRecordIds as $studentRecordId)
    {
        $result = dbDeleteStudentRecord($logId, $db, $clientUserId, $studentRecordId);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Return the result
    $response = ['result' => SUCCESS, 'countDeleted' => sizeof($studentRecordIds)];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// exportStudents()
//  Data Export of Students storing on S3 bucket
//
function exportStudents($logId, PDO $db, $startDate, $endDate)
{
    ini_set('memory_limit', DATA_EXPORT_MAX_MEMORY);
    $studentsData = dbExportStudents($logId, $db, $startDate, $endDate);
    if ($studentsData instanceof ErrorLDR) {
        return $studentsData;
    }

    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
    $reportName = ROOT_ORGANIZATION_NAME."-Students-".$dateTimeInfo;
    $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                ."/".$reportName.".tsv";
    $studentsCSV = generateCSVData($logId, $studentsData->report, null, true, null, "\t");
    if ($studentsCSV instanceof ErrorLDR) {
        return $studentsCSV;
    }

    $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $studentsCSV);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->logError(__FUNCTION__);
        return $reportUrl;
    }

    return $reportUrl;
}

// getClassStudents()
//   Returns a list of student records
//
function getClassStudents($classId = null)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'classIds' => DCO_OPTIONAL | DCO_NO_CHECK
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $classIds) = $args;

    // Check if there is a classId arg
    if (!is_null($classId))
    {
        $argName = 'classId';
        $classId = validateArg($logId, $db, [$argName => $classId], $argName, $argFilterMap[$argName],
            DCO_REQUIRED | DCO_MUST_EXIST);
        if ($classId instanceof ErrorLDR)
        {
            $classId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        $classIds = [$classId];
    }
    // Otherwise, check if there is a classIds arg
    elseif (strlen($classIds) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED,
                                 'Required value classId or classIds not provided');
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    else
    {
        // Check that the classes exist
        $classIds = explode(',', $classIds);
        foreach ($classIds as $classId)
        {
            $class = dbGetClass($logId, $db, $classId);
            if ($class instanceof ErrorLDR)
            {
                $class->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }
        }
    }

    // Get the student records
    $studentRecords = dbGetClassStudents($logId, $db, $classIds);
    if ($studentRecords instanceof ErrorLDR)
    {
        $studentRecords->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the student records with the total count
    finishService($db, $logId, $studentRecords, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getStudentClasses()
//   Returns a student's classes
//
function getStudentClasses($studentRecordId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the studentRecordId
    $argName = 'studentRecordId';
    $studentRecordId = validateArg($logId, $db, [$argName => $studentRecordId], $argName, $argFilterMap[$argName],
                                   DCO_REQUIRED | DCO_MUST_EXIST);
    if ($studentRecordId instanceof ErrorLDR)
    {
        $studentRecordId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the student's classes
    $classes = dbGetStudentClasses($logId, $db, $studentRecordId);
    if ($classes instanceof ErrorLDR)
    {
        $classes->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the student record
    finishService($db, $logId, $classes, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getStudentRecord()
//   Returns a student record
//
function getStudentRecord($studentRecordId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'optionalStateData' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $optionalStateData) = $args;

    // Validate the studentRecordId
    $argName = 'studentRecordId';
    $name = validateArg($logId, $db, [$argName => $studentRecordId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the student record
    $studentRecord = dbGetStudentRecord($logId, $db, $studentRecordId, $optionalStateData);
    if ($studentRecord instanceof ErrorLDR)
    {
        $studentRecord->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Remove the stateOrgId
    unset($studentRecord['stateOrgId']);

    // Return the student record
    finishService($db, $logId, $studentRecord, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getStudentRecords()
//   Returns a list of student records
//
function getStudentRecords($organizationId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'gradeLevel' => DCO_OPTIONAL | DCO_TOUPPER,
            'gender' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAA' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAN' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAS' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceHL' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceHP' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceWH' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusDIS' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusECO' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusELL' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusGAT' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusLEP' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusMIG' => DCO_OPTIONAL | DCO_TOUPPER,
            'disabilityType' => DCO_OPTIONAL | DCO_TOUPPER,
            'pageOffset' => DCO_OPTIONAL,
            'pageLimit' => DCO_OPTIONAL,
            'hasClasses' => DCO_OPTIONAL | DCO_TOUPPER,
            'withClasses' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $gradeLevel, $gender, $raceAA, $raceAN, $raceAS, $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO,
         $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType, $pageOffset, $pageLimit, $hasClasses,
         $withClasses) = $args;

    // Validate the organizationId
    $argName = 'organizationId';
    $organizationId = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName],
        DCO_REQUIRED | DCO_MUST_EXIST);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the student records
    $studentRecords = dbGetStudentRecords($logId, $db, $organizationId, $gradeLevel, $gender, $raceAA, $raceAN,
        $raceAS, $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG,
        $disabilityType, $pageOffset, $pageLimit, $hasClasses, $withClasses);
    if ($studentRecords instanceof ErrorLDR)
    {
        $studentRecords->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the total count of student records
    $totalCount = dbGetStudentRecordCount($logId, $db, $organizationId, $gradeLevel, $gender, $raceAA, $raceAN, $raceAS,
                                          $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT,
                                          $statusLEP, $statusMIG, $disabilityType, $hasClasses);
    if ($totalCount instanceof ErrorLDR)
    {
        $totalCount->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the student records with the total count
    $result = ['totalCount' => $totalCount, 'studentRecords' => $studentRecords];
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// updateStudentRecord()
//   Updates a student record
//
// TODO: IMPLEMENT DELETION OF OPTIONAL VALUES
function updateStudentRecord($studentRecordId)
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,

            // student_record identifiers
            'stateIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,
            'localIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,

            // student attributes
            'lastName' => DCO_OPTIONAL,
            'firstName' => DCO_OPTIONAL,
            'middleName' => DCO_OPTIONAL,
            'dateOfBirth' => DCO_OPTIONAL,
            'gender' => DCO_OPTIONAL | DCO_TOUPPER,

            // student_record attributes
            'gradeLevel' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAA' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAN' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceAS' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceHL' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceHP' => DCO_OPTIONAL | DCO_TOUPPER,
            'raceWH' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusDIS' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusECO' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusELL' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusGAT' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusLEP' => DCO_OPTIONAL | DCO_TOUPPER,
            'statusMIG' => DCO_OPTIONAL | DCO_TOUPPER,
            'disabilityType' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $stateIdentifier, $localIdentifier, $lastName, $firstName, $middleName, $dateOfBirth,
        $gender, $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL,
        $statusGAT, $statusLEP, $statusMIG, $disabilityType) = $args;

    // Get the student record
    $studentRecord = dbGetStudentRecord($logId, $db, $studentRecordId);
    if ($studentRecord instanceof ErrorLDR)
    {
        $studentRecord->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that the stateIdentifier is available
    if (strlen($stateIdentifier) > 0)
    {
        $result = checkStudentIdentifierAvailable($logId, $db, $studentRecord['stateOrgId'], $stateIdentifier);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the dateOfBirth
    if (strlen($dateOfBirth) > 0)
    {
        $dateOfBirth = validateDate($logId, $dateOfBirth, 'dateOfBirth');
        if ($dateOfBirth instanceof ErrorLDR)
        {
            $dateOfBirth->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the gradeLevel
    if (strlen($gradeLevel) > 0)
    {
        $gradeLevel = validateGradeLevel($logId, $gradeLevel);
        if ($gradeLevel instanceof ErrorLDR)
        {
            $gradeLevel->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the disabilityType
    if (strlen($disabilityType) > 0)
    {
        $disabilityType = validateDisabilityType($logId, $disabilityType);
        if ($disabilityType instanceof ErrorLDR)
        {
            $disabilityType->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Check that the statusDIS / disabilityType values are consistent
    $checkStatusDIS = (strlen($statusDIS) > 0) ? $statusDIS : $studentRecord['statusDIS'];
    $checkDisabilityType = (strlen($disabilityType) > 0) ? $disabilityType : $studentRecord['disabilityType'];
    if ($checkStatusDIS == 'Y' && strlen($checkDisabilityType) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DISABILITY_ARGS_ARE_INCONSISTENT,
                                 'statusDIS argument is Y but disabilityType argument is missing');
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check if the disabilityType should be removed
    if (strlen($statusDIS) > 0 && $statusDIS == 'N')
        $disabilityType = DELETE_THIS_VALUE;

    // Update the student record
    $result = dbUpdateStudentRecord($logId, $db, $clientUserId, $studentRecordId, $stateIdentifier, $localIdentifier,
        $lastName, $firstName, $middleName, $dateOfBirth, $gender, $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL,
        $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    $response = ['result' => $result];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

//
// LDR student support functions
//
// buildStudentAttributesQueryClause()
//   Returns an SQL query clause for optional student attributes
//
function buildStudentAttributesQueryClause($prefix, $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL, $raceHP, $raceWH,
                              $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType)
{
    $sql = "";

    if (strlen($gradeLevel) > 0)
        $sql .= " AND " . $prefix . "gradeLevel = '$gradeLevel'";
    if (strlen($raceAA) > 0)
        $sql .= " AND " . $prefix . "raceAA = '$raceAA'";
    if (strlen($raceAN) > 0)
        $sql .= " AND " . $prefix . "raceAN = '$raceAN'";
    if (strlen($raceAS) > 0)
        $sql .= " AND " . $prefix . "raceAS = '$raceAS'";
    if (strlen($raceHL) > 0)
        $sql .= " AND " . $prefix . "raceHL = '$raceHL'";
    if (strlen($raceHP) > 0)
        $sql .= " AND " . $prefix . "raceHP = '$raceHP'";
    if (strlen($raceWH) > 0)
        $sql .= " AND " . $prefix . "raceWH = '$raceWH'";
    if (strlen($statusDIS) > 0)
        $sql .= " AND " . $prefix . "statusDIS = '$statusDIS'";
    if (strlen($statusECO) > 0)
        $sql .= " AND " . $prefix . "statusECO = '$statusECO'";
    if (strlen($statusELL) > 0)
        $sql .= " AND " . $prefix . "statusELL = '$statusELL'";
    if (strlen($statusGAT) > 0)
        $sql .= " AND " . $prefix . "statusGAT = '$statusGAT'";
    if (strlen($statusLEP) > 0)
        $sql .= " AND " . $prefix . "statusLEP = '$statusLEP'";
    if (strlen($statusMIG) > 0)
        $sql .= " AND " . $prefix . "statusMIG = '$statusMIG'";
    if (strlen($disabilityType) > 0)
        $sql .= " AND " . $prefix . "disabilityType = '$disabilityType'";

    return $sql;
}

// dbCheckStudentCanBeDeleted()
//   Returns an ErrorLDR object if a student cannot be deleted
//
function dbCheckStudentCanBeDeleted($logId, $db, $studentRecordId)
{
    // ADS-1283: Deleting student record also delete the class assignments
    // // Check if this student is assigned to any classes
    // $classCount = dbGetStudentClassCount($logId, $db, $studentRecordId);
    // if ($classCount instanceof ErrorLDR)
    // {
    //     $classCount->logError(__FUNCTION__);
    //     return $classCount;
    // }
    // if ($classCount > 0)
    // {
    //     $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_HAS_CLASSES,
    //         'This student has classes and cannot be deleted - studentRecordId: ' . $studentRecordId);
    //     $errorLDR->logError(__FUNCTION__);
    //     return $errorLDR;
    // }

    // Check if this student has any test assignments
    $testAssignments = dbGetTestAssignments($logId, $db, null, $studentRecordId, null, null, null, null, null, null,
                                            null, null);
    if ($testAssignments instanceof ErrorLDR)
    {
        $testAssignments->logError(__FUNCTION__);
        return $testAssignments;
    }
    if (sizeof($testAssignments) > 0)
    {
        foreach ($testAssignments as $testAssignment)
        {
            if ($testAssignment['testStatusId'] != TEST_STATUS_ID_SCHEDULED) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_HAS_TEST_ASSIGNMENTS,
                    'This student has active or canceled tests and cannot be deleted - studentRecordId: ' . $studentRecordId);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        }
    }

    return true;
}

// checkStudentIdentifierAvailable()
//   Returns true if the student stateIdentifier is available, otherwise an error
//
function checkStudentIdentifierAvailable($logId, PDO $db, $stateOrgId, $stateIdentifier)
{
    // No two students in the same state should have the same stateIdentifier
    $studentRecordId = dbGetStudentRecordId_ByStateOrgId_ByStateIdentifier($logId, $db, $stateOrgId, $stateIdentifier);
    if ($studentRecordId instanceof ErrorLDR)
    {
        $studentRecordId->logError(__FUNCTION__);
        return $studentRecordId;
    }
    if ($studentRecordId > 0)
    {
        $stateCode = dbGetOrganizationName($logId, $db, $stateOrgId);
        if ($stateCode instanceof ErrorLDR)
            $stateCode = '';
        $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_STATE_IDENTIFIER_DUP,
            'A student in ' . $stateCode . ' with state identifier ' . $stateIdentifier . ' already exists');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return true;
}

// dbCreateStudentClass()
//   Creates a student class assignment
//
function dbCreateStudentClass($logId, PDO $db, $clientUserId, $classId, $studentRecordId, $checkOrgsMatch = true)
{
    // Check if the student is already assigned to the class
    $studentClassId = dbGetStudentClassId($logId, $db, $studentRecordId, $classId);
    if ($studentClassId instanceof ErrorLDR)
    {
        $studentClassId->logError(__FUNCTION__);
        return $studentClassId;
    }
    if ($studentClassId > 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_STUDENT_DUP, 'A student with studentRecordId ' .
                                 $studentRecordId . ' is already assigned to class ' . $classId);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    $class = dbGetClass($logId, $db, $classId);
    if ($class instanceof ErrorLDR)
    {
        $class->logError(__FUNCTION__);
        return $class;
    }

    if ($checkOrgsMatch)
    {
        // Check that the student and class are in the same organization
        $studentRecord = dbGetStudentRecord($logId, $db, $studentRecordId);
        if ($studentRecord instanceof ErrorLDR)
        {
            $studentRecord->logError(__FUNCTION__);
            return $studentRecord;
        }
        if ($class['organizationId'] != $studentRecord['enrollOrgId'])
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_CLASS_ORG_MISMATCH, 'Student organization ID ' .
                $studentRecord['enrollOrgId'] . ' does not match class organization ID ' . $class['organizationId']);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }

    // Create the student class assignment
    $sql = "INSERT INTO student_class (studentRecordId, classId) VALUES ('$studentRecordId', '$classId')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $studentClassId = $db->lastInsertId();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create a user audit record
    $dataChanges = json_encode(['classId' => $classId, 'studentRecordId' => $studentRecordId]);
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_CLASS_STUDENT, $studentRecordId,
                                     AUDIT_ACTION_CREATE, $dataChanges);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    $testAssignments = autoCreateTestAssignment($logId, $db, $clientUserId, $studentRecordId, $classId, $class['gradeLevel']);
    if ($testAssignments instanceof ErrorLDR) {
        // No Rollback for the database if there is an error while creating test assignments
        $testAssignments->logError(__FUNCTION__);
        return $testAssignments;
    }

    return ['studentClassId' => $studentClassId, 'testAssignments' => $testAssignments];
}

// dbCreateStudentRecord()
//   Adds a new student
//
function dbCreateStudentRecord($logId, $db, $clientUserId, $enrollOrgId, $stateOrgId, $stateIdentifier,
    $localIdentifier, $globalUniqueIdentifier, $lastName, $firstName, $middleName, $dateOfBirth, $gender,
    $optionalStateData1, $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO,
    $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType, $optionalStateData2, $optionalStateData3,
    $optionalStateData4, $optionalStateData5, $optionalStateData6, $optionalStateData7, $optionalStateData8,
    $classAssignments)
{
    // Turn off autocommit
    $db->beginTransaction();

    // Check if a student needs to be created
    if (strlen($globalUniqueIdentifier) == 0)
    {
        // Create a student GUID
        $globalUniqueIdentifier = generateGUID_RFC4122v4();

        // Prepare the values
        $dbLastName = dbPrepareForSQL($lastName, 'lastName');
        $dbFirstName = dbPrepareForSQL($firstName, 'firstName');
        $dbMiddleName = dbPrepareForSQL($middleName, 'middleName');

        // Create the student
        $sql = "INSERT INTO student (globalUniqueIdentifier, lastName, firstName, middleName, dateOfBirth, gender)
                VALUES ('$globalUniqueIdentifier', '$dbLastName', '$dbFirstName', '$dbMiddleName', '$dateOfBirth',
                        '$gender')";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $studentId = $db->lastInsertId();
        }
        catch(PDOException $error) {
            $db->rollBack();
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }
    else
    {
        // Get the studentId
        $student = dbGetStudent_ByGUID($logId, $db, $globalUniqueIdentifier);
        if ($student instanceof ErrorLDR)
        {
            $db->rollBack();
            $student->logError(__FUNCTION__);
            return $student;
        }
        $studentId = $student['studentId'];
    }

    // Create a student record
    $sqlCols = 'INSERT INTO student_record (studentId, enrollOrgId, stateOrgId, stateIdentifier';
    $sqlVals = "VALUES ('$studentId', '$enrollOrgId', '$stateOrgId', '$stateIdentifier'";
    if (strlen($localIdentifier) > 0)
    {
        $sqlCols .= ', localIdentifier';
        $sqlVals .= ", '$localIdentifier'";
    }
    if (strlen($optionalStateData1) > 0)
    {
        $dbOptionalStateData1 = dbPrepareForSQL($optionalStateData1, 'optionalStateData1');
        $sqlCols .= ', optionalStateData1';
        $sqlVals .= ", '$dbOptionalStateData1'";
    }
    $sqlCols .= ', gradeLevel';
    $sqlVals .= ", '$gradeLevel'";
    if (strlen($raceAA) > 0)
    {
        $sqlCols .= ', raceAA';
        $sqlVals .= ", '$raceAA'";
    }
    if (strlen($raceAN) > 0)
    {
        $sqlCols .= ', raceAN';
        $sqlVals .= ", '$raceAN'";
    }
    if (strlen($raceAS) > 0)
    {
        $sqlCols .= ', raceAS';
        $sqlVals .= ", '$raceAS'";
    }
    if (strlen($raceHL) > 0)
    {
        $sqlCols .= ', raceHL';
        $sqlVals .= ", '$raceHL'";
    }
    if (strlen($raceHP) > 0)
    {
        $sqlCols .= ', raceHP';
        $sqlVals .= ", '$raceHP'";
    }
    if (strlen($raceWH) > 0)
    {
        $sqlCols .= ', raceWH';
        $sqlVals .= ", '$raceWH'";
    }
    if (strlen($statusDIS) > 0)
    {
        $sqlCols .= ', statusDIS';
        $sqlVals .= ", '$statusDIS'";
    }
    if (strlen($statusECO) > 0)
    {
        $sqlCols .= ', statusECO';
        $sqlVals .= ", '$statusECO'";
    }
    if (strlen($statusELL) > 0)
    {
        $sqlCols .= ', statusELL';
        $sqlVals .= ", '$statusELL'";
    }
    if (strlen($statusGAT) > 0)
    {
        $sqlCols .= ', statusGAT';
        $sqlVals .= ", '$statusGAT'";
    }
    if (strlen($statusLEP) > 0)
    {
        $sqlCols .= ', statusLEP';
        $sqlVals .= ", '$statusLEP'";
    }
    if (strlen($statusMIG) > 0)
    {
        $sqlCols .= ', statusMIG';
        $sqlVals .= ", '$statusMIG'";
    }
    if (strlen($disabilityType) > 0)
    {
        $sqlCols .= ', disabilityType';
        $sqlVals .= ", '$disabilityType'";
    }
    if (strlen($optionalStateData2) > 0)
    {
        $dbOptionalStateData2 = dbPrepareForSQL($optionalStateData2, 'optionalStateData2');
        $sqlCols .= ', optionalStateData2';
        $sqlVals .= ", '$dbOptionalStateData2'";
    }
    if (strlen($optionalStateData3) > 0)
    {
        $dbOptionalStateData3 = dbPrepareForSQL($optionalStateData3, 'optionalStateData3');
        $sqlCols .= ', optionalStateData3';
        $sqlVals .= ", '$dbOptionalStateData3'";
    }
    if (strlen($optionalStateData4) > 0)
    {
        $dbOptionalStateData4 = dbPrepareForSQL($optionalStateData4, 'optionalStateData4');
        $sqlCols .= ', optionalStateData4';
        $sqlVals .= ", '$dbOptionalStateData4'";
    }
    if (strlen($optionalStateData5) > 0)
    {
        $dbOptionalStateData5 = dbPrepareForSQL($optionalStateData5, 'optionalStateData5');
        $sqlCols .= ', optionalStateData5';
        $sqlVals .= ", '$dbOptionalStateData5'";
    }
    if (strlen($optionalStateData6) > 0)
    {
        $dbOptionalStateData6 = dbPrepareForSQL($optionalStateData6, 'optionalStateData6');
        $sqlCols .= ', optionalStateData6';
        $sqlVals .= ", '$dbOptionalStateData6'";
    }
    if (strlen($optionalStateData7) > 0)
    {
        $dbOptionalStateData7 = dbPrepareForSQL($optionalStateData7, 'optionalStateData7');
        $sqlCols .= ', optionalStateData7';
        $sqlVals .= ", '$dbOptionalStateData7'";
    }
    if (strlen($optionalStateData8) > 0)
    {
        $dbOptionalStateData8 = dbPrepareForSQL($optionalStateData8, 'optionalStateData8');
        $sqlCols .= ', optionalStateData8';
        $sqlVals .= ", '$dbOptionalStateData8'";
    }
    $sql = $sqlCols . ') ' . $sqlVals . ')';
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $studentRecordId = $db->lastInsertId();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Increment the organization student counts
    $result = dbAdjustStudentCounts($logId, $db, $enrollOrgId, '+ 1');
    if ($result instanceof ErrorLDR)
    {
        $db->rollBack();
        $result->logError(__FUNCTION__);
        return $result;
    }

    // Create a user audit record
    $dataChanges = json_encode([
        // organization IDs
        'enrollOrgId' => $enrollOrgId,
        'stateOrgId' => $stateOrgId,

        // student / student_record identifiers
        'stateIdentifier' => $stateIdentifier,
        'localIdentifier' => $localIdentifier,
        'globalUniqueIdentifier' => $globalUniqueIdentifier,

        // student attributes
        'lastName' => $lastName,
        'firstName' => $firstName,
        'middleName' => $middleName,
        'dateOfBirth' => $dateOfBirth,
        'gender' => $gender,

        // student_record attributes
        'optionalStateData1' => $optionalStateData1,
        'gradeLevel' => $gradeLevel,
        'raceAA' => $raceAA,
        'raceAN' => $raceAN,
        'raceAS' => $raceAS,
        'raceHL' => $raceHL,
        'raceHP' => $raceHP,
        'raceWH' => $raceWH,
        'statusDIS' => $statusDIS,
        'statusECO' => $statusECO,
        'statusELL' => $statusELL,
        'statusGAT' => $statusGAT,
        'statusLEP' => $statusLEP,
        'statusMIG' => $statusMIG,
        'disabilityType' => $disabilityType,
        'optionalStateData2' => $optionalStateData2,
        'optionalStateData3' => $optionalStateData3,
        'optionalStateData4' => $optionalStateData4,
        'optionalStateData5' => $optionalStateData5,
        'optionalStateData6' => $optionalStateData6,
        'optionalStateData7' => $optionalStateData7,
        'optionalStateData8' => $optionalStateData8]);
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_STUDENT_RECORD, $studentRecordId,
        AUDIT_ACTION_CREATE, $dataChanges);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    $testAssignments = "Auto assignment is turned off";
    // Create optional student class assignments
    foreach ($classAssignments as $classAssignment)
    {
        // Check if there is a class ID
        if ($classAssignment[1] > 0)
        {
            $result = dbCreateStudentClass($logId, $db, $clientUserId, $classAssignment[1], $studentRecordId, false);
            if ($result instanceof ErrorLDR) {
                $db->rollBack();
                $result->logError(__FUNCTION__);
                return $result;
            }
            if (AUTO_TEST_ASSIGNMENT) {
                $testAssignments = $result['testAssignments'];
                if ($testAssignments->expectedCount != count($testAssignments->testAssignmentIds)) {
                    $errorLDR = new ErrorLDR(LDR_EC_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS, LDR_ED_CANNOT_AUTO_CREATE_TEST_ASSIGNMENTS);
                    $errorLDR->logError(__FUNCTION__);
                    continue;
                } else {
                    $testAssignments = SUCCESS;
                }
            }
        }
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return ['studentRecordId' => $studentRecordId, 'testAssignments' => $testAssignments];
}

// dbCreateStudentRecords()
// Create student along with student records and class assignments
//
function dbCreateStudentRecords($logId, $db, $clientUserId, $students)
{
    // Turn off autocommit
    $db->beginTransaction();

    $students = dbCreateStudents($logId, $db, $students);
    if ($students instanceof errorLDR) {
        $db->rollback();
        return $students;
    }

    $studentRecordCols = [
        'studentId', 'enrollOrgId', 'stateOrgId', 'stateIdentifier', 'localIdentifier', 'optionalStateData1', 'gradeLevel'
        , 'raceAA', 'raceAN', 'raceAS', 'raceHL', 'raceHP', 'raceWH', 'statusDIS', 'statusECO', 'statusELL', 'statusGAT'
        , 'statusLEP', 'statusMIG', 'disabilityType', 'optionalStateData2', 'optionalStateData3', 'optionalStateData4'
        , 'optionalStateData5', 'optionalStateData6', 'optionalStateData7', 'optionalStateData8'
    ];
    $studentRecords = '';
    $studentsId = [];
    $studentsIdStudentRecordsId = [];

    foreach ($students as $key => $student) {
        $studentsId[$key] = $student['studentId'];
        $studentRecordValues = array_intersect_key($student, array_flip($studentRecordCols));
        $studentRecordValues = array_merge(array_flip($studentRecordCols), $studentRecordValues);
        $studentRecords .= "('".implode("', '", $studentRecordValues)."'),";
    }
    $sqlStudentRecords = "INSERT INTO student_record (".implode(",", $studentRecordCols).") VALUES ".rtrim($studentRecords, ',').";";
    $sqlStudentRecordsId = "SELECT studentId, studentRecordId FROM student_record WHERE studentId IN ('".implode("', '", $studentsId)."');";

    try {
        $sql = $sqlStudentRecords;
        $stmt = $db->prepare($sqlStudentRecords);
        $stmt->execute();

        $sql = $sqlStudentRecordsId;
        $stmt = $db->prepare($sqlStudentRecordsId);
        $stmt->execute();
        $studentRecordsId = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
    } catch(PDOException $error) {
        $db->rollback();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    foreach ($students as $key => &$student) {
        $student['studentRecordId'] = $studentRecordsId[$student['studentId']]['studentRecordId'];
        $studentsIdStudentRecordsId[$key]['studentId'] = $student['studentId'];
        $studentsIdStudentRecordsId[$key]['studentRecordId'] = $student['studentRecordId'];
    }

    $result = dbCreateStudentsClasses($logId, $db, $students);
    if ($result instanceof errorLDR) {
        $db->rollback();
        return $result;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return $studentsIdStudentRecordsId;
}

function dbCreateStudents($logId, $db, $students)
{
    $sqlStudent = '';
    $studentCols = ['globalUniqueIdentifier', 'lastName', 'firstName', 'middleName', 'dateOfBirth', 'gender'];

    foreach ($students as $key => &$student) {
        $student['globalUniqueIdentifier'] = generateGUID_RFC4122v4();

        $sqlStudent .= "SELECT ";
        foreach ($studentCols as $studentCol) {
            $sqlStudent .= $db->quote($student[$studentCol])." AS '".$studentCol."', ";
        }
        $sqlStudent = rtrim($sqlStudent, ', ')." UNION ALL ";
    }
    $sqlStudent = rtrim($sqlStudent, " UNION ALL ");

    $sqlStudentCondition = '';
    foreach ($studentCols as $studentCol) {
        $sqlStudentCondition .= 's1.'.$studentCol."=s2.".$studentCol." AND ";
    }
    $sqlStudentCondition = rtrim($sqlStudentCondition, " AND ");

    $sqlStudent = "INSERT INTO student(".implode(", ", $studentCols).") SELECT ".implode(",", $studentCols)." FROM ("
        .$sqlStudent.") s1 WHERE NOT EXISTS (SELECT 1 FROM student s2 WHERE ".$sqlStudentCondition.");";

    try {
        $stmt = $db->prepare($sqlStudent);
        $stmt->execute();
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sqlStudent);
    }

    $studentsGlobalUniqueIdentifier = array_column($students, 'globalUniqueIdentifier');
    $sql = "SELECT globalUniqueIdentifier, studentId FROM student WHERE globalUniqueIdentifier IN ('".implode("', '", $studentsGlobalUniqueIdentifier)."');";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $studentIds = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    foreach ($students as $key => &$student) {
        $student['studentId'] = $studentIds[$student['globalUniqueIdentifier']]['studentId'];
    }

    return $students;
}

function dbCreateStudentsClasses($logId, $db, $students)
{
    $studentsClasses = [];
    foreach ($students as $key => $student) {
        if (isset($student['classAssignments'])) {
            $classAssignments = $student['classAssignments'];

            foreach ($classAssignments as $cKey => $classAssignment) {
                $studentsClasses[] = "('".$student['studentRecordId']."', '".$classAssignment."')";
            }
        }
    }

    if (count($studentsClasses) > 0) {
        $sql = 'INSERT INTO student_class (studentRecordId, classId) VALUES '.implode(",", $studentsClasses).";";

        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        } catch (PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    return SUCCESS;
}

// dbDeleteStudentClass()
//   Deletes a student class assignment
//
function dbDeleteStudentClass($logId, PDO $db, $clientUserId, $studentRecordId, $studentClassId)
{
    // Turn off autocommit
    $db->beginTransaction();

    // Delete the student_class record
    $sql = "UPDATE student_class SET deleted = 'Y' WHERE studentClassId = '" . $studentClassId . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create a user audit record
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_CLASS_STUDENT, $studentRecordId,
        AUDIT_ACTION_DELETE);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return SUCCESS;
}

// dbDeleteStudentRecord()
//   Deletes a student record
//
function dbDeleteStudentRecord($logId, PDO $db, $clientUserId, $studentRecordId)
{
    // Get the student record
    $studentRecord = dbGetStudentRecord($logId, $db, $studentRecordId);
    if ($studentRecord instanceof ErrorLDR)
    {
        $studentRecord->logError(__FUNCTION__);
        return $studentRecord;
    }

    // Delete Class Assignments
    $classCount = dbGetStudentClassCount($logId, $db, $studentRecordId);
    if ($classCount instanceof ErrorLDR) {
        $classCount->logError(__FUNCTION__);
        return $classCount;
    }

    // Delete Test Assignments
    // Check if this student has any test assignments
    $testAssignments = dbGetTestAssignments($logId, $db, null, $studentRecordId, null, null, null, null, null, null,
                                            null, null);
    if ($testAssignments instanceof ErrorLDR) {
        $testAssignments->logError(__FUNCTION__);
        return $testAssignments;
    }

    // Turn off autocommit
    $db->beginTransaction();

    if ($classCount > 0) {
        $sql = "UPDATE student_class SET deleted = 'Y' WHERE studentRecordId = '".$studentRecordId."';";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();

            if ($classCount != $stmt->rowCount()) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_UNABLE_TO_DELETE_CLASS_ASSIGNMENT
                , LDR_ED_UNABLE_TO_DELETE_CLASS_ASSIGNMENT.$studentRecordId);
                $errorLDR->logError(__FUNCTION__);
                $db->rollBack();
                return $errorLDR;
            }
        } catch (PDOException $error) {
            $db->rollBack();
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    if (count($testAssignments) > 0) {
        $deleteTestAssignmentStatus = dbDeleteTestAssignments($logId, $db, $testAssignments);
        if ($deleteTestAssignmentStatus instanceof ErrorLDR) {
            $db->rollBack();
            return $deleteTestAssignmentStatus;
        }

        // Check if all test assignments have been deleted
        if ($deleteTestAssignmentStatus->expectedTestAssignmentsCount != $deleteTestAssignmentStatus->successCount) {
            $db->rollBack();
            $errorLDR = new ErrorLDR($logId, LDR_EC_CANNOT_DELETE_TEST_ASSIGNMENTS
                                        , LDR_ED_CANNOT_DELETE_TEST_ASSIGNMENTS.$studentRecordId);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }

    // Delete the student_record
    $sql = "UPDATE student_record SET deleted = 'Y' WHERE studentRecordId = '" . $studentRecordId . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Delete the student
    $sql = "UPDATE student SET deleted = 'Y' WHERE studentId = '" . $studentRecord['studentId'] . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Decrement the organization student counts
    $result = dbAdjustStudentCounts($logId, $db, $studentRecord['enrollOrgId'], '- 1');
    if ($result instanceof ErrorLDR)
    {
        $db->rollBack();
        $result->logError(__FUNCTION__);
        return $result;
    }

    // Create a user audit record
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_STUDENT_RECORD, $studentRecordId,
                                     AUDIT_ACTION_DELETE);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return SUCCESS;
}

// dbDataExportStudents()
//  Build the SQL query to export students
//
function dbDataExportStudents($testStatus)
{
    $sql = "SELECT DISTINCT sr.studentRecordId 'Student Record ID', s.studentId 'Student ID', sr.enrollOrgId 'Org ID'
                , sr.stateIdentifier 'State Student ID', s.firstName 'First Name', s.middleName 'Middle Name'
                , s.lastName 'Last Name', sr.gradeLevel 'Grade Level', s.dateOfBirth 'Date Of Birth', s.gender 'Gender'
                , sr.raceAA, sr.raceAN, sr.raceAS, sr.raceHL, sr.raceHP, sr.raceWH, sr.statusDis, sr.statusECO
                , sr.statusELL, sr.statusGAT, sr.statusLEP, sr.statusMIG, sr.disabilityType 'Disability Type'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN student s ON s.studentId = sr.studentId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
            WHERE ta.deleted = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime;";
    return $sql;
}

// dbExportStudents()
//  Getting Data Export Data for the Students File
//
function dbExportStudents($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT DISTINCT sr.studentRecordId 'Student Record ID', s.studentId 'Student ID', sr.enrollOrgId 'Org ID'
                , sr.stateIdentifier 'State Student ID', s.firstName 'First Name', s.middleName 'Middle Name'
                , s.lastName 'Last Name', sr.gradeLevel 'Grade Level', s.dateOfBirth 'Date Of Birth', s.gender 'Gender'
                , sr.raceAA, sr.raceAN, sr.raceAS, sr.raceHL, sr.raceHP, sr.raceWH, sr.statusDis, sr.statusECO
                , sr.statusELL, sr.statusGAT, sr.statusLEP, sr.statusMIG, sr.disabilityType 'Disability Type'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN student s ON s.studentId = sr.studentId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
            WHERE ta.deleted = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_STUDENTS_EXPORT_DATA, LDR_ED_MISSING_STUDENTS_EXPORT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $studentsData = new stdClass();
        $studentsData->report = null;
        $studentsData->rowsCount = 0;

        $rawStudentsData = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $studentsData->report = $rawStudentsData;
        $studentsData->rowsCount = $stmt->rowCount();

        return $studentsData;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetClassStudentCount()
//   Returns a count of students assigned to a class
//
function dbGetClassStudentCount($logId, PDO $db, $classId)
{
    $sql = "SELECT COUNT(*) FROM student_class WHERE classId = '$classId' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetClassStudentRecordIds()
//   Returns an array of student record IDs for students assigned to one or more classes
//
function dbGetClassStudentRecordIds($logId, PDO $db, $classIds)
{
    $studentRecordIds = [];
    $classIds = implode(',', $classIds);
    $sql = "SELECT DISTINCT sr.studentRecordId
            FROM student_record sr
            JOIN student_class sc on sc.studentRecordId = sr.studentRecordId
            JOIN class c on c.classId = sc.classId
            WHERE sc.classId IN ($classIds)
            AND c.deleted = 'N'
            AND sc.deleted = 'N'
            AND sr.deleted = 'N'
            ORDER BY sr.studentRecordId";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM))
            $studentRecordIds[] = $row[0];
        return $studentRecordIds;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetClassStudents()
//   Returns an array of student records for students assigned to one or more classes
//
function dbGetClassStudents($logId, PDO $db, $classIds)
{
    $classIds = implode(',', $classIds);
    $sql = "SELECT DISTINCT sr.studentRecordId, sr.stateIdentifier, sr.localIdentifier, s.lastName,
                            s.firstName, s.middleName, s.dateOfBirth, s.gender, sr.enrollOrgId, sr.gradeLevel
            FROM student_record sr
            JOIN student_class sc on sc.studentRecordId = sr.studentRecordId
            JOIN class c on c.classId = sc.classId
            JOIN student s on s.studentId = sr.studentId
            WHERE sc.classId IN ($classIds)
            AND c.deleted = 'N'
            AND sc.deleted = 'N'
            AND sr.deleted = 'N'
            ORDER BY sr.studentRecordId";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $studentRecords = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $studentRecords;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudent_ByGUID()
//   Returns data for a student
//
function dbGetStudent_ByGUID($logId, $db, $globalUniqueIdentifier)
{
    // Get the student data
    $sql = "SELECT studentId, globalUniqueIdentifier, lastName, firstName, middleName, dateOfBirth, gender
            FROM student
            WHERE globalUniqueIdentifier = '$globalUniqueIdentifier'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $student = $stmt->fetch(PDO::FETCH_ASSOC);
            return $student;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The student was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_GUID_NOT_FOUND,
        'Student not found for GUID ' . $globalUniqueIdentifier);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetStudentClassCount()
//   Returns a count of classes a student is assigned to
//
function dbGetStudentClassCount($logId, PDO $db, $studentRecordId)
{
    $sql = "SELECT COUNT(*) FROM student_class WHERE studentRecordId = '$studentRecordId' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudentClassId()
//   Returns a studentClassId if found, 0 (zero) otherwise
//
function dbGetStudentClassId($logId, PDO $db, $studentRecordId, $classId)
{
    $sql = "SELECT studentClassId FROM student_class
            WHERE studentRecordId = '$studentRecordId'
            AND classId = '$classId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return 0; // Student class record not found
}

// dbGetStudentClassIds_ByClassId()
//   Returns an array of student_class IDs for all the student assigned to a class
//
function dbGetStudentClassIds_ByClassId($logId, PDO $db, $classId)
{
    $studentClassIds = [];

    $sql = "SELECT studentClassId FROM student_class
            WHERE classId = '$classId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM))
            $studentClassIds[] = $row[0];
        return $studentClassIds;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudentClassIds_ByOrganizationId()
//   Returns an array of student_class IDs for all the student assigned to classes in or under the organization
//
function dbGetStudentClassIds_ByOrganizationId($logId, PDO $db, $organizationId)
{
    $studentClassIds = [];

    // Prepare the query organization selection clause based on the organization type
    $orgClause = buildStudentOrgQueryClause($logId, $db, $organizationId, 'c.organizationId');
    if ($orgClause === false)
        return $studentClassIds;

    // Get the student_class IDs
    $sql = "SELECT sc.studentClassId FROM student_class sc
            JOIN class c on c.classId = sc.classId
            WHERE sc.deleted = 'N'
            AND c.deleted = 'N'";
    $sql .= $orgClause;
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM))
            $studentClassIds[] = $row[0];
        return $studentClassIds;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudentClasses()
//   Returns an array of the classes a student is assigned to
//
function dbGetStudentClasses($logId, PDO $db, $studentRecordId)
{
    $sql = "SELECT c.classId, c.organizationId, c.classIdentifier, c.sectionNumber, c.gradeLevel
            FROM class c
            JOIN student_class sc on sc.classId = c.classId
            WHERE sc.studentRecordId = '$studentRecordId'
            AND c.deleted = 'N'
            AND sc.deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $classes = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $classes;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudentRecord()
//   Returns a student record
//
function dbGetStudentRecord($logId, PDO $db, $studentRecordId, $optionalStateData = null)
{
    // Get the student record data
    $sql = "SELECT sr.studentRecordId, sr.studentId, sr.enrollOrgId, sr.stateOrgId, sr.stateIdentifier,
                   sr.localIdentifier, s.globalUniqueIdentifier, s.lastName, s.firstName, s.middleName,
                   s.dateOfBirth, s.gender";
    if ($optionalStateData == 'Y')
        $sql .= ", sr.optionalStateData1";
    $sql .= ", sr.gradeLevel, sr.raceAA, sr.raceAN, sr.raceAS, sr.raceHL, sr.raceHP, sr.raceWH, sr.statusDIS,
               sr.statusECO, sr.statusELL, sr.statusGAT, sr.statusLEP, sr.statusMIG, sr.disabilityType";
    if ($optionalStateData == 'Y')
        $sql .= ", sr.optionalStateData2, sr.optionalStateData3, sr.optionalStateData4, sr.optionalStateData5,
                   sr.optionalStateData6, sr.optionalStateData7, sr.optionalStateData8";
    $sql .= " FROM student_record sr
              JOIN student s on s.studentId = sr.studentId
             WHERE sr.studentRecordId = '$studentRecordId'
               AND sr.deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $studentRecord = $stmt->fetch(PDO::FETCH_ASSOC);
            return $studentRecord;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The student record was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_RECORD_ID_NOT_FOUND,
                             LDR_ED_STUDENT_RECORD_ID_NOT_FOUND . $studentRecordId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetStudentRecordCount()
//   Returns the count of students
//
function dbGetStudentRecordCount($logId, PDO $db, $organizationId, $gradeLevel, $gender, $raceAA, $raceAN, $raceAS,
                                 $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT,
                                 $statusLEP, $statusMIG, $disabilityType, $hasClasses)
{
    // Prepare the query organization selection clause based on the organization type
    $orgClause = buildStudentOrgQueryClause($logId, $db, $organizationId, 'enrollOrgId');
    if ($orgClause === false)
        return 0;

    // Create the base query
    $sql = "SELECT COUNT(*) FROM student_record sr";
    if (strlen($gender) > 0)
        $sql .= " JOIN student s on s.studentId = sr.studentId";
    $sql .= " WHERE sr.deleted = 'N'";
    $sql .= $orgClause;

    // Check if filtering student records based on the student having or not having any class assignments
    if (strlen($hasClasses) > 0)
    {
        if ($hasClasses == 'Y')
            $verb = 'IN';
        else
            $verb = 'NOT IN';
        $sql .= " AND studentRecordId " . $verb . " (SELECT studentRecordId FROM student_class WHERE deleted = 'N')";
    }

    // Append any optional constraints
    $sql .= buildStudentAttributesQueryClause('sr.', $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL, $raceHP,
        $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType);
    if (strlen($gender) > 0)
        $sql .= " AND s.gender = '$gender'";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudentRecordId_ByStateOrgId_ByStateIdentifier()
//   Returns a studentRecordId if found, 0 (zero) otherwise
//
function dbGetStudentRecordId_ByStateOrgId_ByStateIdentifier($logId, $db, $stateOrgId, $stateIdentifier)
{
    $sql = "SELECT studentRecordId FROM student_record
            WHERE stateOrgId = '$stateOrgId'
            AND stateIdentifier = '$stateIdentifier'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return 0; // Student record not found
}

// dbGetStudentRecordId_ByStateCode_ByStateIdentifier()
//   Returns a studentRecordId corresponding to the state cord and state identifier
//
function dbGetStudentsRecordId_ByStateOrgId_ByStateIdentifier($logId, $db, $stateOrgIdStateIdentifier)
{
    $sql = "SELECT
                CONCAT_WS('".FIELD_SEPARATOR."', stateOrgId, stateIdentifier) AS 'StateOrgId_StateIdentifier'
                , studentRecordId
            FROM student_record
            WHERE CONCAT_WS('".FIELD_SEPARATOR."', stateOrgId, stateIdentifier)
                    IN ('".implode("','", $stateOrgIdStateIdentifier)."')
                AND deleted = 'N';";
    try{
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        return $data;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudentRecordIds()
//   Returns an array of student record IDs for students enrolled in or under an organization
//
function dbGetStudentRecordIds($logId, PDO $db, $organizationId)
{
    $studentRecordIds = [];

    // Prepare the query organization selection clause based on the organization type
    $orgClause = buildStudentOrgQueryClause($logId, $db, $organizationId, 'enrollOrgId');
    if ($orgClause === false)
        return $studentRecordIds;

    // Get the student record IDs
    $sql = "SELECT studentRecordId FROM student_record WHERE deleted = 'N'";
    $sql .= $orgClause;
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM))
            $studentRecordIds[] = $row[0];
        return $studentRecordIds;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetStudentRecords()
//   Returns an array of student records for students enrolled in or under an organization
//
function dbGetStudentRecords($logId, PDO $db, $organizationId, $gradeLevel, $gender, $raceAA, $raceAN, $raceAS,
    $raceHL, $raceHP, $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType,
    $pageOffset, $pageLimit, $hasClasses, $withClasses)
{
    // Prepare the query organization selection clause based on the organization type
    $orgClause = buildStudentOrgQueryClause($logId, $db, $organizationId, 'sr.enrollOrgId');
    if ($orgClause === false)
        return [];

    // Create the base query
    $sql = "SELECT sr.studentRecordId, sr.stateIdentifier, sr.localIdentifier, s.lastName, s.firstName,
                   s.middleName, s.dateOfBirth, s.gender, sr.enrollOrgId, sr.gradeLevel
            FROM student_record sr
            JOIN student s on s.studentId = sr.studentId
            WHERE sr.deleted = 'N'";
    $sql .= $orgClause;

    // Check if filtering student records based on the student having or not having any class assignments
    if (strlen($hasClasses) > 0)
    {
        if ($hasClasses == 'Y')
            $verb = 'IN';
        else
            $verb = 'NOT IN';
        $sql .= " AND studentRecordId " . $verb . " (SELECT studentRecordId FROM student_class WHERE deleted = 'N')";
    }

    // Append any optional constraints
    $sql .= buildStudentAttributesQueryClause('sr.', $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL, $raceHP,
        $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType);
    if (strlen($gender) > 0)
        $sql .= " AND s.gender = '$gender'";

    // Check if the output is limited to a page of data
    if (strlen($pageOffset) > 0 && strlen($pageLimit) > 0)
        $sql .= " LIMIT " . $pageOffset . ', ' . $pageLimit;

    // Get the student records
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $studentRecords = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Check if each student's assigned classes should be included
    if (strlen($withClasses) > 0 && $withClasses == 'Y')
    {
        for ($idx = 0; $idx < sizeof($studentRecords); $idx++)
        {
            // Get the student's classes
            $classes = dbGetStudentClasses($logId, $db, $studentRecords[$idx]['studentRecordId']);
            if ($classes instanceof ErrorLDR)
            {
                $classes->logError(__FUNCTION__);
                return $classes;
            }
            $studentRecords[$idx]['classes'] = $classes;
        }
    }

    return $studentRecords;
}

// UNVETTED FUNCTIONS

// dbGetStudentId_By_OrganizationId_StudentIdentifier()
//   Returns a Student ID if found, 0 (zero) otherwise
//
function dbGetStudentId_By_OrganizationId_StudentIdentifier($logId, $db, $organizationId, $studentIdentifier)
{
    // Prepare the query
    $sql = "SELECT studentId FROM student
            WHERE organizationId = '$organizationId'
            AND studentIdentifier = '$studentIdentifier'";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return 0; // Student record not found
}

// dbGetStudentIdentifier_By_StudentId()
//   Returns a student identifier if found, null otherwise
//
function dbGetStudentIdentifier_By_StudentId($logId, $db, $studentId)
{
    $sql = "SELECT studentIdentifier FROM student WHERE studentId = '$studentId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null; // Student record not found
}

// dbUpdateStudentRecord()
//   Updates a student record
//
function dbUpdateStudentRecord($logId, PDO $db, $clientUserId, $studentRecordId, $stateIdentifier, $localIdentifier,
    $lastName, $firstName, $middleName, $dateOfBirth, $gender, $gradeLevel, $raceAA, $raceAN, $raceAS, $raceHL, $raceHP,
    $raceWH, $statusDIS, $statusECO, $statusELL, $statusGAT, $statusLEP, $statusMIG, $disabilityType)
{
    global $selectedDatabase;

    // Get the student record
    $studentRecord = dbGetStudentRecord($logId, $db, $studentRecordId);
    if ($studentRecord instanceof ErrorLDR)
    {
        $studentRecord->logError(__FUNCTION__);
        return $studentRecord;
    }

    // Determine what and if changes are being made
    $dataChanges = [];
    $studentChanges = false;
    $studentRecordChanges = false;
    $adpStudentDataChanges = false;
    $sqlStudent = "UPDATE student SET ";
    $sqlStudentRecord = "UPDATE student_record SET ";
    $adpStudentData = ['studentId' => $studentRecordId];
    if (strlen($stateIdentifier) > 0 && $stateIdentifier != $studentRecord['stateIdentifier'])
    {
        $dataChanges['stateIdentifier'] = $stateIdentifier;
        $sqlStudentRecord .= "stateIdentifier = '$stateIdentifier', ";
        $adpStudentData['personalId'] = $stateIdentifier;
        $adpStudentDataChanges = true;
        $studentRecordChanges = true;
    }
    if (strlen($localIdentifier) > 0 && $localIdentifier != $studentRecord['localIdentifier'])
    {
        $dataChanges['localIdentifier'] = $localIdentifier;
        $sqlStudentRecord .= "localIdentifier = '$localIdentifier', ";
        $studentRecordChanges = true;
    }
    if (strlen($lastName) > 0 && $lastName != $studentRecord['lastName'])
    {
        $dataChanges['lastName'] = $lastName;
        $adpStudentData['lastName'] = $lastName;
        $dbLastName = dbPrepareForSQL($lastName, 'lastName');
        $sqlStudent .= "lastName = '$dbLastName', ";
        $adpStudentDataChanges = true;
        $studentChanges = true;
    }
    if (strlen($firstName) > 0 && $firstName != $studentRecord['firstName'])
    {
        $dataChanges['firstName'] = $firstName;
        $adpStudentData['firstName'] = $firstName;
        $dbFirstName = dbPrepareForSQL($firstName, 'firstName');
        $sqlStudent .= "firstName = '$dbFirstName', ";
        $adpStudentDataChanges = true;
        $studentChanges = true;
    }
    if (strlen($middleName) > 0 && $middleName != $studentRecord['middleName'])
    {
        $dataChanges['middleName'] = $middleName;
        $dbMiddleName = dbPrepareForSQL($middleName, 'middleName');
        $sqlStudent .= "middleName = '$dbMiddleName', ";
        $studentChanges = true;
    }
    if (strlen($dateOfBirth) > 0 && $dateOfBirth != $studentRecord['dateOfBirth'])
    {
        $dataChanges['dateOfBirth'] = $dateOfBirth;
        $adpStudentData['dateOfBirth'] = $dateOfBirth;
        $sqlStudent .= "dateOfBirth = '$dateOfBirth', ";
        $adpStudentDataChanges = true;
        $studentChanges = true;
    }
    if (strlen($gender) > 0 && $gender != $studentRecord['gender'])
    {
        $dataChanges['gender'] = $gender;
        $sqlStudent .= "gender = '$gender', ";
        $studentChanges = true;
    }
    if (strlen($gradeLevel) > 0 && $gradeLevel != $studentRecord['gradeLevel'])
    {
        $dataChanges['gradeLevel'] = $gradeLevel;
        $sqlStudentRecord .= "gradeLevel = '$gradeLevel', ";
        $adpStudentData['grade'] = $gradeLevel;
        $adpStudentDataChanges = true;
        $studentRecordChanges = true;
    }
    if (strlen($raceAA) > 0 && $raceAA != $studentRecord['raceAA'])
    {
        $dataChanges['raceAA'] = $raceAA;
        $sqlStudentRecord .= "raceAA = '$raceAA', ";
        $studentRecordChanges = true;
    }
    if (strlen($raceAN) > 0 && $raceAN != $studentRecord['raceAN'])
    {
        $dataChanges['raceAN'] = $raceAN;
        $sqlStudentRecord .= "raceAN = '$raceAN', ";
        $studentRecordChanges = true;
    }
    if (strlen($raceAS) > 0 && $raceAS != $studentRecord['raceAS'])
    {
        $dataChanges['raceAS'] = $raceAS;
        $sqlStudentRecord .= "raceAS = '$raceAS', ";
        $studentRecordChanges = true;
    }
    if (strlen($raceHL) > 0 && $raceHL != $studentRecord['raceHL'])
    {
        $dataChanges['raceHL'] = $raceHL;
        $sqlStudentRecord .= "raceHL = '$raceHL', ";
        $studentRecordChanges = true;
    }
    if (strlen($raceHP) > 0 && $raceHP != $studentRecord['raceHP'])
    {
        $dataChanges['raceHP'] = $raceHP;
        $sqlStudentRecord .= "raceHP = '$raceHP', ";
        $studentRecordChanges = true;
    }
    if (strlen($raceWH) > 0 && $raceWH != $studentRecord['raceWH'])
    {
        $dataChanges['raceWH'] = $raceWH;
        $sqlStudentRecord .= "raceWH = '$raceWH', ";
        $studentRecordChanges = true;
    }
    if (strlen($statusDIS) > 0 && $statusDIS != $studentRecord['statusDIS'])
    {
        $dataChanges['statusDIS'] = $statusDIS;
        $sqlStudentRecord .= "statusDIS = '$statusDIS', ";
        $studentRecordChanges = true;
    }
    if (strlen($statusECO) > 0 && $statusECO != $studentRecord['statusECO'])
    {
        $dataChanges['statusECO'] = $statusECO;
        $sqlStudentRecord .= "statusECO = '$statusECO', ";
        $studentRecordChanges = true;
    }
    if (strlen($statusELL) > 0 && $statusELL != $studentRecord['statusELL'])
    {
        $dataChanges['statusELL'] = $statusELL;
        $sqlStudentRecord .= "statusELL = '$statusELL', ";
        $studentRecordChanges = true;
    }
    if (strlen($statusGAT) > 0 && $statusGAT != $studentRecord['statusGAT'])
    {
        $dataChanges['statusGAT'] = $statusGAT;
        $sqlStudentRecord .= "statusGAT = '$statusGAT', ";
        $studentRecordChanges = true;
    }
    if (strlen($statusLEP) > 0 && $statusLEP != $studentRecord['statusLEP'])
    {
        $dataChanges['statusLEP'] = $statusLEP;
        $sqlStudentRecord .= "statusLEP = '$statusLEP', ";
        $studentRecordChanges = true;
    }
    if (strlen($statusMIG) > 0 && $statusMIG != $studentRecord['statusMIG'])
    {
        $dataChanges['statusMIG'] = $statusMIG;
        $sqlStudentRecord .= "statusMIG = '$statusMIG', ";
        $studentRecordChanges = true;
    }
    if (strlen($disabilityType) > 0 && $disabilityType != $studentRecord['disabilityType'])
    {
        if ($disabilityType == DELETE_THIS_VALUE)
            $disabilityType = '';
        $dataChanges['disabilityType'] = $disabilityType;
        $sqlStudentRecord .= "disabilityType = '$disabilityType', ";
        $studentRecordChanges = true;
    }

    if (sizeof($dataChanges) < 1)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_UPDATE_HAS_NO_CHANGES, LDR_ED_UPDATE_HAS_NO_CHANGES);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    if ($studentChanges)
    {
        $sqlStudent = substr($sqlStudent, 0, -2); // Strip off the trailing ", "
        $sqlStudent .= " WHERE studentId = '" . $studentRecord['studentId'] . "'";
    }
    if ($studentRecordChanges)
    {
        $sqlStudentRecord = substr($sqlStudentRecord, 0, -2); // Strip off the trailing ", "
        $sqlStudentRecord .= " WHERE studentRecordId = '" . $studentRecordId . "'";
    }

    // Turn off autocommit
    $db->beginTransaction();

    // Update the student
    if ($studentChanges)
    {
        try {
            $stmt = $db->prepare($sqlStudent);
            $stmt->execute();
        }
        catch(PDOException $error) {
            $db->rollBack();
            return errorQueryException($logId, $error, __FUNCTION__, $sqlStudent);
        }
    }

    // Update the student_record
    if ($studentRecordChanges)
    {
        try {
            $stmt = $db->prepare($sqlStudentRecord);
            $stmt->execute();
        }
        catch(PDOException $error) {
            $db->rollBack();
            return errorQueryException($logId, $error, __FUNCTION__, $sqlStudentRecord);
        }
    }

    // Create a user audit record
    $dataChanges = json_encode($dataChanges);
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_STUDENT_RECORD, $studentRecordId,
        AUDIT_ACTION_UPDATE, $dataChanges);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Check if ADP needs to be updated
    if ($adpStudentDataChanges)
    {
        if (SKIP_ADP_COMM == false)
        {
            // Check if the student has any test assignments
            $testAssignments = dbGetTestAssignments($logId, $db, null, $studentRecordId, null, null, null, null, null,
                                                    null, null, null);
            if ($testAssignments instanceof ErrorLDR)
            {
                $db->rollBack();
                $testAssignments->logError(__FUNCTION__);
                return $testAssignments;
            }
            if (sizeof($testAssignments) > 0)
            {
                // Update one of the test assignments in ADP - it does not matter which one
                $result = adpUpdateTestAssignment($logId, $testAssignments[0], $adpStudentData, null, null, null);
                if ($result instanceof ErrorLDR)
                {
                    $db->rollBack();
                    $result->logError(__FUNCTION__);
                    return $result;
                }
            }
        }
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return SUCCESS;
}

/**
 * Validates student record. It does the following validation
 * 	1. Built-in LDR validation for each input value
 * 	2. either enrollOrgId or StateCode + OrgIdentifier is available
 * 	3. Validates student's date of birth
 * 	4. Validates student's grade level
 * 	5. Validates student's disability
 * 	6. Checks for missing student's state identifier
 * @param  [type] $logId         [description]
 * @param  [type] $studentRecord [description]
 * @return [type]                [description]
 */
function validateStudentRecord($logId, $db, $studentRecord)
{
    global $argFilterMap;

    $student = [];

    $expectedStudentRecordArgs = [
        // organization identifiers/IDs
        'enrollStateCode' => DCO_OPTIONAL | DCO_TOUPPER,
        'enrollSchoolIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,
        'enrollOrgId' => DCO_OPTIONAL | DCO_MUST_EXIST,

        // student / student_record identifiers
        'stateIdentifier' => DCO_REQUIRED | DCO_TOUPPER,
        'localIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,

        // student attributes
        'lastName' => DCO_REQUIRED,
        'firstName' => DCO_REQUIRED,
        'middleName' => DCO_OPTIONAL,
        'dateOfBirth' => DCO_REQUIRED,
        'gender' => DCO_REQUIRED | DCO_TOUPPER,

        // student_record attributes
        'optionalStateData1' => DCO_OPTIONAL,
        'gradeLevel' => DCO_REQUIRED | DCO_TOUPPER,
        'raceAA' => DCO_OPTIONAL | DCO_TOUPPER,
        'raceAN' => DCO_OPTIONAL | DCO_TOUPPER,
        'raceAS' => DCO_OPTIONAL | DCO_TOUPPER,
        'raceHL' => DCO_OPTIONAL | DCO_TOUPPER,
        'raceHP' => DCO_OPTIONAL | DCO_TOUPPER,
        'raceWH' => DCO_OPTIONAL | DCO_TOUPPER,
        'statusDIS' => DCO_OPTIONAL | DCO_TOUPPER,
        'statusECO' => DCO_OPTIONAL | DCO_TOUPPER,
        'statusELL' => DCO_OPTIONAL | DCO_TOUPPER,
        'statusGAT' => DCO_OPTIONAL | DCO_TOUPPER,
        'statusLEP' => DCO_OPTIONAL | DCO_TOUPPER,
        'statusMIG' => DCO_OPTIONAL | DCO_TOUPPER,
        'disabilityType' => DCO_OPTIONAL | DCO_TOUPPER,
        'optionalStateData2' => DCO_OPTIONAL,
        'optionalStateData3' => DCO_OPTIONAL,
        'optionalStateData4' => DCO_OPTIONAL,
        'optionalStateData5' => DCO_OPTIONAL,
        'optionalStateData6' => DCO_OPTIONAL,
        'optionalStateData7' => DCO_OPTIONAL,
        'optionalStateData8' => DCO_OPTIONAL,

        // class assignments
        'classAssignment1' => DCO_OPTIONAL,
        'classAssignment2' => DCO_OPTIONAL,
        'classAssignment3' => DCO_OPTIONAL,
        'classAssignment4' => DCO_OPTIONAL,
        'classAssignment5' => DCO_OPTIONAL,
        'classAssignment6' => DCO_OPTIONAL,
        'classAssignment7' => DCO_OPTIONAL,
        'classAssignment8' => DCO_OPTIONAL,
        'classAssignment9' => DCO_OPTIONAL,
        'classAssignment10' => DCO_OPTIONAL,
    ];

    foreach ($expectedStudentRecordArgs as $argName => $argDco) {
        $argFilterName = $argName;
        // Check if this arg is in an arg filter group
        if (strncmp($argName, 'classAssignment', strlen('classAssignment')) == 0)
            $argFilterName = 'classAssignmentX';
        else if (strncmp($argName, 'optionalStateData', strlen('optionalStateData')) == 0 &&
                 $argName != 'optionalStateData')
        {
            $argFilterName = 'optionalStateDataX';
        }
        $argValue = validateArg($logId, $db, $studentRecord, $argName, $argFilterMap[$argFilterName], $argDco);
        if ($argValue instanceof ErrorLDR) {
            return $argValue;
        }
        $student[$argName] = $argValue;
    }

    if (strlen($student['enrollOrgId']) === 0) {
        if (strlen($student['enrollStateCode']) === 0 || strlen($student['enrollSchoolIdentifier']) === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED,
                                     'One or more enrollment organization values not provided');
            return $errorLDR;
        }
    }

    // Validate the dateOfBirth
    $dateOfBirth = validateDate($logId, $student['dateOfBirth'], 'dateOfBirth');
    if ($dateOfBirth instanceof ErrorLDR) {
        return $dateOfBirth;
    }

    // Validate the gradeLevel
    $gradeLevel = validateGradeLevel($logId, $student['gradeLevel']);
    if ($gradeLevel instanceof ErrorLDR === false) {
        $student['gradeLevel'] = $gradeLevel;
    } else {
        return $gradeLevel;
    }

    // Validate the disabilityType
    $disabilityType = $student['disabilityType'];
    if (strlen($disabilityType) > 0) {
        $disabilityType = validateDisabilityType($logId, $disabilityType);
        if ($disabilityType instanceof ErrorLDR === false) {
            $student['disabilityType'] = $disabilityType;
        } else {
            return $disabilityType;
        }
    }

    // Check that the statusDIS / disabilityType values are consistent
    if ($student['statusDIS'] == 'Y' && strlen($disabilityType) == 0) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DISABILITY_ARGS_ARE_INCONSISTENT,
                                 'statusDIS argument is Y but disabilityType argument is missing');
        return $errorLDR;
    }
    if ($student['statusDIS'] != 'Y' && strlen($disabilityType) > 0) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DISABILITY_ARGS_ARE_INCONSISTENT, 'disabilityType argument is ' .
                                 $disabilityType . ' but statusDIS argument is not Y');
        return $errorLDR;
    }

    $enrollOrgId = (isset($student['enrollOrgId']) ? $student['enrollOrgId'] : 0);
    $stateCodeOrgIdentifier = (isset($student['enrollStateCode']) ? $student['enrollStateCode'].FIELD_SEPARATOR.$student['enrollSchoolIdentifier'] : 0);

    // Validate the class assignments
    $classAssignments =
    [
        $student['classAssignment1'], $student['classAssignment2'], $student['classAssignment3']
        , $student['classAssignment4'], $student['classAssignment5'], $student['classAssignment6']
        , $student['classAssignment7'], $student['classAssignment8'], $student['classAssignment9']
        , $student['classAssignment10']
    ];

    return [$student, $student['enrollOrgId'], $stateCodeOrgIdentifier, $classAssignments];
}
