<?php
//
// LDR test battery services
//
// createTestBatteryForm()          create_test_battery_form.php
// getTestBatteries()               get_test_batteries.php
// getTestBattery()                 get_test_battery.php
// getTestBatteryForms()            get_test_battery_forms.php
// getTestBatterySubjects()
// updateTestBattery()
// updateTestBatteryForm()          update_test_battery_form.php
//
// LDR test battery support functions
//
// dbCreateTestBattery()
// dbCreateTestBatteryForm()
// dbGetTestBatteries()
// dbGetTestBattery()
// dbGetTestBatteryForm()
// dbGetTestBatteryFormCount()
// dbGetTestBatteryFormId_ByName()
// dbGetTestBatteryForms()
// dbGetTestBatteryId_ByFormId()
// dbGetTestBatteryId_ByNameSubjectGrade()
// dbGetTestBatterySubjects()
// dbUpdateTestBattery()
// dbUpdateTestBatteryForm()

//
// LDR test battery services
//
// createTestBatteryForm()
//   Creates a test battery form
//
function createTestBatteryForm()
{
    publishTestBatteryForm(AUDIT_ACTION_CREATE);
}

// publishTestBatteryForm()
//   Creates or updates a test battery form
//
function publishTestBatteryForm($actionType)
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'testBatteryId' => DCO_REQUIRED,
            'testBatteryName' => DCO_OPTIONAL | DCO_URL_DECODE,
            'testBatterySubject' => DCO_OPTIONAL,
            'testBatteryGrade' => DCO_OPTIONAL,
            'testBatteryProgram' => DCO_OPTIONAL,
            'testBatterySecurity' => DCO_OPTIONAL,
            'testBatteryPermissions' => DCO_OPTIONAL,
            'testBatteryScoring' => DCO_OPTIONAL,
            'testBatteryDescription' => DCO_OPTIONAL | DCO_URL_DECODE,
            'testBatteryFormName' => DCO_REQUIRED | DCO_URL_DECODE,
            'testBatteryFormId' => DCO_REQUIRED
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testBatteryId, $testBatteryName, $testBatterySubject, $testBatteryGrade, $testBatteryProgram,
         $testBatterySecurity, $testBatteryPermissions, $testBatteryScoring, $testBatteryDescription,
         $testBatteryFormName, $testBatteryFormId) = $args;

    // Check if the test battery already exists
    $testBatteryExists = dbTableValueExists($logId, $db, 'test_battery', 'testBatteryId', $testBatteryId);
    if ($testBatteryExists instanceof ErrorLDR)
    {
        $testBatteryExists->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check if the test battery form already exists
    // If the test battery form does not exist then unconditionally switch the action to CREATE
    $testBatteryFormExists = dbTableValueExists($logId, $db, 'test_battery_form', 'testBatteryFormId', $testBatteryFormId);
    if ($testBatteryFormExists instanceof ErrorLDR)
    {
        $testBatteryFormExists->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    if (!$testBatteryFormExists)
        $actionType = AUDIT_ACTION_CREATE;

    // Sanity-check the requested action
    if ($actionType == AUDIT_ACTION_CREATE)
    {
        // If a test battery form is being created then it cannot have a testBatteryFormId value that already exists
        if ($testBatteryFormExists)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_FORM_ID_CONFLICT,
                                     LDR_ED_TEST_BATTERY_FORM_ID_CONFLICT . $testBatteryFormId);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // If a test battery form is being created then it cannot have a testBatteryFormName value that already exists
        // within the same test battery
        $foundTestBatteryFormId = dbGetTestBatteryFormId_ByName($logId, $db, $testBatteryFormName, $testBatteryId);
        if ($foundTestBatteryFormId !== null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_FORM_DUP, 'testBatteryFormName = |' .
                $testBatteryFormName . '| already exists for testBatteryId ' . $testBatteryId);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    elseif ($actionType == AUDIT_ACTION_UPDATE)
    {
        // If a test battery form is being updated then it must already belong to the specified test battery
        $foundTestBatteryId = dbGetTestBatteryId_ByFormId($logId, $db, $testBatteryFormId);
        if ($testBatteryId != $foundTestBatteryId)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_ID_MISMATCH, 'testBatteryId argument ' .
                                     $testBatteryId . ' does not match existing testBatteryId ' .
                                     $foundTestBatteryId . ' for testBatteryFormId ' . $testBatteryFormId);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // If a test battery form is being updated then it cannot have a testBatteryFormName value that already exists
        // for a different form within the same test battery
        $foundTestBatteryFormId = dbGetTestBatteryFormId_ByName($logId, $db, $testBatteryFormName, $testBatteryId);
        if ($foundTestBatteryFormId !== null && $foundTestBatteryFormId != $testBatteryFormId)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_FORM_DUP, 'testBatteryFormName = |' .
                $testBatteryFormName . '| already exists for testBatteryId ' . $testBatteryId);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Check the testBatteryName
    if (strlen($testBatteryName) == 0 && !$testBatteryExists)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED, 'Required value testBatteryName not provided');
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatterySubject
    $testBatterySubjectId = checkReferenceValueArg($logId, $db, $testBatterySubject, 'testBatterySubject',
                                                   'test_battery_subject', !$testBatteryExists);
    if ($testBatterySubjectId instanceof ErrorLDR)
    {
        $testBatterySubjectId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryGrade
    $testBatteryGradeId = checkReferenceValueArg($logId, $db, $testBatteryGrade, 'testBatteryGrade',
                                                 'test_battery_grade', !$testBatteryExists);
    if ($testBatteryGradeId instanceof ErrorLDR)
    {
        $testBatteryGradeId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryProgram
    $testBatteryProgramId = checkReferenceValueArg($logId, $db, $testBatteryProgram, 'testBatteryProgram',
                                                   'test_battery_program', !$testBatteryExists);
    if ($testBatteryProgramId instanceof ErrorLDR)
    {
        $testBatteryProgramId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatterySecurity
    $testBatterySecurityId = checkReferenceValueArg($logId, $db, $testBatterySecurity, 'testBatterySecurity',
                                                    'test_battery_security', !$testBatteryExists);
    if ($testBatterySecurityId instanceof ErrorLDR)
    {
        $testBatterySecurityId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryPermissions
    $testBatteryPermissionsId = checkReferenceValueArg($logId, $db, $testBatteryPermissions, 'testBatteryPermissions',
                                                       'test_battery_permissions', !$testBatteryExists);
    if ($testBatteryPermissionsId instanceof ErrorLDR)
    {
        $testBatteryPermissionsId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryScoring
    $testBatteryScoringId = checkReferenceValueArg($logId, $db, $testBatteryScoring, 'testBatteryScoring',
                                                   'test_battery_scoring', !$testBatteryExists);
    if ($testBatteryScoringId instanceof ErrorLDR)
    {
        $testBatteryScoringId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Enforce testBattery testBatteryName/testBatterySubject/testBatteryGrade uniqueness
    if (!$testBatteryExists)
    {
        // If creating a test battery then check that the testBatteryName/testBatterySubject/testBatteryGrade
        // combination does not already exist
        $foundTestBatteryId = dbGetTestBatteryId_ByNameSubjectGrade($logId, $db, $testBatteryName,
                                                                    $testBatterySubject, $testBatteryGrade);
        if ($foundTestBatteryId !== null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_DUP, 'testBatteryName = |' . $testBatteryName .
                '|, testBatterySubject = |' . $testBatterySubject . '|, testBatteryGrade = |' . $testBatteryGrade .
                '| already exists for testBatteryId ' . $foundTestBatteryId);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    else
    {
        // If updating a test battery then check that the update will not create a
        // testBatteryName/testBatterySubject/testBatteryGrade combination that already exists
        // for a different test battery
        //
        // Get any values that were not passed
        $testBatteryNameCheck = $testBatteryName;
        $testBatteryGradeCheck = $testBatteryGrade;
        $testBatterySubjectCheck = $testBatterySubject;
        if (strlen($testBatteryName) == 0 || strlen($testBatterySubject) == 0 || strlen($testBatteryGrade) == 0)
        {
            // Get the test battery
            $testBattery = dbGetTestBattery($logId, $db, $testBatteryId);
            if ($testBattery instanceof ErrorLDR)
            {
                $testBattery->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }
            if (strlen($testBatteryName) == 0)
                $testBatteryNameCheck = $testBattery['testBatteryName'];
            if (strlen($testBatteryGrade) == 0)
                $testBatteryGradeCheck = $testBattery['testBatteryGrade'];
            if (strlen($testBatterySubject) == 0)
                $testBatterySubjectCheck = $testBattery['testBatterySubject'];
        }
        //
        // Check if the value(s) to be updated will cause a match with a different test battery
        $foundTestBatteryId = dbGetTestBatteryId_ByNameSubjectGrade($logId, $db, $testBatteryNameCheck,
                                                                    $testBatterySubjectCheck, $testBatteryGradeCheck);
        if ($foundTestBatteryId !== null && $foundTestBatteryId != $testBatteryId)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_DUP, 'testBatteryName = |' . $testBatteryNameCheck .
                '|, testBatterySubject = |' . $testBatterySubjectCheck . '|, testBatteryGrade = |' .
                $testBatteryGradeCheck . '| already exists for testBatteryId ' . $foundTestBatteryId);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Turn off autocommit
    $db->beginTransaction();
    $changesMade = false;

    // Create or update the test battery
    if (!$testBatteryExists)
    {
        $result = dbCreateTestBattery($logId, $db, $testBatteryId, $testBatteryName, $testBatterySubjectId,
                                      $testBatteryGradeId, $testBatteryProgramId, $testBatterySecurityId,
                                      $testBatteryPermissionsId, $testBatteryScoringId, $testBatteryDescription);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db->rollBack();
            $db = null;
            return;
        }
        $changesMade = true;
    }
    else
    {
        $result = dbUpdateTestBattery($logId, $db, $testBatteryId, $testBatteryName, $testBatterySubjectId,
                                      $testBatteryGradeId, $testBatteryProgramId, $testBatterySecurityId,
                                      $testBatteryPermissionsId, $testBatteryScoringId, $testBatteryDescription);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db->rollBack();
            $db = null;
            return;
        }
        if ($result === true)
            $changesMade = true;
    }

    // Create or update the test battery form
    if (!$testBatteryFormExists)
    {
        $result = dbCreateTestBatteryForm($logId, $db, $testBatteryId, $testBatteryFormId, $testBatteryFormName);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db->rollBack();
            $db = null;
            return;
        }
        $changesMade = true;
    }
    else
    {
        $result = dbUpdateTestBatteryForm($logId, $db, $testBatteryFormId, $testBatteryFormName);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db->rollBack();
            $db = null;
            return;
        }
        if ($result === true)
            $changesMade = true;
    }

    // If changes were made then commit them, otherwise just turn autocommit back on
    if ($changesMade)
        $db->commit();
    else
        $db->rollBack();

    // Return the result
    finishService($db, $logId, ['result' => SUCCESS], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getTestBatteries()
//   Returns zero or more test batteries
//
function getTestBatteries()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'testBatterySubject' => DCO_OPTIONAL,
            'testBatteryGrade' => DCO_OPTIONAL,
            'testBatteryProgram' => DCO_OPTIONAL,
            'testBatterySecurity' => DCO_OPTIONAL,
            'testBatteryPermissions' => DCO_REQUIRED,
            'testBatteryScoring' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testBatterySubject, $testBatteryGrade, $testBatteryProgram, $testBatterySecurity,
         $testBatteryPermissions, $testBatteryScoring) = $args;

    // Get the test batteries
    $testBatteries = dbGetTestBatteries($logId, $db, $testBatterySubject, $testBatteryGrade, $testBatteryProgram,
                                        $testBatterySecurity, $testBatteryPermissions, $testBatteryScoring);
    if ($testBatteries instanceof ErrorLDR)
    {
        $testBatteries->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the form counts
    for ($idx = 0; $idx < sizeof($testBatteries); $idx++)
    {
        $formCount = dbGetTestBatteryFormCount($logId, $db, $testBatteries[$idx]['testBatteryId']);
        if ($formCount instanceof ErrorLDR)
        {
            $formCount->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        $testBatteries[$idx]['formCount'] = $formCount;
    }

    // Return the test batteries
    finishService($db, $logId, $testBatteries, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getTestBattery()
//   Returns a test battery
//
function getTestBattery($testBatteryId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the testBatteryId
    $argName = 'testBatteryId';
    $name = validateArg($logId, $db, [$argName => $testBatteryId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test battery
    $testBattery = dbGetTestBattery($logId, $db, $testBatteryId);
    if ($testBattery instanceof ErrorLDR)
    {
        $testBattery->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the form count
    $formCount = dbGetTestBatteryFormCount($logId, $db, $testBatteryId);
    if ($formCount instanceof ErrorLDR)
    {
        $formCount->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $testBattery['formCount'] = $formCount;

    // Return the test battery
    finishService($db, $logId, $testBattery, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getTestBatteryForms()
//   Returns the forms for a test battery
//
function getTestBatteryForms($testBatteryId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the testBatteryId
    $argName = 'testBatteryId';
    $name = validateArg($logId, $db, [$argName => $testBatteryId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test battery forms
    $testBatteryForms = dbGetTestBatteryForms($logId, $db, $testBatteryId);
    if ($testBatteryForms instanceof ErrorLDR)
    {
        $testBatteryForms->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the test battery forms
    finishService($db, $logId, $testBatteryForms, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getTestBatterySubjects()
//   Returns zero or more test battery subjects
//
function getTestBatterySubjects()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'testBatteryGrade' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testBatteryGrade) = $args;

    // Get the test battery subjects
    $testBatterySubjects = dbGetTestBatterySubjects($logId, $db, $testBatteryGrade);
    if ($testBatterySubjects instanceof ErrorLDR)
    {
        $testBatterySubjects->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the test battery subjects
    finishService($db, $logId, $testBatterySubjects, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// updateTestBattery()
//   Updates a test battery
//
function updateTestBattery()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'testBatteryId' => DCO_REQUIRED,
            'testBatteryName' => DCO_OPTIONAL | DCO_URL_DECODE,
            'testBatterySubject' => DCO_OPTIONAL,
            'testBatteryGrade' => DCO_OPTIONAL,
            'testBatteryProgram' => DCO_OPTIONAL,
            'testBatterySecurity' => DCO_OPTIONAL,
            'testBatteryPermissions' => DCO_OPTIONAL,
            'testBatteryScoring' => DCO_OPTIONAL,
            'testBatteryDescription' => DCO_OPTIONAL | DCO_URL_DECODE
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testBatteryId, $testBatteryName, $testBatterySubject, $testBatteryGrade, $testBatteryProgram,
        $testBatterySecurity, $testBatteryPermissions, $testBatteryScoring, $testBatteryDescription) = $args;

    // Check the testBatterySubject
    $testBatterySubjectId = checkReferenceValueArg($logId, $db, $testBatterySubject, 'testBatterySubject',
        'test_battery_subject', false);
    if ($testBatterySubjectId instanceof ErrorLDR)
    {
        $testBatterySubjectId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryGrade
    $testBatteryGradeId = checkReferenceValueArg($logId, $db, $testBatteryGrade, 'testBatteryGrade',
        'test_battery_grade', false);
    if ($testBatteryGradeId instanceof ErrorLDR)
    {
        $testBatteryGradeId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryProgram
    $testBatteryProgramId = checkReferenceValueArg($logId, $db, $testBatteryProgram, 'testBatteryProgram',
        'test_battery_program', false);
    if ($testBatteryProgramId instanceof ErrorLDR)
    {
        $testBatteryProgramId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatterySecurity
    $testBatterySecurityId = checkReferenceValueArg($logId, $db, $testBatterySecurity, 'testBatterySecurity',
        'test_battery_security', false);
    if ($testBatterySecurityId instanceof ErrorLDR)
    {
        $testBatterySecurityId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryPermissions
    $testBatteryPermissionsId = checkReferenceValueArg($logId, $db, $testBatteryPermissions, 'testBatteryPermissions',
        'test_battery_permissions', false);
    if ($testBatteryPermissionsId instanceof ErrorLDR)
    {
        $testBatteryPermissionsId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the testBatteryScoring
    $testBatteryScoringId = checkReferenceValueArg($logId, $db, $testBatteryScoring, 'testBatteryScoring',
        'test_battery_scoring', false);
    if ($testBatteryScoringId instanceof ErrorLDR)
    {
        $testBatteryScoringId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that the update will not create a testBatteryName/testBatterySubject/testBatteryGrade
    // combination that already exists for a different test battery
    //
    // Get any values that were not passed
    $testBatteryNameCheck = $testBatteryName;
    $testBatteryGradeCheck = $testBatteryGrade;
    $testBatterySubjectCheck = $testBatterySubject;
    if (strlen($testBatteryName) == 0 || strlen($testBatterySubject) == 0 || strlen($testBatteryGrade) == 0)
    {
        // Get the test battery
        $testBattery = dbGetTestBattery($logId, $db, $testBatteryId);
        if ($testBattery instanceof ErrorLDR)
        {
            $testBattery->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        if (strlen($testBatteryName) == 0)
            $testBatteryNameCheck = $testBattery['testBatteryName'];
        if (strlen($testBatteryGrade) == 0)
            $testBatteryGradeCheck = $testBattery['testBatteryGrade'];
        if (strlen($testBatterySubject) == 0)
            $testBatterySubjectCheck = $testBattery['testBatterySubject'];
    }
    //
    // Check if the value(s) to be updated will cause a match with a different test battery
    $foundTestBatteryId = dbGetTestBatteryId_ByNameSubjectGrade($logId, $db, $testBatteryNameCheck,
        $testBatterySubjectCheck, $testBatteryGradeCheck);
    if ($foundTestBatteryId !== null && $foundTestBatteryId != $testBatteryId)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_DUP, 'testBatteryName = |' . $testBatteryNameCheck .
            '|, testBatterySubject = |' . $testBatterySubjectCheck . '|, testBatteryGrade = |' .
            $testBatteryGradeCheck . '| already exists for testBatteryId ' . $foundTestBatteryId);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Update the test battery
    $result = dbUpdateTestBattery($logId, $db, $testBatteryId, $testBatteryName, $testBatterySubjectId,
        $testBatteryGradeId, $testBatteryProgramId, $testBatterySecurityId,
        $testBatteryPermissionsId, $testBatteryScoringId, $testBatteryDescription);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    else if ($result === false)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_UPDATE_HAS_NO_CHANGES, LDR_ED_UPDATE_HAS_NO_CHANGES);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => SUCCESS], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// updateTestBatteryForm()
//   Updates a test battery form
//
function updateTestBatteryForm()
{
    publishTestBatteryForm(AUDIT_ACTION_UPDATE);
}

//
// LDR test battery support functions
//
// dbCreateTestBattery()
//   Creates a test battery
//
function dbCreateTestBattery($logId, PDO $db, $testBatteryId, $testBatteryName, $testBatterySubjectId,
                             $testBatteryGradeId, $testBatteryProgramId, $testBatterySecurityId,
                             $testBatteryPermissionsId, $testBatteryScoringId, $testBatteryDescription)
{
    $dbTestBatteryName = dbPrepareForSQL($testBatteryName, 'testBatteryName');
    $dbTestBatteryDescription = dbPrepareForSQL($testBatteryDescription, 'testBatteryDescription');
    $sql = "INSERT INTO test_battery (testBatteryId, testBatteryName, testBatterySubjectId,
                                      testBatteryGradeId, testBatteryProgramId, testBatterySecurityId,
                                      testBatteryPermissionsId, testBatteryScoringId, testBatteryDescription)
            VALUES ('$testBatteryId', '$dbTestBatteryName', '$testBatterySubjectId',
                    '$testBatteryGradeId', '$testBatteryProgramId', '$testBatterySecurityId',
                    '$testBatteryPermissionsId', '$testBatteryScoringId', '$dbTestBatteryDescription')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // TODO: Create a user audit record

    return SUCCESS;
}

// dbCreateTestBatteryForm()
//   Creates a test battery form
//
function dbCreateTestBatteryForm($logId, PDO $db, $testBatteryId, $testBatteryFormId, $testBatteryFormName)
{
    $dbTestBatteryFormName = dbPrepareForSQL($testBatteryFormName, 'testBatteryFormName');
    $sql = "INSERT INTO test_battery_form (testBatteryId, testBatteryFormId, testBatteryFormName)
            VALUES ('$testBatteryId', '$testBatteryFormId', '$dbTestBatteryFormName')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // TODO: Create a user audit record

    return SUCCESS;
}

// dbGetTestBatteries()
//   Returns zero or more test batteries
//
function dbGetTestBatteries($logId, PDO $db, $testBatterySubject, $testBatteryGrade, $testBatteryProgram,
                            $testBatterySecurity, $testBatteryPermissions, $testBatteryScoring)
{
    // Create the base query
    $sql = "SELECT tb.testBatteryId, tb.testBatteryName, tb_sub.testBatterySubject, tbg.testBatteryGrade,
                   tb_pro.testBatteryProgram, tb_sec.testBatterySecurity, tb_per.testBatteryPermissions,
                   tb_sco.testBatteryScoring, tb.testBatteryDescription
              FROM test_battery tb
              JOIN test_battery_subject tb_sub ON tb_sub.testBatterySubjectId = tb.testBatterySubjectId
              JOIN test_battery_grade tbg ON tbg.testBatteryGradeId = tb.testBatteryGradeId
              JOIN test_battery_program tb_pro ON tb_pro.testBatteryProgramId = tb.testBatteryProgramId
              JOIN test_battery_security tb_sec ON tb_sec.testBatterySecurityId = tb.testBatterySecurityId
              JOIN test_battery_permissions tb_per ON tb_per.testBatteryPermissionsId = tb.testBatteryPermissionsId
              JOIN test_battery_scoring tb_sco ON tb_sco.testBatteryScoringId = tb.testBatteryScoringId
             WHERE tb_sub.deleted = 'N'
               AND tb_pro.deleted = 'N'
               AND tb_sec.deleted = 'N'
               AND tb_per.deleted = 'N'
               AND tb_sco.deleted = 'N'
               AND tbg.deleted = 'N'
               AND tb.deleted = 'N'";

    // Append optional constraints
    if (strlen($testBatterySubject) > 0)
        $sql .= " AND tb_sub.testBatterySubject = '$testBatterySubject'";
    if (strlen($testBatteryGrade) > 0)
        $sql .= " AND tbg.testBatteryGrade = '$testBatteryGrade'";
    if (strlen($testBatteryProgram) > 0)
        $sql .= " AND tb_pro.testBatteryProgram = '$testBatteryProgram'";
    if (strlen($testBatterySecurity) > 0)
        $sql .= " AND tb_sec.testBatterySecurity = '$testBatterySecurity'";
    if (strlen($testBatteryPermissions) > 0)
        $sql .= " AND tb_per.testBatteryPermissions = '$testBatteryPermissions'";
    if (strlen($testBatteryScoring) > 0)
        $sql .= " AND tb_sco.testBatteryScoring = '$testBatteryScoring'";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testBatteries = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $testBatteries;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetTestBattery()
//   Returns a test battery
//
function dbGetTestBattery($logId, PDO $db, $testBatteryId)
{
    // Get the test battery data
    $sql = "SELECT tb.testBatteryId, tb.testBatteryName, tb_sub.testBatterySubject, tbg.testBatteryGrade,
                   tb_pro.testBatteryProgram, tb_sec.testBatterySecurity, tb_per.testBatteryPermissions,
                   tb_sco.testBatteryScoring, tb.testBatteryDescription
              FROM test_battery tb
              JOIN test_battery_subject tb_sub ON tb_sub.testBatterySubjectId = tb.testBatterySubjectId
              JOIN test_battery_grade tbg ON tbg.testBatteryGradeId = tb.testBatteryGradeId
              JOIN test_battery_program tb_pro ON tb_pro.testBatteryProgramId = tb.testBatteryProgramId
              JOIN test_battery_security tb_sec ON tb_sec.testBatterySecurityId = tb.testBatterySecurityId
              JOIN test_battery_permissions tb_per ON tb_per.testBatteryPermissionsId = tb.testBatteryPermissionsId
              JOIN test_battery_scoring tb_sco ON tb_sco.testBatteryScoringId = tb.testBatteryScoringId
             WHERE tb.testBatteryId = '$testBatteryId'
               AND tb_sub.deleted = 'N'
               AND tb_pro.deleted = 'N'
               AND tb_sec.deleted = 'N'
               AND tb_per.deleted = 'N'
               AND tb_sco.deleted = 'N'
               AND tbg.deleted = 'N'
               AND tb.deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $testBattery = $stmt->fetch(PDO::FETCH_ASSOC);
            return $testBattery;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test battery was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_ID_NOT_FOUND, LDR_ED_TEST_BATTERY_ID_NOT_FOUND . $testBatteryId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestBatteryForm()
//   Returns a test battery form
//
function dbGetTestBatteryForm($logId, PDO $db, $testBatteryFormId)
{
    // Get the test battery form data
    $sql = "SELECT testBatteryId, testBatteryFormId, testBatteryFormName
            FROM test_battery_form
            WHERE testBatteryFormId = '$testBatteryFormId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $testBatteryForm = $stmt->fetch(PDO::FETCH_ASSOC);
            return $testBatteryForm;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test battery form was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_FORM_ID_NOT_FOUND,
                             LDR_ED_TEST_BATTERY_FORM_ID_NOT_FOUND . $testBatteryFormId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestBatteryFormCount()
//   Returns the number of forms for a test battery
//
function dbGetTestBatteryFormCount($logId, PDO $db, $testBatteryId)
{
    // Get the test battery form count
    $sql = "SELECT COUNT(*) FROM test_battery_form WHERE testBatteryId = '$testBatteryId' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test battery was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_ID_NOT_FOUND,
                             LDR_ED_TEST_BATTERY_ID_NOT_FOUND . $testBatteryId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestBatteryForms()
//   Returns the forms for a test battery
//
function dbGetTestBatteryForms($logId, PDO $db, $testBatteryId)
{
    // Get the test battery forms
    $sql = "SELECT testBatteryId, testBatteryFormId, testBatteryFormName
            FROM test_battery_form WHERE testBatteryId = '$testBatteryId' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testBatteryForms = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (sizeof($testBatteryForms) > 0)
            return $testBatteryForms;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test battery was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_ID_NOT_FOUND,
                             LDR_ED_TEST_BATTERY_ID_NOT_FOUND . $testBatteryId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestBatteryId_ByFormId()
//   Returns a test battery ID
//
function dbGetTestBatteryId_ByFormId($logId, PDO $db, $testBatteryFormId)
{
    // Get the test battery data
    $sql = "SELECT testBatteryId FROM test_battery_form
            WHERE testBatteryFormId = '$testBatteryFormId' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test battery form was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_BATTERY_FORM_ID_NOT_FOUND,
                             LDR_ED_TEST_BATTERY_FORM_ID_NOT_FOUND . $testBatteryFormId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestBatteryFormId_ByName()
//   Returns a test battery form ID or null if the test battery form does not exist
//
function dbGetTestBatteryFormId_ByName($logId, PDO $db, $testBatteryFormName, $testBatteryId)
{
    // Get the test battery form ID
    $sql = "SELECT testBatteryFormId FROM test_battery_form
            WHERE testBatteryFormName = '$testBatteryFormName'
            AND testBatteryId = '$testBatteryId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test battery form was not found
    return null;
}

// dbGetTestBatteryId_ByNameSubjectGrade()
//   Returns a test battery ID or null if the test battery does not exist
//
function dbGetTestBatteryId_ByNameSubjectGrade($logId, PDO $db, $testBatteryName, $testBatterySubject, $testBatteryGrade)
{
    // Get the test battery data
    $sql = "SELECT testBatteryId FROM test_battery tb
              JOIN test_battery_subject tbs ON tbs.testBatterySubjectId = tb.testBatterySubjectId
              JOIN test_battery_grade tbg ON tbg.testBatteryGradeId = tb.testBatteryGradeId
             WHERE tb.testBatteryName = '$testBatteryName'
               AND tbs.testBatterySubject = '$testBatterySubject'
               AND tbg.testBatteryGrade = '$testBatteryGrade'
               AND tbs.deleted = 'N'
               AND tbg.deleted = 'N'
               AND tb.deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test battery form was not found
    return null;
}

// dbGetTestBatterySubjects()
//   Returns zero or more test battery subjects
//
function dbGetTestBatterySubjects($logId, PDO $db, $testBatteryGrade)
{
    // Create the base query
    $sql = "SELECT DISTINCT tb_sub.testBatterySubject
              FROM test_battery_subject tb_sub
              JOIN test_battery tb on tb.testBatterySubjectId = tb_sub.testBatterySubjectId
              JOIN test_battery_grade tbg ON tbg.testBatteryGradeId = tb.testBatteryGradeId
             WHERE tb_sub.deleted = 'N'
               AND tbg.deleted = 'N'
               AND tb.deleted = 'N'";

    // Append optional constraints
    if (strlen($testBatteryGrade) > 0)
        $sql .= " AND tbg.testBatteryGrade = '$testBatteryGrade'";

    $sql .= " ORDER BY testBatterySubject";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_NUM);
        $testBatterySubjects = [];
        foreach ($results as $result)
            $testBatterySubjects[] = $result[0];
        return $testBatterySubjects;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbUpdateTestBattery()
//   Checks if a test battery should be updated and returns true if the update is performed, false if not
//
function dbUpdateTestBattery($logId, PDO $db, $testBatteryId, $testBatteryName, $testBatterySubjectId,
                             $testBatteryGradeId, $testBatteryProgramId, $testBatterySecurityId,
                             $testBatteryPermissionsId, $testBatteryScoringId, $testBatteryDescription)
{
    // Determine what and if changes are being made
    $changes = false;
    $sql = "UPDATE test_battery SET ";
    if (strlen($testBatteryName) > 0)
    {
        $dbTestBatteryName = dbPrepareForSQL($testBatteryName, 'testBatteryName');
        $sql .= "testBatteryName = '$dbTestBatteryName', ";
        $changes = true;
    }
    if (!is_null($testBatterySubjectId))
    {
        $sql .= "testBatterySubjectId = '$testBatterySubjectId', ";
        $changes = true;
    }
    if (!is_null($testBatteryGradeId))
    {
        $sql .= "testBatteryGradeId = '$testBatteryGradeId', ";
        $changes = true;
    }
    if (!is_null($testBatteryProgramId))
    {
        $sql .= "testBatteryProgramId = '$testBatteryProgramId', ";
        $changes = true;
    }
    if (!is_null($testBatterySecurityId))
    {
        $sql .= "testBatterySecurityId = '$testBatterySecurityId', ";
        $changes = true;
    }
    if (!is_null($testBatteryPermissionsId))
    {
        $sql .= "testBatteryPermissionsId = '$testBatteryPermissionsId', ";
        $changes = true;
    }
    if (!is_null($testBatteryScoringId))
    {
        $sql .= "testBatteryScoringId = '$testBatteryScoringId', ";
        $changes = true;
    }
    if (strlen($testBatteryDescription) > 0)
    {
        $dbTestBatteryDescription = dbPrepareForSQL($testBatteryDescription, 'testBatteryDescription');
        $sql .= "testBatteryDescription = '$dbTestBatteryDescription', ";
        $changes = true;
    }
    if ($changes)
    {
        $sql = substr($sql, 0, -2); // Strip off the trailing ", "
        $sql .= " WHERE testBatteryId = '$testBatteryId'";

        // Update the test battery
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            return true;
        }
        catch(PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }

        // TODO: Create a user audit record

    }

    return false;
}

// dbUpdateTestBatteryForm()
//   Checks if a test battery form should be updated and returns true if the update is performed, false if not
//
function dbUpdateTestBatteryForm($logId, PDO $db, $testBatteryFormId, $testBatteryFormName)
{
    // Get the test battery form
    $testBatteryForm = dbGetTestBatteryForm($logId, $db, $testBatteryFormId);
    if ($testBatteryForm instanceof ErrorLDR)
    {
        $testBatteryForm->logError(__FUNCTION__);
        return $testBatteryForm;
    }

    // Determine what and if changes are being made
    $changes = false;
    $sql = "UPDATE test_battery_form SET ";
    if (strlen($testBatteryFormName) > 0 && $testBatteryFormName != $testBatteryForm['testBatteryFormName'])
    {
        $dbTestBatteryFormName = dbPrepareForSQL($testBatteryFormName, 'testBatteryFormName');
        $sql .= "testBatteryFormName = '$dbTestBatteryFormName', ";
        $changes = true;
    }
    if ($changes)
    {
        $sql = substr($sql, 0, -2); // Strip off the trailing ", "
        $sql .= " WHERE testBatteryFormId = '$testBatteryFormId'";

        // Update the test battery form
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            return true;
        }
        catch(PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }

        // TODO: Create a user audit record
    }

    return false;
}
