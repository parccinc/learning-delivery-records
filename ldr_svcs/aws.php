<?php

//
// LDR AWS services
//
// checkTestStatusQueue()
// getS3BucketNames()
// getS3JsonFileContents()
//
// LDR AWS support functions
//
// awsCheckTestStatusQueue()
// awsGetS3JsonFileContents()
// awsStore2S3()
// awsStoreFile2S3()

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Sqs\SqsClient;
use Aws\Sqs\Exception\SqsException;

// checkTestStatusQueue()
//   Processes messages in the test status queue
//
function checkTestStatusQueue()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Check the test status queue
    $statistics = awsCheckTestStatusQueue($logId, $db);

    // Check for a test status processing error
    if ($statistics instanceof ErrorLDR)
    {
        $statistics->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    finishService($db, $logId, $statistics, $startTime, __FUNCTION__);
}

// getS3BucketNames()
//   Returns the S3 bucket names
//
function getS3BucketNames()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Initialize an S3 client
    $s3Client = S3Client::factory(['profile' => AWS_S3_PROFILE]);

    // Get the S3 bucket names
    $buckets = [];
    try {
        $result = $s3Client->listBuckets();

        foreach ($result['Buckets'] as $bucket)
            $buckets[] = $bucket['Name'];

    }
    catch (S3Exception $error)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_AWS_ERROR_RESPONSE, $error->getMessage());
        $errorLDR->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the table names
    finishService($db, $logId, $buckets, $startTime, __FUNCTION__);
}

// getS3JsonFileContents()
//   Returns the contents of a JSON file in an S3 bucket
//
function getS3JsonFileContents()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            's3Bucket' => DCO_REQUIRED,
            's3Key' => DCO_REQUIRED
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $s3Bucket, $s3Key) = $args;

    // Get the file contents
    $contents = awsGetS3JsonFileContents($logId, $s3Bucket, $s3Key);
    if ($contents instanceof ErrorLDR)
    {
        $contents->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the file contents
    echo $contents;

    // Log the response
    logResponse($logId, $contents);

    // Log the elapsed time
    $elapsedTime = microtime(true) - $startTime;
    logElapsed($logId, $elapsedTime);

    // Optionally update the daily performance record
    if (RECORD_PERFORMANCE)
        dbRecordPerformance($logId, $db, date("Y-m-d"), __FUNCTION__, $elapsedTime);

    // Close the database connection
    $db = null;
}

//
// LDR AWS support functions
//
// awsCheckTestStatusQueue()
//   Processes messages in the test status queue
//
function awsCheckTestStatusQueue($logId, PDO $db)
{
    $startTime = microtime(true);

    // Initialize the processing statistics
    $queueProcessing = new QueueProcessing();

    try
    {
        // Initialize an SQS client
        $createSqsClientStartTime = microtime(true);
        $sqsClient = SqsClient::factory(['profile' => AWS_SQS_PROFILE, 'region' => AWS_SQS_REGION]);
        $queueProcessing->sqsCreateClientElapsedTime += microtime(true) - $createSqsClientStartTime;
        if (!($sqsClient instanceof SqsClient))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_051, "Unable to create SqsClient: profile = " .
                                     AWS_SQS_PROFILE . ", region = " . AWS_SQS_REGION);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Check if there is a message in the SQS queue
        $sqsReceiveMessageStartTime = microtime(true);
        $messages = $sqsClient->receiveMessage(['QueueUrl' => AWS_SQS_QUEUE_URL]);
        $queueProcessing->sqsReceiveMessageElapsedTime += microtime(true) - $sqsReceiveMessageStartTime;
        $message = $messages['Messages'][0];

        // Keep processing messages until the queue is empty
        while ($message !== null)
        {
            // Initialize processing for this message
            $msgId = null;
            $s3Key = null;
            $preface = '';
            $record = null;
            $tokens = null;
            $result = false;
            $statusChange = null;
            $adpTestResultsId = 0;
            $testAssignmentId = 0;
            $testAssignment = null;
            $logStatusChange = null;
            $adpTestAssignmentId = 0;
            $messageFormatOkay = true;
            $queueProcessing->totalMessagesProcessed++;

            // Get the message body
            $body = $message['Body'];
            if ($body === null)
                $messageFormatOkay = false;

            // Get the message records
            if ($messageFormatOkay)
            {
                $body = json_decode($body, true);
                if (!array_key_exists('Records', $body))
                    $messageFormatOkay = false;
            }

            // Get the S3 object key for the test results file
            if ($messageFormatOkay)
            {
                $record = $body['Records'][0];

                if (array_key_exists('s3', $record) &&
                    array_key_exists('object', $record['s3']) &&
                    array_key_exists('key', $record['s3']['object']))
                {
                    $s3Key = $record['s3']['object']['key'];

                    $preface = 's3Key = ' . $s3Key . ', error detail = ';
                }
                else
                    $messageFormatOkay = false;
            }

            // Check that the file is in the ADP test results folder
            if ($messageFormatOkay && strlen(ADP_S3_RESULTS_ROOT) > 0)
            {
                if (strncmp(ADP_S3_RESULTS_ROOT, $s3Key, strlen(ADP_S3_RESULTS_ROOT)) != 0)
                    $messageFormatOkay = false;
            }

            // Start parsing the S3 object key
            if ($messageFormatOkay)
            {
                // Remove the test results folder name
                if (strlen(ADP_S3_RESULTS_ROOT) > 0)
                    $subKey = substr($s3Key, strlen(ADP_S3_RESULTS_ROOT));
                else
                    $subKey = $s3Key;

                $tokens = explode('.', $subKey);

                if (sizeof($tokens) != 3 && sizeof($tokens) != 4) {
                    $result = new ErrorLDR($logId, LDR_EC_INVALID_S3_OBJECT_KEY, LDR_ED_INVALID_S3_OBJECT_KEY.$subKey);
                    $messageFormatOkay = false;
                }

                if (sizeof($tokens) == 4) {
                    if ($tokens[2] != TENANT_ID) {
                        $result = new ErrorLDR($logId, LDR_EC_INVALID_S3_OBJECT_KEY
                            , LDR_ED_INVALID_S3_OBJECT_KEY.$subKey.". It does not belong to current Tenant.");
                        $messageFormatOkay = false;
                    }
                }
            }

            // Get the statusChange, adpTestAssignmentId and adpTestResultsId
            if ($messageFormatOkay)
            {
                $statusChange = strtolower($tokens[1]);
                $logStatusChange = ucfirst($statusChange);

                $ids = explode('/', $tokens[0]);

                if (sizeof($ids) == 2)
                {
                    $adpTestAssignmentId = strrev($ids[0]);
                    $adpTestResultsId = strrev($ids[1]);
                }
                else
                    $messageFormatOkay = false;
            }

            // Start logging for this message
            if ($messageFormatOkay)
            {
                // Generate unique message identifier
                $msgId = generateIdentifier(4);

                logTestStatus($logId, $msgId, $message['Body']);
            }

            // Update the processing statistics
            if ($messageFormatOkay)
            {
                switch ($statusChange)
                {
                    case 'submitted':
                        $queueProcessing->submittedMessagesProcessed++;
                        break;
                    case 'started':
                        $queueProcessing->startedMessagesProcessed++;
                        break;
                    case 'resumed':
                        $queueProcessing->resumedMessagesProcessed++;
                        break;
                    case 'paused':
                        $queueProcessing->pausedMessagesProcessed++;
                        break;
                    default:
                        $queueProcessing->unknownStatusChangesProcessed++;
                        $result = new ErrorLDR($logId, LDR_EC_ADP_STATUS_CHANGE_UNKNOWN,
                                               'This status change is unknown: ' . $statusChange);
                        break;
                }
            }
            else
                $queueProcessing->unknownMessagesProcessed++;

            // Get the testAssignmentId
            if ($messageFormatOkay && !($result instanceof ErrorLDR))
            {
                $ldrGetTestAssignmentIdStartTime = microtime(true);
                $testAssignmentId = dbGetTestAssignmentId_ByAdpId($logId, $db, $adpTestAssignmentId);
                $queueProcessing->ldrGetTestAssignmentIdElapsedTime +=
                    microtime(true) - $ldrGetTestAssignmentIdStartTime;
                if ($testAssignmentId instanceof ErrorLDR)
                {
                    $result = $testAssignmentId;

                    switch ($statusChange)
                    {
                        case 'submitted':
                            $queueProcessing->submittedMessagesFailed++;
                            break;
                        case 'started':
                            $queueProcessing->startedMessagesFailed++;
                            break;
                        case 'resumed':
                            $queueProcessing->resumedMessagesFailed++;
                            break;
                        case 'paused':
                            $queueProcessing->pausedMessagesFailed++;
                            break;
                    }
                }
                else
                {
                    logTestStatus($logId, $msgId, 'testAssignmentId = ' . $testAssignmentId . ', statusChange = ' .
                                  $logStatusChange . ', adpTestAssignmentId = ' . $adpTestAssignmentId .
                                  ', adpTestResultsId = ' . $adpTestResultsId);
                }
            }

            // Get the test assignment
            if ($messageFormatOkay && !($result instanceof ErrorLDR))
            {
                $ldrGetTestAssignmentStartTime = microtime(true);
                $testAssignment = dbGetTestAssignment_ById($logId, $db, $testAssignmentId);
                $queueProcessing->ldrGetTestAssignmentElapsedTime += microtime(true) - $ldrGetTestAssignmentStartTime;
                if ($testAssignment instanceof ErrorLDR)
                {
                    $result = $testAssignment;

                    switch ($statusChange)
                    {
                        case 'submitted':
                            $queueProcessing->submittedMessagesFailed++;
                            break;
                        case 'started':
                            $queueProcessing->startedMessagesFailed++;
                            break;
                        case 'resumed':
                            $queueProcessing->resumedMessagesFailed++;
                            break;
                        case 'paused':
                            $queueProcessing->pausedMessagesFailed++;
                            break;
                    }
                }
                else
                {
                    logTestStatus($logId, $msgId, 'testStatus = ' . $testAssignment['testStatus'] .
                                  ', lastQueueEventTime = ' . $testAssignment['lastQueueEventTime']);
                }
            }

            // Check the message event time
            if ($messageFormatOkay && !($result instanceof ErrorLDR))
            {
                if (array_key_exists('eventTime', $record))
                {
                    $queueEventTime = strtotime($record['eventTime']);
                    if ($queueEventTime !== false)
                    {
                        if ($queueEventTime > $testAssignment['lastQueueEventTime'])
                        {
                            logTestStatus($logId, $msgId, 'queueEventTime ' . $queueEventTime .
                                                   ' > lastQueueEventTime ' . $testAssignment['lastQueueEventTime']);

                            // Update the test assignment lastQueueEventTime value
                            $ldrUpdateLastQueueEventStartTime = microtime(true);
                            $result = dbUpdateLastQueueEventTime($logId, $db, $testAssignmentId, $queueEventTime);
                            $queueProcessing->ldrUpdateLastQueueEventTime +=
                                microtime(true) - $ldrUpdateLastQueueEventStartTime;
                        }
                        else
                        {
                            logTestStatus($logId, $msgId, 'queueEventTime ' . $queueEventTime .
                                                  ' <= lastQueueEventTime ' . $testAssignment['lastQueueEventTime']);

                            // Ignore messages that have an earlier event time
                            switch ($statusChange)
                            {
                                case 'submitted':
                                    $queueProcessing->submittedMessagesIgnored++;
                                    break;
                                case 'started':
                                    $queueProcessing->startedMessagesIgnored++;
                                    break;
                                case 'resumed':
                                    $queueProcessing->resumedMessagesIgnored++;
                                    break;
                                case 'paused':
                                    $queueProcessing->pausedMessagesIgnored++;
                                    break;
                            }

                            $result = new ErrorLDR($logId, LDR_EC_ADP_STATUS_CHANGE_IGNORED, 'Status change ' .
                                $statusChange . ' at ' . $queueEventTime . ' ignored because not later than status ' .
                                'change at ' . $testAssignment['lastQueueEventTime'] . ' which has already been processed');
                        }
                    }
                }
            }

            // Check that the test assignment status can be updated
            if ($messageFormatOkay && !($result instanceof ErrorLDR))
            {
                if ($testAssignment['testStatusId'] == TEST_STATUS_ID_SUBMITTED ||
                    $testAssignment['testStatusId'] == TEST_STATUS_ID_CANCELED)
                {
                    $result = new ErrorLDR($logId, LDR_EC_ADP_STATUS_CHANGE_INVALID, 'Status change ' . $statusChange .
                        ' is invalid because the test assignment has already been ' . $testAssignment['testStatus']);

                    switch ($statusChange)
                    {
                        case 'submitted':
                            $queueProcessing->submittedMessagesInvalid++;
                            break;
                        case 'started':
                            $queueProcessing->startedMessagesInvalid++;
                            break;
                        case 'resumed':
                            $queueProcessing->resumedMessagesInvalid++;
                            break;
                        case 'paused':
                            $queueProcessing->pausedMessagesInvalid++;
                            break;
                    }
                }
            }

            // Process the test assignment status change
            if ($messageFormatOkay && !($result instanceof ErrorLDR))
            {
                // Lock the test assignment and user_audit tables
                $sql = "LOCK TABLES test_assignment WRITE, user_audit WRITE";
                try {
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                }
                catch(PDOException $error) {
                    $result = errorQueryException($logId, $error, __FUNCTION__, $sql);
                }

                $db->beginTransaction();
                $testStatusMessage = 'statusChange = ' . $logStatusChange;
                switch ($statusChange)
                {
                    case 'submitted':
                        // Store the test results
                        $ldrStoreTestResultsStartTime = microtime(true);
                        $testStatusMessage .= ', dbStoreTestResults: ';
                        $storageResult = dbStoreTestResults($logId, $db, $testAssignmentId, $adpTestAssignmentId,
                                                            $adpTestResultsId);
                        $testStatusMessage .= ($storageResult instanceof ErrorLDR) ? 'FAIL' : 'OKAY';
                        $queueProcessing->ldrStoreTestResultsElapsedTime +=
                            microtime(true) - $ldrStoreTestResultsStartTime;

                        // Update the test assignment status to Submitted
                        $ldrUpdateTestStatusStartTime = microtime(true);
                        $testAssignmentStatus = dbGetReferenceIdValue($logId, $db, 'test_status',
                                                                      TEST_STATUS_ID_SUBMITTED);
                        $testStatusMessage .= ', dbUpdateTestAssignment: ';
                        $updateResult = dbUpdateTestAssignment($logId, $db, ROOT_CLIENT_USER_ID, $testAssignmentId,
                                                         $testAssignment, $testAssignmentStatus, null, null, null);
                        $testStatusMessage .= ($updateResult instanceof ErrorLDR) ? 'FAIL' : 'OKAY';
                        $queueProcessing->ldrUpdateTestStatusElapsedTime +=
                            microtime(true) - $ldrUpdateTestStatusStartTime;

                        // Update the ADP test assignment status to Completed
                        if (SKIP_ADP_COMM == false)
                        {
                            $adpUpdateTestStatusStartTime = microtime(true);
                            $adpStudentData = ['studentId' => $testAssignment['studentRecordId']];
                            $testStatusMessage .= ', adpUpdateTestAssignment: ';
                            $result = adpUpdateTestAssignment($logId, $testAssignment, $adpStudentData, 'Completed',
                                                              null, null);
                            $testStatusMessage .= ($result instanceof ErrorLDR) ? 'FAIL' : 'OKAY';
                            $queueProcessing->adpUpdateTestStatusElapsedTime +=
                                microtime(true) - $adpUpdateTestStatusStartTime;
                        }

                        // Report the most important error
                        if ($storageResult instanceof ErrorLDR)
                            $result = $storageResult;
                        else if ($updateResult instanceof ErrorLDR)
                            $result = $updateResult;
                        if ($result instanceof ErrorLDR)
                            $queueProcessing->submittedMessagesFailed++;
                        break;
                    case 'started':
                        // Update the test assignment status to InProgress
                        $ldrUpdateTestStatusStartTime = microtime(true);
                        $testAssignmentStatus = dbGetReferenceIdValue($logId, $db, 'test_status',
                                                                      TEST_STATUS_ID_INPROGRESS);
                        $testStatusMessage .= ', dbUpdateTestAssignment: ';
                        $result = dbUpdateTestAssignment($logId, $db, ROOT_CLIENT_USER_ID, $testAssignmentId,
                                                         $testAssignment, $testAssignmentStatus, null, null, null);
                        $testStatusMessage .= ($result instanceof ErrorLDR) ? 'FAIL' : 'OKAY';
                        $queueProcessing->ldrUpdateTestStatusElapsedTime +=
                            microtime(true) - $ldrUpdateTestStatusStartTime;
                        if ($result instanceof ErrorLDR)
                            $queueProcessing->startedMessagesFailed++;
                        break;
                    case 'resumed':
                        // Update the test assignment status to InProgress
                        $ldrUpdateTestStatusStartTime = microtime(true);
                        $testAssignmentStatus = dbGetReferenceIdValue($logId, $db, 'test_status',
                                                                      TEST_STATUS_ID_INPROGRESS);
                        $testStatusMessage .= ', dbUpdateTestAssignment: ';
                        $result = dbUpdateTestAssignment($logId, $db, ROOT_CLIENT_USER_ID, $testAssignmentId,
                                                         $testAssignment, $testAssignmentStatus, null, null, null);
                        $testStatusMessage .= ($result instanceof ErrorLDR) ? 'FAIL' : 'OKAY';
                        $queueProcessing->ldrUpdateTestStatusElapsedTime +=
                            microtime(true) - $ldrUpdateTestStatusStartTime;
                        if ($result instanceof ErrorLDR)
                            $queueProcessing->resumedMessagesFailed++;
                        break;
                    case 'paused':
                        // Update the test assignment status to Paused
                        $ldrUpdateTestStatusStartTime = microtime(true);
                        $testAssignmentStatus = dbGetReferenceIdValue($logId, $db, 'test_status',
                                                                      TEST_STATUS_ID_PAUSED);
                        $testStatusMessage .= ', dbUpdateTestAssignment: ';
                        $result = dbUpdateTestAssignment($logId, $db, ROOT_CLIENT_USER_ID, $testAssignmentId,
                                                         $testAssignment, $testAssignmentStatus, null, null, null);
                        $testStatusMessage .= ($result instanceof ErrorLDR) ? 'FAIL' : 'OKAY';
                        $queueProcessing->ldrUpdateTestStatusElapsedTime +=
                            microtime(true) - $ldrUpdateTestStatusStartTime;
                        if ($result instanceof ErrorLDR)
                            $queueProcessing->pausedMessagesFailed++;
                        break;
                }
                if ($result instanceof ErrorLDR)
                    $db->rollBack();
                else
                    $db->commit();

                // Unlock the tables
                $unlock = dbUnlockTables($logId, $db);
                if ($unlock instanceof ErrorLDR)
                    $unlock->logError(__FUNCTION__);

                logTestStatus($logId, $msgId, $testStatusMessage);
            }

            // Log any error to HAL and the test status log
            if ($result instanceof ErrorLDR)
            {
                $halLoggingStartTime = microtime(true);
                logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(), $preface . $result->getErrorDetail(), $msgId);
                $queueProcessing->halLoggingElapsedTime += microtime(true) - $halLoggingStartTime;

                $errorMessage = $result->getErrorCode() . ' ' . $preface . $result->getErrorDetail();
                logTestStatus($logId, $msgId, $errorMessage);
            }

            // Delete the message from the queue
            $sqsDeleteMessageStartTime = microtime(true);
            $sqsClient->deleteMessage(['QueueUrl' => AWS_SQS_QUEUE_URL, 'ReceiptHandle' => $message['ReceiptHandle']]);
            $queueProcessing->sqsDeleteMessageElapsedTime += microtime(true) - $sqsDeleteMessageStartTime;

            // Save the elapsed time
            $elapsedTime = microtime(true) - $startTime;
            if ($statusChange !== null)
            {
                switch ($statusChange)
                {
                    case 'submitted':
                        $queueProcessing->submittedElapsedTime += $elapsedTime;
                        break;
                    case 'started':
                        $queueProcessing->startedElapsedTime += $elapsedTime;
                        break;
                    case 'paused':
                        $queueProcessing->pausedElapsedTime += $elapsedTime;
                        break;
                    case 'resumed':
                        $queueProcessing->resumedElapsedTime += $elapsedTime;
                        break;
                    default:
                        $queueProcessing->unknownElapsedTime += $elapsedTime;
                        break;
                }
            }
            else
                $queueProcessing->unknownElapsedTime += $elapsedTime;

            // Check if there is another message in the SQS queue
            $startTime = microtime(true);
            $messages = $sqsClient->receiveMessage(['QueueUrl' => AWS_SQS_QUEUE_URL]);
            $queueProcessing->sqsReceiveMessageElapsedTime += microtime(true) - $startTime;
            $message = $messages['Messages'][0];
        }
    }
    catch (SqsException $error)
    {
        $result = new ErrorLDR($logId, LDR_EC_AWS_ERROR_RESPONSE, $error->getMessage());
        $halLoggingStartTime = microtime(true);
        logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(), $result->getErrorDetail());
        $queueProcessing->halLoggingElapsedTime += microtime(true) - $halLoggingStartTime;
        $result->logError(__FUNCTION__);
        return $result;
    }

    // If messages were processed then save the queue processing statistics
    if ($queueProcessing->totalMessagesProcessed > 0)
    {
        $result = dbAddQueueProcessingRecord($logId, $db, date("Y-m-d"), gethostname(), $queueProcessing);
        if ($result instanceof ErrorLDR)
            $result->logError(__FUNCTION__);
    }

    // Return the queue processing statistics
    $queueProcessing->sqsCreateClientElapsedTime = roundElapsedTime($queueProcessing->sqsCreateClientElapsedTime);
    $queueProcessing->sqsReceiveMessageElapsedTime = roundElapsedTime($queueProcessing->sqsReceiveMessageElapsedTime);
    $queueProcessing->sqsDeleteMessageElapsedTime = roundElapsedTime($queueProcessing->sqsDeleteMessageElapsedTime);
    $queueProcessing->ldrGetTestAssignmentIdElapsedTime = roundElapsedTime($queueProcessing->ldrGetTestAssignmentIdElapsedTime);
    $queueProcessing->ldrGetTestAssignmentElapsedTime = roundElapsedTime($queueProcessing->ldrGetTestAssignmentElapsedTime);
    $queueProcessing->ldrUpdateLastQueueEventTime = roundElapsedTime($queueProcessing->ldrUpdateLastQueueEventTime);
    $queueProcessing->ldrUpdateTestStatusElapsedTime = roundElapsedTime($queueProcessing->ldrUpdateTestStatusElapsedTime);
    $queueProcessing->ldrStoreTestResultsElapsedTime = roundElapsedTime($queueProcessing->ldrStoreTestResultsElapsedTime);
    $queueProcessing->adpUpdateTestStatusElapsedTime = roundElapsedTime($queueProcessing->adpUpdateTestStatusElapsedTime);
    $queueProcessing->halLoggingElapsedTime = roundElapsedTime($queueProcessing->halLoggingElapsedTime);
    $queueProcessing->submittedElapsedTime = roundElapsedTime($queueProcessing->submittedElapsedTime);
    $queueProcessing->startedElapsedTime = roundElapsedTime($queueProcessing->startedElapsedTime);
    $queueProcessing->pausedElapsedTime = roundElapsedTime($queueProcessing->pausedElapsedTime);
    $queueProcessing->resumedElapsedTime = roundElapsedTime($queueProcessing->resumedElapsedTime);
    $queueProcessing->unknownElapsedTime = roundElapsedTime($queueProcessing->unknownElapsedTime);
    return $queueProcessing;
}

function roundElapsedTime($number)
{
    return intval(($number * 1000) + 0.5) / 1000;
}

// awsGetPresignedRequestUrl()
//   Gets a pre signed URL for the S3 object using the S3 bucket and key
//
function awsGetPresignedRequestUrl($logId, $s3Bucket, $s3Key)
{
    // Initialize an S3 client
    $s3Client = S3Client::factory(['profile' => AWS_S3_PROFILE]);
    if (!($s3Client instanceof S3Client))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_052,
                                 "Unable to create S3Client: profile = " . AWS_SQS_PROFILE);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Get the file contents
    try {
        $s3Command = $s3Client->getCommand('GetObject', [
            'Bucket'    => $s3Bucket
            , 'Key'     => $s3Key
            ]);
        $presignedUrl = $s3Command->createPresignedUrl(LDR_S3_PRESIGNED_EXPIRATION);
        return $presignedUrl;
    }
    catch (Exception $error)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_AWS_ERROR_RESPONSE, $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
}

// awsGetS3JsonFileContents()
//   Returns the contents of a JSON file in an S3 bucket
//
function awsGetS3JsonFileContents($logId, $s3Bucket, $s3Key)
{
    // Initialize an S3 client
    $s3Client = S3Client::factory(['profile' => AWS_S3_PROFILE]);
    if (!($s3Client instanceof S3Client))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_052,
                                 "Unable to create S3Client: profile = " . AWS_SQS_PROFILE);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Get the file contents
    try {
        $result = $s3Client->getObject(['Bucket' => $s3Bucket, 'Key' => $s3Key]);

        $contents = (string)$result['Body'];
    }
    catch (Exception $error)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_AWS_ERROR_RESPONSE, $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $contents;
}

// awsStore2S3()
//   Stores a content to a new S3 Object
//
function awsStore2S3($logId, $s3Bucket, $s3Key, &$content)
{
    // Initialize an S3 client
    $s3Client = S3Client::factory(['profile' => AWS_S3_PROFILE]);
    if (!($s3Client instanceof S3Client)) {
        $errorLDR = new ErrorLDR(
            $logId,
            LDR_EC_INTERNAL_ERROR_052,
            "Unable to create S3Client: profile = " . AWS_SQS_PROFILE
        );
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Get the file contents
    try {
        $result = $s3Client->putObject([
            'Bucket' => $s3Bucket
            , 'Key' => $s3Key
            , 'Body' => $content
        ]);
    } catch (Exception $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_AWS_ERROR_RESPONSE, $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return SUCCESS;
}

// awsStoreFile2S3()
//  Stores a file to a new S3 Object
//
function awsStoreFile2S3($logId, $s3Bucket, $s3Key, $fileHandler)
{
    // Initialize an S3 client
    $s3Client = S3Client::factory(['profile' => AWS_S3_PROFILE]);
    if (!($s3Client instanceof S3Client)) {
        $errorLDR = new ErrorLDR(
            $logId,
            LDR_EC_INTERNAL_ERROR_052,
            "Unable to create S3Client: profile = " . AWS_SQS_PROFILE
        );
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Get the file contents
    try {
        $result = $s3Client->putObject([
            'Bucket' => $s3Bucket
            , 'Key' => $s3Key
            , 'Body' => $fileHandler
        ]);
    } catch (Exception $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_AWS_ERROR_RESPONSE, $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return SUCCESS;
}
