<?php

//
// LDR ADP communications services
//
// getResultsADP
// pingADP()
//
// LDR ADP communications support functions
//
// adpCreateTestAssignment()
// adpDeleteTestAssignment()
// adpGetTestResults()
// adpPrepareAuthentication()
// adpUnlockTestAssignment()
// adpUpdateTestAssignment()
// filterStateIdentifier()

//
// LDR ADP communications services
//
// getResultsADP
//   Retrieves test results data references
//
function getResultsADP($adpTestAssignmentId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    $argName = 'adpTestAssignmentId';
    $adpTestAssignmentId =
        validateArg($logId, $db, [$argName => $adpTestAssignmentId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($adpTestAssignmentId instanceof ErrorLDR)
    {
        $adpTestAssignmentId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test results data references
    $results = adpGetTestResults($logId, $adpTestAssignmentId);
    if ($results instanceof ErrorLDR)
    {
        $results->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the test results data references
    finishService($db, $logId, $results, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// pingADP()
//   Tests if ADP services are reachable
//
function pingADP()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Configure the request
    $apiURI = ADP_RESOURCE_PATH . 'ping/status';
    $base_url =  ADP_REQUEST_SCHEME . '://' . ADP_HOSTNAME;
    $fullURL = $base_url . $apiURI;
    $ch = curl_init($fullURL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    logCallADP($logId, $fullURL);

    // Send the request
    $response = curl_exec($ch);
    if ($response === false)
    {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_PING_FAILED, 'ADP ping failed: ' . $errorCode . ' : ' . $errorText);
        $errorLDR->returnErrorJSON(__FUNCTION__);
    }
    else
    {
        $response = json_encode(['result' => $response]);
        logResponse($logId, $response);
        echo $response;
    }
    curl_close($ch);

    // Log the elapsed time
    $elapsedTime = microtime(true) - $startTime;
    logElapsed($logId, $elapsedTime);
}

//
// LDR ADP communications support functions
//
// adpCreateTestAssignment()
//   Creates an ADP test assignment
//
function adpCreateTestAssignment($logId, $testBatteryId, $testBatteryFormId, $testAssignmentId, $testKey, $studentData,
                                 $enableLineReader, $enableTextToSpeech)
{
    // Prepare the ADP request authentication
    $apiURI = ADP_RESOURCE_PATH . 'assignment';
    $authentication = adpPrepareAuthentication($apiURI);

    $enableLineReader = ($enableLineReader == 'Y') ? true : false;
    $enableTextToSpeech = ($enableTextToSpeech == 'Y') ? true : false;

    // Initialize the request
    $fullURL = ADP_REQUEST_SCHEME . '://' . ADP_HOSTNAME . $apiURI;
    $ch = curl_init($fullURL);

    // Configure the request
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Content-Type: application/json; charset=UTF-8",
        "Authorization: Basic " . base64_encode(ADP_USERNAME . ":" . ADP_PASSWORD),
        "Authentication:" . $authentication
    ]);

    $studentData['stateIdentifier'] = filterStateIdentifier($studentData['stateIdentifier']);

    $payload = json_encode([
        'testAssignmentId' => $testAssignmentId,
        'test' => [
            'testId' => $testBatteryId,
            'testFormId' => $testBatteryFormId,
            'testKey' => $testKey,
            'enableLineReader' => $enableLineReader,
            'enableTextToSpeech' => $enableTextToSpeech
        ],
        'student' => [
            'studentId' => $studentData['studentRecordId'],
            'personalId' => $studentData['stateIdentifier'],
            'firstName' => $studentData['firstName'],
            'lastName' => $studentData['lastName'],
            'dateOfBirth' => $studentData['dateOfBirth'],
            'state' => $studentData['stateCode'],
            'schoolName' => $studentData['organizationName'],
            'grade' => $studentData['gradeLevel']
        ]
    ]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    logCallADP($logId, $fullURL, $payload);

    // Send the request
    $jsonResponse = curl_exec($ch);

    // Check the response
    if ($jsonResponse === false)
    {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CURL_ERROR_RESPONSE,
                                 'curl error code ' . $errorCode . ' : ' . $errorText);
        $errorLDR->logError(__FUNCTION__);
        curl_close($ch);
        return $errorLDR;
    }
    else
    {
        // Get the response
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($jsonResponse, true);
        curl_close($ch);

        // Check the response
        if ($response === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_RESPONSE_DECODE_FAILED,
                                     LDR_ED_ADP_RESPONSE_DECODE_FAILED . $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        else if ($status == 200 && is_array($response))
        {
            // Return the ADP test assignment ID


            if (isset($response['testAssignmentId']))
                $adpTestAssignmentId = $response['testAssignmentId'];
            elseif (isset($response['result']) && isset($response['result']['testAssignmentId']))
                $adpTestAssignmentId = $response['result']['testAssignmentId'];
            else
                $adpTestAssignmentId = new ErrorLDR($logId, LDR_EC_ADP_NO_TEST_ASSIGNMENT_ID, $jsonResponse);
            return $adpTestAssignmentId;
        }
        else
        {
            // Return the ADP error
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_ERROR_RESPONSE, $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
}

// adpDeleteTestAssignment()
//  Attempt to delete test assignments from ADP
//
function adpDeleteTestAssignment($logId, $testAssignmentId, $adpTestAssignmentId, $testBatteryId, $testBatteryFormId
                                , $testKey, $studentId)
{
    // Prepare the ADP request authentication
    $apiURI = ADP_RESOURCE_PATH . 'assignment/'.$adpTestAssignmentId;
    $authentication = adpPrepareAuthentication($apiURI);

    // Initialize the request
    $fullURL = ADP_REQUEST_SCHEME . '://' . ADP_HOSTNAME . $apiURI;
    $ch = curl_init($fullURL);

    // Configure the request
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Content-Type: application/json; charset=UTF-8",
        "Authorization: Basic " . base64_encode(ADP_USERNAME . ":" . ADP_PASSWORD),
        "Authentication:" . $authentication
    ]);

    $payload = json_encode([
        'testAssignmentId' => $testAssignmentId,
        'test' => [
            'testId' => $testBatteryId,
            'testFormId' => $testBatteryFormId,
            'testKey' => $testKey
        ],
        'student' => [
            'studentId' => $studentId
        ]
    ]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    logCallADP($logId, $fullURL, $payload);

    // Send the request
    $jsonResponse = curl_exec($ch);

    // Check the response
    if ($jsonResponse === false) {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CURL_ERROR_RESPONSE,
                                 'curl error code ' . $errorCode . ' : ' . $errorText);
        $errorLDR->logError(__FUNCTION__);
        curl_close($ch);
        return $errorLDR;
    } else {
        // Get the response
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($jsonResponse, true);
        curl_close($ch);

        // Check the response
        if ($response === null) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_RESPONSE_DECODE_FAILED,
                                     LDR_ED_ADP_RESPONSE_DECODE_FAILED . $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        } else if ($status == 200 && is_array($response)) {
            // Return the ADP test assignment ID
            $deleteStatus = false;
            if (isset($response['testAssignmentId'])) {
                $adpTestAssignmentId = $response['testAssignmentId'];
                $deleteStatus = (strcmp(strtolower($response['status']), 'deleted') == 0 ? SUCCESS:$response['status']);
            } elseif (isset($response['result']) && isset($response['result']['testAssignmentId'])) {
                $adpTestAssignmentId = $response['result']['testAssignmentId'];
                $deleteStatus = (strcmp(strtolower($response['result']['status']), 'deleted') == 0 ? SUCCESS:$response['result']['status']);
            } else
                $adpTestAssignmentId = new ErrorLDR($logId, LDR_EC_ADP_NO_TEST_ASSIGNMENT_ID, $jsonResponse);

            return $deleteStatus;
        } else {
            // Return the ADP error
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_ERROR_RESPONSE, $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
}

// adpGetTestResults()
//   Retrieves test results data references
//
function adpGetTestResults($logId, $adpTestAssignmentId)
{
    // Prepare the ADP request authentication
    $apiURI = ADP_RESOURCE_PATH . 'results/' . $adpTestAssignmentId;
    $authentication = adpPrepareAuthentication($apiURI);

    // Initialize the request
    $fullURL = ADP_REQUEST_SCHEME . '://' . ADP_HOSTNAME . $apiURI;
    $ch = curl_init($fullURL);

    // Configure the request
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Content-Type: application/json; charset=UTF-8",
        "Authorization: Basic " . base64_encode(ADP_USERNAME . ":" . ADP_PASSWORD),
        "Authentication:" . $authentication
    ]);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    logCallADP($logId, $fullURL);

    // Send the request
    $jsonResponse = curl_exec($ch);

    // Check the response
    if ($jsonResponse === false)
    {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CURL_ERROR_RESPONSE,
            'curl error code ' . $errorCode . ' : ' . $errorText);
        $errorLDR->logError(__FUNCTION__);
        curl_close($ch);
        return $errorLDR;
    }
    else
    {
        // Get the response
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($jsonResponse, true);
        curl_close($ch);

        // Check the response
        if ($response === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_RESPONSE_DECODE_FAILED,
                                     LDR_ED_ADP_RESPONSE_DECODE_FAILED . $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        else if ($status == 200 && is_array($response))
        {
            // Open the "envelope" if there is one
            if (isset($response['result']))
                $response = $response['result'];
            return $response;
        }
        else
        {
            // Return the ADP error
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_ERROR_RESPONSE, $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
}

// adpPrepareAuthentication()
//   Prepares an ADP request authentication value
//
function adpPrepareAuthentication($apiURI)
{
    $microtime = round(microtime(true) * 1000); // 13-digit Unix timestamp in UTC timezone
    $nonce = mt_rand(0, 99999); // 1-5 digit long random integer in [0, 99999) range
    $message = $apiURI . round($microtime / 1000) . $nonce; // Message uses rounded 10-digit Unix timestamp
    $hash = hash_hmac('sha256', $message, ADP_SECRET);
    $authentication = $microtime . ':' . $nonce . ':' . base64_encode($hash);
    return base64_encode($authentication);
}

// adpUnlockTestAssignment()
//   Unlocks an ADP test assignment
//
function adpUnlockTestAssignment($logId, $testAssignment, $studentData, $testStatus)
{
    // Prepare the ADP request authentication
    $apiURI = ADP_RESOURCE_PATH . 'assignment/' . $testAssignment['adpTestAssignmentId'];
    $authentication = adpPrepareAuthentication($apiURI);

    // Initialize the request
    $fullURL = ADP_REQUEST_SCHEME . '://' . ADP_HOSTNAME . $apiURI;
    $ch = curl_init($fullURL);

    // Configure the request body
    $payload =
        [
            'testAssignmentId' => $testAssignment['testAssignmentId'],
            'testStatus' => $testStatus,
            'test' => [
                'testId' => $testAssignment['testBatteryId'],
                'testFormId' => $testAssignment['testBatteryFormId'],
                'testKey' => $testAssignment['testKey']
            ],
            'student' => $studentData
        ];
    $payload = json_encode($payload);

    // Configure the request
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Content-Type: application/json; charset=UTF-8",
        "Authorization: Basic " . base64_encode(ADP_USERNAME . ":" . ADP_PASSWORD),
        "Authentication:" . $authentication
    ]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    logCallADP($logId, $fullURL, $payload);

    // Send the request
    $jsonResponse = curl_exec($ch);

    // Check the response
    if ($jsonResponse === false)
    {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CURL_ERROR_RESPONSE,
                                 'curl error code ' . $errorCode . ' : ' . $errorText);
        $errorLDR->logError(__FUNCTION__);
        curl_close($ch);
        return $errorLDR;
    }
    else
    {
        // Get the response
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $response = json_decode($jsonResponse, true);
        curl_close($ch);

        // Check the response
        if ($response === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_RESPONSE_DECODE_FAILED,
                                     LDR_ED_ADP_RESPONSE_DECODE_FAILED . $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        else if ($status == 200 && is_array($response))
        {
            // Open the "envelope" if there is one
            if (isset($response['result']))
                $response = $response['result'];
            return $response['status'];
        }
        else
        {
            // Return the ADP error
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_ERROR_RESPONSE, $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
}

// adpUpdateTestAssignment()
//   Updates an ADP test assignment
//
function adpUpdateTestAssignment($logId, $testAssignment, $studentData, $testStatus, $enableLineReader,
                                 $enableTextToSpeech)
{
    // Prepare the ADP request authentication
    $apiURI = ADP_RESOURCE_PATH . 'assignment/' . $testAssignment['adpTestAssignmentId'];
    $authentication = adpPrepareAuthentication($apiURI);

    // Initialize the request
    $fullURL = ADP_REQUEST_SCHEME . '://' . ADP_HOSTNAME . $apiURI;
    $ch = curl_init($fullURL);

    if (isset($studentData['personalId'])) {
        $studentData['personalId'] = filterStateIdentifier($studentData['personalId']);
    } elseif (isset($studentData['stateIdentifier'])) {
        $studentData['stateIdentifier'] = filterStateIdentifier($studentData['stateIdentifier']);
    }

    // Configure the request body
    $payload =
        [
            'testAssignmentId' => $testAssignment['testAssignmentId'],
            'test' => [
                'testId' => $testAssignment['testBatteryId'],
                'testFormId' => $testAssignment['testBatteryFormId'],
                'testKey' => $testAssignment['testKey']
            ],
            'student' => $studentData
        ];
    if (strlen($testStatus) > 0)
        $payload['testStatus'] = $testStatus;
    if (strlen($enableLineReader) > 0)
        $payload['test']['enableLineReader'] = ($enableLineReader == 'Y') ? true : false;
    if (strlen($enableTextToSpeech) > 0)
        $payload['test']['enableTextToSpeech'] = ($enableTextToSpeech == 'Y') ? true : false;
    $payload = json_encode($payload);

    // Configure the request
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        "Content-Type: application/json; charset=UTF-8",
        "Authorization: Basic " . base64_encode(ADP_USERNAME . ":" . ADP_PASSWORD),
        "Authentication:" . $authentication
    ]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    logCallADP($logId, $fullURL, $payload);

    // Send the request
    $jsonResponse = curl_exec($ch);

    // Check the response
    if ($jsonResponse === false)
    {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CURL_ERROR_RESPONSE,
            'curl error code ' . $errorCode . ' : ' . $errorText);
        $errorLDR->logError(__FUNCTION__);
        curl_close($ch);
        return $errorLDR;
    }
    else
    {
        // Get the response
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // Check if the request was successful
        if ($status == 200)
        {
            return SUCCESS;
        }
        else
        {
            // Return the ADP error
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_ERROR_RESPONSE, $jsonResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
}

// filterStateIdentifier()
//    PATCH done as part of ADS-1381. PS does not support underscore within the stateIdentifier and the decision
//    was made to send the stateIdentifier to PS without the underscore and the trailing organization identifier.
//    The composite stateIdentifier for a walk-in student follows: stateIdentifier_organizationIdentifier
//
function filterStateIdentifier($stateIdentifier)
{
    $underscorePos = strpos($stateIdentifier, '_');
    if ($underscorePos > 0 && $underscorePos != false) {
        $stateIdentifier = substr($stateIdentifier, 0, $underscorePos);
    }

    return $stateIdentifier;
}
