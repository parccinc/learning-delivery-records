<?php
//
// LDR test results services
//
// exportItems()
// getEmbeddedScoreReport()
// storeTestResults()
// storeTestsResults()
//
// LDR test results support functions
//
// dbCreateTestResults()
// dbCreateTestResultsAssessmentItem()
// dbCreateTestResultsItemElementResponse()
// dbExportItems()
// dbStoreTestResults()

//
// LDR test results services
//
// exportItemElements()
//  Data Export of Item Elements and storing on S3 bucket
//
function exportItemElements($logId, PDO $db, $startDate, $endDate)
{
    $itemElementsData = dbExportItemElements($logId, $db, $startDate, $endDate);
    if ($itemElementsData instanceof ErrorLDR) {
        return $itemElementsData;
    }

    $selectedCols = [
        'Form Revision ID', 'Item ID', 'Item Element ID', 'QTI Class', 'QTI Max Choices', 'QTI Min Choices', 'Answer Key'
    ];
    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
    $reportName = ROOT_ORGANIZATION_NAME."-ItemElements-".$dateTimeInfo;
    $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                ."/".$reportName.".tsv";
    $itemElementsCSV = generateCSVData($logId, $itemElementsData->report, null, true, $selectedCols, "\t");
    if ($itemElementsCSV instanceof ErrorLDR) {
        return $itemElementsCSV;
    }

    $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $itemElementsCSV);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->logError(__FUNCTION__);
        return $reportUrl;
    }

    return $reportUrl;
}

// exportItemElementResponses()
//  Data Export of Item Element Responses and storing on S3 bucket
//
function exportItemElementResponses($logId, PDO $db, $startDate, $endDate)
{
    ini_set('memory_limit', DATA_EXPORT_MAX_MEMORY);
    $itemElementResponses = dbExportItemElementResponses($logId, $db, $startDate, $endDate);
    if ($itemElementResponses instanceof ErrorLDR) {
        return $itemElementResponses;
    }

    $selectedCols = ['Student Test Assignment ID', 'Form Revision ID', 'Item ID', 'Item Element ID'
                        , 'Response Identifier', 'Response Value'];
    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
    $reportName = ROOT_ORGANIZATION_NAME."-ItemElementResponses-".$dateTimeInfo;
    $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                ."/".$reportName.".tsv";
    $itemElementResponsesCSV = generateCSVData($logId, $itemElementResponses->report, null, true, $selectedCols, "\t");
    if ($itemElementResponsesCSV instanceof ErrorLDR) {
        return $itemElementResponsesCSV;
    }

    $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $itemElementResponsesCSV);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->logError(__FUNCTION__);
        return $reportUrl;
    }

    return $reportUrl;
}

// exportItemResults()
//  Data Export of Item Results and storing on S3 bucket
//
function exportItemResults($logId, PDO $db, $startDate, $endDate)
{
    ini_set('memory_limit', DATA_EXPORT_MAX_MEMORY);
    $itemResultsData = dbExportItemResults($logId, $db, $startDate, $endDate);
    if ($itemResultsData instanceof ErrorLDR) {
        return $itemResultsData;
    }

    $selectedCols = [
        'Student Test Assignment ID', 'Form Revision ID', 'Item ID', 'Part Number', 'Part Name', 'Item Position',
        'Item Score', 'Item Response Duration', 'Item Tools Used', 'Is Item Answered',
        'Is Item Scored', 'Is Item Correct'
    ];

    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
    $reportName = ROOT_ORGANIZATION_NAME."-ItemResults-".$dateTimeInfo;
    $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                ."/".$reportName.".tsv";
    $itemsCSV = generateCSVData($logId, $itemResultsData->report, null, true, $selectedCols, "\t");
    if ($itemsCSV instanceof ErrorLDR) {
        return $itemsCSV;
    }

    $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $itemsCSV);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->logError(__FUNCTION__);
        return $reportUrl;
    }

    return $reportUrl;
}

// exportItems()
//  Data Export of Items and storing on S3 bucket
//
function exportItems($logId, PDO $db, $startDate, $endDate)
{
    $itemsData = dbExportItems($logId, $db, $startDate, $endDate);
    if ($itemsData instanceof ErrorLDR) {
        return $itemsData;
    }

    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
    $reportName = ROOT_ORGANIZATION_NAME."-Items-".$dateTimeInfo;
    $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                ."/".$reportName.".tsv";
    $itemsCSV = generateCSVData($logId, $itemsData->report, null, true, null, "\t");
    if ($itemsCSV instanceof ErrorLDR) {
        return $itemsCSV;
    }

    $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $itemsCSV);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->logError(__FUNCTION__);
        return $reportUrl;
    }

    return $reportUrl;
}

// getEmbeddedScoreReport()
//  Return an embedded score report from the database
//
function getEmbeddedScoreReport($embeddedScoreReportId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the embeddedScoreReportId
    $argName = 'embeddedScoreReportId';
    $name = validateArg($logId, $db, [$argName => $embeddedScoreReportId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the embedded score report
    $sql = "SELECT embeddedScoreReport FROM embedded_score_report
            WHERE embeddedScoreReportId = '$embeddedScoreReportId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
        }
        else
        {
            // The embedded score report was not found
            $errorLDR = new ErrorLDR($logId, LDR_EC_EMBEDDED_SCORE_REPORT_NOT_FOUND,
                'Embedded score report not found for embeddedScoreReportId ' . $embeddedScoreReportId);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    catch(PDOException $error) {
        $errorLDR = errorQueryException($logId, $error, __FUNCTION__, $sql);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the embedded score report
    finishService($db, $logId, ['embeddedScoreReport' => $row[0]], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// storeTestResults()
//  Store the results from a test assignment
//
function storeTestResults()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'testAssignmentId' => DCO_REQUIRED
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testAssignmentId) = $args;

    // Get the test assignment
    $testAssignment = dbGetTestAssignment_ById($logId, $db, $testAssignmentId);
    if ($testAssignment instanceof ErrorLDR)
    {
        $testAssignment->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test results data references
    $adpTestAssignmentId = $testAssignment['adpTestAssignmentId'];
    $results = adpGetTestResults($logId, $adpTestAssignmentId);
    if ($results instanceof ErrorLDR)
    {
        $results->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    if (isset($results['testResultsId']))
        $adpTestResultsId = $results['testResultsId'];
    else
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_RESULTS_NO_RESULTS_ID,
            "testAssignmentId = $testAssignmentId, adpTestAssignmentId = $adpTestAssignmentId");
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Store the test results
    $result = dbStoreTestResults($logId, $db, $testAssignmentId, $adpTestAssignmentId, $adpTestResultsId);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// storeTestsResults()
//  Store the results from all submitted test assignments that have not already been stored
//
function storeTestsResults()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__, [
        'startDate' => DCO_OPTIONAL
        , 'endDate' => DCO_OPTIONAL
        , 'pageLimit' => DCO_OPTIONAL
        , 'testStatus' => DCO_OPTIONAL
    ]);
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $startDate, $endDate, $pageLimit, $testStatus) = $args;

    if (isset($startDate)) {
        if (isset($endDate)) {
            if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $startDate) !== 1) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INVALID_DATE_FORMAT, LDR_ED_INVALID_DATE_FORMAT.$startDate);
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            if (strtotime($startDate) > strtotime(date('Y-m-d'))) {
                $errorLDR = new ErrorLDR(
                    $logId,
                    LDR_EC_DATE_CANNOT_BE_FUTURE_DATE,
                    "Start date (".$startDate.") cannot be in the future"
                );
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $endDate) !== 1) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INVALID_DATE_FORMAT, LDR_ED_INVALID_DATE_FORMAT.$endDate);
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            if (strtotime($endDate) > strtotime(date('Y-m-d'))) {
                $errorLDR = new ErrorLDR(
                                    $logId,
                                    LDR_EC_DATE_CANNOT_BE_FUTURE_DATE,
                                    "End date (".$endDate.") cannot be in the future"
                                );
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            if ($startDate != $endDate) {
                // Check that the endDate is not before the startDate
                if (!datesInOrder($startDate, $endDate)) {
                    $errorLDR = new ErrorLDR(
                                        $logId,
                                        LDR_EC_END_DATE_BEFORE_START_DATE,
                                        LDR_ED_END_DATE_BEFORE_START_DATE
                                    );
                    $errorLDR->returnErrorJSON(__FUNCTION__);
                    $db = null;
                    return;
                }
            }
        } else {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_END_DATE, LDR_ED_MISSING_END_DATE);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    } elseif (isset($endDate)) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_START_DATE, LDR_ED_MISSING_START_DATE);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    if (isset($testStatus)) {
        $availableTestStatus = [
            'Submitted' => TEST_STATUS_ID_SUBMITTED,
            'InProgress' => TEST_STATUS_ID_INPROGRESS,
            'Paused' => TEST_STATUS_ID_PAUSED,
            'InProgress/Paused' => "'".TEST_STATUS_ID_INPROGRESS."','".TEST_STATUS_ID_PAUSED."'",
        ];

        if (array_key_exists($testStatus, $availableTestStatus) === false) {
            $errorLDR = new ErrorLDR($logId, '', '');
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        } else {
            $testStatus = $availableTestStatus[$testStatus];
        }
    } else {
        $testStatus = TEST_STATUS_ID_SUBMITTED;
    }

    // Get the test assignment
    $testAssignments = dbGetTestAssignmentsWithNoStoredResults(
        $logId,
        $db,
        $startDate,
        $endDate,
        $pageLimit,
        $testStatus
    );
    if ($testAssignments instanceof ErrorLDR) {
        $testAssignments->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    $result = new stdClass();
    $result->successTestAssignments = 0;
    $result->totalTestAssignments = count($testAssignments);
    $result->errors = [];

    set_time_limit(STORE_TESTS_RESULTS);

    $testIsSubmitted = ($testStatus == TEST_STATUS_ID_SUBMITTED) ? true : false;

    // Store the test results
    foreach ($testAssignments as $testAssignment) {
        $adpTestResults = adpGetTestResults($logId, $testAssignment['adpTestAssignmentId']);
        if ($adpTestResults instanceof ErrorLDR) {
            $result->errors[$testAssignment['testAssignmentId']] = $adpTestResults->getErrorArray();
            continue;
        }

        $adpTestResultsId = null;
        if (isset($adpTestResults['testResultsId'])) {
            $adpTestResultsId = $adpTestResults['testResultsId'];
        } else {
            $errorLDR = new ErrorLDR(
                $logId,
                LDR_EC_ADP_RESULTS_NO_RESULTS_ID,
                "testAssignmentId = ".$testAssignment['testAssignmentId'].", adpTestAssignmentId = ".
                    $testAssignment['adpTestAssignmentId']
            );
            $result->errors[$testAssignment['testAssignmentId']] = $errorLDR->getErrorArray();
            continue;
        }

        $db->beginTransaction();
        $statusOfStoreTestResults = dbStoreTestResults(
            $logId,
            $db,
            $testAssignment['testAssignmentId'],
            $testAssignment['adpTestAssignmentId'],
            $adpTestResultsId,
            $adpTestResults['lastResultsPath'],
            $testIsSubmitted
        );
        if ($statusOfStoreTestResults instanceof ErrorLDR) {
            $db->rollBack();
            $result->errors[$testAssignment['testAssignmentId']] = $statusOfStoreTestResults->getErrorArray();
            continue;
        }

        $db->commit();
        $result->successTestAssignments++;
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

//
// LDR test results support functions
//
// dbCreateTestResults()
//  Creates a test results record
//
function dbCreateTestResults($logId, PDO $db, $testFormRevisionId, $values)
{
    $colsAndVals = array_merge(['testFormRevisionId' => $testFormRevisionId], $values);
    $testResultsId = dbInsertWithBindParams($logId, $db, 'test_results', $colsAndVals);
    if ($testResultsId instanceof ErrorLDR) {
        $testResultsId->logError(__FUNCTION__);
    }

    return $testResultsId;
}

// dbCreateTestResultsItemElementResponse()
//  Creates a test results assessment item element response
//
function dbCreateTestResultsItemElementResponse($logId, PDO $db, $testFormRevisionId, $testResultsId,
                                                $testFormAssessmentItemId, $testResultsAssessmentItemId, $values)
{
    $colsAndVals = array_merge(
        [
            'testFormRevisionId' => $testFormRevisionId,
            'testFormAssessmentItemId' => $testFormAssessmentItemId,
            'testResultsId' => $testResultsId,
            'testResultsAssessmentItemId' => $testResultsAssessmentItemId,
        ],
        $values
    );
    $insertStatus = dbInsertWithBindParams($logId, $db, 'test_results_item_element_response', $colsAndVals);
    if ($insertStatus instanceof ErrorLDR) {
        $insertStatus->logError(__FUNCTION__);
        return $insertStatus;
    }

    return SUCCESS;
}

// dbCreateTestResultsAssessmentItem()
//  Creates a test results assessment item record
//
function dbCreateTestResultsAssessmentItem($logId, PDO $db, $testFormRevisionId, $testResultsId,
                                           $testFormAssessmentItemId, $values)
{
    $colsAndVals = array_merge(
        [
            'testFormRevisionId' => $testFormRevisionId,
            'testFormAssessmentItemId' => $testFormAssessmentItemId,
            'testResultsId' => $testResultsId
        ],
        $values
    );
    $testResultsAssessmentItemId = dbInsertWithBindParams($logId, $db, 'test_results_assessment_item', $colsAndVals);
    if ($testResultsAssessmentItemId instanceof ErrorLDR) {
        $testResultsAssessmentItemId->logError(__FUNCTION__);
    }

    return $testResultsAssessmentItemId;
}

// dbGetTestAssignmentsWithNoStoredResults
//   Get list of test assignment Ids and adp test assignment Ids
//
function dbGetTestAssignmentsWithNoStoredResults(
    $logId,
    PDO $db,
    $startDate = null,
    $endDate = null,
    $pageLimit = null,
    $testStatus = TEST_STATUS_ID_SUBMITTED
) {
    $timeFilter = '';
    if (isset($startDate) && isset($endDate)) {
        $timeFilter = "	AND DATE(FROM_UNIXTIME(lastQueueEventTime)) BETWEEN '".$startDate."' AND '".$endDate."' ";
    }

    $limit = '';
    if (isset($pageLimit)) {
        $limit = ' LIMIT '.$pageLimit;
    }

    $sql = "SELECT testAssignmentId, adpTestAssignmentId FROM test_assignment
            WHERE testStatusId IN (".$testStatus.") AND testResultsId IS NULL AND deleted = 'N'".$timeFilter."
            ORDER BY lastQueueEventTime".$limit.";";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testAssignments = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $testAssignments;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

function dbGetFinalResponses($dynamic, $results)
{
    // Check if the final response is in the results array
    if (array_key_exists('itemResponseFinal', $results) &&
        array_key_exists('value', $results['itemResponseFinal'])) {
        return $results['itemResponseFinal']['value'];
    }

    // Otherwise, check the score value
    $score = (array_key_exists('score', $dynamic)) ? $dynamic['score'] : null;
    if ($score !== null) {
        /**
        * There is a bug in the scoring library. This bug prevents LDR from
        * capturing the item element responses and successfully storing them.
        * The code below was modified to provide a patch for this bug until
        * this bug is being resolved in the scoring library iteself.
        *
        * Please refer to ticket ADS-1605 for more details.
        */
        $error = (array_key_exists('error', $score)) ? $score['error'] : 0;
        if ($error == 0) {
            if (array_key_exists('value', $score) && $score['value'] !== null) {
                return $score['value'];
            }
        }
    }

    // Otherwise, check the response value(s)
    $responses = (array_key_exists('response', $dynamic)) ? $dynamic['response'] : null;
    if ($responses !== null && is_array($responses)) {
        $responseCount = count($responses);
        if ($responseCount > 0) {
            // The last non-null response in the array is the final response
            $nullResponseKeys = array_keys($responses, null);
            $nonNullResponses = array_diff_key($responses, array_flip($nullResponseKeys));

            return end($nonNullResponses);
        }
    }

    return null;
}

function dbGetIsItemAnswered($dynamic, $results)
{
    // Check if the item answered boolean is in the results array
    if (array_key_exists('isItemAnswered', $results))
        return $results['isItemAnswered'];

    // Otherwise, figure it out by checking the responses
    $responses = (array_key_exists('response', $dynamic)) ? $dynamic['response'] : null;
    if ($responses !== null) {
        if (is_array($responses) && sizeof($responses) > 0) {
            $countNullOccurences = count(array_keys($responses, null));
            if ($countNullOccurences < count($responses)) {
                return true;
            }
        }
    }

    return false;
}

function dbGetIsItemScored($dynamic, $results)
{
    // Check if the item answered boolean is in the results array
    if (array_key_exists('isItemScored', $results))
        return $results['isItemScored'];

    // Otherwise, check the dynamic array
    $score = (array_key_exists('score', $dynamic)) ? $dynamic['score'] : null;
    if ($score !== null)
    {
        $value = (array_key_exists('value', $score)) ? $score['value'] : null;
        if ($value !== null)
            return true;
    }

    return false;
}

function dbGetItemScore($dynamic, $results)
{
    // Check if the item score is in the results array
    if (array_key_exists('itemScore', $results))
        return $results['itemScore'];

    // Otherwise, get the item score the hard way
    $score = (array_key_exists('score', $dynamic)) ? $dynamic['score'] : null;
    if ($score !== null)
    {
        $value = (array_key_exists('value', $score)) ? $score['value'] : null;
        if ($value !== null)
        {
            $SCORE = (array_key_exists('SCORE', $value)) ? $value['SCORE'] : null;
            if ($SCORE !== null)
            {
                $base = (array_key_exists('base', $SCORE)) ? $SCORE['base'] : null;
                if ($base !== null)
                {
                    $itemScore = (array_key_exists('float', $base)) ? $base['float'] : null;
                    if ($itemScore !== null)
                        return $itemScore;

                    $itemScore = (array_key_exists('integer', $base)) ? $base['integer'] : null;
                    if ($itemScore !== null)
                        return $itemScore;
                }
            }
        }
    }

    return null;
}

// dbDataExportItemElements()
//  Build the SQL query to export item elements
//
function dbDataExportItemElements($testStatus)
{
    $sql = "SELECT
                DISTINCT ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID'
                , CASE
                    WHEN @fri = ta.testFormRevisionId AND @ii = tfai.itemId AND @ri != tfie.responseIdentifier THEN @iei := @iei + 1
                    WHEN @fri = ta.testFormRevisionId AND @ii = tfai.itemId AND @ri = tfie.responseIdentifier THEN @iei := @iei
                    ELSE @iei := 1
                    END 'Item Element ID'
                , qc.qtiClass 'QTI Class', tfie.maxChoices 'QTI Max Choices', tfie.minChoices 'QTI Min Choices'
                , tficr.value 'Answer Key'
                , @ri := tfie.responseIdentifier 'responseIdentifier'
                , @ii := tfai.itemId 'itemId'
                , @fri := ta.testFormRevisionId 'testFormRevisionId'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                JOIN test_form_item_element tfie ON tfie.testFormAssessmentItemId = tfai.testFormAssessmentItemId
                JOIN qti_class qc ON qc.qtiClassId = tfie.qtiCLassId
                JOIN test_form_item_element_response tfier ON tfier.testFormItemElementId = tfie.testFormItemElementId
                LEFT JOIN test_form_item_correct_response tficr ON tficr.testFormItemElementResponseId = tfier.testFormItemElementResponseId
                JOIN (SELECT @iei := 0, @ri := 0, @ii := 0, @fri := 0) ids
            WHERE ta.deleted = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime;";
    return $sql;
}

// dbExportItemElements()
//  Getting Data Export Data for the Item Elements File
//
function dbExportItemElements($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT
                DISTINCT ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID'
                , CASE
                    WHEN @fri = ta.testFormRevisionId AND @ii = tfai.itemId AND @ri != tfie.responseIdentifier THEN @iei := @iei + 1
                    WHEN @fri = ta.testFormRevisionId AND @ii = tfai.itemId AND @ri = tfie.responseIdentifier THEN @iei := @iei
                    ELSE @iei := 1
                    END 'Item Element ID'
                , qc.qtiClass 'QTI Class', tfie.maxChoices 'QTI Max Choices', tfie.minChoices 'QTI Min Choices'
                , tficr.value 'Answer Key'
                , @ri := tfie.responseIdentifier 'responseIdentifier'
                , @ii := tfai.itemId 'itemId'
                , @fri := ta.testFormRevisionId 'testFormRevisionId'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                JOIN test_form_item_element tfie ON tfie.testFormAssessmentItemId = tfai.testFormAssessmentItemId
                JOIN qti_class qc ON qc.qtiClassId = tfie.qtiCLassId
                JOIN test_form_item_element_response tfier ON tfier.testFormItemElementId = tfie.testFormItemElementId
                LEFT JOIN test_form_item_correct_response tficr ON tficr.testFormItemElementResponseId = tfier.testFormItemElementResponseId
                JOIN (SELECT @iei := 0, @ri := 0, @ii := 0, @fri := 0) ids
            WHERE ta.deleted = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_ITEM_ELEMENTS_EXPORT_DATA, LDR_ED_MISSING_ITEM_ELEMENTS_EXPORT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $itemElementsData = new stdClass();
        $itemElementsData->report = null;
        $itemElementsData->rowsCount = 0;

        $rawItemElementsData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $itemElementsData->report = $rawItemElementsData;
        $itemElementsData->rowsCount = $stmt->rowCount();

        return $itemElementsData;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbDataExportItemElementResponses()
//  Build the SQL query to export item element responses
//
function dbDataExportItemElementResponses($testStatus)
{
    $sql = "SELECT
                ta.testAssignmentId 'Student Test Assignment ID', ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID'
                , CASE
                    WHEN @tai = ta.testAssignmentId AND @ii = tfai.itemId AND @ri != trier.responseIdentifier THEN @iei := @iei + 1
                    WHEN @tai = ta.testAssignmentId AND @ii = tfai.itemId AND @ri = trier.responseIdentifier THEN @iei := @iei
                    ELSE @iei := 1
                    END 'Item Element ID'
                , trier.responseIdentifier 'Response Identifier', trier.value 'Response Value'
                , @ri := trier.responseIdentifier 'responseIdentifier'
                , @ii := tfai.itemId 'itemId'
                , @tai := ta.testAssignmentId 'testAssignmentId'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                JOIN test_results_assessment_item trai ON (trai.testFormAssessmentItemId = tfai.testFormAssessmentItemId AND trai.testResultsId = ta.testResultsId)
                JOIN test_results_item_element_response trier ON trier.testResultsAssessmentItemId = trai.testResultsAssessmentItemId
                JOIN (SELECT @iei := 0, @ri := 0, @ii := 0, @tai := 0) ids
            WHERE ta.deleted = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime, tfp.testFormPartId, tfai.testFormAssessmentItemId;";
    return $sql;
}

// dbExportItemElementResponses()
//  Getting Data Export Data for the Item Element Responses File
//
function dbExportItemElementResponses($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT
                ta.testAssignmentId 'Student Test Assignment ID', ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID'
                , CASE
                    WHEN @tai = ta.testAssignmentId AND @ii = tfai.itemId AND @ri != trier.responseIdentifier THEN @iei := @iei + 1
                    WHEN @tai = ta.testAssignmentId AND @ii = tfai.itemId AND @ri = trier.responseIdentifier THEN @iei := @iei
                    ELSE @iei := 1
                    END 'Item Element ID'
                , trier.responseIdentifier 'Response Identifier', trier.value 'Response Value'
                , @ri := trier.responseIdentifier 'responseIdentifier'
                , @ii := tfai.itemId 'itemId'
                , @tai := ta.testAssignmentId 'testAssignmentId'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                JOIN test_results_assessment_item trai ON (trai.testFormAssessmentItemId = tfai.testFormAssessmentItemId AND trai.testResultsId = ta.testResultsId)
                JOIN test_results_item_element_response trier ON trier.testResultsAssessmentItemId = trai.testResultsAssessmentItemId
                JOIN (SELECT @iei := 0, @ri := 0, @ii := 0, @tai := 0) ids
            WHERE ta.deleted = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime, tfp.testFormPartId, tfai.testFormAssessmentItemId;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_ITEM_ELEMENT_RESPONSES_EXPORT_DATA, LDR_ED_MISSING_ITEM_ELEMENT_RESPONSES_EXPORT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $itemElementResponses = new stdClass();
        $itemElementResponses->report = null;
        $itemElementResponses->rowsCount = 0;

        $rawItemElementResponsesData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $itemElementResponses->report = $rawItemElementResponsesData;
        $itemElementResponses->rowsCount = $stmt->rowCount();

        return $itemElementResponses;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbDataExportItemResults()
//  Build the SQL query to export item results
//
function dbDataExportItemResults($testStatus)
{
    $sql = "SELECT
                ta.testAssignmentId 'Student Test Assignment ID', ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID'
                , CASE
                    WHEN @curPartId != tfp.testFormPartId AND @curTestAssignmentId = ta.testAssignmentId THEN @partNumber := @partNumber + 1
                    WHEN @curPartId = tfp.testFormPartId AND @curTestAssignmentId = ta.testAssignmentId THEN @partNumber := @partNumber
                    ELSE @partNumber := 1 END AS 'Part Number'
                , tfp.identifier 'Part Name'
                , CASE WHEN @curPartId = tfp.testFormPartId AND @curTestAssignmentId = ta.testAssignmentId THEN @itemPosition := @itemPosition + 1 ELSE @itemPosition := 1 END AS 'Item Position'
                , trai.itemScore 'Item Score', trai.timeSpent 'Item Response Duration', trai.toolsUsed 'Item Tools Used'
                , trai.isItemAnswered 'Is Item Answered', trai.isItemScored 'Is Item Scored', trai.isItemCorrect 'Is Item Correct'
                , @curPartId := tfp.testFormPartId
                , @curTestAssignmentId := ta.testAssignmentId
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                JOIN test_results_assessment_item trai ON (trai.testFormAssessmentItemId = tfai.testFormAssessmentItemId AND trai.testResultsId = ta.testResultsId)
                JOIN (SELECT @curTestAssignmentId := 0, @curPartId := 0, @partNumber := 0, @itemPosition := 0) AS curIds
            WHERE ta.deleted = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime, tfp.testFormPartId, tfai.testFormAssessmentItemId;";
    return $sql;
}

// dbExportItemResults()
//  Getting Data Export Data for the Item Results File
//
function dbExportItemResults($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT
                ta.testAssignmentId 'Student Test Assignment ID', ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID'
                , CASE
                    WHEN @curPartId != tfp.testFormPartId AND @curTestAssignmentId = ta.testAssignmentId THEN @partNumber := @partNumber + 1
                    WHEN @curPartId = tfp.testFormPartId AND @curTestAssignmentId = ta.testAssignmentId THEN @partNumber := @partNumber
                    ELSE @partNumber := 1 END AS 'Part Number'
                , tfp.identifier 'Part Name'
                , CASE WHEN @curPartId = tfp.testFormPartId AND @curTestAssignmentId = ta.testAssignmentId THEN @itemPosition := @itemPosition + 1 ELSE @itemPosition := 1 END AS 'Item Position'
                , trai.itemScore 'Item Score', trai.timeSpent 'Item Response Duration', trai.toolsUsed 'Item Tools Used'
                , trai.isItemAnswered 'Is Item Answered', trai.isItemScored 'Is Item Scored', trai.isItemCorrect 'Is Item Correct'
                , @curPartId := tfp.testFormPartId
                , @curTestAssignmentId := ta.testAssignmentId
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                JOIN test_results_assessment_item trai ON (trai.testFormAssessmentItemId = tfai.testFormAssessmentItemId AND trai.testResultsId = ta.testResultsId)
                JOIN (SELECT @curTestAssignmentId := 0, @curPartId := 0, @partNumber := 0, @itemPosition := 0) AS curIds
            WHERE ta.deleted = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime, tfp.testFormPartId, tfai.testFormAssessmentItemId;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_ITEM_RESULTS_EXPORT_DATA, LDR_ED_MISSING_ITEM_RESULTS_EXPORT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $itemResultsData = new stdClass();
        $itemResultsData->report = null;
        $itemResultsData->rowsCount = 0;

        $rawItemResultsData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $itemResultsData->report = $rawItemResultsData;
        $itemResultsData->rowsCount = $stmt->rowCount();

        return $itemResultsData;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbDataExportItems()
//  Build the SQL query to export items
//
function dbDataExportItems($testStatus)
{
    $sql = "SELECT
                DISTINCT ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID', tfai.uin 'UIN', tfai.itemGrade 'Grade'
                , IFNULL(tfai.maxScorePoints, tm.maxPoints) 'Max Score Points'
                , ct.calculatorType 'Calculator'
                , tfai.domain 'Domain', tfai.cluster 'Cluster', tfai.evidenceStatement 'Evidence Statement'
                , tfai.contentStandard 'Content Standard', tfai.itemStrand 'Item Strand', tfai.fluencySkillArea 'Fluency Skill Area'
                , tfai.passageType 'Passage Type', tfai.subclaim 'Subclaim'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                LEFT JOIN calculator_type ct ON ct.calculatorTypeId = tfai.calculatorTypeId
                LEFT JOIN test_map tm ON tm.itemId = tfai.itemId
            WHERE ta.deleted = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime;";
    return $sql;
}

// dbExportItems()
//  Getting Data Export Data for the Items File
//
function dbExportItems($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT
                DISTINCT ta.testFormRevisionId 'Form Revision ID', tfai.itemId 'Item ID', tfai.uin 'UIN', tfai.itemGrade 'Grade'
                , IFNULL(tfai.maxScorePoints, tm.maxPoints) 'Max Score Points'
                , ct.calculatorType 'Calculator'
                , tfai.domain 'Domain', tfai.cluster 'Cluster', tfai.evidenceStatement 'Evidence Statement'
                , tfai.contentStandard 'Content Standard', tfai.itemStrand 'Item Strand', tfai.fluencySkillArea 'Fluency Skill Area'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
                JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_part tfp ON tfp.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_form_assessment_section tfas ON tfas.testFormPartId = tfp.testFormPartId
                JOIN test_form_assessment_item tfai ON tfai.testFormAssessmentSectionId = tfas.testFormAssessmentSectionId
                LEFT JOIN calculator_type ct ON ct.calculatorTypeId = tfai.calculatorTypeId
                LEFT JOIN test_map tm ON tm.itemId = tfai.itemId
            WHERE ta.deleted = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_ITEMS_EXPORT_DATA, LDR_ED_MISSING_ITEMS_EXPORT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $itemsData = new stdClass();
        $itemsData->report = null;
        $itemsData->rowsCount = 0;

        $rawItemsData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $itemsData->report = $rawItemsData;
        $itemsData->rowsCount = $stmt->rowCount();

        return $itemsData;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbStoreTestResults()
//  Store the results from a test assignment
//
function dbStoreTestResults(
    $logId,
    PDO $db,
    $testAssignmentId,
    $adpTestAssignmentId,
    $adpTestResultsId,
    $s3Key = null,
    $testIsSubmitted = true
) {
    if (!isset($s3Key)) {
        // Build the S3 key
        $s3Key = ADP_S3_RESULTS_ROOT . strrev($adpTestAssignmentId) . '/' . strrev($adpTestResultsId) . '.submitted.' .
                    TENANT_ID . '.json';
    } else {
        $s3Key = ADP_S3_RESULTS_ROOT . $s3Key;
    }

    // Initialize the error message suffix
    $errMsgRoot = ' : testAssignmentId = ' . $testAssignmentId . ', s3Key = |' . $s3Key . '|';

    // Get the test result file contents
    $contents = awsGetS3JsonFileContents($logId, ADP_S3_RESULTS_BUCKET, $s3Key);
    if ($contents instanceof ErrorLDR)
    {
        logHAL($logId, HAL_SEVERITY_ERROR, $contents->getErrorCode(), $contents->getErrorDetail() . $errMsgRoot);
        $contents->logError(__FUNCTION__);
        return $contents;
    }

    // Get the test results
    $testResults = json_decode($contents, true);
    if ($testResults === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_DECODE_FAILED,
                                 LDR_ED_TEST_RESULTS_DECODE_FAILED . $errMsgRoot);
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Get the embedded score report
    if (array_key_exists('dynamic', $testResults) && array_key_exists('html', $testResults['dynamic']))
    {
        $embeddedScoreReport = $testResults['dynamic']['html'];
        if (strlen($embeddedScoreReport) > 0)
        {
            // Check that the score report can be decoded
            $scoreReport = base64_decode($embeddedScoreReport);
            if ($scoreReport === false)
            {
                // Log the error but continue on
                $errorLDR = new ErrorLDR($logId, LDR_EC_SCORE_REPORT_DECODE_FAILED,
                                         LDR_ED_SCORE_REPORT_DECODE_FAILED . $errMsgRoot);
                $errorLDR->logError(__FUNCTION__);
            }
            else
            {
                // Store the embedded score report
                $sql = "INSERT INTO embedded_score_report (embeddedScoreReport) VALUES ('$embeddedScoreReport')";
                try {
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                    $embeddedScoreReportId = $db->lastInsertId();

                    $sql = "UPDATE test_assignment SET embeddedScoreReportId = '$embeddedScoreReportId'" .
                           " WHERE testAssignmentId = '$testAssignmentId'";
                    $stmt = $db->prepare($sql);
                    $stmt->execute();
                }
                catch(PDOException $error) {
                    return errorQueryException($logId, $error, __FUNCTION__, $sql);
                }
            }
        }
    }

    // Get the test results data references from ADP Publisher
    $results = adpGetTestResults($logId, $adpTestAssignmentId);
    if ($results instanceof ErrorLDR)
    {
        $results->logError(__FUNCTION__);
        return $results;
    }
    if (isset($results['testFormRevisionId']))
        $testFormRevisionId = $results['testFormRevisionId'];
    else
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_RESULTS_NO_REVISION_ID,
                                 "testAssignmentId = $testAssignmentId, adpTestAssignmentId = $adpTestAssignmentId");
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if the test form revision needs to be stored
    if (STORE_TEST_FORM_REVISIONS)
    {
        $testFormRevision = dbGetTestFormRevision($logId, $db, $testFormRevisionId);
        if ($testFormRevision instanceof ErrorLDR)
        {
            // It is not an error if the test form revision was not found
            if ($testFormRevision->getErrorCode() != LDR_EC_TEST_FORM_REVISION_ID_NOT_FOUND)
            {
                $testFormRevision->logError(__FUNCTION__);
                return $testFormRevision;
            }

            // Store the test form revision
            $result = dbStoreTestFormRevision($logId, $db, $testFormRevisionId);
            if ($result instanceof ErrorLDR)
            {
                $result->logError(__FUNCTION__);
                return $result;
            }
        }
    }

    // Check if the test results need to be stored
    if (STORE_TEST_RESULTS)
    {
        // Get the test form revision assessment item record IDs
        $testFormAssessmentItemIds = dbGetTestFormAssementItemIds($logId, $db, $testFormRevisionId);
        if ($testFormAssessmentItemIds instanceof ErrorLDR)
        {
            logHAL($logId, HAL_SEVERITY_ERROR, $testFormAssessmentItemIds->getErrorCode(),
                   $testFormAssessmentItemIds->getErrorDetail() . $errMsgRoot);
            $testFormAssessmentItemIds->logError(__FUNCTION__);
            return $testFormAssessmentItemIds;
        }

        if (ITEM_CORRECT_BY_MAX_POINTS) {
            // Get Max Scoring Points
            $itemMaxPoints = dbGetTestFormAssessmentItemMaxPoints($logId, $db, $testFormAssessmentItemIds);
            if ($itemMaxPoints instanceof ErrorLDR) {
                logHAL($logId, HAL_SEVERITY_ERROR, $itemMaxPoints->getErrorCode(),
                       $itemMaxPoints->getErrorDetail() . $errMsgRoot);
                $itemMaxPoints->logError(__FUNCTION__);
                return $itemMaxPoints;
            }

            if (count($itemMaxPoints) !== count($testFormAssessmentItemIds)) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_ITEMS_MAX_POINTS, LDR_ED_MISSING_ITEMS_MAX_POINTS.$errMsgRoot);
                logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(),
                       $errorLDR->getErrorDetail() . $errMsgRoot);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        }

        // Get the test-level results
        if ($testIsSubmitted === true) {
            if (array_key_exists('dynamic', $testResults)
                && array_key_exists('results', $testResults['dynamic'])
            ) {
                $results = $testResults['dynamic']['results'];
            } else {
                $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_TEST_LEVEL_RESULTS_NOT_FOUND,
                                         LDR_ED_TEST_RESULTS_TEST_LEVEL_RESULTS_NOT_FOUND . $errMsgRoot);
                logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        } else {
            $results = [];
        }

        // Create the test results record
        $values = [];
        $timeStamp = (array_key_exists('testStartDateTime', $results)) ? $results['testStartDateTime'] : null;
        $values['testStartDateTime'] = ($timeStamp !== null) ? date("Y-m-d H:i:s", substr($timeStamp, 0, 10)) : null;
        $timeStamp = (array_key_exists('testSubmitDateTime', $results)) ? $results['testSubmitDateTime'] : null;
        $values['testSubmitDateTime'] = ($timeStamp !== null) ? date("Y-m-d H:i:s", substr($timeStamp, 0, 10)) : null;
        $values['timeSpentOnItems'] = (array_key_exists('timeSpentOnItems', $results)) ?
                                       $results['timeSpentOnItems'] : null;
        $values['timeSpentOnAllItems'] = (array_key_exists('timeSpentOnAllItems', $results)) ?
                                          $results['timeSpentOnAllItems'] : null;
        $values['testDuration'] = (array_key_exists('testDuration', $results)) ?
                                   $results['testDuration'] : null;
        $values['totalItemsPresented'] = (array_key_exists('totalItemsPresented', $results)) ?
                                          $results['totalItemsPresented'] : null;
        $values['totalQuestionsPresented'] = (array_key_exists('totalQuestionsPresented', $results)) ?
                                              $results['totalQuestionsPresented'] : null;
        $values['totalItemsAnsweredCorrectly'] = (array_key_exists('totalItemsAnsweredCorrectly', $results)) ?
                                                  $results['totalItemsAnsweredCorrectly'] : null;
        $values['totalItemsAnsweredIncorrectly'] = (array_key_exists('totalItemsAnsweredIncorrectly', $results)) ?
                                                    $results['totalItemsAnsweredIncorrectly'] : null;
        $values['totalItemsNotAnswered'] = (array_key_exists('totalItemsNotAnswered', $results)) ?
                                            $results['totalItemsNotAnswered'] : null;
        $values['totalItemsNotScored'] = (array_key_exists('totalItemsNotScored', $results)) ?
                                          $results['totalItemsNotScored'] : null;
        $values['testRawScore'] = (array_key_exists('testRawScore', $results)) ?
                                   $results['testRawScore'] : null;
        $values['testScoreTheta'] = (array_key_exists('testScoreTheta', $results)) ?
                                     $results['testScoreTheta'] : null;
        $values['testScoreScale'] = (array_key_exists('testScoreScale', $results)) ?
                                     $results['testScoreScale'] : null;
        $testResultsId = dbCreateTestResults($logId, $db, $testFormRevisionId, $values);
        if ($testResultsId instanceof ErrorLDR)
        {
            logHAL($logId, HAL_SEVERITY_ERROR, $testResultsId->getErrorCode(),
                   $testResultsId->getErrorDetail() . $errMsgRoot);
            $testResultsId->logError(__FUNCTION__);
            return $testResultsId;
        }

        // Update the test assignment testResultsId and testFormRevisionId values
        $result = dbUpdateTestAssignmentResults($logId, $db, $testAssignmentId, $testResultsId, $testFormRevisionId);
        if ($result instanceof ErrorLDR)
        {
            logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(), $result->getErrorDetail() . $errMsgRoot);
            $result->logError(__FUNCTION__);
            return $result;
        }

        // Get the test results part(s)
        $testParts = (array_key_exists('testPart', $testResults)) ? $testResults['testPart'] : null;
        if ($testParts === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_TEST_PARTS_NOT_FOUND,
                                     LDR_ED_TEST_RESULTS_TEST_PARTS_NOT_FOUND . $errMsgRoot);
            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $testPartIdx = 0;
        foreach ($testParts as $testPart)
        {
            $errMsgTestPart = $errMsgRoot . ', testPart ' . $testPartIdx++;

            // Get the test results assessment section(s)
            $assessmentSections = (array_key_exists('assessmentSection', $testPart)) ?
                                   $testPart['assessmentSection'] : null;
            if ($assessmentSections === null)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ASSESSMENT_SECTIONS_NOT_FOUND,
                                         LDR_ED_TEST_RESULTS_ASSESSMENT_SECTIONS_NOT_FOUND . $errMsgTestPart);
                logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
            $assessmentSectionIdx = 0;
            foreach ($assessmentSections as $assessmentSection)
            {
                $errMsgAssessmentSection = $errMsgTestPart . ', assessmentSection ' . $assessmentSectionIdx++;

                // Get the test results assessment item(s)
                $assessmentItems = (array_key_exists('assessmentItem', $assessmentSection)) ?
                                    $assessmentSection['assessmentItem'] : null;
                if ($assessmentItems === null)
                {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ASSESSMENT_ITEMS_NOT_FOUND,
                                             LDR_ED_TEST_RESULTS_ASSESSMENT_ITEMS_NOT_FOUND . $errMsgAssessmentSection);
                    logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
                $assessmentItemIdx = 0;
                foreach ($assessmentItems as $assessmentItem)
                {
                    $errMsgAssessmentItem = $errMsgAssessmentSection . ', assessmentItem ' . $assessmentItemIdx++;

                    // Check that there is a dynamic element
                    $dynamic = (array_key_exists('dynamic', $assessmentItem)) ? $assessmentItem['dynamic'] : null;
                    if ($dynamic === null)
                    {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_DYNAMIC_NOT_FOUND,
                                                 LDR_ED_TEST_RESULTS_ITEM_DYNAMIC_NOT_FOUND . $errMsgAssessmentItem);
                        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    // If this is a system item then skip it
                    $systemItemNavProperties = (array_key_exists('systemItemNavProperties', $dynamic)) ?
                                                $dynamic['systemItemNavProperties'] : null;
                    if ($systemItemNavProperties !== null)
                        continue;
$testIsSubmitted = false;
                    $itemInternalIdentifier = null;
                    if ($testIsSubmitted === true) {
                        // Check that there is a results element
                        $results = (array_key_exists('results', $dynamic)) ? $dynamic['results'] : null;
                        if ($results === null)
                        {
                            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_RESULTS_NOT_FOUND,
                                                LDR_ED_TEST_RESULTS_ITEM_RESULTS_NOT_FOUND . $errMsgAssessmentItem);
                            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                            $errorLDR->logError(__FUNCTION__);
                            return $errorLDR;
                        }

                        // Check that there is an itemInternalIdentifier element
                        $itemInternalIdentifier = (array_key_exists('itemInternalIdentifier', $results)) ?
                        $results['itemInternalIdentifier'] : null;
                        if ($itemInternalIdentifier === null)
                        {
                            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_NOT_FOUND,
                                                LDR_ED_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_NOT_FOUND . $errMsgAssessmentItem);
                            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                            $errorLDR->logError(__FUNCTION__);
                            return $errorLDR;
                        }

                        // Get the corresponding test form revision assessment item record ID
                        $testFormAssessmentItemId = (array_key_exists($itemInternalIdentifier, $testFormAssessmentItemIds)) ?
                                                        $testFormAssessmentItemIds[$itemInternalIdentifier] : null;
                        if ($testFormAssessmentItemId === null)
                        {
                            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_MISMATCH,
                                                LDR_ED_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_MISMATCH . $errMsgAssessmentItem);
                            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                            $errorLDR->logError(__FUNCTION__);
                            return $errorLDR;
                        }
                    } else {
                        $results = [];
                        /**
                         * The internal identifier is missing in case of InProgress and Paused. Therefore, the
                         * testAssignmentItemId is acquired based on the assumption that item Ids are stored in order
                         */
                        // $testFormAssessmentItemId = current($testFormAssessmentItemIds);
                        $itemInternalIdentifier = current(array_flip($testFormAssessmentItemIds));
                        $testFormAssessmentItemId = array_shift($testFormAssessmentItemIds);
                    }

                    // Create the test results assessment item record
                    $values = [];
                    $values['timeSpent'] = (array_key_exists('timeSpent', $results)) ? $results['timeSpent'] : null;
                    if ($values['timeSpent'] === null)
                        $values['timeSpent'] = 0;
                    $values['itemScore'] = dbGetItemScore($dynamic, $results);
                    if ($values['itemScore'] === null)
                        $values['itemScore'] = 0;
                    $values['toolsUsed'] = (array_key_exists('toolsUsed', $results)) ? $results['toolsUsed'] : null;
                    $values['isItemCorrect'] =
                                   (array_key_exists('isItemCorrect', $results)) ? $results['isItemCorrect'] : null;
                    if (ITEM_CORRECT_BY_MAX_POINTS && $itemInternalIdentifier !== false && $itemInternalIdentifier !== null) {
                        // Get the corresponding test form revision assessment item record ID
                        $maxPoints = (array_key_exists($itemInternalIdentifier, $itemMaxPoints)) ?
                                            $itemMaxPoints[$itemInternalIdentifier] : null;
                        if ($maxPoints === null) {
                            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_MISMATCH,
                               LDR_ED_TEST_RESULTS_ITEM_RESULTS_IDENTIFIER_MISMATCH . $errMsgAssessmentItem);
                            logHAL(
                               $logId,
                               HAL_SEVERITY_ERROR,
                               $errorLDR->getErrorCode(),
                               $errorLDR->getErrorDetail()
                            );
                            $errorLDR->logError(__FUNCTION__);
                            return $errorLDR;
                        }
                        $values['isItemCorrect'] = ($values['itemScore'] == $maxPoints) ? "Y" : "N";
                    } elseif ($values['isItemCorrect'] !== null && !ITEM_CORRECT_BY_MAX_POINTS) {
                       $values['isItemCorrect'] = ($values['isItemCorrect']) ? 'Y' : 'N';
                    } else {
                        $values['isItemCorrect'] = 'N';
                    }

                    $values['isItemAnswered'] = dbGetIsItemAnswered($dynamic, $results);
                    if ($values['isItemAnswered'] !== null)
                        $values['isItemAnswered'] = ($values['isItemAnswered']) ? 'Y' : 'N';
                    $values['isItemScored'] = dbGetIsItemScored($dynamic, $results);
                    if ($values['isItemScored'] !== null)
                        $values['isItemScored'] = ($values['isItemScored']) ? 'Y' : 'N';
                    $testResultsAssessmentItemId = dbCreateTestResultsAssessmentItem($logId, $db, $testFormRevisionId,
                                                                  $testResultsId, $testFormAssessmentItemId, $values);
                    if ($testResultsAssessmentItemId instanceof ErrorLDR)
                    {
                        logHAL($logId, HAL_SEVERITY_ERROR, $testResultsAssessmentItemId->getErrorCode(),
                               $testResultsAssessmentItemId->getErrorDetail() . $errMsgAssessmentItem);
                        $testResultsAssessmentItemId->logError(__FUNCTION__);
                        return $testResultsAssessmentItemId;
                    }

                    // Check if there are any final responses
                    $responses = dbGetFinalResponses($dynamic, $results);
                    if (!is_array($responses))
                        continue;

                    // Store the final response(s)
                    //
                    // If an item has a single element AND that element response can have multiple values AND the
                    // item element is not answered THEN $responses will be null so this foreach loop is bypassed.
                    //
                    // The scenario of an item that has multiple elements, one of which is as above but where there
                    // also are answered elements is not being handled and will result in an error because no such item
                    // exists in the tests created thus far nor has the requirement been defined.
                    //
                    foreach ($responses as $identifier => $response)
                    {
                        // Skip the 'SCORE' element if there is one
                        if ($identifier == 'SCORE')
                            continue;

                        // The 'base' key indicates this response can have only one value
                        if (array_key_exists('base', $response))
                        {
                            $values = [];
                            $values['responseIdentifier'] = $identifier;

                            // If the 'base' value is not an array then this item element was not answered
                            if (is_array($response['base'])) {
                                $values['value'] = array_shift($response['base']);
                            } else {
                                $values['value'] = null;
                            }

                            $result = dbCreateTestResultsItemElementResponse($logId, $db, $testFormRevisionId,
                                        $testResultsId, $testFormAssessmentItemId, $testResultsAssessmentItemId
                                        , $values);
                            if ($result instanceof ErrorLDR) {
                                logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(),
                                $result->getErrorDetail() . $errMsgRoot);
                                $result->logError(__FUNCTION__);
                                return $result;
                            }
                        }
                        elseif (array_key_exists('list', $response) && is_array($response['list']))
                        {
                            $list = $response['list'];

                            // The 'list' key indicates this response can have multiple values
                            if (array_key_exists('identifier', $list) && is_array($list['identifier']))
                            {
                                // choiceInteraction identifiers
                                foreach ($list['identifier'] as $value)
                                {
                                    $values = [];
                                    $values['value'] = $value;
                                    $values['responseIdentifier'] = $identifier;
                                    $result = dbCreateTestResultsItemElementResponse($logId, $db, $testFormRevisionId,
                                        $testResultsId, $testFormAssessmentItemId, $testResultsAssessmentItemId, $values);
                                    if ($result instanceof ErrorLDR)
                                    {
                                        logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(),
                                            $result->getErrorDetail() . $errMsgAssessmentItem);
                                        $result->logError(__FUNCTION__);
                                        return $result;
                                    }
                                }
                            }
                            elseif (array_key_exists('directedPair', $list) && is_array($list['directedPair']))
                            {
                                // matchInteraction simpleAssociableChoice pairs
                                foreach ($list['directedPair'] as $directedPair)
                                {
                                    if (!is_array($directedPair) || count($directedPair) != 2)
                                    {
                                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_RESULTS_DIRECTED_PAIR,
                                            LDR_ED_TEST_RESULTS_ITEM_RESULTS_DIRECTED_PAIR . $errMsgAssessmentItem);
                                        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(),
                                            $errorLDR->getErrorDetail());
                                        $errorLDR->logError(__FUNCTION__);
                                        return $errorLDR;
                                    }
                                    $values = [];
                                    $values['value'] = $directedPair[0] . ' ' . $directedPair[1];
                                    $values['responseIdentifier'] = $identifier;
                                    $result = dbCreateTestResultsItemElementResponse($logId, $db, $testFormRevisionId,
                                        $testResultsId, $testFormAssessmentItemId, $testResultsAssessmentItemId, $values);
                                    if ($result instanceof ErrorLDR)
                                    {
                                        logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(),
                                            $result->getErrorDetail() . $errMsgAssessmentItem);
                                        $result->logError(__FUNCTION__);
                                        return $result;
                                    }
                                }
                            }
                            else
                            {
                                $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_RESULTS_RESPONSE_UNKNOWN,
                                    LDR_ED_TEST_RESULTS_ITEM_RESULTS_RESPONSE_UNKNOWN . $errMsgRoot);
                                logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                                $errorLDR->logError(__FUNCTION__);
                                return $errorLDR;
                            }
                        }
                        else
                        {
                            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_RESULTS_ITEM_RESULTS_RESPONSE_UNKNOWN,
                                                     LDR_ED_TEST_RESULTS_ITEM_RESULTS_RESPONSE_UNKNOWN . $errMsgRoot);
                            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                            $errorLDR->logError(__FUNCTION__);
                            return $errorLDR;
                        }
                    }
                }
            }
        }
    }

    return SUCCESS;
}
