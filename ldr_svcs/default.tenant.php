<?php
/**
 * Copy this file to tenant.php and add the each tenants' URL as separate
 * case in the case statement below. URL should be like: http://HOSTNAME
 * If you are specifying the port number in the URL, then it has to exist in
 * the full URL
 */

// Select the REST framework
$restFramework = REST_FRAMEWORK_SLIM;
//$restFramework = REST_FRAMEWORK_PHALCON;

define('DISABLED_IN_PRODUCTION', true); // MUST BE TRUE for PROD

$requestScheme = "https";
if (isset($_SERVER['REQUEST_SCHEME']) === true) {
    $requestScheme = $_SERVER['REQUEST_SCHEME'];
} else {
    $requestScheme = (strcmp($_SERVER['HTTPS'], 'on') === 0 ? "https":"http");
}

$curServerHost = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'];
switch ($curServerHost) {
    case 'FULL URL GOES HERE':
        require_once TENANT_DIR."/<TENANT_NAME>.config.php";
        break;
    default:
        // Generate unique log identifier
        $logId = generateIdentifier(4);
        $errorLDR = new ErrorLDR($logId, LDR_EC_UNAUTHORIZED_TENANT, LDR_ED_UNAUTHORIZED_TENANT.$curServerHost);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        exit(1);
        break;
}
