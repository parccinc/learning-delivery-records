<?php
//
// LDR test form services
//
// createTestMap()
// exportForms()
// getDetailedActivityReport()
// getTestFormRevision()
// storeTextEntryInteractionAnswerKey()
//
// LDR test form support functions
//
// dbCreateTestFormAssessmentItem()
// dbCreateTestFormAssessmentSection()
// dbCreateTestFormItemCorrectResponse()
// dbCreateTestFormItemElement()
// dbCreateTestFormItemElementResponse()
// dbCreateTestFormPart()
// dbCreateTestFormRevision()
// dbCreateTestFormSystemItem()
// dbCreateTestMap()
// dbDeleteTestFormRevision()
// dbExprotForms()
// dbGetTestFormAssementItemIds()
// dbGetTestFormAssessmentItemMaxPoints()
// dbGetDetailedActivityReport()
// dbGetTestFormRevision()
// dbGetTestFormRevisionDetails()
// dbStoreMissingTextEntryInteractionsAnswerKey()
// dbStoreTestFormAssessmentItem()
// dbStoreTestFormRevision()
// dbStoreTestFormSystemItem()

//
// LDR test form services
//
// createTestMap()
//  Add content of test map to the DB
//
function createTestMap()
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__, ['testMapContent' => DCO_REQUIRED]);
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testMapContent) = $args;

    // /**
    //  * for testing only
    //  */
    // $testContent = json_decode(file_get_contents('/tmp/tmp.json'));
    // $testMapContent = $testContent->testMapContent;

    $expectedTestMapArgs = [
        'taoId' => DCO_REQUIRED,
        'itemId' => DCO_REQUIRED,
        'maxPoints' => DCO_REQUIRED,
        'qtiInteraction' => DCO_OPTIONAL,
        'cardinality' => DCO_OPTIONAL,
        'gradeLevel' => DCO_OPTIONAL,
        'scoringStatus' => DCO_OPTIONAL,
    ];

    $testMapContent = explode(PHP_EOL, trim($testMapContent));

    // Extract Headers first
    $rawHeaders = explode(',', current($testMapContent));
    unset($testMapContent[0]);

    $results = new stdClass();
    $results->errors = [];
    $results->count = 0;

    $testMaps = [];

    // Proceed with test map content
    foreach ($testMapContent as $rowId => $testMap) {
        $testMap = explode(',', $testMap);

        if (count($rawHeaders) != count($testMap)) {
            $errorLDR = new ErrorLDR(
                                $logId,
                                LDR_EC_MISSING_INPUT_DATA,
                                "Row $rowId should have ".count($rawHeaders).' elements, only '.count($testMap).
                                    ' is given'
                            );
            $errorLDR->logError(__FUNCTION__);
            $results->errors[$rowId][] = $errorLDR->getErrorArray();
            continue;
        }

        $testMap = array_intersect_key(array_combine($rawHeaders, $testMap), $expectedTestMapArgs);
        if (count($testMap) != count($expectedTestMapArgs)) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_INPUT_DATA, "Row $rowId is missing data");
            $errorLDR->logError(__FUNCTION__);
            $results->errors[$rowId][] = $errorLDR->getErrorArray();
            continue;
        }

        $tmpTestMap = [];
        $references2get = [
            'cardinality' => ['colName' => 'responseCardinalityId', 'table' => 'response_cardinality'],
            'qtiInteraction' => ['colName' => 'qtiClassId', 'table' => 'qti_class'],
        ];
        foreach ($expectedTestMapArgs as $argName => $argDco) {
            $argFilterName = $argName;

            $argValue = validateArg($logId, $db, $testMap, $argName, $argFilterMap[$argFilterName], $argDco);
            if ($argValue instanceof ErrorLDR) {
                $argValue->logError(__FUNCTION__);
                $results->errors[$rowId][] = $argValue->getErrorArray();
                continue;
            }

            if (array_key_exists($argName, $references2get) && isset($argValue)) {
                $tmpReferenceId = dbGetReferenceValueId($logId, $db, $references2get[$argName]['table'], $argValue);
                if ($tmpReferenceId instanceof ErrorLDR) {
                    $tmpReferenceId->logError(__FUNCTION__);
                    $results->errors[$rowId][] = $tmpReferenceId;
                    continue;
                }
                $tmpTestMap[$references2get[$argName]['colName']] = $tmpReferenceId;
            } else {
                $tmpTestMap[$argName] = $argValue;
            }
        }

        $testMaps[] = $tmpTestMap;
    }
    $results->count = count($testMaps);

    // Data Integrity Checking
    $cols4DupChecking = ['taoId', 'itemId'];
    foreach ($cols4DupChecking as $col) {
        $colVals = array_column($testMaps, $col);
        if ($results->count != count(array_unique($colVals))) {
            $dupVals = array_diff_assoc($colVals, array_unique($colVals));
            $errorLDR = new ErrorLDR(
                $logId,
                LDR_EC_DUPLICATE_VALUE,
                "Column $col contains duplicate data. Duplicate value(s): (".implode(', ', array_unique($dupVals)).")"
            );
            $errorLDR->logError(__FUNCTION__);
            $results->errors[] = $errorLDR->getErrorArray();
        }
    }

    if (count($results->errors) === 0) {
        // Insert test map to the DB
        $testMapsCount = dbCreateTestMap($logId, $db, $testMaps);
        if ($testMapsCount instanceof ErrorLDR) {
            $testMapsCount->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Return the result
    finishService($db, $logId, $results, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// exportForms()
//  Data Export of Forms and storing on S3 bucket
//
function exportForms($logId, PDO $db, $startDate, $endDate)
{
    $formsData = dbExportForms($logId, $db, $startDate, $endDate);
    if ($formsData instanceof ErrorLDR) {
        return $formsData;
    }

    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
    $reportName = ROOT_ORGANIZATION_NAME."-Forms-".$dateTimeInfo;
    $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                ."/".$reportName.".tsv";
    $formsCSV = generateCSVData($logId, $formsData->report, null, true, null, "\t");
    if ($formsCSV instanceof ErrorLDR) {
        return $formsCSV;
    }

    $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $formsCSV);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->logError(__FUNCTION__);
        return $reportUrl;
    }

    return $reportUrl;
}

// getDetailedActivityReport()
//      Returns Activity Report in JSON format
//
function getDetailedActivityReport()
{
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    $activityReport = dbGetDetailedActivityReport($logId, $db);
    if ($activityReport instanceof ErrorLDR) {
        $activityReport->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the test form revision
    finishService($db, $logId, $activityReport, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getTestFormRevision()
//   Returns a test form revision
//
function getTestFormRevision($testFormRevisionId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the testFormRevisionId
    $argName = 'testFormRevisionId';
    $name = validateArg($logId, $db, [$argName => $testFormRevisionId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test form revision
    $testFormRevision = dbGetTestFormRevision($logId, $db, $testFormRevisionId);
    if ($testFormRevision instanceof ErrorLDR)
    {
        $testFormRevision->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the test form revision
    finishService($db, $logId, $testFormRevision, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getSchoolActivityReport()
// Get Testing Activity Report by School
//
function getSchoolActivityReport($schoolOrganizationId = null)
{
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    $organizationIdentifier = '';
    if (isset($schoolOrganizationId)) {
        // Get the enrollment organization
        $organization = dbGetOrganization($logId, $db, $schoolOrganizationId);
        if ($organization instanceof ErrorLDR) {
            $organization->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // Check that the enrollment organization can enroll students
        if ($organization['organizationType'] != ORG_TYPE_SCHOOL)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_TYPE_IS_NOT_SCHOOL,
                "Organization Id: ".$schoolOrganizationId." is not a valid organization type for the report");
            $db = null;
            $errorLDR->returnErrorJSON(__FUNCTION__);
            return;
        }
        $organizationIdentifier = $organization['organizationIdentifier'];
    } else {
        set_time_limit(REPORT_MAX_EXECUTION_TIME);
    }

    $schoolActivityReport = dbGetSchoolActivityReport($logId, $db, $schoolOrganizationId);
    if ($schoolActivityReport instanceof ErrorLDR) {
        $schoolActivityReport->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $reportName = "SchoolActivityReport-".(isset($schoolOrganizationId) ? $organizationIdentifier:"ALL");
    $s3Key = LDR_S3_ACTIVITY_REPORTS_ROOT."/".TENANT_ID."/".LDR_S3_ACTIVITY_REPORTS_PATH
                    ."/school/".$reportName."_".date("YmdHis").".csv";
    $result = awsStore2S3($logId, LDR_S3_ACTIVITY_REPORTS_BUCKET, $s3Key, $schoolActivityReport);
    unset($schoolActivityReport);
    if ($result instanceof ErrorLDR) {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_ACTIVITY_REPORTS_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $result = [];
    $result['filename'] = $s3Key;
    $result['fileURL'] = $reportUrl;

    // Return the test form revision
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// storeTextEntryInteractionAnswerKey()
//  Patch made to store the missing answer keys for textEntryInteractions
//
function storeTextEntryInteractionAnswerKey()
{
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__, ['testFormRevisionIds' => DCO_REQUIRED]);
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testFormRevisionIds) = $args;

    $result = new stdClass();
    $result->errors = [];
    $result->expectedCount = 0;
    $result->successfulCount = 0;

    if (STORE_TEST_FORM_REVISIONS) {
        $testFormRevisionIds = explode(",", trim($testFormRevisionIds, ','));
        $result->expectedCount = count($testFormRevisionIds);

        foreach ($testFormRevisionIds as $testFormRevisionId) {
            $db->beginTransaction();
            $storeStatus = dbStoreMissingTextEntryInteractionsAnswerKey($logId, $db, $testFormRevisionId);
            if ($storeStatus instanceof ErrorLDR) {
                $storeStatus->logError(__FUNCTION__);
                $result->errors[$testFormRevisionId] = $storeStatus->getErrorArray();
                $db->rollBack();
                continue;
            }
            $db->commit();
            $result->successfulCount = $result->successfulCount + 1;
        }
    }

    // Return the test form revision
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

//
// LDR test form support functions
//
// dbCreateTestFormAssessmentItem()
//  Creates a test form assessment item
//
function dbCreateTestFormAssessmentItem($logId, PDO $db, $testFormRevisionId, $testFormAssessmentSectionId, $values)
{
    $colsAndVals = array_merge(
            [
                'testFormRevisionId' => $testFormRevisionId,
                'testFormAssessmentSectionId' => $testFormAssessmentSectionId
            ],
            $values
        );
    $testFormAssessmentItemId = dbInsertWithBindParams($logId, $db, 'test_form_assessment_item', $colsAndVals);
    if ($testFormAssessmentItemId instanceof ErrorLDR) {
        $testFormAssessmentItemId->logError(__FUNCTION__);
    }

    return $testFormAssessmentItemId;
}

// dbCreateTestFormAssessmentSection()
//  Creates a test form assessment section
//
function dbCreateTestFormAssessmentSection($logId, PDO $db, $testFormRevisionId, $testFormPartId, $values)
{
    $colsAndVals = array_merge(
            [
                'testFormRevisionId' => $testFormRevisionId,
                'testFormPartId' => $testFormPartId
            ],
            $values
        );
    $testFormAssessmentSectionId = dbInsertWithBindParams($logId, $db, 'test_form_assessment_section', $colsAndVals);
    if ($testFormAssessmentSectionId instanceof ErrorLDR) {
        $testFormAssessmentSectionId->logError(__FUNCTION__);
    }

    return $testFormAssessmentSectionId;
}

// dbCreateTestFormItemCorrectResponse()
//  Creates a test form assessment item element response correct response
//
function dbCreateTestFormItemCorrectResponse($logId, PDO $db, $testFormRevisionId, $testFormItemElementResponseId,
                                             $value, $position)
{
    $sql = "INSERT INTO test_form_item_correct_response (testFormRevisionId, testFormItemElementResponseId, value, position)
                 VALUES ('$testFormRevisionId', '$testFormItemElementResponseId', '$value', '$position')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testFormItemCorrectResponseId = $db->lastInsertId();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return $testFormItemCorrectResponseId;
}

// dbCreateTestFormItemElement()
//  Creates a test form assessment item element
//
function dbCreateTestFormItemElement($logId, PDO $db, $testFormRevisionId, $testFormAssessmentItemId, $values)
{
    $colsAndVals = array_merge(
        [
            'testFormRevisionId' => $testFormRevisionId,
            'testFormAssessmentItemId' => $testFormAssessmentItemId
        ],
        $values
    );
    $testFormItemElementId = dbInsertWithBindParams($logId, $db, 'test_form_item_element', $colsAndVals);
    if ($testFormItemElementId instanceof ErrorLDR) {
        $testFormItemElementId->logError(__FUNCTION__);
    }

    return $testFormItemElementId;
}

// dbCreateTestFormItemElementResponse()
//  Creates a test form assessment item element response
//
function dbCreateTestFormItemElementResponse($logId, PDO $db, $testFormRevisionId, $testFormItemElementId, $values)
{
    $colsAndVals = array_merge(
        [
            'testFormRevisionId' => $testFormRevisionId,
            'testFormItemElementId' => $testFormItemElementId
        ],
        $values
    );
    $testFormItemElementResponseId = dbInsertWithBindParams($logId, $db, 'test_form_item_element_response', $colsAndVals);
    if ($testFormItemElementResponseId instanceof ErrorLDR) {
        $testFormItemElementResponseId->logError(__FUNCTION__);
    }

    return $testFormItemElementResponseId;
}

// dbCreateTestFormPart()
//  Creates a test form part
//
function dbCreateTestFormPart($logId, PDO $db, $testFormRevisionId, $values)
{
    $colsAndVals = array_merge(['testFormRevisionId' => $testFormRevisionId], $values);
    $testFormPartId = dbInsertWithBindParams($logId, $db, 'test_form_part', $colsAndVals);
    if ($testFormPartId instanceof ErrorLDR) {
        $testFormPartId->logError(__FUNCTION__);
    }

    return $testFormPartId;
}

// dbCreateTestFormRevision()
//  Creates a test form revision
//
function dbCreateTestFormRevision($logId, PDO $db, $testFormRevisionId, $values)
{
    $colsAndVals = array_merge(['testFormRevisionId' => $testFormRevisionId,], $values);
    $insertStatus = dbInsertWithBindParams($logId, $db, 'test_form_revision', $colsAndVals);
    if ($insertStatus instanceof ErrorLDR) {
        $insertStatus->logError(__FUNCTION__);
        return $insertStatus;
    }

    return SUCCESS;
}

// dbCreateTestFormSystemItem()
//  Creates a test form system item
//
function dbCreateTestFormSystemItem($logId, PDO $db, $testFormRevisionId, $testFormAssessmentSectionId, $values)
{
    $colsAndVals = array_merge(
        [
            'testFormRevisionId' => $testFormRevisionId,
            'testFormAssessmentSectionId' => $testFormAssessmentSectionId
        ],
        $values
    );
    $insertStatus = dbInsertWithBindParams($logId, $db, 'test_form_system_item', $colsAndVals);
    if ($insertStatus instanceof ErrorLDR) {
        $insertStatus->logError(__FUNCTION__);
        return $insertStatus;
    }

    return SUCCESS;
}

// dbCreateTestMap()
//  Creates test maps in the DB
//
function dbCreateTestMap($logId, PDO $db, $testMaps)
{
    $countOfTestMapRows = dbMultiInsertWithBindParams(
                            $logId,
                            $db,
                            'test_map',
                            array_keys(current($testMaps)),
                            $testMaps
                        );
    if ($countOfTestMapRows instanceof ErrorLDR) {
        $countOfTestMapRows->logError(__FUNCTION__);
    }

    return $countOfTestMapRows;
}

// dbDeleteTestFormRevision()
//  Delete Test Form Revision info
//
function dbDeleteTestFormRevision($logId, PDO $db, $testFormRevisionId)
{
    $tables2update = ['test_form_item_correct_response', 'test_form_item_element_response', 'test_form_item_element'
        , 'test_form_assessment_item', 'test_form_system_item', 'test_form_assessment_section', 'test_form_part'
    ];

    foreach ($tables2update as $table) {
        $sql = "UPDATE ".$table." SET deleted = 'Y' WHERE testFormRevisionId = ".$testFormRevisionId." AND deleted = 'N';";

        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        } catch (PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    return SUCCESS;
}

// dbDataExportForms()
//  Build the SQL query to export forms
//
function dbDataExportForms($testStatus)
{
    $sql = "SELECT DISTINCT ta.testFormRevisionId 'Form Revision ID', tb.testBatteryName 'Battery Name'
                , tbp.testBatteryProgram 'Program Name', tbs.testBatterySubject 'Subject'
                , tbg.testBatteryGrade 'Grade', tbf.testBatteryFormName 'Form Name'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN test_battery tb ON tb.testBatteryId = ta.testBatteryId
                JOIN test_battery_form tbf ON tbf.testBatteryFormId = ta.testBatteryFormId
                --  JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_battery_program tbp ON tb.testBatteryProgramId = tbp.testBatteryProgramId
                JOIN test_battery_subject tbs ON tb.testBatterySubjectId = tbs.testBatterySubjectId
                JOIN test_battery_grade tbg ON tb.testBatteryGradeId = tbg.testBatteryGradeId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
            WHERE ta.deleted = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime;";
    return $sql;

}

// dbExportForms()
//  Getting Data Export Data for the Forms File
//
function dbExportForms($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT DISTINCT ta.testFormRevisionId 'Form Revision ID', tb.testBatteryName 'Battery Name'
                , tbp.testBatteryProgram 'Program Name', tbs.testBatterySubject 'Subject'
                , tbg.testBatteryGrade 'Grade', tbf.testBatteryFormName 'Form Name'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN test_battery tb ON tb.testBatteryId = ta.testBatteryId
                JOIN test_battery_form tbf ON tbf.testBatteryFormId = ta.testBatteryFormId
                --  JOIN test_form_revision tfr ON ta.testFormRevisionId = tfr.testFormRevisionId
                JOIN test_battery_program tbp ON tb.testBatteryProgramId = tbp.testBatteryProgramId
                JOIN test_battery_subject tbs ON tb.testBatterySubjectId = tbs.testBatterySubjectId
                JOIN test_battery_grade tbg ON tb.testBatteryGradeId = tbg.testBatteryGradeId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
            WHERE ta.deleted = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_FORMS_EXPORT_DATA, LDR_ED_MISSING_FORMS_EXPORT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $formsData = new stdClass();
        $formsData->report = null;
        $formsData->rowsCount = 0;

        $rawFormsData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $formsData->report = $rawFormsData;
        $formsData->rowsCount = $stmt->rowCount();

        return $formsData;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetTestFormAssementItemIds()
//  Returns the assessment item record IDs for a test form revision
//
function dbGetTestFormAssementItemIds($logId, PDO $db, $testFormRevisionId)
{
    $sql = "SELECT testFormAssessmentItemId, itemId
              FROM test_form_assessment_item
             WHERE testFormRevisionId = '$testFormRevisionId'
               AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Build an itemId -> testFormAssessmentItemId map
        $testFormAssessmentItemIds = [];
        foreach ($rows as $row)
            $testFormAssessmentItemIds[$row['itemId']] = $row['testFormAssessmentItemId'];
        return $testFormAssessmentItemIds;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
    // The test form revision was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ID_NOT_FOUND,
                             LDR_ED_TEST_FORM_REVISION_ID_NOT_FOUND . $testFormRevisionId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestFormAssessmentItemMaxPoints()
//  Get max scoring points for list of assessment item IDs
//
function dbGetTestFormAssessmentItemMaxPoints($logId, PDO $db, $assessmentItemIds)
{
    $sql = "SELECT DISTINCT tm.itemId, tm.maxPoints
            FROM test_map tm JOIN test_form_assessment_item tfai ON tm.itemId = tfai.itemId
            WHERE tfai.testFormAssessmentItemId IN (".implode(',', $assessmentItemIds).");";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $itemMaxPoints = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);

        return $itemMaxPoints;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetDetailedActivityReport()
//      Returns activity report
//
function dbGetDetailedActivityReport($logId, $db)
{
    $sql = "SELECT
                tb.testBatteryName AS 'Battery Name', tbs.testBatterySubject AS 'Subject',
                tbg.testBatteryGrade AS 'Grade', tbf.testBatteryFormName AS 'From Name',
                COUNT(CASE WHEN (t.testStatus = 'Scheduled') THEN t.testStatus ELSE NULL END) AS '# of Assignments - Scheduled',
                COUNT(CASE WHEN (t.testStatus = 'Submitted') THEN t.testStatus ELSE NULL END) AS '# of Assignments - Submitted',
                COUNT(CASE WHEN (t.testStatus = 'Paused') THEN t.testStatus ELSE NULL END) AS '# of Assignments - Paused',
                COUNT(CASE WHEN (t.testStatus = 'InProgress') THEN t.testStatus ELSE NULL END) AS '# of Assignments - InProgress',
                COUNT(CASE WHEN (t.testStatus = 'Canceled') THEN t.testStatus ELSE NULL END) AS '# of Assignments - Canceled'
            FROM
                test_battery_form tbf
                JOIN test_battery tb ON tb.testBatteryId = tbf.testBatteryId
                JOIN test_battery_grade tbg ON tbg.testBatteryGradeId = tb.testBatteryGradeId
                JOIN test_battery_subject tbs ON tbs.testBatterySubjectId = tb.testBatterySubjectId
                JOIN test_battery_permissions tbp ON tbp.testBatteryPermissionsId = tb.testBatteryPermissionsId
                LEFT JOIN (SELECT ts.testStatus, ta.testBatteryFormId
                    FROM test_assignment ta
                    JOIN test_status ts ON ts.testStatusId = ta.testStatusId
                    JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                    JOIN organization o ON o.organizationId = sr.enrollOrgId AND o.fake = 'N'
                    WHERE ta.deleted = 'N'
                ) AS t ON tbf.testBatteryFormId = t.testBatteryFormId
            WHERE
                tbp.testBatteryPermissions = 'Non-Restricted'
            GROUP BY tbf.testBatteryFormId
            ORDER BY tbg.testBatteryGradeId ASC, tb.testBatteryName, tbs.testBatterySubject, tbf.testBatteryFormName;";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $activityReport = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $activityReport;
        } else {
            // No test battery form found in DB
            $errorLDR = new ErrorLDR($logId, LDR_EC_NO_TEST_BATTERY_FORM_FOUND, LDR_ED_NO_TEST_BATTERY_FORM_FOUND);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetTestFormRevision()
//  Returns a test form revision
//
function dbGetTestFormRevision($logId, PDO $db, $testFormRevisionId)
{
    // Get the test form revision data
    $sql = "SELECT testFormRevisionId, title, identifier
              FROM test_form_revision
             WHERE testFormRevisionId = '$testFormRevisionId'
               AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $testFormRevision = $stmt->fetch(PDO::FETCH_ASSOC);
            return $testFormRevision;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test form revision was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ID_NOT_FOUND,
                             LDR_ED_TEST_FORM_REVISION_ID_NOT_FOUND . $testFormRevisionId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestFormRevisionDetails()
//  Returns a test form revision details
//
function dbGetTestFormRevisionDetails($logId, PDO $db, $testFormRevisionId)
{
    // Get Test Form Part info
    // Get Test Form Assessment Section info
    // Get Test Form Assessment Item info
    // Get Test Form Item Element info
    // Get Test Form Item Element Response info
    // Get Test Form Item Correct Response info

    $tables = [
        "test_form_part" => ["testFormPartId", "identifier"]
        , 'test_form_assessment_section' => ['testFormAssessmentSectionId', 'testFormPartId', 'identifier', 'title']
        , 'test_form_assessment_item' => ['testFormAssessmentItemId', 'testFormAssessmentSectionId', 'itemId', 'serial']
        , 'test_form_item_element' => ['testFormItemElementId', 'testFormAssessmentItemId', 'serial', 'qtiClassId']
        , 'test_form_item_element_response' => ['testFormItemElementResponseId', 'testFormItemElementId', 'serial']
        , 'test_form_item_correct_response' => ['testFormItemCorrectResponseId', 'testFormItemElementResponseId']
    ];

    $data = [];

    foreach ($tables as $table => $cols) {
        $sql = "SELECT ".implode(", ", $cols)." FROM ".$table." WHERE deleted = 'N' AND testFormRevisionId = ".$testFormRevisionId;
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $data[] = $stmt->fetchAll(PDO::FETCH_UNIQUE|PDO::FETCH_GROUP|PDO::FETCH_ASSOC);
        }
        catch(PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    return $data;
}

// dbGetSchoolActivityReport
//   Get testing activity report by school
//
function dbGetSchoolActivityReport($logId, $db, $schoolOrganizationId = null)
{
    $enrollOrgWhereCond = "";
    $orgWhereCond = "";

    if (isset($schoolOrganizationId)) {
        $enrollOrgWhereCond = " AND sr.enrollOrgId = '".$schoolOrganizationId."' ";
        $orgWhereCond = " AND o.organizationId = '".$schoolOrganizationId."' ";
    }
    $sql = "SELECT
            	os.organizationName AS 'State', od.organizationIdentifier AS 'District Code', od.organizationName AS 'District Name'
            	, ot.organizationIdentifier AS 'School Identifier', ot.organizationName AS 'School Name', ot.studentCount AS 'Student Count'
            	, ot.testBatteryName AS 'Test Battery Name', tbg.testBatteryGrade AS 'Test Battery Grade', tbs.testBatterySubject AS 'Test Battery Subject'
            	, COUNT(CASE WHEN (ta.testStatusId = ".TEST_STATUS_ID_SCHEDULED.") THEN ta.testStatusId ELSE NULL END) AS '# of Assignments - Scheduled'
            	, COUNT(CASE WHEN (ta.testStatusId = ".TEST_STATUS_ID_INPROGRESS.") THEN ta.testStatusId ELSE NULL END) AS '# of Assignments - In Progress'
            	, COUNT(CASE WHEN (ta.testStatusId = ".TEST_STATUS_ID_PAUSED.") THEN ta.testStatusId ELSE NULL END) AS '# of Assignments - Paused'
            	, COUNT(CASE WHEN (ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.") THEN ta.testStatusId ELSE NULL END) AS '# of Assignments - Submitted'
            	, COUNT(CASE WHEN (ta.testStatusId = ".TEST_STATUS_ID_CANCELED.") THEN ta.testStatusId ELSE NULL END) AS '# of Assignments - Canceled'
            FROM (
            	SELECT ta.testBatteryId, ta.testStatusId, sr.enrollOrgId
            	FROM test_assignment ta, student_record sr
            	JOIN organization o ON o.organizationId = sr.enrollOrgId
            	WHERE sr.studentRecordId = ta.studentRecordId AND o.fake = 'N' AND ta.deleted = 'N'".$enrollOrgWhereCond."
            ) as ta
            	RIGHT JOIN (
            		SELECT
            			o.organizationId, o.organizationName, o.organizationIdentifier, o.parentOrganizationId, o.stateOrganizationId, o.studentCount
            			, tb.testBatteryId, tb.testBatteryName, tb.testBatteryGradeId, tb.testBatterySubjectId
            		FROM organization o, test_battery tb
            		WHERE o.organizationType = 4 AND o.fake = 'N'".$orgWhereCond."
            	) AS ot ON ot.testBatteryId = ta.testBatteryId AND ot.organizationId = ta.enrollOrgId
            	JOIN organization od ON od.organizationId = ot.parentOrganizationId
            	JOIN organization os ON os.organizationId = ot.stateOrganizationId
            	JOIN test_battery_grade tbg ON tbg.testBatteryGradeId = ot.testBatteryGradeId
            	JOIN test_battery_subject tbs ON tbs.testBatterySubjectId = ot.testBatterySubjectId
            GROUP BY ot.organizationId, ot.testBatteryId
            ORDER BY
                os.organizationName, od.organizationName, od.organizationIdentifier, ot.organizationName
                , ot.organizationIdentifier, tbs.testBatterySubject, tbg.testBatteryGrade, ot.testBatteryName;";

    try {
        ini_set('memory_limit', DATA_EXPORT_MAX_MEMORY);
        $stmt = $db->prepare($sql);
        $stmt->execute();

        // Getting the first row only to capture the header data
        if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data = '';

            $headers = array_keys($row);
            $data = '"'.implode('","', $headers).'"'.PHP_EOL;
            $data .= '"'.implode('","', $row).'"'.PHP_EOL;

            // Getting the rest of the data
            while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
                $data .= '"'.implode('","', $row).'"'.PHP_EOL;
            }
            return $data;
        }
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbStoreMissingTextEntryInteractionsAnswerKey()
//  Store missing answer keys for textEntryInteractions
//
function dbStoreMissingTextEntryInteractionsAnswerKey($logId, PDO $db, $testFormRevisionId)
{
    // Build the S3 key
    $s3Key = ADP_S3_CONTENT_ROOT . $testFormRevisionId . '/test.json';

    // Initialize the error message suffix
    $errMsgRoot = ' : s3Key = |' . $s3Key . '|';

    // Get the test form revision file contents
    $contents = awsGetS3JsonFileContents($logId, ADP_S3_CONTENT_BUCKET, $s3Key);
    if ($contents instanceof ErrorLDR) {
        logHAL($logId, HAL_SEVERITY_ERROR, $contents->getErrorCode(), $contents->getErrorDetail() . $errMsgRoot);
        $contents->logError(__FUNCTION__);
        return $contents;
    }

    // Get the test form revision
    $testFormRevision = json_decode($contents, true);
    if ($testFormRevision === null) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_DECODE_FAILED,
                                 LDR_ED_TEST_FORM_REVISION_DECODE_FAILED . $s3Key);
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    $formDetails = dbGetTestFormRevisionDetails($logId, $db, $testFormRevisionId);
    if ($formDetails instanceof ErrorLDR) {
        $formDetails->logError(__FUNCTION__);
        return $formDetails;
    }
    list($parts, $sections, $sectionItems, $elements, $elementResponses, $correctResponses) = $formDetails;

    if (!empty($correctResponses)) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_HAS_CORRECT_RESPONSES
                                    , LDR_ED_TEST_FORM_REVISION_HAS_CORRECT_RESPONSES.$testFormRevisionId);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Create the test form part(s)
    $testParts = (array_key_exists('testPart', $testFormRevision)) ? $testFormRevision['testPart'] : null;
    if ($testParts === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_TEST_PARTS_NOT_FOUND,
                                 LDR_ED_TEST_FORM_REVISION_TEST_PARTS_NOT_FOUND . $errMsgRoot);
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    foreach ($testParts as $testPart) {
        $partIdentifier = (array_key_exists('identifier', $testPart)) ? $testPart['identifier'] : null;
        $errMsgTestPart = $errMsgRoot . ', testPart = |' . $partIdentifier . '|';
        // Create the assessment section(s)
        $assessmentSections = (array_key_exists('assessmentSection', $testPart)) ? $testPart['assessmentSection'] : null;
        if ($assessmentSections === null) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ASSESSMENT_SECTIONS_NOT_FOUND,
            LDR_ED_TEST_FORM_REVISION_ASSESSMENT_SECTIONS_NOT_FOUND . $errMsgTestPart);
            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        foreach ($assessmentSections as $assessmentSection) {
            $sectionIdentifier =
                (array_key_exists('identifier', $assessmentSection)) ? $assessmentSection['identifier'] : null;
            $errMsgAssessmentSection = $errMsgTestPart . ', assessmentSection = |' . $sectionIdentifier . '|';
            // Create the assessment item(s)
            $assessmentItems = (array_key_exists('assessmentItem', $assessmentSection)) ? $assessmentSection['assessmentItem'] : null;
            if ($assessmentItems === null) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ASSESSMENT_ITEMS_NOT_FOUND,
                LDR_ED_TEST_FORM_REVISION_ASSESSMENT_ITEMS_NOT_FOUND . $errMsgAssessmentSection);
                logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
            foreach ($assessmentItems as $assessmentItem) {
                // Check that there is an assessmentItemContent element
                $itemContent = (array_key_exists('assessmentItemContent', $assessmentItem)) ?
                $assessmentItem['assessmentItemContent'] : null;
                if ($itemContent === null) {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_CONTENT_NOT_FOUND,
                    LDR_ED_TEST_FORM_REVISION_ITEM_CONTENT_NOT_FOUND . $errMsgAssessmentSection);
                    logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }

                // Check that there is a data element
                $itemData = (array_key_exists('data', $itemContent)) ? $itemContent['data'] : null;
                if ($itemData === null) {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_DATA_NOT_FOUND,
                    LDR_ED_TEST_FORM_REVISION_ITEM_DATA_NOT_FOUND . $errMsgAssessmentSection);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
                $itemIdentifier = (array_key_exists('identifier', $itemData)) ? $itemData['identifier'] : null;
                $errMsgAssessmentItem = $errMsgAssessmentSection . ', assessmentItem = |' . $itemIdentifier . '|';

                // Store the item based on the item type
                $category = (array_key_exists('category', $assessmentItem)) ? $assessmentItem['category'] : null;
                if ($category !== null && is_array($category) && sizeof($category) > 0 && $category[0] == "systemItem") {
                    continue;
                }

                // Gather the item elements
                $elementQtiClasses = [];
                $responsePositions = [];
                $responseIdentifierElementIds = [];
                $itemBody = (array_key_exists('body', $itemData)) ? $itemData['body'] : null;
                if ($itemBody === null) {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_BODY_NOT_FOUND,
                                             LDR_ED_TEST_FORM_REVISION_ITEM_BODY_NOT_FOUND . $errMsgAssessmentItem);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
                $itemElements = (array_key_exists('elements', $itemBody)) ? $itemBody['elements'] : null;
                if ($itemElements === null) {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENTS_NOT_FOUND,
                                             LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENTS_NOT_FOUND . $errMsgAssessmentItem);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
                $interactionElementFound = false;
                foreach ($itemElements as $itemElement) {
                    // Get the item element qtiClass
                    $elemQtiClass = (array_key_exists('qtiClass', $itemElement)) ? $itemElement['qtiClass'] : null;
                    if ($elemQtiClass === null) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_QTI_CLASS_NOT_FOUND,
                                                 LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_QTI_CLASS_NOT_FOUND . $errMsgAssessmentItem);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    // Check the item element qtiClass
                    if ($elemQtiClass != 'textEntryInteraction')
                        continue;

                    // Get the item element attributes
                    $attributes = (array_key_exists('attributes', $itemElement)) ? $itemElement['attributes'] : null;
                    if ($attributes === null) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_ATTR_NOT_FOUND,
                                                 LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_ATTR_NOT_FOUND . $errMsgItemElement);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    // Get the item element response identifier
                    $responseIdentifier =
                        (array_key_exists('responseIdentifier', $attributes)) ? $attributes['responseIdentifier'] : null;
                    if ($responseIdentifier === null) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND,
                            LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND . $errMsgItemElement);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }
                    // Check that the response identifier is unique for this assessment item
                    if (array_key_exists($responseIdentifier, $responseIdentifierElementIds)) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_DUP,
                            LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_DUP . $responseIdentifier . ' : ' .
                            $errMsgItemElement);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    $elementQtiClasses[$responseIdentifier] = $elemQtiClass;
                }

                // Gather the item element responses
                $itemResponses = (array_key_exists('responses', $itemData)) ? $itemData['responses'] : null;
                if ($itemResponses === null)
                {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSES_NOT_FOUND,
                                             LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSES_NOT_FOUND . $errMsgAssessmentItem);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
                foreach ($itemResponses as $itemResponse) {
                    // Get the item element response serial value
                    $responseSerial = (array_key_exists('serial', $itemResponse)) ? $itemResponse['serial'] : null;
                    if ($responseSerial === null) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_SERIAL_NOT_FOUND,
                                                    LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_SERIAL_NOT_FOUND.$errMsgAssessmentItem);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }
                    $errMsgItemElementResponse = $errMsgAssessmentItem . ', itemElementResponse = |' . $responseSerial . '|';

                    // Get the item element response attributes
                    $attributes = (array_key_exists('attributes', $itemResponse)) ? $itemResponse['attributes'] : null;
                    if ($attributes === null)
                    {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_ATTR_NOT_FOUND,
                                                 LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_ATTR_NOT_FOUND . $errMsgAssessmentItem);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    // Get the item element response identifier
                    $identifier = (array_key_exists('identifier', $itemResponse)) ? $itemResponse['identifier'] : null;
                    if ($identifier === null) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_IDENTIFIER_NOT_FOUND,
                            LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_IDENTIFIER_NOT_FOUND . $errMsgItemElementResponse);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    if (isset($elementQtiClasses[$identifier])) {
                        if ($elementQtiClasses[$identifier] != 'textEntryInteraction') {
                            continue;
                        }
                    } else {
                        continue;
                    }

                    $testFormItemElementResponseId = null;
                    $status = array_walk($elementResponses, function ($v, $k, $responseSerial) use (&$testFormItemElementResponseId) {
                        if ($responseSerial == $v['serial']) {
                            $testFormItemElementResponseId = $k;
                        }
                    }, $responseSerial);
                    if ($testFormItemElementResponseId === null) {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND,
                            LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND . $errMsgItemElementResponse);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    // Get the item element response correct responses
                    $correctResponses =
                        (array_key_exists('correctResponses', $itemResponse)) ? $itemResponse['correctResponses'] : null;
                    if ($correctResponses === null)
                    {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND,
                            LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND . $errMsgAssessmentItem);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }

                    $position = 0;
                    // Create the item element response correct responses
                    foreach ($correctResponses as $correctResponse)
                    {
                        $position++;
                        $result = dbCreateTestFormItemCorrectResponse($logId, $db, $testFormRevisionId,
                                                                      $testFormItemElementResponseId, $correctResponse, $position);
                        if ($result instanceof ErrorLDR)
                        {
                            $errorLDR = new ErrorLDR($result->getErrorCode(), $result->getErrorDetail() . $errMsgItemElementResponse);
                            $errorLDR->logError(__FUNCTION__);
                            return $errorLDR;
                        }
                    }
                }
            }
        }
    }
}

// dbStoreTestFormAssessmentItem()
//  Stores a test form assessment item
//
function dbStoreTestFormAssessmentItem($logId, PDO $db, $testFormRevisionId, $testFormAssessmentSectionId,
                                       $assessmentItem, $itemData, $errMsgAssessmentSection)
{
    // Gather the assessment item attributes
    $values = [];
    $values['uin'] = (array_key_exists('uin', $assessmentItem)) ? $assessmentItem['uin'] : null;
    $values['itemId'] = (array_key_exists('itemId', $assessmentItem)) ? $assessmentItem['itemId'] : null;
    $values['identifier'] = (array_key_exists('identifier', $itemData)) ? $itemData['identifier'] : null;
    $values['serial'] = (array_key_exists('serial', $itemData)) ? $itemData['serial'] : null;
    $itemQtiClass = (array_key_exists('qtiClass', $itemData)) ? $itemData['qtiClass'] : null;
    $values['domain'] = (array_key_exists('domain', $assessmentItem)) ? $assessmentItem['domain'] : null;
    $values['cluster'] = (array_key_exists('cluster', $assessmentItem)) ? $assessmentItem['cluster'] : null;
    $values['contentStandard'] = (array_key_exists('contentStandard', $assessmentItem)) ?
                                   $assessmentItem['contentStandard'] : null;
    $values['evidenceStatement'] = (array_key_exists('evidenceStatement', $assessmentItem)) ?
                                     $assessmentItem['evidenceStatement'] : null;
    $values['fluencySkillArea'] = (array_key_exists('fluencySkillArea', $assessmentItem)) ?
        $assessmentItem['fluencySkillArea'] : null;
    $values['itemStrand'] = (array_key_exists('itemStrand', $assessmentItem)) ? $assessmentItem['itemStrand'] : null;
    $values['subclaim'] = (array_key_exists('subclaim', $assessmentItem)) ? $assessmentItem['subclaim'] : null;
    $values['passageType'] = (array_key_exists('passageType', $assessmentItem)) ? $assessmentItem['passageType'] : null;
    $values['itemGrade'] = (array_key_exists('itemGrade', $assessmentItem)) ? $assessmentItem['itemGrade'] : null;
    $values['maxScorePoints'] =
        (array_key_exists('maxScorePoints', $assessmentItem)) ? $assessmentItem['maxScorePoints'] : null;
    $calculatorType = (array_key_exists('calculator', $assessmentItem)) ? $assessmentItem['calculator'] : null;
    $errMsgAssessmentItem = $errMsgAssessmentSection . ', assessmentItem = |' . $values['identifier'] . '|';

    // Get any reference value IDs
    $calculatorTypeId = null;
    if ($calculatorType !== null)
    {
        $calculatorTypeId = dbGetReferenceValueId($logId, $db, 'calculator_type', $calculatorType);
        if ($calculatorTypeId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($calculatorTypeId->getErrorCode(),
                                     $calculatorTypeId->getErrorDetail() . $errMsgAssessmentItem);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
    $values['calculatorTypeId'] = $calculatorTypeId;
    $qtiClassId = null;
    if ($itemQtiClass !== null)
    {
        $qtiClassId = dbGetReferenceValueId($logId, $db, 'qti_class', $itemQtiClass);
        if ($qtiClassId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($qtiClassId->getErrorCode(),
                                     $qtiClassId->getErrorDetail() . $errMsgAssessmentItem);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
    $values['qtiClassId'] = $qtiClassId;

    // Create the assessment item
    $testFormAssessmentItemId =
        dbCreateTestFormAssessmentItem($logId, $db, $testFormRevisionId, $testFormAssessmentSectionId, $values);
    if ($testFormAssessmentItemId instanceof ErrorLDR)
    {
        $errorLDR = new ErrorLDR($testFormAssessmentItemId->getErrorCode(),
                                 $testFormAssessmentItemId->getErrorDetail() . $errMsgAssessmentItem);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Gather the item elements
    $elementQtiClasses = [];
    $responsePositions = [];
    $responseIdentifierElementIds = [];
    $itemBody = (array_key_exists('body', $itemData)) ? $itemData['body'] : null;
    if ($itemBody === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_BODY_NOT_FOUND,
                                 LDR_ED_TEST_FORM_REVISION_ITEM_BODY_NOT_FOUND . $errMsgAssessmentItem);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    $itemElements = (array_key_exists('elements', $itemBody)) ? $itemBody['elements'] : null;
    if ($itemElements === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENTS_NOT_FOUND,
                                 LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENTS_NOT_FOUND . $errMsgAssessmentItem);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    $interactionElementFound = false;
    foreach ($itemElements as $itemElement)
    {
        // Get the item element qtiClass
        $elemQtiClass = (array_key_exists('qtiClass', $itemElement)) ? $itemElement['qtiClass'] : null;
        if ($elemQtiClass === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_QTI_CLASS_NOT_FOUND,
                                     LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_QTI_CLASS_NOT_FOUND . $errMsgAssessmentItem);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Check that the item element qtiClass is a known interaction type
        if (!in_array($elemQtiClass, ['choiceInteraction', 'textEntryInteraction', 'extendedTextInteraction',
                                      'matchInteraction', 'inlineChoiceInteraction'], true))
        {
            continue;
        }

        // Get the element serial value
        $values = [];
        $values['serial'] = (array_key_exists('serial', $itemElement)) ? $itemElement['serial'] : null;
        $errMsgItemElement = $errMsgAssessmentItem . ', itemElement = |' . $values['serial'] . '|';

        // Get the item element attributes
        $attributes = (array_key_exists('attributes', $itemElement)) ? $itemElement['attributes'] : null;
        if ($attributes === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_ATTR_NOT_FOUND,
                                     LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_ATTR_NOT_FOUND . $errMsgItemElement);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Get the item element response identifier
        $responseIdentifier =
            (array_key_exists('responseIdentifier', $attributes)) ? $attributes['responseIdentifier'] : null;
        if ($responseIdentifier === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND,
                LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_NOT_FOUND . $errMsgItemElement);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        // Check that the response identifier is unique for this assessment item
        if (array_key_exists($responseIdentifier, $responseIdentifierElementIds))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_DUP,
                LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_RESPONSE_ID_DUP . $responseIdentifier . ' : ' .
                $errMsgItemElement);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $values['responseIdentifier'] = $responseIdentifier;

        // Get the qtiClass reference value ID
        $qtiClassId = dbGetReferenceValueId($logId, $db, 'qti_class', $elemQtiClass);
        if ($qtiClassId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($qtiClassId->getErrorCode(),
                $qtiClassId->getErrorDetail() . $errMsgItemElement);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $values['qtiClassId'] = $qtiClassId;

        // Perform qtiClass-specific processing
        switch ($elemQtiClass)
        {
            case 'choiceInteraction':
                // Build the list of choice positions for this response identifier
                $responsePositions[$responseIdentifier] = [];
                $choices = (array_key_exists('choices', $itemElement)) ? $itemElement['choices'] : null;
                if ($choices === null)
                {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_CHOICES_NOT_FOUND,
                        LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_CHOICES_NOT_FOUND . $errMsgItemElement);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
                $position = 1;
                foreach ($choices as $choice)
                {
                    $choiceIdentifier = (array_key_exists('identifier', $choice)) ? $choice['identifier'] : null;
                    if ($choiceIdentifier === null)
                    {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_CHOICE_IDENTIFIER_NOT_FOUND,
                            LDR_ED_TEST_FORM_REVISION_ITEM_CHOICE_IDENTIFIER_NOT_FOUND . $errMsgItemElement);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }
                    $responsePositions[$responseIdentifier][$choiceIdentifier] = $position++;
                }

                // Get the other element attributes
                $values['maxChoices'] = (array_key_exists('maxChoices', $attributes)) ? $attributes['maxChoices'] : null;
                $values['minChoices'] = (array_key_exists('minChoices', $attributes)) ? $attributes['minChoices'] : null;
                $values['shuffle'] = (array_key_exists('shuffle', $attributes)) ? $attributes['shuffle'] : null;
                if ($values['shuffle'] !== null)
                    $values['shuffle'] = ($values['shuffle']) ? 'Y' : 'N';

                $interactionElementFound = true;
                break;
            case 'textEntryInteraction':
                $interactionElementFound = true;
                break;
            case 'extendedTextInteraction':
                $interactionElementFound = true;
                break;
            case 'inlineChoiceInteraction':
                $interactionElementFound = true;
                break;
            case 'matchInteraction':
                // Get the other element attributes
                $values['maxAssociations'] =
                    (array_key_exists('maxAssociations', $attributes)) ? $attributes['maxAssociations'] : null;
                $values['minAssociations'] =
                    (array_key_exists('minAssociations', $attributes)) ? $attributes['minAssociations'] : null;
                $values['shuffle'] = (array_key_exists('shuffle', $attributes)) ? $attributes['shuffle'] : null;
                if ($values['shuffle'] !== null)
                    $values['shuffle'] = ($values['shuffle']) ? 'Y' : 'N';

                $interactionElementFound = true;
                break;
            default:
                break;
        }

        // Create the item element
        $testFormItemElementId =
            dbCreateTestFormItemElement($logId, $db, $testFormRevisionId, $testFormAssessmentItemId, $values);
        if ($testFormItemElementId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($testFormItemElementId->getErrorCode(),
                $testFormItemElementId->getErrorDetail() . $errMsgItemElement);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Save the response identifier element ID reference and elemQtiClass values
        $responseIdentifierElementIds[$responseIdentifier] = $testFormItemElementId;
        $elementQtiClasses[$responseIdentifier] = $elemQtiClass;
    }

    // Check that at least one interaction element was found
    if (!$interactionElementFound)
    {
        $errorLDR = new ErrorLDR(LDR_EC_TEST_FORM_REVISION_ITEM_INTERACTION_NOT_FOUND,
                                 LDR_ED_TEST_FORM_REVISION_ITEM_INTERACTION_NOT_FOUND . $errMsgAssessmentItem);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Gather the item element responses
    $itemResponses = (array_key_exists('responses', $itemData)) ? $itemData['responses'] : null;
    if ($itemResponses === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSES_NOT_FOUND,
                                 LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSES_NOT_FOUND . $errMsgAssessmentItem);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    foreach ($itemResponses as $itemResponse)
    {
        // Get the item element response attributes
        $attributes = (array_key_exists('attributes', $itemResponse)) ? $itemResponse['attributes'] : null;
        if ($attributes === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_ATTR_NOT_FOUND,
                                     LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_ATTR_NOT_FOUND . $errMsgAssessmentItem);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Get the item element response serial value
        $values = [];
        $values['serial'] = (array_key_exists('serial', $itemResponse)) ? $itemResponse['serial'] : null;
        $errMsgItemElementResponse = $errMsgAssessmentItem . ', itemElementResponse = |' . $values['serial'] . '|';

        // Get the item element response identifier
        $identifier = (array_key_exists('identifier', $itemResponse)) ? $itemResponse['identifier'] : null;
        if ($identifier === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_IDENTIFIER_NOT_FOUND,
                LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_IDENTIFIER_NOT_FOUND . $errMsgItemElementResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $values['identifier'] = $identifier;

        // Get the other element attributes
        $respQtiClass = (array_key_exists('qtiClass', $itemResponse)) ? $itemResponse['qtiClass'] : null;
        $cardinality = (array_key_exists('cardinality', $attributes)) ? $attributes['cardinality'] : null;

        // Get the qtiClass reference value ID
        $qtiClassId = dbGetReferenceValueId($logId, $db, 'qti_class', $respQtiClass);
        if ($qtiClassId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($qtiClassId->getErrorCode(),
                                     $qtiClassId->getErrorDetail() . $errMsgItemElementResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $values['qtiClassId'] = $qtiClassId;

        // Get the response cardinality reference value ID
        $responseCardinalityId = dbGetReferenceValueId($logId, $db, 'response_cardinality', $cardinality);
        if ($responseCardinalityId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($responseCardinalityId->getErrorCode(),
                                     $responseCardinalityId->getErrorDetail() . $errMsgItemElementResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $values['responseCardinalityId'] = $responseCardinalityId;

        // Check that this response corresponds to an item element
        if (!array_key_exists($identifier, $responseIdentifierElementIds))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_NO_MATCHING_ELEMENT,
                LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_NO_MATCHING_ELEMENT . $errMsgItemElementResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Map the response to the item element and remove it from the list of responses for this item
        $testFormItemElementId = $responseIdentifierElementIds[$identifier];
        unset($responseIdentifierElementIds[$identifier]);

        // Create the item element response
        $testFormItemElementResponseId =
            dbCreateTestFormItemElementResponse($logId, $db, $testFormRevisionId, $testFormItemElementId, $values);
        if ($testFormItemElementResponseId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($testFormItemElementResponseId->getErrorCode(),
                                     $testFormItemElementResponseId->getErrorDetail() . $errMsgItemElementResponse);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Correct responses are not available for all interaction types
        if (!in_array($elementQtiClasses[$identifier], ['choiceInteraction', 'textEntryInteraction',
                                                        'inlineChoiceInteraction', 'matchInteraction'], true))
        {
            continue;
        }

        // Get the item element response correct responses
        $correctResponses =
            (array_key_exists('correctResponses', $itemResponse)) ? $itemResponse['correctResponses'] : null;
        if ($correctResponses === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND,
                LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND . $errMsgAssessmentItem);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Create the item element response correct responses
        if (count($correctResponses) > 0)
        {
            foreach ($correctResponses as $correctResponse)
            {
                // For choiceInteraction get the correct response position - all others just assume position 1
                $position = 1;
                if ($elementQtiClasses[$identifier] == 'choiceInteraction')
                {
                    if (array_key_exists($identifier, $responsePositions) &&
                        array_key_exists($correctResponse, $responsePositions[$identifier]))
                    {
                        $position = $responsePositions[$identifier][$correctResponse];
                    }
                    else {
                        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_CORRECT_NO_MATCHING_CHOICE,
                            LDR_ED_TEST_FORM_REVISION_ITEM_CORRECT_NO_MATCHING_CHOICE . $errMsgItemElementResponse);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }
                }

                $result = dbCreateTestFormItemCorrectResponse($logId, $db, $testFormRevisionId,
                    $testFormItemElementResponseId, $correctResponse, $position);
                if ($result instanceof ErrorLDR)
                {
                    $errorLDR = new ErrorLDR($result->getErrorCode(), $result->getErrorDetail() . $errMsgItemElementResponse);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
            }
        }
        elseif ($elementQtiClasses[$identifier] == 'choiceInteraction')
        {
            // For choiceInteraction element types if the correctResponses array is empty then the correct responses
            // should be indicated in the mapping array
            $mapping = (array_key_exists('mapping', $itemResponse)) ? $itemResponse['mapping'] : null;
            if ($mapping === null || !is_array($mapping))
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND,
                    LDR_ED_TEST_FORM_REVISION_ITEM_RESPONSE_CORRECT_NOT_FOUND . $errMsgAssessmentItem);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }

            // Mapping responses with a score of 1 are presumed correct
            foreach ($mapping as $response => $score)
            {
                $position = 1; // The response position is indeterminate so just set it to 1
                if ($score == '1')
                {
                    $result = dbCreateTestFormItemCorrectResponse($logId, $db, $testFormRevisionId,
                        $testFormItemElementResponseId, $response, $position);
                    if ($result instanceof ErrorLDR)
                    {
                        $errorLDR = new ErrorLDR($result->getErrorCode(),
                                                 $result->getErrorDetail() . $errMsgItemElementResponse);
                        $errorLDR->logError(__FUNCTION__);
                        return $errorLDR;
                    }
                }
            }
        }
    }

    // Check that all responses were found for all item elements
    if (sizeof($responseIdentifierElementIds) > 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_ELEMENT_NO_MATCHING_RESPONSE,
                                 LDR_ED_TEST_FORM_REVISION_ITEM_ELEMENT_NO_MATCHING_RESPONSE . $errMsgAssessmentItem);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return SUCCESS;
}

// dbStoreTestFormRevision()
//  Store a test form revision
//  Note: This function is normally invoked as part of a cron job where there is no user front end on which
//        errors are displayed. This means that all errors must be logged with HAL to provide visibility.
//
function dbStoreTestFormRevision($logId, PDO $db, $testFormRevisionId)
{
    // Build the S3 key
    $s3Key = ADP_S3_CONTENT_ROOT . $testFormRevisionId . '/test.json';

    // Initialize the error message suffix
    $errMsgRoot = ' : s3Key = |' . $s3Key . '|';

    // Get the test form revision file contents
    $contents = awsGetS3JsonFileContents($logId, ADP_S3_CONTENT_BUCKET, $s3Key);
    if ($contents instanceof ErrorLDR)
    {
        logHAL($logId, HAL_SEVERITY_ERROR, $contents->getErrorCode(), $contents->getErrorDetail() . $errMsgRoot);
        $contents->logError(__FUNCTION__);
        return $contents;
    }

    // Get the test form revision
    $testFormRevision = json_decode($contents, true);
    if ($testFormRevision === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_DECODE_FAILED,
                                 LDR_ED_TEST_FORM_REVISION_DECODE_FAILED . $s3Key);
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    // Create the test form revision
    $values = [];
    $values['title'] = (array_key_exists('title', $testFormRevision)) ? $testFormRevision['title'] : null;
    $values['identifier'] = (array_key_exists('identifier', $testFormRevision)) ? $testFormRevision['identifier'] : null;

    $result = dbCreateTestFormRevision($logId, $db, $testFormRevisionId, $values);
    if ($result instanceof ErrorLDR)
    {
        logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(), $result->getErrorDetail() . $errMsgRoot);
        $result->logError(__FUNCTION__);
        return $result;
    }

    // Create the test form part(s)
    $testParts = (array_key_exists('testPart', $testFormRevision)) ? $testFormRevision['testPart'] : null;
    if ($testParts === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_TEST_PARTS_NOT_FOUND,
                                 LDR_ED_TEST_FORM_REVISION_TEST_PARTS_NOT_FOUND . $errMsgRoot);
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    foreach ($testParts as $testPart)
    {
        $values = [];
        $values['identifier'] = (array_key_exists('identifier', $testPart)) ? $testPart['identifier'] : null;
        $values['navigationMode'] = (array_key_exists('navigationMode', $testPart)) ? $testPart['navigationMode'] : null;
        $values['submissionMode'] = (array_key_exists('submissionMode', $testPart)) ? $testPart['submissionMode'] : null;
        $errMsgTestPart = $errMsgRoot . ', testPart = |' . $values['identifier'] . '|';
        $testFormPartId = dbCreateTestFormPart($logId, $db, $testFormRevisionId, $values);
        if ($testFormPartId instanceof ErrorLDR)
        {
            logHAL($logId, HAL_SEVERITY_ERROR, $testFormPartId->getErrorCode(),
                   $testFormPartId->getErrorDetail() . $errMsgTestPart);
            $testFormPartId->logError(__FUNCTION__);
            return $testFormPartId;
        }

        // Create the assessment section(s)
        $assessmentSections = (array_key_exists('assessmentSection', $testPart)) ? $testPart['assessmentSection'] : null;
        if ($assessmentSections === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ASSESSMENT_SECTIONS_NOT_FOUND,
                                     LDR_ED_TEST_FORM_REVISION_ASSESSMENT_SECTIONS_NOT_FOUND . $errMsgTestPart);
            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        foreach ($assessmentSections as $assessmentSection)
        {
            $values = [];
            $values['identifier'] =
                (array_key_exists('identifier', $assessmentSection)) ? $assessmentSection['identifier'] : null;
            $errMsgAssessmentSection = $errMsgTestPart . ', assessmentSection = |' . $values['identifier'] . '|';
            $values['title'] = (array_key_exists('title', $assessmentSection)) ? $assessmentSection['title'] : null;
            $testFormAssessmentSectionId = dbCreateTestFormAssessmentSection($logId, $db, $testFormRevisionId,
                                                                             $testFormPartId, $values);
            if ($testFormAssessmentSectionId instanceof ErrorLDR)
            {
                logHAL($logId, HAL_SEVERITY_ERROR, $testFormAssessmentSectionId->getErrorCode(),
                       $testFormAssessmentSectionId->getErrorDetail() . $errMsgAssessmentSection);
                $testFormAssessmentSectionId->logError(__FUNCTION__);
                return $testFormAssessmentSectionId;
            }

            // Create the assessment item(s)
            $assessmentItems = (array_key_exists('assessmentItem', $assessmentSection)) ?
                              $assessmentSection['assessmentItem'] : null;
            if ($assessmentItems === null)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ASSESSMENT_ITEMS_NOT_FOUND,
                                     LDR_ED_TEST_FORM_REVISION_ASSESSMENT_ITEMS_NOT_FOUND . $errMsgAssessmentSection);
                logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
            foreach ($assessmentItems as $assessmentItem)
            {
                // Check that there is an assessmentItemContent element
                $itemContent = (array_key_exists('assessmentItemContent', $assessmentItem)) ?
                                 $assessmentItem['assessmentItemContent'] : null;
                if ($itemContent === null)
                {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_CONTENT_NOT_FOUND,
                                         LDR_ED_TEST_FORM_REVISION_ITEM_CONTENT_NOT_FOUND . $errMsgAssessmentSection);
                    logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }

                // Check that there is a data element
                $itemData = (array_key_exists('data', $itemContent)) ? $itemContent['data'] : null;
                if ($itemData === null)
                {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_FORM_REVISION_ITEM_DATA_NOT_FOUND,
                                             LDR_ED_TEST_FORM_REVISION_ITEM_DATA_NOT_FOUND . $errMsgAssessmentSection);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }

                // Store the item based on the item type
                $category = (array_key_exists('category', $assessmentItem)) ? $assessmentItem['category'] : null;
                if ($category !== null && is_array($category) && sizeof($category) > 0 && $category[0] == "systemItem")
                {
                    $result = dbStoreTestFormSystemItem($logId, $db, $testFormRevisionId,
                        $testFormAssessmentSectionId, $assessmentItem, $itemData, $errMsgAssessmentSection);
                }
                else
                {
                    $result = dbStoreTestFormAssessmentItem($logId, $db, $testFormRevisionId,
                        $testFormAssessmentSectionId, $assessmentItem, $itemData, $errMsgAssessmentSection);
                }
                if ($result instanceof ErrorLDR)
                {
                    logHAL($logId, HAL_SEVERITY_ERROR, $result->getErrorCode(), $result->getErrorDetail());
                    $result->logError(__FUNCTION__);
                    return $result;
                }
            }
        }
    }

    return SUCCESS;
}

// dbStoreTestFormSystemItem()
//  Stores a test form system item
//
function dbStoreTestFormSystemItem($logId, PDO $db, $testFormRevisionId, $testFormAssessmentSectionId, $assessmentItem,
                                   $itemData, $errMsgAssessmentSection)
{
    // Gather the assessment item attributes
    $values = [];
    $values['itemId'] = (array_key_exists('itemId', $assessmentItem)) ? $assessmentItem['itemId'] : null;
    $values['identifier'] = (array_key_exists('identifier', $itemData)) ? $itemData['identifier'] : null;
    $errMsgSystemItem = $errMsgAssessmentSection . ', systemItem = |' . $values['identifier'] . '|';
    $values['serial'] = (array_key_exists('serial', $itemData)) ? $itemData['serial'] : null;
    $qtiClass = (array_key_exists('qtiClass', $itemData)) ? $itemData['qtiClass'] : null;

    // Get any reference value IDs
    $qtiClassId = null;
    if ($qtiClass !== null)
    {
        $qtiClassId = dbGetReferenceValueId($logId, $db, 'qti_class', $qtiClass);
        if ($qtiClassId instanceof ErrorLDR)
        {
            $errorLDR = new ErrorLDR($qtiClassId->getErrorCode(), $qtiClassId->getErrorDetail() . $errMsgSystemItem);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $values['qtiClassId'] = $qtiClassId;
    }

    // Create the assessment item
    $result = dbCreateTestFormSystemItem($logId, $db, $testFormRevisionId, $testFormAssessmentSectionId, $values);
    if ($result instanceof ErrorLDR)
    {
        $errorLDR = new ErrorLDR($result->getErrorCode(), $result->getErrorDetail() . $errMsgSystemItem);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $result;
}
