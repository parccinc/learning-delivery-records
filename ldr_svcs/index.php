<?php
define('ROOT_DIR', __DIR__);
define('LOG_DIR', ROOT_DIR.'/Log');
define('TENANT_DIR', ROOT_DIR.'/tenant');
define('DB_DIR', ROOT_DIR.'/db');
define('DATA_EXPORT_DIR', ROOT_DIR.'/../export');

// These primary includes must be first and in this order
require_once '../ldr_common/definitions.php';
require_once 'tenant.php';

require_once 'vendor/autoload.php';

// These secondary includes can be in any order
require_once 'adp_comm.php';
require_once 'aws.php';
require_once 'class.php';
require_once 'database.php';
require_once 'hal_logging.php';
require_once 'organization.php';
require_once 'redis.php';
require_once 'student.php';
require_once 'test_assignment.php';
require_once 'test_battery.php';
require_once 'test_form.php';
require_once 'test_results.php';
require_once 'user_audit.php';
require_once 'validation.php';

define('LDR_SOFTWARE_VERSION', '2.2.4');
define('LDR_COMMIT_DATE', '2016-07-05');

date_default_timezone_set('UTC');

$sessionClientId = null;
$selectedDatabase = null;

$logFilename = LOG_DIR."/".(strlen(TENANT_DIR) > 0 ? TENANT_LOG_DIR."/":"").date("Ymd");

//
// PARCC-LDR services
//
// createLogEntry()                     create_log_entry.php
// createSessionToken()                 index.php => login_user.php
// createUser()                         create_user.php
// createClient()
// deleteSessionToken()                 logout.php
// genDataExportFile()
// generateDataExport()
// generateDataExportOptimized()
// getDataExport()
// getPasswordHash()
// getPerformanceStatistics()           get_performance_statistics.php
// getQueueProcessingStatistics()       get_queue_processing_statistics.php
// getTableNames()                      get_table_names.php
// getTableRecord()                     get_table_record.php
// getVersion()                         get_version.php
//
//
// LDR support functions
//
// beginService()
// dateInRange()
// dbCreateClient()
// errorQueryExceptionJSON
// finishService()
// flattenArray()
// generateCSVData()
// generateGUID_RFC4122v4
// generateIdentifier
// generatePasswordHash
// getNonEmptyParameterCount()
// loadRequestBodyJsonData()
// logCallADP
// logDebug
// logElapsed
// logError
// logRequest
// logResponse
// skipService()

//
// Initialize the LDR framework application object
//
//if ($restFramework == REST_FRAMEWORK_SLIM)
//{
    $app = new Slim(['debug' => true, 'log.enable' => true]);
    $app->setName('LDR Services');
    $app->response()->header('Content-Type', 'application/json; charset=utf-8');

    $app->notFound(function ()
    {
        echo json_encode(['error' => 'Slim service request is unknown']);
    });

    require_once 'routes.php'; // Configure the routing
    $app->run(); // Perform the routing
//}
//else
//{
//    $app = new Phalcon\Mvc\Micro();
//
//    $app->notFound(function () use ($app)
//    {
//        $app->response->setContentType('application/json; charset=utf-8')->sendHeaders();
//
//        echo json_encode(['error' => 'Phalcon service request is unknown']);
//    });
//
//    require_once 'routes.php'; // Configure the routing
//    $app->handle(); // Perform the routing
//}

//
// PARCC-LDR services
//
// createLogEntry()
//   Creates a HAL log entry and returns SUCCESS as a JSON encoded value
//
function createLogEntry()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    // NOTE: When run on my Windows 7 development laptop the combination of using Phalcon and TOKEN_REQUIRED
    // here results in a broken connection when invoking this function from the LDR Client. This behavior was
    // observed using Chrome, IE, and Safari browsers. Switching to Slim or setting TOKEN_OPTIONAL mitigated the
    // problem for some reason. When running under Linux (Centos 6.5) this problem does not occur. It has not been
    // determined if the issue is specific to my development laptop or to Windows 7 in general.
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'severity' => DCO_REQUIRED,
            'eventType' => DCO_REQUIRED,
            'eventDesc' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $severity, $eventType, $eventDesc) = $args;

    // Create the HAL log entry
    $response = logHAL($logId, $severity, $eventType, $eventDesc);
    if ($response instanceof ErrorLDR)
    {
        $response->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the response
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createSessionToken()
//   Checks a user password and returns a session token as a JSON encoded value if the password is correct
//     OR
//   Returns a JSON encoded LDR error if there is an error or the password is incorrect
//
function createSessionToken()
{
    // Log the service call
    $startTime = microtime(true);
//    $logId = logRequest();    // TODO: TEMPORARILY DISABLED TO REMOVE AUTHENTICATION VALUES FROM THE REQUEST LOGS
    $logId = generateIdentifier(4);

    // Initialize the service
    $args = beginService($logId, TOKEN_OPTIONAL, __FUNCTION__,
        array
        (
            'clientName' => DCO_REQUIRED | DCO_TOLOWER,
            'password' => DCO_REQUIRED
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientName, $password) = $args;

    // Get the clientID and clientPassword hash
    $errorLDR = new ErrorLDR($logId, LDR_EC_CREDENTIALS_NOT_VALID, LDR_ED_CREDENTIALS_NOT_VALID);
    $sql = "SELECT clientId, clientType, clientPassword FROM client WHERE clientName = '" . $clientName . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 0)
        {
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $valid_password_hash = $result['clientPassword'];
        $clientType = $result['clientType'];
        $clientId = $result['clientId'];
    }
    catch(PDOException $error) {
        errorQueryExceptionJSON($logId, $error, __FUNCTION__, $sql);
        $db = null;
        return;
    }

    // Check the client type
    if ($clientType != ALLOWED_CLIENT_TYPE)
    {
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check if the password is correct
    $password_hash = generatePasswordHash($password, $valid_password_hash);
    if ($password_hash != $valid_password_hash)
    {
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create a session token
    $token = strtoupper(sha1(uniqid(rand(), true)));
    if ($token === false)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_CREATION_FAILED, LDR_ED_TOKEN_CREATION_FAILED);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $result = dbCreateSessionToken($logId, $db, $clientId, $token);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the session token
    $response = ['token' => $token];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createUser()
//   Creates or finds a user record and returns SUCCESS as a JSON encoded value
//     OR
//   Returns a JSON encoded LDR error if there is an error
//
function createUser()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED,
            'organizationId' => DCO_REQUIRED | DCO_MUST_EXIST
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $organizationId) = $args;

    // Create the user
    $userId = dbCreateUser($logId, $db, $clientUserId, $organizationId);
    if ($userId instanceof ErrorLDR)
    {
        // Check for an integrity constraint violation which means the record already exists - which we are not
        // considering to be an error. Otherwise, return the exception.
        $error = $userId->getErrorArray();
        if (strpos($error['error'], '2002') === false || strpos($error['detail'], 'SQLSTATE[23000]') === false)
        {
            $userId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    finishService($db, $logId, ['result' => SUCCESS], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createClient()
//   Create new LDR client and returns result status as a JSON encoded value
//     OR
//   Returns a JSON encoded LDR error if there is an error
//
function createClient()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_OPTIONAL, __FUNCTION__,
        array
        (
            'clientName' => DCO_REQUIRED,
            'clientPassword' => DCO_REQUIRED,
            'clientURL' => DCO_REQUIRED,
            'clientType' => DCO_OPTIONAL,
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientName, $clientPassword, $clientURL, $clientType) = $args;

    $result = dbCreateClient($logId, $db, $clientName, $clientPassword, $clientURL, $clientType);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteSessionToken()
//   Deletes a session_token record and returns the token status as a JSON encoded value
//     OR
//   Returns a JSON encoded LDR error if there is an error
//
function deleteSessionToken()
{
//    global $restFramework;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Get the request headers
//    if ($restFramework == REST_FRAMEWORK_SLIM)
        $headers = Slim::getInstance()->request()->headers();
//    else
//    {
//        $request = new Phalcon\Http\Request();
//        $headers = $request->getHeaders();
//    }

    // Get the token
    $token = null;
    if (array_key_exists('TOKEN', $headers))
        $token = $headers['TOKEN'];

    // Check the token length
    if (strlen($token) != LENGTH_SESSION_TOKEN)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_LENGTH_INVALID, LDR_ED_TOKEN_LENGTH_INVALID);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Check if the token exists
    $dateTime = dbGetSessionDatetime_By_Token($logId, $db, $token);
    if ($dateTime instanceof ErrorLDR)
    {
        $dateTime->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    if ($dateTime == null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_NOT_FOUND, LDR_ED_TOKEN_NOT_FOUND);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Delete the token
	$result = dbDeleteSessionToken($logId, $db, $token);
	if ($result instanceof ErrorLDR)
	{
		$result->returnErrorJSON(__FUNCTION__);
		$db = null;
		return;
	}

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

function genDataExportFile($logId, $dataGenerator, $fileHandler, $fileParams = null, $headers = null)
{
    if (!isset($fileParams)) {
        $fileParams = [
            'delim' => "\t",
            'escape' => null,
            'newline' => PHP_EOL,
        ];
    }
    $rowsCount = 0;
    $totalRowsCount = 0;
    $reportData = [];

    // Write the headers first
    if (isset($headers)) {
        fwrite(
            $fileHandler,
            generateCSVRow($headers, $fileParams['escape'], $fileParams['delim']).$fileParams['newline']
        );
    }

    // Write data in batches
    $wrote2file = false;
    foreach ($dataGenerator as $data) {
        // Error handling for generator data
        $reportData[] = generateCSVRow(
            $data,
            $fileParams['escape'],
            $fileParams['delim'],
            array_flip($headers),
            DATA_EXPORT_REPLACE_WITH_CR
        );

        $rowsCount++;
        $totalRowsCount++;
        $wrote2file = false;

        if ($rowsCount === DATA_EXPORT_BATCH_LIMIT) {
            // SAVE TO FILE
            fwrite($fileHandler, implode($fileParams['newline'], $reportData).$fileParams['newline']);
            unset($reportData);
            $reportData = [];
            $wrote2file = true;
            $rowsCount = 0;
        }
    }

    if ($wrote2file === false && $rowsCount > 0) {
        // SAVE TO FILE
        fwrite($fileHandler, implode($fileParams['newline'], $reportData).$fileParams['newline']);
    }

    return $totalRowsCount;
}

// generateDataExport()
//  Generate Individual data export file
//
function generateDataExport($logId, PDO $db, $startDate, $endDate)
{
    $result = [];
    $result['fileCount'] = 0;

    // Generate Test Assignments file
    $testAssignmentsFile = exportTestAssignments($logId, $db, $startDate, $endDate);
    if ($testAssignmentsFile instanceof ErrorLDR) {
        return $testAssignmentsFile;
    }

    if ($testAssignmentsFile != SUCCESS) {
        $result['Test Assignments'] = $testAssignmentsFile;
        $result['fileCount'] = $result['fileCount'] + 1;

        // Generate Students file
        $studentsFile = exportStudents($logId, $db, $startDate, $endDate);
        if ($studentsFile instanceof ErrorLDR) {
            return $studentsFile;
        }
        $result['Students'] = $studentsFile;
        $result['fileCount'] = $result['fileCount'] + 1;

        // Generate Organizations file
        $organizationsFile = exportOrganizations($logId, $db, $startDate, $endDate);
        if ($organizationsFile instanceof ErrorLDR) {
            return $organizationsFile;
        }
        $result['Organizations'] = $organizationsFile;
        $result['fileCount'] = $result['fileCount'] + 1;

        // Generate Forms file
        $formsFile = exportForms($logId, $db, $startDate, $endDate);
        if ($formsFile instanceof ErrorLDR) {
            return $formsFile;
        }
        $result['Forms'] = $formsFile;
        $result['fileCount'] = $result['fileCount'] + 1;

        // Generate Items file
        $itemsFile = exportItems($logId, $db, $startDate, $endDate);
        if ($itemsFile instanceof ErrorLDR) {
            return $itemsFile;
        }
        $result['Items'] = $itemsFile;
        $result['fileCount'] = $result['fileCount'] + 1;

        // Generate Item Elements file
        $itemElementsFile = exportItemElements($logId, $db, $startDate, $endDate);
        if ($itemElementsFile instanceof ErrorLDR) {
            return $itemElementsFile;
        }
        $result['Item Elements'] = $itemElementsFile;
        $result['fileCount'] = $result['fileCount'] + 1;

        // Generate Item Results file
        $itemResultsFile = exportItemResults($logId, $db, $startDate, $endDate);
        if ($itemResultsFile instanceof ErrorLDR) {
            return $itemResultsFile;
        }
        $result['Item Results'] = $itemResultsFile;
        $result['fileCount'] = $result['fileCount'] + 1;

        // Generate Item Element Responses file
        $itemElementResponsesFile = exportItemElementResponses($logId, $db, $startDate, $endDate);
        if ($itemElementResponsesFile instanceof ErrorLDR) {
            return $itemElementResponsesFile;
        }
        $result['Item Element Responses'] = $itemElementResponsesFile;
        $result['fileCount'] = $result['fileCount'] + 1;
    }

    return $result;
}

// generateDataExportOptimized()
//  Generate data export using yield data
//
function generateDataExportOptimized($logId, PDO $db, $startDate, $endDate, $testStatus)
{
    $dataExportFiles = [
        "Test Assignments" => [
            'func' => 'dbDataExportTestAssignments',
            'cols' => [
                'Student Test Assignment ID', 'Student Record ID', 'Form Revision ID', 'Test Status', 'Start Timestamp',
                'Submit Timestamp', 'Test Duration', 'Instruction Status', 'Test Raw Score'
            ]
        ],
        "Students" => [
            'func' => 'dbDataExportStudents',
            'cols' => [
                'Student Record ID', 'Student ID', 'Org ID', 'State Student ID', 'First Name', 'Middle Name',
                'Last Name', 'Grade Level', 'Date Of Birth', 'Gender', 'raceAA', 'raceAN', 'raceAS', 'raceHL',
                'raceHP', 'raceWH', 'statusDis', 'statusECO', 'statusELL', 'statusGAT', 'statusLEP',
                'statusMIG', 'Disability Type'
            ]
        ],
        "Organizations" => [
            'func' => 'dbDataExportOrganizations',
            'cols' => [
                'School Organization Identifier', 'School Code', 'School Name', 'State', 'District Code',
                'District Name'
            ]
        ],
        "Forms" => [
            'func' => 'dbDataExportForms',
            'cols' => [
                'Form Revision ID', 'Battery Name', 'Program Name', 'Subject', 'Grade', 'Form Name'
            ]
        ],
        "Items" => [
            'func' => 'dbDataExportItems',
            'cols' => [
                'Form Revision ID', 'Item ID', 'UIN', 'Grade', 'Max Score Points', 'Calculator', 'Domain', 'Cluster',
                'Evidence Statement', 'Content Standard', 'Item Strand', 'Fluency Skill Area', 'Passage Type',
                'Subclaim'
            ]
        ],
        "Item Elements" => [
            'func' => 'dbDataExportItemElements',
            'cols' => [
                'Form Revision ID', 'Item ID', 'Item Element ID', 'QTI Class', 'QTI Max Choices', 'QTI Min Choices'
                , 'Answer Key'
            ]
        ],
        "Item Results" => [
            'func' => 'dbDataExportItemResults',
            'cols' => [
                'Student Test Assignment ID', 'Form Revision ID', 'Item ID', 'Part Number', 'Part Name',
                'Item Position', 'Item Score', 'Item Response Duration', 'Item Tools Used', 'Is Item Answered',
                'Is Item Scored', 'Is Item Correct'
            ]
        ],
        "Item Element Responses" => [
            'func' => 'dbDataExportItemElementResponses',
            'cols' => [
                'Student Test Assignment ID', 'Form Revision ID', 'Item ID', 'Item Element ID', 'Response Identifier',
                'Response Value'
            ]
        ],
    ];

    $files = [];
    $result = [];
    $result['fileCount'] = 0;
    $sqlParams = [$startDate, $endDate];
    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');

    if (isset($testStatus)) {
        if ($testStatus !== TEST_STATUS_ID_SUBMITTED) {
            $sqlParams = null;
            $dateTimeInfo = date('YmdHis');
        }
    }

    $fileParams = [
        'delim' => DATA_EXPORT_DELIM,
        'escape' => DATA_EXPORT_ESCAPE,
        'newline' => DATA_EXPORT_NEWLINE,
    ];

    foreach ($dataExportFiles as $dataExportFile => $fileDetails) {
        // Create an empty file
        $filename = ROOT_ORGANIZATION_NAME."-".str_replace(" ", "_", $dataExportFile)."-".$dateTimeInfo.".tsv";
        $fileHandler = fopen(DATA_EXPORT_DIR.'/'.$filename, 'w');

        $err = null;
        $totalRowsCount = genDataExportFile(
            $logId,
            dbYieldSelect($logId, $db, call_user_func($fileDetails['func'], $testStatus), $err, $sqlParams),
            $fileHandler,
            $fileParams,
            $fileDetails['cols']
        );

        if ($err instanceof ErrorLDR) {
            $err->logError(__FUNCTION__);
            return $err;
        }

        fclose($fileHandler);

        if ($dataExportFile === "Test Assignments" && $totalRowsCount == 0) {
            break;
        }
        $files[$dataExportFile] = $filename;
        $result['fileCount'] = $result['fileCount'] + 1;
    }

    if (count($files) > 0) {
        foreach ($files as $dataExportFile => $file) {
            // Upload to S3 bucket
            $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                        ."/".$file;

            $fileHandler = fopen(DATA_EXPORT_DIR.'/'.$file, 'r');
            $fileStatus = awsStoreFile2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $fileHandler);
            if ($fileStatus instanceof ErrorLDR) {
                $fileStatus->logError(__FUNCTION__);
                return $fileStatus;
            }

            // Get presigned URL
            $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
            if ($reportUrl instanceof ErrorLDR) {
                $reportUrl->logError(__FUNCTION__);
                return $reportUrl;
            }

            $result[$dataExportFile] = $reportUrl;
        }
    }

    return $result;
}

// getDataExport()
//  Generate Data Export files and stores those on S3 bucket
//
function getDataExport()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        [
            'startDate' => DCO_OPTIONAL,
            'endDate' => DCO_OPTIONAL,
            'testStatus' => DCO_OPTIONAL,
            'useBuffer' => DCO_OPTIONAL,
        ]
    );
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $startDate, $endDate, $testStatus, $useBuffer) = $args;

    if (isset($testStatus)) {
        $availableTestStatus = [
            'Submitted' => TEST_STATUS_ID_SUBMITTED,
            'InProgress' => TEST_STATUS_ID_INPROGRESS,
            'Paused' => TEST_STATUS_ID_PAUSED,
            'InProgress/Paused' => "'".TEST_STATUS_ID_INPROGRESS."','".TEST_STATUS_ID_PAUSED."'",
        ];

        if (array_key_exists($testStatus, $availableTestStatus) === false) {
            $errorLDR = new ErrorLDR($logId, '', '');
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        } else {
            $testStatus = $availableTestStatus[$testStatus];
        }
    } else {
        $testStatus = TEST_STATUS_ID_SUBMITTED;
    }

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        if (!isset($startDate)) {
            $errorLDR = new ErrorLDR(
                                $logId,
                                LDR_EC_ARG_NOT_PROVIDED,
                                "Required value startDate not provided"
                            );
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $startDate) !== 1) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_INVALID_DATE_FORMAT, LDR_ED_INVALID_DATE_FORMAT.$startDate);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        if (strtotime($startDate) == strtotime(date('Y-m-d'))) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_DATE_CANNOT_BE_TODAY_DATE, "Start date cannot be today's date");
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        } elseif (strtotime($startDate) > strtotime(date('Y-m-d'))) {
            $errorLDR = new ErrorLDR(
                $logId,
                LDR_EC_DATE_CANNOT_BE_FUTURE_DATE,
                "Start date (".$startDate.") cannot be in the future"
            );
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        if (isset($endDate)) {
            if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $endDate) !== 1) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INVALID_DATE_FORMAT, LDR_ED_INVALID_DATE_FORMAT.$endDate);
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            // Check that the endDate is not before the startDate
            if (!datesInOrder($startDate, $endDate)) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_END_DATE_BEFORE_START_DATE, LDR_ED_END_DATE_BEFORE_START_DATE);
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }

            if (strtotime($endDate) == strtotime(date('Y-m-d'))) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_DATE_CANNOT_BE_TODAY_DATE, "End date cannot be today's date");
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            } elseif (strtotime($endDate) > strtotime(date('Y-m-d'))) {
                $errorLDR = new ErrorLDR(
                                $logId,
                                LDR_EC_DATE_CANNOT_BE_FUTURE_DATE,
                                "End date (".$endDate.") cannot be in the future"
                            );
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            } elseif (strtotime($endDate) > strtotime(date('Y-m-d', strtotime($startDate.' + '.DATA_EXPORT_MAX_RANGE.' days')))) {
                $errorLDR = new ErrorLDR(
                                $logId,
                                LDR_EC_DATE_RANGE_EXCEEDS_MAX,
                                "Requested date range exceeds the max allowed range for data export (".DATA_EXPORT_MAX_RANGE." days)"
                            );
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }
        } else {
            $endDate = date('Y-m-d');

            $endDate1 = date('Y-m-d', strtotime($startDate.' + '.DATA_EXPORT_DEFAULT_RANGE.' days'));
            $endDate2 = date('Y-m-d', strtotime($endDate.' - 1 day'));

            if (strtotime($endDate1) < strtotime($endDate2)) {
                $endDate = $endDate1;
            } else {
                $endDate = $endDate2;
            }
        }
    }

    if ($useBuffer === 'N' || $useBuffer === 'n') {
        // Configure the connection to not use buffer
        if (!$db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false)) {
            $errorLDR = new ErrorLDR(
                $logId,
                LDR_EC_DATABASE_SET_ATTRIBUTE_FAILED,
                'Database connection configuration failed'
            );
            logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
            $errorLDR->logError(__FUNCTION__);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    ini_set('memory_limit', DATA_EXPORT_MAX_MEMORY);
    set_time_limit(DATA_EXPORT_MAX_EXECUTION_TIME);
    // $dataExport = generateDataExport($logId, $db, $startDate, $endDate);
    $dataExport = generateDataExportOptimized($logId, $db, $startDate, $endDate, $testStatus);
    if ($dataExport instanceof ErrorLDR) {
        $dataExport->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService(
        $db,
        $logId,
        ['startDate' => $startDate, 'endDate' => $endDate, 'report' => $dataExport],
        $startTime,
        __FUNCTION__,
        RECORD_PERFORMANCE
    );
}

// getPasswordHash()
//   Hashes a password and returns the password and the hash
//     OR
//   Returns an error if the password is too short or has invalid characters
//
function getPasswordHash($password)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();
    logHAL($logId, HAL_SEVERITY_NOTICE, HAL_LDR_PASSWORD_HASH_REQUESTED, 'Password hash requested');

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Initialize the response
    $response = [];
    $response['password'] = $password;

    // Validate the password
    $argName = 'password';
    $password = validateArg($logId, $db, [$argName => $password], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($password instanceof ErrorLDR)
    {
        $password->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Generate the password hash
    $passwordHash = generatePasswordHash($password);

    // Return the password hash and its length
    $response['hash'] = $passwordHash;
    $response['length'] = strlen($passwordHash);
    $response = json_encode($response);
    echo $response;

    // Log the elapsed time
    logElapsed($logId, microtime(true) - $startTime);

    // Log the response
    logResponse($logId, $response);

    // Close the database connection
    $db = null;
}

// getPerformanceStatistics()
//   Returns LDR service performance statistics for a given date range
//
function getPerformanceStatistics()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'startDate' => DCO_OPTIONAL,
            'endDate' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $startDate, $endDate) = $args;

    if (strlen($startDate) > 0)
    {
        $startDate = validateDate($logId, $startDate, 'startDate');
        if ($startDate instanceof ErrorLDR)
        {
            $startDate->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    if (strlen($endDate) > 0)
    {
        $endDate = validateDate($logId, $endDate, 'endDate');
        if ($endDate instanceof ErrorLDR)
        {
            $endDate->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    if (strlen($startDate) > 0 && strlen($endDate) > 0)
    {
        // Check that the endDate is not before the startDate
        if (!datesInOrder($startDate, $endDate))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_END_DATE_BEFORE_START_DATE, LDR_ED_END_DATE_BEFORE_START_DATE);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    $services = dbGetPerformanceStatistics($logId, $db, $startDate, $endDate);

    // Return the statistics
    echo '{"services":'. json_encode($services) .'}';

    // Log the elapsed time
    $elapsedTime = microtime(true) - $startTime;
    logElapsed($logId, $elapsedTime);

    // Close the database connection
    $db = null;
}

// getQueueProcessingStatistics()
//   Returns LDR queue processing performance statistics for a given date range
//
function getQueueProcessingStatistics()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'startDate' => DCO_OPTIONAL,
            'endDate' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $startDate, $endDate) = $args;

    if (strlen($startDate) > 0)
    {
        $startDate = validateDate($logId, $startDate, 'startDate');
        if ($startDate instanceof ErrorLDR)
        {
            $startDate->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    if (strlen($endDate) > 0)
    {
        $endDate = validateDate($logId, $endDate, 'endDate');
        if ($endDate instanceof ErrorLDR)
        {
            $endDate->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    if (strlen($startDate) > 0 && strlen($endDate) > 0)
    {
        // Check that the endDate is not before the startDate
        if (!datesInOrder($startDate, $endDate))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_END_DATE_BEFORE_START_DATE, LDR_ED_END_DATE_BEFORE_START_DATE);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    $queueProcessing = dbGetQueueProcessingStatistics($logId, $db, $startDate, $endDate);

    // Check for a test status processing error
    if ($queueProcessing instanceof ErrorLDR)
    {
        $queueProcessing->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    finishService($db, $logId, $queueProcessing, $startTime, __FUNCTION__);
}

// getTableNames()
//   Returns the LDR database table names
//
function getTableNames()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Get the table names
    $tables = dbGetTableNames($logId, $db);
    if ($tables instanceof ErrorLDR)
    {
        $tables->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the table names
    finishService($db, $logId, $tables, $startTime, __FUNCTION__);
}

// getVersion()
//   Returns LDR code version info
//
function getVersion()
{
//    global $restFramework;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Get the REST framework version
//    if ($restFramework == REST_FRAMEWORK_SLIM)
        $framework = 'Slim 1.6.0';
//    else
//        $framework = 'Phalcon ' . Phalcon\Version::get();

    // Get the selected database
    try
    {
        $database = DB_PRODUCTION;
        if ($database === false)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_SELECTION_FAILED, LDR_ED_DATABASE_SELECTION_FAILED);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            return;
        }
    }
    catch(Exception $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_SELECTION_FAILED, $error->getMessage());
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the response
    $response = ['Server Software Version' => LDR_SOFTWARE_VERSION,
                 'GitHub Commit Date' => LDR_COMMIT_DATE,
                 'Request Routing Framework' => $framework,
                 'Required Database Version' => LDR_DATABASE_VERSION,
                 'Currently Selected Database' => $database];
    $response = json_encode($response);
    echo $response;

    // Log the response
    logResponse($logId, $response);

    // Log the elapsed time
    logElapsed($logId, microtime(true) - $startTime);
}

//
// LDR support functions
//
// beginService()
//  Performs common start of service operations
//
function beginService($logId, $tokenStatus, $service, $expectedArgs = null, $requestData = null)
{
    global $argFilterMap; //, $restFramework;

    // Get a database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
        return $db;

    // Initialize the service request arguments
    $args = [];

    // Return the database connection as the first argument
    $args[] = $db;

    if ($requestData === null)
    {
        // Get the request data
        if ($expectedArgs !== null)
        {
            switch ($service)
            {
                case 'getAllOrganizations':
                case 'getAssignedTests':
                case 'getChildOrganizationIds':
                case 'getChildOrganizations':
                case 'getClasses':
                case 'getClassStudents':
                case 'getDataExport':
                case 'getDescendentOrganizationIds':
                case 'getOrganization':
                case 'getOrganizationId':
                case 'getOrganizationInfo':
                case 'getPerformanceStatistics':
                case 'getQueueProcessingStatistics':
                case 'getStudentRecord':
                case 'getS3JsonFileContents':
                case 'getStudentRecords':
                case 'getTestAssignments':
                case 'getTestBatteries':
                case 'getTestBatterySubjects':
                case 'getUserAuditRecords':
//                    if ($restFramework == REST_FRAMEWORK_SLIM)
//                    {
                        $request = Slim::getInstance()->request();
                        $requestData = $request->params();
//                    }
//                    else
//                    {
//                        $request = new Phalcon\Http\Request();
//                        $requestData = $request->getQuery();
//                    }
                    break;
                default:
                    $requestData = loadRequestBodyJsonData($logId);
                    if ($requestData instanceof ErrorLDR)
                        return $requestData;
                    break;
            }
        }

        // Check if a session token is required
        if ($tokenStatus != TOKEN_OPTIONAL)
        {
            // Get the request headers
//            if ($restFramework == REST_FRAMEWORK_SLIM)
                $headers = Slim::getInstance()->request()->headers();
//            else
//            {
//                $request = new Phalcon\Http\Request();
//                $headers = $request->getHeaders();
//            }

            // Get the token
            $token = null;
            if (array_key_exists('TOKEN', $headers))
                $token = $headers['TOKEN'];

            // Validate the token
            $token = validateToken($logId, $db, $token);
            if ($token instanceof ErrorLDR)
            {
                $db = null;
                return $token;
            }
        }
    }

    // Validate the expected arguments
    if (is_array($expectedArgs))
    {
        foreach ($expectedArgs as $argName => $argDco)
        {
            // Check if just the JSON data is expected
            if ($argName == 'jsonData')
            {
                $args[] = $requestData;
                break;
            }

            // Check if this arg is in an arg filter group
            if (strncmp($argName, 'classAssignment', strlen('classAssignment')) == 0)
                $argFilterName = 'classAssignmentX';
            else if (strncmp($argName, 'optionalStateData', strlen('optionalStateData')) == 0 &&
                     $argName != 'optionalStateData')
            {
                $argFilterName = 'optionalStateDataX';
            }
            else
                $argFilterName = $argName;

            // Check that the argument filtering has been defined
            if (!array_key_exists($argFilterName, $argFilterMap))
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_003, $argName . ' is missing from $argFilterMap');
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }

            // Validate the argument
            $arg = validateArg($logId, $db, $requestData, $argName, $argFilterMap[$argFilterName], $argDco);
            if ($arg instanceof ErrorLDR)
            {
                $db = null;
                return $arg;
            }

            // Append the argument to the list
            $args[] = $arg;
        }
    }

    // Return the parameters
    return $args;
}

// dateInRange()
//  Returns true if a date is within a date range, false otherwise
//
function dateInRange($startDate, $endDate, $testDate)
{
	// Convert the date strings to UNIX timestamps
	$startTS = strtotime($startDate);
	$endTS = strtotime($endDate);
	$testTS = strtotime($testDate);

	// If any of the conversions failed then return false
	if ($startTS === false || $endTS === false || $testTS === false)
		return false;

	// Check if the test date lies within the start and end date range
	if ($testTS < $startTS || $testTS > $endTS)
		return false;
	else
		return true;
}

// dbCreateClient
//   Creates a new LDR client
//
function dbCreateClient($logId, $db, $clientName, $clientPassword, $clientURL, $clientType = null)
{
    if (isset($clientType) === true) {
        $clientType = (($clientType == 1) || ($clientType == 2) ? $clientType : CLIENT_TYPE_DEVELOPMENT);
    } else {
        $clientType = CLIENT_TYPE_DEVELOPMENT;
    }

    $sql = "INSERT INTO client (clientType, clientName, clientPassword, clientURL)
            VALUES(".$clientType.", '".$clientName."', '".$clientPassword."', '".$clientURL."');";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    } catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// errorQueryExceptionJSON
//   Logs and returns a database query exception
//
function errorQueryExceptionJSON($logId, $error, $functionName, $sql)
{
	$errorLDR = new ErrorLDR($logId, LDR_EC_QUERY_EXCEPTION, $error->getMessage());
	$errorLDR->logMessage($functionName, 'SQL = |' . $sql . '|');
	$errorLDR->returnErrorJSON($functionName);
}

// finishService()
//  Performs common end of service operations
//
function finishService(&$db, $logId, $response, $startTime, $service, $recordPerformance = false)
{
    // Return the response
    $response = json_encode($response);
    echo $response;

    // Log the response
    logResponse($logId, $response);

    // Log the elapsed time
    $elapsedTime = microtime(true) - $startTime;
    logElapsed($logId, $elapsedTime);

    // Optionally update the daily performance record
    if ($recordPerformance) {
        // Lock the test assignment and user_audit tables
        $sql = "LOCK TABLES performance WRITE";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        } catch(PDOException $error) {
            $result = errorQueryException($logId, $error, __FUNCTION__, $sql);
            $result->logError(__FUNCTION__);
            return;
        }

        dbRecordPerformance($logId, $db, date("Y-m-d"), $service, $elapsedTime);

        $result = dbUnlockTables($logId, $db);
        if ($result instanceof ErrorLDR) {
            $result->logError(__FUNCTION__);
            return;
        }
    }

    // Close the database connection
    $db = null;
}

// flattenArray()
//  Takes a multi-dimensional array and return one dimentional array.
//  Note: It doesn't maintain array keys
//
function flattenArray($inputArray)
{
    $array = [];
    foreach ($inputArray as $arr) {
        if (!is_array($arr)) {
            $array[] = $arr;
        } else {
            $array = array_merge($array, flattenArray($arr));
        }
    }

    return $array;
}

// generateCSVData()
//  Generates a CSV data from an array
//
function generateCSVData($logId, $data, $escape = null, $addHeaders = true, $selectedCols = null, $delim = ",", $newLine = PHP_EOL)
{
    if (is_array($data)) {
        if ((!empty($data)) && (count($data) > 0)) {
            $csvData = [];
            reset($data);

            // Validate input data
            if (isset($escape)) {
                if (preg_match('/^["]$/', $escape) !== 1) {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_INVALID_CSV_ESCAPE_CHARACTER
                                                , LDR_ED_INVALID_CSV_ESCAPE_CHARACTER.$escape);
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
            }

            if (preg_match('/^[\t,]$/', $delim) !== 1) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INVALID_CSV_DELIM, LDR_ED_INVALID_CSV_DELIM.$delim);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }

            if (preg_match('/^(\n\r|\n)$/', $newLine) !== 1) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INVALID_CSV_NEWLINE, LDR_ED_INVALID_CSV_NEWLINE);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }

            if ($addHeaders === true) {
                // Extract Headers form the first element
                $dataKeys = array_keys($data);
                $firstKey = current($dataKeys);

                $firstRow = $data[$firstKey];
                $headers = array_keys($firstRow);

                if (isset($selectedCols)) {
                    $headers = $selectedCols;
                    $selectedCols = array_flip($selectedCols);
                    $csvData[] = generateCSVRow($headers, $escape, $delim, null);
                } else {
                    $csvData[] = generateCSVRow($headers, $escape, $delim, $selectedCols);
                }

                $csvData[] = generateCSVRow($firstRow, $escape, $delim, $selectedCols);
                unset($data[$firstKey]);
            }

            if (count($data) > 0) {
                // Convert data to CSV and escape strings with double quotes
                foreach ($data as $dataRow) {
                    $csvData[] = generateCSVRow($dataRow, $escape, $delim, $selectedCols);
                }
            }

            $csvData = implode($newLine, $csvData);

            return $csvData;
        } else {
            $errorLDR = new ErrorLDR($logId, LDR_EC_DATA_IS_EMPTY, LDR_ED_DATA_IS_EMPTY);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    } else {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATA_IS_NOT_AN_ARRAY, LDR_ED_DATA_IS_NOT_AN_ARRAY);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
}

// generateCSVRow()
//  Converts the input array to a string of CSV data
//
function generateCSVRow($dataRow, $escape = null, $delim = ',', $selectedCols = null, $replaceWithCR = false)
{
    if (isset($selectedCols)) {
        $intersectedData = array_intersect_key($dataRow, $selectedCols);
        $dataRow = array_merge($selectedCols, $intersectedData);
    }

    if (isset($escape)) {
        foreach ($dataRow as &$rowElement) {
            if (!is_numeric($rowElement)) {
                $rowElement = $escape.$rowElement.$escape;
            }
        }
    }

    if ($replaceWithCR === true) {
        $dataRow = str_replace("\n", "\r", $dataRow);
    }
    $csvData = implode($delim, $dataRow);

    return $csvData;
}

// generateGUID_RFC4122v4
//  Generates an RFC-4122 version 4-compliant GUID
//
function generateGUID_RFC4122v4()
{
    $uuidCharSet = '0123456789ABCDEF';
    $nibbleCharSet = '89AB';

    $uuid = '';
    for ($idx = 0; $idx < LENGTH_GUID_RFC4122; $idx++)
    {
        switch ($idx)
        {
            case 14:
                $uuid .= '4'; // RFC4122 version 4
                break;
            case 19:
                $uuid .= substr($nibbleCharSet, mt_rand(0, 3), 1);
                break;
            case 8:
            case 13:
            case 18:
            case 23:
            $uuid .= '-'; // RFC4122 version 4
                break;
            default:
                $uuid .= substr($uuidCharSet, mt_rand(0, 15), 1);
                break;
        }
    }

    return $uuid;
}

// generateIdentifier
//  Generates a random alphanumeric identifier
//
function generateIdentifier($length)
{
    $identifierCharSet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'; // Excludes 0, 1, I, and O
    $charSetMaxIndex = strlen($identifierCharSet) - 1;

    $identifier = "";
    while ($length-- > 0)
        $identifier .= substr($identifierCharSet, mt_rand(0, $charSetMaxIndex), 1);

    return $identifier;
}

// generatePasswordHash
//  Generates a password hash for LDR session authentication
//
function generatePasswordHash($plaintext, $ciphertext = null)
{
    define('PASS_LENGTH', 37);
    define('SALT_LENGTH', 23);
    define('CACA_BEFORE', 29);
    define('CACA_AFTER',  39);

    if ($ciphertext === null)
    {
        $salt = strtoupper(substr(sha1(uniqid(rand(), true)), 0, SALT_LENGTH));
        $caca_before = strtoupper(substr(sha1(uniqid(rand(), true)), 0, CACA_BEFORE));
        $caca_after = strtoupper(substr(sha1(uniqid(rand(), true)), 0, CACA_AFTER));
    }
    else
    {
        $caca_before = substr($ciphertext, 0, CACA_BEFORE);
        $salt = substr($ciphertext, CACA_BEFORE, SALT_LENGTH);
        $caca_after = substr($ciphertext, -CACA_AFTER);
    }

    $password = strtoupper(substr(sha1($salt . $plaintext), 0, PASS_LENGTH));

    return $caca_before . $salt . $password . $caca_after;
}

// getNonEmptyParameterCount()
//   Returns the count of non-empty parameters
//
function getNonEmptyParameterCount($parameters)
{
    // Initialize the count
    $parameterCount = 0;

    // Count the non-empty parameters
    if (is_array($parameters))
    {
        foreach ($parameters as $parameter)
        {
            if (strlen($parameter) > 0)
                $parameterCount++;
        }
    }

    // Return the count
    return $parameterCount;
}

// loadRequestBodyJsonData()
//   Loads the request body JSON as an associative array
//
function loadRequestBodyJsonData($logId)
{
//    global $restFramework;

    // Get the request body
//    if ($restFramework == REST_FRAMEWORK_SLIM)
//    {
        $request = Slim::getInstance()->request();
        $requestBody = $request->getBody();
//    }
//    else
//    {
//        $request = new Phalcon\Http\Request();
//        $requestBody = $request->getRawBody();
//    }

    // Check if request body is missing
    if (strlen($requestBody) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REQUEST_BODY_NOT_PROVIDED, LDR_ED_REQUEST_BODY_NOT_PROVIDED);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Decode the request body JSON into associative arrays
    $jsonData = json_decode($requestBody, true);
    if ($jsonData === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REQUEST_BODY_DECODE_FAILED,
                                 LDR_ED_REQUEST_BODY_DECODE_FAILED . $requestBody);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    logRequestData($logId, $jsonData);

    return $jsonData;
}

// logCallADP
//  Appends a message to the LDR call ADP log
//
function logCallADP($logId, $url, $payload = '')
{
    if (LOG_ADP_CALLS)
    {
        global $logFilename;
        // Generate the ADP call log file name
        $filename = $logFilename . LOG_NAME_CALL_ADP;

        // Log the request
        error_log(date('H:i:s') . ' [' . $logId . '] ' . $url . ' ' . $payload . CR, 3, $filename);
    }
}

// logDebug
//  Appends a message to the LDR debug log
//
function logDebug($logId, $message)
{
    global $logFilename;

    // Generate the debug log file name
    $filename = $logFilename . LOG_NAME_DEBUG;

    // Log the message
    error_log(date('H:i:s') . ' [' . $logId . '] ' . $message . CR, 3, $filename);
}

// logElapsed
//  Appends an elapsed time message to the LDR request log
//
function logElapsed($logId, $elapsed)
{
    if (LOG_REQUESTS)
    {
        global $logFilename;

        // Generate the request log file name
        $filename = $logFilename . LOG_NAME_REQUEST;

        // Log the elapsed time
        error_log(date('H:i:s') . ' [' . $logId . '] [' . $elapsed . ']' . CR, 3, $filename);
    }
}

// logError
//  Appends a message to the LDR error log
//
function logError($logId, $error)
{
    global $logFilename;

    // Generate the error log file name
    $filename = $logFilename . LOG_NAME_ERROR;

	// Log the error
	error_log(date('H:i:s') . ' [' . $logId . '] [' . $_SERVER['REMOTE_ADDR'] . '] ERROR ' . $error . CR, 3, $filename);
}

// logRequest
//  Appends a message to the LDR request log
//
function logRequest()
{
//    global $restFramework;
    global $logFilename;

    // Generate the request log file name
    $filename = $logFilename . LOG_NAME_REQUEST;

    // Generate unique log identifier
    $logId = generateIdentifier(4);

    if (LOG_REQUESTS)
    {
        // Build an output string that shows the request method, path, and body
//        if ($restFramework == REST_FRAMEWORK_SLIM)
//        {
            $request = Slim::getInstance()->request();
            $output = $request->getMethod() . ' ' . $request->getPathInfo() . ' ' . $request->getBody();
//        }
//        else
//        {
//            $request = new Phalcon\Http\Request();
//            $output = $request->getMethod() . ' ' . $request->getRawBody();
//        }

        // Log the message
        error_log(date('H:i:s') . ' [' . $logId . '] [' . $_SERVER['REMOTE_ADDR'] . '] ' . $output . CR, 3, $filename);
    }

    return $logId;
}

// logRequestData
// Logs data within the request body
//
function logRequestData($logId, $requestData)
{
    if (LOG_REQUESTS) {
        global $logFilename;

        // Generate the request log file name
        $filename = $logFilename . LOG_NAME_REQUEST;

        $requestData = json_encode($requestData);

        error_log(date('H:i:s') . ' [' . $logId . '] ' . $requestData . CR, 3, $filename);
    }
}

// logResponse
//  Appends a message to the LDR response log
//
function logResponse($logId, $response)
{
    if (LOG_RESPONSES)
    {
        global $logFilename;

        // Generate the response log file name
        $filename = $logFilename . LOG_NAME_RESPONSE;

        // Log the response
        error_log(date('H:i:s') . ' [' . $logId . '] [' . $_SERVER['REMOTE_ADDR'] . '] ' . $response . CR, 3, $filename);
    }
}

// logTestStatus
//  Appends a message to the LDR test status log
//
function logTestStatus($logId, $msgId, $message)
{
    if (LOG_TEST_STATUS)
    {
        global $logFilename;

        // Generate the test status log file name
        $filename = $logFilename . LOG_NAME_TEST_STATUS;

        // Log the test status
        error_log(date('H:i:s') . ' [' . $logId . '] [' . $msgId . '] ' . $message . CR, 3, $filename);
    }
}

// skipService()
//  Substitutes for finishService() when a service is skipped for performance testing
//
function skipService($logId, $response, $startTime)
{
    // Return the response
    $response = json_encode($response);
    echo $response;

    // Log the response
    logResponse($logId, $response);

    // Log the elapsed time
    $elapsedTime = microtime(true) - $startTime;
    logElapsed($logId, $elapsedTime);
}
