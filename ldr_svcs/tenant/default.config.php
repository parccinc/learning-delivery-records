<?php
define('TENANT_LOG_DIR', '');
define('TENANT_ID', 0);

// Database
define('DB_HOSTNAME',   "");
//define('DB_PORT', 3306);
define('DB_PRODUCTION', "");
define('DB_USERNAME',   "");
define('DB_PASSWORD',   "");
define('DB_FIRST_STUDENT_RECORD', 1);
define('DB_FIRST_TEST_ASSIGNMENT', 1);

// Redis
define('REDIS_ENABLED', false);
define('REDIS_HOSTNAME', '127.0.0.1');
define('REDIS_STATUS_FILENAME', "redis_status.txt");

// ADP Publisher
define('ADP_HOSTNAME', "");
define('ADP_PASSWORD', "");
define('ADP_REQUEST_SCHEME', "");
define('ADP_RESOURCE_PATH', "/api/");
define('ADP_SECRET', "");
define('ADP_USERNAME', "");

// AWS
define('ADP_S3_CONTENT_BUCKET', "");
define('ADP_S3_CONTENT_ROOT', "");
define('ADP_S3_RESULTS_BUCKET', "");
define('ADP_S3_RESULTS_ROOT', "");
define('LDR_S3_ACTIVITY_REPORTS_BUCKET', "");
define('LDR_S3_ACTIVITY_REPORTS_ROOT', "");
define('LDR_S3_ACTIVITY_REPORTS_PATH', "");
define('LDR_S3_RESULTS_EXPORT_BUCKET', "");
define('LDR_S3_RESULTS_EXPORT_ROOT', "");
define('LDR_S3_RESULTS_EXPORT_PATH', "");
define('AWS_S3_PROFILE', "parcc-ads");
define('AWS_SQS_PROFILE', "sqs-queue");
define('AWS_SQS_QUEUE_URL', "https://sqs.us-west-2.amazonaws.com/578207864729/MyQueue");
define('AWS_SQS_REGION', "us-west-2");
define('LDR_S3_PRESIGNED_EXPIRATION', '+120 minutes');

// HAL logging
define('HAL_HOSTNAME', "localhost");
define('HAL_REQUEST_SCHEME', "https");
define('HAL_RESOURCE_PATH', "");

// Application behavior
define('ALLOWED_CLIENT_TYPE', 1); // 1 = Development, 2 = Production
define('ENABLE_NESTED_SETS', false);
define('LOG_ADP_CALLS', true);
define('LOG_REQUESTS', true);
define('LOG_RESPONSES', true);
define('LOG_TEST_STATUS', true);
define('RECORD_PERFORMANCE', true);
define('REGISTRATION_KEY_TIMEOUT', 600); // 10 minutes
define('SKIP_ADP_COMM', false);
define('SKIP_PROCESSING', false);
define('SKIP_VALIDATION', false);
define('STORE_TEST_FORM_REVISIONS', true);
define('STORE_TEST_RESULTS', true);
define('TESTKEY', "");
define('TOKEN_TIMEOUT', 1200); // 20 minutes
define('REPORT_MAX_EXECUTION_TIME', 60); // 2 minutes
define('TEST_ASSIGNMENT_LINE_READER_DEFAULT', 'N');
define('TEST_ASSIGNMENT_TTS_DEFAULT', 'N');
define('AUTO_TEST_ASSIGNMENT', true);
define('BATCHED_VERSION', false);
define('BULK_ASSIGNMENT_TIMEOUT', 1200);    // 20 minutes
define('STORE_TESTS_RESULTS', 1200);    // 20 minutes
define('ITEM_CORRECT_BY_MAX_POINTS', false);

// Data Export Behavior
define('DATA_EXPORT_DEFAULT_RANGE', 31);     // Range in days
define('DATA_EXPORT_MAX_RANGE', 31);     // Range in days
define('DATA_EXPORT_MAX_MEMORY', '1024M');
define('DATA_EXPORT_TIME_ZONE', 'US/Eastern');
define('DATA_EXPORT_BATCH_LIMIT', 1000);  // Rows count to save to file
define('DATA_EXPORT_MAX_EXECUTION_TIME', 300);  // Should match the load balancer timeout time
define('DATA_EXPORT_DELIM', "\t");
define('DATA_EXPORT_ESCAPE', null);
define('DATA_EXPORT_NEWLINE', "\r\n");
define('DATA_EXPORT_REPLACE_WITH_CR', false);   // Replace any response that contains newline with a carriage return

// Application values
define('ROOT_ORGANIZATION_NAME', "PARCC");
