<?php
// LDR database migration definitions
$forwardMigrationVersions =
[
    '0.7.5' => [],
    '0.9.0' =>
    [
        "CREATE TABLE test_battery_grade
           (testBatteryGradeId INT NOT NULL AUTO_INCREMENT,
            testBatteryGrade VARCHAR(" . MAXLEN_TEST_BATTERY_GRADE . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatteryGradeId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO test_battery_grade (testBatteryGrade) VALUES ('K'), ('1'), ('2'), ('3'), ('4'),
           ('5'), ('6'), ('7'), ('8'), ('9'), ('10'), ('11'), ('12'), ('Multi-level')",

        "CREATE TABLE test_battery_permissions
           (testBatteryPermissionsId INT NOT NULL AUTO_INCREMENT,
            testBatteryPermissions VARCHAR(" . MAXLEN_TEST_BATTERY_PERMISSIONS . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatteryPermissionsId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO test_battery_permissions (testBatteryPermissions) VALUES ('Non-Restricted'), ('Restricted')",

        "CREATE TABLE test_battery_program
           (testBatteryProgramId INT NOT NULL AUTO_INCREMENT,
            testBatteryProgram VARCHAR(" . MAXLEN_TEST_BATTERY_PROGRAM . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatteryProgramId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO test_battery_program (testBatteryProgram) VALUES ('Diagnostic Assessment'),
           ('K2 Formative'), ('Mid-Year/Interim'), ('Practice Test'), ('Speaking & Listening')",

        "CREATE TABLE test_battery_scoring
           (testBatteryScoringId INT NOT NULL AUTO_INCREMENT,
            testBatteryScoring VARCHAR(" . MAXLEN_TEST_BATTERY_SCORING . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatteryScoringId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO test_battery_scoring (testBatteryScoring) VALUES ('Immediate'), ('Delayed')",

        "CREATE TABLE test_battery_security
           (testBatterySecurityId INT NOT NULL AUTO_INCREMENT,
            testBatterySecurity VARCHAR(" . MAXLEN_TEST_BATTERY_SECURITY . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatterySecurityId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO test_battery_security (testBatterySecurity) VALUES ('Non-Secure'), ('Secure')",

        "CREATE TABLE test_battery_subject
           (testBatterySubjectId INT NOT NULL AUTO_INCREMENT,
            testBatterySubject VARCHAR(" . MAXLEN_TEST_BATTERY_SUBJECT . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatterySubjectId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO test_battery_subject (testBatterySubject) VALUES ('ELA'), ('Math'), ('N/A')",

        "CREATE TABLE test_battery
           (testBatteryId INT NOT NULL,
            testBatteryName VARCHAR(" . MAXLEN_TEST_BATTERY_NAME . ") NOT NULL,
            testBatterySubjectId INT NOT NULL,
            testBatteryGradeId INT NOT NULL,
            testBatteryProgramId INT NOT NULL,
            testBatterySecurityId INT NOT NULL,
            testBatteryPermissionsId INT NOT NULL,
            testBatteryScoringId INT NOT NULL,
            testBatteryDescription VARCHAR(" . MAXLEN_TEST_BATTERY_DESCRIPTION . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatteryId),
            INDEX (testBatteryName),
            FOREIGN KEY (testBatterySubjectId)
              REFERENCES test_battery_subject(testBatterySubjectId),
            FOREIGN KEY (testBatteryGradeId)
              REFERENCES test_battery_grade(testBatteryGradeId),
            FOREIGN KEY (testBatteryProgramId)
              REFERENCES test_battery_program(testBatteryProgramId),
            FOREIGN KEY (testBatterySecurityId)
              REFERENCES test_battery_security(testBatterySecurityId),
            FOREIGN KEY (testBatteryPermissionsId)
              REFERENCES test_battery_permissions(testBatteryPermissionsId),
            FOREIGN KEY (testBatteryScoringId)
              REFERENCES test_battery_scoring(testBatteryScoringId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_battery_form
           (testBatteryId INT NOT NULL,
            testBatteryFormId INT NOT NULL,
            testBatteryFormName VARCHAR(" . MAXLEN_TEST_BATTERY_FORM_NAME . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testBatteryFormId),
            INDEX (testBatteryFormName),
            FOREIGN KEY (testBatteryId)
              REFERENCES test_battery(testBatteryId))
            ENGINE=InnoDB COLLATE=utf8_bin"
    ],
    '0.9.6' =>
    [
        "CREATE TABLE test_status
           (testStatusId INT NOT NULL AUTO_INCREMENT,
            testStatus VARCHAR(" . MAXLEN_TEST_STATUS . ") NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testStatusId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO test_status (testStatus)
         VALUES ('Scheduled'), ('InProgress'), ('Paused'), ('Submitted'), ('Completed'), ('Canceled')",

        "CREATE TABLE test_assignment
           (testAssignmentId INT NOT NULL AUTO_INCREMENT,
            studentRecordId INT NOT NULL,
            testBatteryFormId INT NOT NULL,
            testBatteryId INT NOT NULL,
            testStatusId INT NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testAssignmentId),
            FOREIGN KEY (studentRecordId)
              REFERENCES student_record(studentRecordId),
            FOREIGN KEY (testBatteryFormId)
              REFERENCES test_battery_form(testBatteryFormId),
            FOREIGN KEY (testBatteryId)
              REFERENCES test_battery(testBatteryId),
            FOREIGN KEY (testStatusId)
              REFERENCES test_status(testStatusId))
            ENGINE=InnoDB COLLATE=utf8_bin"
    ],
    '1.1.4' =>
    [
        "ALTER TABLE student_record
            ADD optionalStateData1 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER enrollEndDate",

        "ALTER TABLE student_record
            ADD optionalStateData2 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER disabilityType",

        "ALTER TABLE student_record
            ADD optionalStateData3 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER optionalStateData2",

        "ALTER TABLE student_record
            ADD optionalStateData4 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER optionalStateData3",

        "ALTER TABLE student_record
            ADD optionalStateData5 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER optionalStateData4",

        "ALTER TABLE student_record
            ADD optionalStateData6 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER optionalStateData5",

        "ALTER TABLE student_record
            ADD optionalStateData7 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER optionalStateData6",

        "ALTER TABLE student_record
            ADD optionalStateData8 VARCHAR(" . MAXLEN_OPTIONAL_STATE_DATA . ") NULL AFTER optionalStateData7"
    ],
    '1.2.0' =>
    [
        "ALTER TABLE test_assignment
            ADD adpTestAssignmentId INT NULL AFTER testAssignmentId",

        "ALTER TABLE test_assignment
            ADD testKey CHAR(" . LENGTH_TEST_KEY . ") NOT NULL AFTER testStatusId"
    ],
    '1.2.8' =>
    [
        "ALTER TABLE test_assignment
            ADD instructionStatus INT NOT NULL DEFAULT 0 AFTER testKey"
    ],
    '1.3.2' =>
    [
        "ALTER TABLE test_assignment
            ADD enableLineReader ENUM('Y', 'N') NOT NULL DEFAULT 'N' AFTER instructionStatus",

        "ALTER TABLE test_assignment
            ADD enableTextToSpeech ENUM('Y', 'N') NOT NULL DEFAULT 'N' AFTER enableLineReader"
    ],
    '1.3.6' =>
    [
        "ALTER TABLE student_record DROP INDEX testingOrgId",
        "ALTER TABLE student_record DROP INDEX localIdentifier",
        "ALTER TABLE student_record DROP COLUMN testingOrgId",
        "ALTER TABLE student_record DROP COLUMN enrollEndDate",
        "ALTER TABLE student_record DROP COLUMN enrollStartDate"
    ],
    '1.4.2' =>
    [
        "CREATE TABLE embedded_score_report
           (embeddedScoreReportId INT NOT NULL AUTO_INCREMENT,
            embeddedScoreReport TEXT,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (embeddedScoreReportId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "ALTER TABLE test_assignment
            ADD embeddedScoreReportId INT NOT NULL DEFAULT 0 AFTER testStatusId"
    ],
    '1.5.5' =>
    [
        "CREATE TABLE queue_processing
           (queueProcessingId INT NOT NULL AUTO_INCREMENT,
            statisticsDate DATE NULL,
            hostname TEXT NOT NULL,
            totalMessagesProcessed INT NOT NULL,
            sqsCreateClientElapsedTime DOUBLE NULL,
            sqsReceiveMessageElapsedTime DOUBLE NULL,
            sqsDeleteMessageElapsedTime DOUBLE NULL,
            ldrGetTestAssignmentIdElapsedTime DOUBLE NULL,
            ldrGetTestAssignmentElapsedTime DOUBLE NULL,
            ldrUpdateTestStatusElapsedTime DOUBLE NULL,
            ldrStoreTestResultsElapsedTime DOUBLE NULL,
            adpUpdateTestStatusElapsedTime DOUBLE NULL,
            halLoggingElapsedTime DOUBLE NULL,
            submittedMessagesProcessed INT NOT NULL,
            submittedMessagesIgnored INT NOT NULL,
            submittedMessagesInvalid INT NOT NULL,
            submittedMessagesFailed INT NOT NULL,
            submittedElapsedTime DOUBLE NULL,
            startedMessagesProcessed INT NOT NULL,
            startedMessagesIgnored INT NOT NULL,
            startedMessagesInvalid INT NOT NULL,
            startedMessagesFailed INT NOT NULL,
            startedElapsedTime DOUBLE NULL,
            pausedMessagesProcessed INT NOT NULL,
            pausedMessagesIgnored INT NOT NULL,
            pausedMessagesInvalid INT NOT NULL,
            pausedMessagesFailed INT NOT NULL,
            pausedElapsedTime DOUBLE NULL,
            resumedMessagesProcessed INT NOT NULL,
            resumedMessagesIgnored INT NOT NULL,
            resumedMessagesInvalid INT NOT NULL,
            resumedMessagesFailed INT NOT NULL,
            resumedElapsedTime DOUBLE NULL,
            unknownStatusChangesProcessed INT NOT NULL,
            unknownMessagesProcessed INT NOT NULL,
            unknownElapsedTime DOUBLE NULL,
            PRIMARY KEY (queueProcessingId),
            INDEX (statisticsDate))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "ALTER TABLE test_assignment
            ADD lastQueueEventTime INT NOT NULL DEFAULT 0 AFTER testStatusId"
    ],
    '1.6.3' =>
    [
        "ALTER TABLE client
            ADD clientType INT NOT NULL DEFAULT '" . CLIENT_TYPE_DEVELOPMENT . "' AFTER clientId",

        "ALTER TABLE queue_processing
            ADD ldrUpdateLastQueueEventTime DOUBLE NULL DEFAULT 0 AFTER ldrGetTestAssignmentElapsedTime"
    ],
    '1.6.4' =>
    [
        "INSERT INTO client (clientType, clientName, clientPassword) VALUES " .
        "('" . CLIENT_TYPE_PRODUCTION . "', 'prod_adp', " .
        "'EDC8E69ED34B58399815F9C58E56DCF085EEF4C6FDD149DF7CFB036EF10E89485052F1179DA80697F9FA97C468ABE857B5784EBF85F5F29BA30FD1D214815EB9'), " .
        "('" . CLIENT_TYPE_PRODUCTION . "', 'prod_tap', " .
        "'DCC1B52E292C05AE6F17DF1219B0DF1728208BE947C9C12F595366889B701CBB8BC80390178AD3FF6AF963F13873F36863ED8021B8EFBBAB5670BA2F9A6AF5E1'), " .
        "('" . CLIENT_TYPE_PRODUCTION . "', 'prod_user', " .
        "'2A9F5571308B7E5EC834BACF4C60337CDEF74730DF5EB41FFBD932E9FE61DA871C0D604D2DA647B2BA1B5B415A40125BBF1A03F2D85CA094A686AEF024938A49')"
    ],
    '1.7.0' =>
    [
        "CREATE TABLE qti_class
       (qtiClassId INT NOT NULL AUTO_INCREMENT,
        qtiClass VARCHAR(" . MAXLEN_QTI_CLASS . ") NOT NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (qtiClassId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO qti_class (qtiClass)
          VALUES ('assessmentItem'), ('choiceInteraction'), ('simpleChoice'), ('responseDeclaration')",

        "CREATE TABLE calculator_type
       (calculatorTypeId INT NOT NULL AUTO_INCREMENT,
        calculatorType VARCHAR(" . MAXLEN_CALCULATOR_TYPE . ") NOT NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (calculatorTypeId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO calculator_type (calculatorType)
          VALUES ('No Calculator'), ('Scientific Calculator')",

        "CREATE TABLE test_form_revision
       (testFormRevisionId INT NOT NULL,
        title VARCHAR(" . MAXLEN_TEST_FORM_REVISION_TITLE . ") NULL,
        identifier VARCHAR(" . MAXLEN_TEST_FORM_REVISION_IDENTIFIER . ") NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (testFormRevisionId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_form_part
       (testFormRevisionId INT NOT NULL,
        testFormPartId INT NOT NULL AUTO_INCREMENT,
        identifier VARCHAR(" . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ") NULL,
        navigationMode VARCHAR(" . MAXLEN_TEST_FORM_PART_NAVIGATION_MODE . ") NULL,
        submissionMode VARCHAR(" . MAXLEN_TEST_FORM_PART_SUBMISSION_MODE . ") NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (testFormPartId),
        FOREIGN KEY (testFormRevisionId)
          REFERENCES test_form_revision(testFormRevisionId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_form_assessment_section
       (testFormRevisionId INT NOT NULL,
        testFormPartId INT NOT NULL,
        testFormAssessmentSectionId INT NOT NULL AUTO_INCREMENT,
        identifier VARCHAR(" . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ") NULL,
        title VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_SECTION_TITLE . ") NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (testFormAssessmentSectionId),
        FOREIGN KEY (testFormRevisionId)
          REFERENCES test_form_revision(testFormRevisionId),
        FOREIGN KEY (testFormPartId)
          REFERENCES test_form_part(testFormPartId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_form_assessment_item
       (testFormRevisionId INT NOT NULL,
        testFormAssessmentSectionId INT NOT NULL,
        testFormAssessmentItemId INT NOT NULL AUTO_INCREMENT,
        uin VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_UIN . ") NULL,
        itemId VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEMID . ") NULL,
        identifier VARCHAR(" . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ") NULL,
        serial VARCHAR(" . MAXLEN_TEST_FORM_ANY_SERIAL . ") NULL,
        domain VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_DOMAIN . ") NULL,
        cluster VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_CLUSTER . ") NULL,
        contentStandard VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_CONTENT_STANDARD . ") NULL,
        evidenceStatement VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_EVIDENCE_STATEMENT . ") NULL,
        itemGrade CHAR(" . LENGTH_GRADE_LEVEL . ") NULL,
        maxScorePoints INT NULL,
        calculatorTypeId INT NULL,
        qtiClassId INT NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (testFormAssessmentItemId),
        FOREIGN KEY (testFormRevisionId)
          REFERENCES test_form_revision(testFormRevisionId),
        FOREIGN KEY (testFormAssessmentSectionId)
          REFERENCES test_form_assessment_section(testFormAssessmentSectionId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_form_system_item
       (testFormRevisionId INT NOT NULL,
        testFormAssessmentSectionId INT NOT NULL,
        testFormSystemItemId INT NOT NULL AUTO_INCREMENT,
        itemId VARCHAR(" . MAXLEN_TEST_FORM_SYSTEM_ITEM_ITEMID . ") NULL,
        identifier VARCHAR(" . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ") NULL,
        serial VARCHAR(" . MAXLEN_TEST_FORM_ANY_SERIAL . ") NULL,
        qtiClassId INT NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (testFormSystemItemId),
        FOREIGN KEY (testFormRevisionId)
          REFERENCES test_form_revision(testFormRevisionId),
        FOREIGN KEY (testFormAssessmentSectionId)
          REFERENCES test_form_assessment_section(testFormAssessmentSectionId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE response_cardinality
       (responseCardinalityId INT NOT NULL AUTO_INCREMENT,
        responseCardinality VARCHAR(" . MAXLEN_RESPONSE_CARDINALITY . ") NOT NULL,
        deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
        PRIMARY KEY (responseCardinalityId))
        ENGINE=InnoDB COLLATE=utf8_bin",

        "INSERT INTO response_cardinality (responseCardinality)
          VALUES ('single'), ('multiple')"
    ],
    '1.7.1' =>
    [
        "ALTER TABLE organization
          ADD COLUMN fake ENUM('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'",
    ],
    '1.7.2' =>
    [
        "ALTER TABLE client
          ADD COLUMN clientURL VARCHAR(" . MAXLEN_CLIENT_URL . ") NOT NULL"
    ],
    '1.7.4' =>
    [
        "INSERT INTO test_battery_program (testBatteryProgramId, testBatteryProgram) VALUES (6, 'Summative')",
    ],
    '1.7.5' =>
    [
        "CREATE TABLE test_form_item_element
           (testFormRevisionId INT NOT NULL,
            testFormAssessmentItemId INT NOT NULL,
            testFormItemElementId INT NOT NULL AUTO_INCREMENT,
            serial VARCHAR(" . MAXLEN_TEST_FORM_ANY_SERIAL . ") NULL,
            responseIdentifier VARCHAR(" . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ") NULL,
            qtiClassId INT NULL,
            maxChoices INT NULL,
            minChoices INT NULL,
            shuffle ENUM('Y', 'N') NULL DEFAULT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testFormItemElementId),
            FOREIGN KEY (testFormRevisionId)
              REFERENCES test_form_revision(testFormRevisionId),
            FOREIGN KEY (testFormAssessmentItemId)
              REFERENCES test_form_assessment_item(testFormAssessmentItemId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_form_item_element_response
           (testFormRevisionId INT NOT NULL,
            testFormItemElementId INT NOT NULL,
            testFormItemElementResponseId INT NOT NULL AUTO_INCREMENT,
            serial VARCHAR(" . MAXLEN_TEST_FORM_ANY_SERIAL . ") NULL,
            identifier VARCHAR(" . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ") NULL,
            responseCardinalityId INT NULL,
            qtiClassId INT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testFormItemElementResponseId),
            FOREIGN KEY (testFormRevisionId)
              REFERENCES test_form_revision(testFormRevisionId),
            FOREIGN KEY (testFormItemElementId)
              REFERENCES test_form_item_element(testFormItemElementId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_form_item_correct_response
           (testFormRevisionId INT NOT NULL,
            testFormItemElementResponseId INT NOT NULL,
            testFormItemCorrectResponseId INT NOT NULL AUTO_INCREMENT,
            value TEXT NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testFormItemCorrectResponseId),
            FOREIGN KEY (testFormRevisionId)
              REFERENCES test_form_revision(testFormRevisionId),
            FOREIGN KEY (testFormItemElementResponseId)
              REFERENCES test_form_item_element_response(testFormItemElementResponseId))
            ENGINE=InnoDB COLLATE=utf8_bin"
    ],
    '1.7.6' =>
    [
        "ALTER TABLE test_form_item_correct_response
          ADD COLUMN position TINYINT NOT NULL AFTER value"
    ],
    '1.7.7' =>
    [
        "INSERT INTO qti_class (qtiClassId, qtiClass)
              VALUES (5, 'textEntryInteraction')",

        "ALTER TABLE test_assignment
          ADD COLUMN testResultsId INT NULL AFTER testStatusId",
        "ALTER TABLE test_assignment
          ADD COLUMN testFormRevisionId INT NULL AFTER testResultsId",

        "ALTER TABLE test_form_assessment_item ADD COLUMN fluencySkillArea
          VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_FLUENCY_SKILL_AREA . ") NULL AFTER evidenceStatement",
        "ALTER TABLE test_form_assessment_item ADD COLUMN itemStrand
          VARCHAR(" . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEM_STRAND . ") NULL AFTER fluencySkillArea",

        "CREATE TABLE test_results
           (testFormRevisionId INT NOT NULL,
            testResultsId INT NOT NULL AUTO_INCREMENT,
            testStartDateTime DATETIME NOT NULL,
            testSubmitDateTime DATETIME NOT NULL,
            timeSpentOnItems INT NOT NULL,
            timeSpentOnAllItems INT NOT NULL,
            testDuration INT NOT NULL,
            totalItemsPresented INT NOT NULL,
            totalQuestionsPresented INT NOT NULL,
            totalItemsAnsweredCorrectly INT NOT NULL,
            totalItemsAnsweredIncorrectly INT NOT NULL,
            totalItemsNotAnswered INT NOT NULL,
            totalItemsNotScored INT NOT NULL,
            testRawScore INT NOT NULL,
            testScoreTheta INT NOT NULL,
            testScoreScale INT NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testResultsId),
            FOREIGN KEY (testFormRevisionId)
              REFERENCES test_form_revision(testFormRevisionId),
            INDEX (testStartDateTime),
            INDEX (testSubmitDateTime))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_results_assessment_item
           (testFormRevisionId INT NOT NULL,
            testFormAssessmentItemId INT NOT NULL,
            testResultsId INT NOT NULL,
            testResultsAssessmentItemId INT NOT NULL AUTO_INCREMENT,
            timeSpent INT NOT NULL,
            itemScore INT NOT NULL,
            isItemCorrect ENUM('Y', 'N') NOT NULL,
            isItemAnswered ENUM('Y', 'N') NOT NULL,
            isItemScored ENUM('Y', 'N') NOT NULL,
            toolsUsed VARCHAR(" . MAXLEN_TEST_RESULTS_TOOLS_USED . ") NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testResultsAssessmentItemId),
            FOREIGN KEY (testFormAssessmentItemId)
              REFERENCES test_form_assessment_item(testFormAssessmentItemId),
            FOREIGN KEY (testFormRevisionId)
              REFERENCES test_form_revision(testFormRevisionId),
            FOREIGN KEY (testResultsId)
              REFERENCES test_results(testResultsId))
            ENGINE=InnoDB COLLATE=utf8_bin",

        "CREATE TABLE test_results_item_element_response
           (testFormRevisionId INT NOT NULL,
            testFormAssessmentItemId INT NOT NULL,
            testResultsId INT NOT NULL,
            testResultsAssessmentItemId INT NOT NULL,
            testResultsItemElementResponseId INT NOT NULL AUTO_INCREMENT,
            responseIdentifier VARCHAR(" . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ") NULL,
            value TEXT NOT NULL,
            deleted ENUM('Y', 'N') NOT NULL DEFAULT 'N',
            PRIMARY KEY (testResultsItemElementResponseId),
            FOREIGN KEY (testResultsAssessmentItemId)
              REFERENCES test_results_assessment_item(testResultsAssessmentItemId),
            FOREIGN KEY (testFormAssessmentItemId)
              REFERENCES test_form_assessment_item(testFormAssessmentItemId),
            FOREIGN KEY (testFormRevisionId)
              REFERENCES test_form_revision(testFormRevisionId),
            FOREIGN KEY (testResultsId)
              REFERENCES test_results(testResultsId))
            ENGINE=InnoDB COLLATE=utf8_bin"
    ],
    '1.7.8' =>
    [
        "ALTER TABLE class MODIFY sectionNumber VARCHAR(" . MAXLEN_SECTION_NUMBER . ")"
    ],
    '1.7.9' =>
    [
        "INSERT INTO test_battery_subject(testBatterySubjectId, testBatterySubject) VALUES(4, 'Science')"
    ],
    '1.8.0' =>
    [
        "INSERT INTO qti_class (qtiClassId, qtiClass)
              VALUES (6, 'extendedTextInteraction'), (7, 'matchInteraction'), (8, 'inlineChoiceInteraction')",

        "ALTER TABLE test_form_item_element
          ADD COLUMN maxAssociations INT NULL AFTER minChoices",

        "ALTER TABLE test_form_item_element
          ADD COLUMN minAssociations INT NULL AFTER maxAssociations"
    ],
    '1.8.1' =>
    [
        "CREATE TABLE `test_map` (
            `testMapId` INT unsigned NOT NULL AUTO_INCREMENT,
            `taoId` VARCHAR(".MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEMID.") COLLATE utf8_bin NOT NULL,
            `itemId` VARCHAR(".MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEMID.") COLLATE utf8_bin NOT NULL,
            `gradeLevel` CHAR(2) COLLATE utf8_bin NOT NULL,
            `qtiClassId` INT NOT NULL,
            `responseCardinalityId` INT NOT NULL,
            `maxPoints` INT NOT NULL,
            `scoringStatus` VARCHAR(".MAXLEN_SCORING_STATUS.") COLLATE utf8_bin DEFAULT NULL,
            PRIMARY KEY (`testMapId`),
            UNIQUE KEY `taoId` (`taoId`),
            UNIQUE KEY `itemId` (`itemId`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;"
    ],
    '1.8.2' =>
    [
        "ALTER TABLE test_form_assessment_item
        	ADD COLUMN subclaim VARCHAR(25) DEFAULT NULL AFTER itemStrand,
        	ADD COLUMN passageType VARCHAR(15) DEFAULT NULL AFTER subclaim"
    ],
];
