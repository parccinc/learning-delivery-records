<?php
// LDR database migration definitions
$backwardMigrationVersions =
[
    '1.8.2' =>
    [
        "ALTER TABLE test_form_assessment_item DROP COLUMN subclaim",
        "ALTER TABLE test_form_assessment_item DROP COLUMN passageType"
    ],
    '1.8.1' =>
    [
        "DROP TABLE test_map"
    ],
    '1.8.0' =>
    [
        "ALTER TABLE test_form_item_element DROP COLUMN minAssociations",
        "ALTER TABLE test_form_item_element DROP COLUMN maxAssociations"
    ],
    '1.7.9' =>
    [
        "DELETE FROM test_battery_subject WHERE testBatterySubject = 'Science'"
    ],
    '1.7.8' =>
    [
        "ALTER TABLE class MODIFY sectionNumber VARCHAR(" . MAXLEN_SECTION_NUMBER . ")"
    ],
    '1.7.7' =>
    [
        "DROP TABLE test_results_item_element_response",
        "DROP TABLE test_results_assessment_item",
        "DROP TABLE test_results",

        "ALTER TABLE test_form_assessment_item DROP COLUMN itemStrand",
        "ALTER TABLE test_form_assessment_item DROP COLUMN fluencySkillArea",

        "ALTER TABLE test_assignment DROP COLUMN testFormRevisionId",
        "ALTER TABLE test_assignment DROP COLUMN testResultsId"
    ],
    '1.7.6' =>
    [
        "ALTER TABLE test_form_item_correct_response DROP COLUMN position",
    ],
    '1.7.5' =>
    [
        "DROP TABLE test_form_item_correct_response",
        "DROP TABLE test_form_item_element_response",
        "DROP TABLE test_form_item_element"
    ],
    '1.7.4' =>
    [
        "DELETE FROM test_battery_program WHERE testBatteryProgram = 'Summative'",
    ],
    '1.7.2' =>
    [
        "ALTER TABLE client DROP COLUMN clientURL"
    ],
    '1.7.1' =>
    [
        "ALTER TABLE organization DROP COLUMN fake",
    ],
    '1.7.0' =>
    [
        "DROP TABLE test_form_system_item",
        "DROP TABLE test_form_assessment_item",
        "DROP TABLE test_form_assessment_section",
        "DROP TABLE test_form_part",
        "DROP TABLE test_form_revision",
        "DROP TABLE response_cardinality",
        "DROP TABLE calculator_type",
        "DROP TABLE qti_class"
    ],
    '1.6.4' =>
    [
        "DELETE FROM client WHERE clientName = 'prod_user'",
        "DELETE FROM client WHERE clientName = 'prod_tap'",
        "DELETE FROM client WHERE clientName = 'prod_adp'"
    ],
    '1.6.3' =>
    [
        "ALTER TABLE queue_processing DROP COLUMN ldrUpdateLastQueueEventTime",

        "ALTER TABLE client DROP COLUMN clientType"
    ],
    '1.5.5' =>
    [
        "ALTER TABLE test_assignment DROP COLUMN lastQueueEventTime",

        "DROP TABLE queue_processing"
    ],
    '1.4.2' =>
    [
        "ALTER TABLE test_assignment DROP COLUMN embeddedScoreReportId",

        "DROP TABLE embedded_score_report"
    ],
    '1.3.6' =>
    [
        "ALTER TABLE student_record ADD enrollStartDate DATE NULL AFTER localIdentifier",
        "ALTER TABLE student_record ADD enrollEndDate INT NULL AFTER enrollStartDate",
        "ALTER TABLE student_record ADD testingOrgId INT NULL AFTER stateOrgId",
        "ALTER TABLE student_record ADD INDEX (localIdentifier)",
        "ALTER TABLE student_record ADD INDEX (testingOrgId)"
    ],
    '1.3.2' =>
    [
        "ALTER TABLE test_assignment DROP COLUMN enableTextToSpeech",
        "ALTER TABLE test_assignment DROP COLUMN enableLineReader"
    ],
    '1.2.8' =>
    [
        "ALTER TABLE test_assignment DROP COLUMN instructionStatus"
    ],
    '1.2.0' =>
    [
        "ALTER TABLE test_assignment DROP COLUMN testKey",
        "ALTER TABLE test_assignment DROP COLUMN adpTestAssignmentId"
    ],
    '1.1.4' =>
    [
        "ALTER TABLE student_record DROP COLUMN optionalStateData8",
        "ALTER TABLE student_record DROP COLUMN optionalStateData7",
        "ALTER TABLE student_record DROP COLUMN optionalStateData6",
        "ALTER TABLE student_record DROP COLUMN optionalStateData5",
        "ALTER TABLE student_record DROP COLUMN optionalStateData4",
        "ALTER TABLE student_record DROP COLUMN optionalStateData3",
        "ALTER TABLE student_record DROP COLUMN optionalStateData2",
        "ALTER TABLE student_record DROP COLUMN optionalStateData1"
    ],
    '0.9.6' =>
    [
        "DROP TABLE test_assignment",
        "DROP TABLE test_status"
    ],
    '0.9.0' =>
    [
        "DROP TABLE test_battery_form",
        "DROP TABLE test_battery",
        "DROP TABLE test_battery_subject",
        "DROP TABLE test_battery_security",
        "DROP TABLE test_battery_scoring",
        "DROP TABLE test_battery_program",
        "DROP TABLE test_battery_permissions",
        "DROP TABLE test_battery_grade"
    ],
    '0.7.5' => []
];
