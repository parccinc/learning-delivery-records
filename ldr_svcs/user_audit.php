<?php
//
// LDR user_audit services
//
// getUserAuditRecords()
//
// LDR user_audit support functions
//
// actionTypeSelectionClause()
// dataTypeSelectionClause()
// dbCreateUserAuditRecord()
// dbGetUserAuditRecordCount()
// dbGetUserAuditRecords()
// optionalConstraintsClause()

// getUserAuditRecords()
//   Returns a list of audit records
//
function getUserAuditRecords()
{
    $actionTypes = ['1' => 'Create', '2' => 'Update', '4' => 'Delete'];
    $dataTypes = ['1' => 'Organization', '2' => 'Student Record', '4' => 'Class', '8' => 'Class Student'];

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_OPTIONAL | DCO_MUST_EXIST,
            'startDateTime' => DCO_OPTIONAL,
            'endDateTime' => DCO_OPTIONAL,
            'dataType' => DCO_OPTIONAL,
            'primaryId' => DCO_OPTIONAL,
            'actionType' => DCO_OPTIONAL,
            'pageOffset' => DCO_OPTIONAL,
            'pageLimit' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $startDateTime, $endDateTime, $dataType, $primaryId, $actionType, $pageOffset,
         $pageLimit) = $args;

    // Validate the startDateTime
    if (strlen($startDateTime) > 0)
    {
        $startDateTime = validateDateTime($logId, $startDateTime, 'startDateTime');
        if ($startDateTime instanceof ErrorLDR)
        {
            $startDateTime->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the endDateTime
    if (strlen($endDateTime) > 0)
    {
        $endDateTime = validateDateTime($logId, $endDateTime, 'endDateTime');
        if ($endDateTime instanceof ErrorLDR)
        {
            $endDateTime->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Check that the startDateTime is before the endDateTime
    if (strlen($startDateTime) > 0 && strlen($endDateTime) > 0)
    {
        if (!datesInOrder($startDateTime, $endDateTime))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_END_DATETIME_BEFORE_START_DATETIME,
                                     LDR_ED_END_DATETIME_BEFORE_START_DATETIME);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the dataType
    if (strlen($dataType) > 0)
    {
        $dataType = validateAuditDataType($logId, $dataType);
        if ($dataType instanceof ErrorLDR)
        {
            $dataType->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the actionType
    if (strlen($actionType) > 0)
    {
        $actionType = validateAuditActionType($logId, $actionType);
        if ($actionType instanceof ErrorLDR)
        {
            $actionType->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the audit records
    $auditRecords = dbGetUserAuditRecords($logId, $db, $clientUserId, $startDateTime, $endDateTime, $dataType,
                                          $primaryId, $actionType, $pageOffset, $pageLimit);
    if ($auditRecords instanceof ErrorLDR)
    {
        $auditRecords->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Format the audit records
    for ($idx = 0; $idx < sizeof($auditRecords); $idx++)
    {
        // Decode the data changes
        $auditRecords[$idx]['dataChanges'] = json_decode($auditRecords[$idx]['dataChanges']);

        // Map the action type
        $auditRecords[$idx]['actionType'] = $actionTypes[$auditRecords[$idx]['actionType']];

        // Map the data type
        $auditRecords[$idx]['dataType'] = $dataTypes[$auditRecords[$idx]['dataType']];
    }

    // Get the total count of audit records
    $totalCount = dbGetUserAuditRecordCount($logId, $db, $clientUserId, $startDateTime, $endDateTime, $dataType,
                                            $primaryId, $actionType);
    if ($totalCount instanceof ErrorLDR)
    {
        $totalCount->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the student records with the total count
    $result = ['totalCount' => $totalCount, 'auditRecords' => $auditRecords];
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

//
// LDR user_audit support functions
//
// actionTypeSelectionClause()
//   Returns the actionType selection clause based on the specified action types
//
function actionTypeSelectionClause($actionType)
{
    $actionTypes = [];
    for ($bit = AUDIT_ACTION_CREATE; $bit <= AUDIT_ACTION_LAST_TYPE; $bit <<= 1)
    {
        if ($actionType & $bit)
            $actionTypes[] = $bit;
    }
    if (sizeof($actionTypes) == 1)
        return " AND actionType = '" . $actionTypes[0] . "'";
    else
        return " AND actionType IN (" . join(',', $actionTypes) . ")";
}

// dataTypeSelectionClause()
//   Returns the dataType selection clause based on the specified data types
//
function dataTypeSelectionClause($dataType)
{
    $dataTypes = [];
    for ($bit = AUDIT_DATA_ORGANIZATION; $bit <= AUDIT_DATA_LAST_TYPE; $bit <<= 1)
    {
        if ($dataType & $bit)
            $dataTypes[] = $bit;
    }
    if (sizeof($dataTypes) == 1)
        return " AND dataType = '" . $dataTypes[0] . "'";
    else
        return " AND dataType IN (" . join(',', $dataTypes) . ")";
}

// dbCreateUserAuditRecord()
//   Creates a user_audit record
//
function dbCreateUserAuditRecord($logId, PDO $db, $clientUserId, $dataType, $primaryId, $actionType,
                                 $dataChanges = null)
{
    // Get a time stamp
    $dateTime = date("Y-m-d H:i:s");

    // Create the user_audit record
    $dbDataChanges = dbPrepareForSQL($dataChanges, 'dataChanges');
    $sql = "INSERT INTO user_audit
                (clientUserId,
                 dataType,
                 primaryId,
                 actionType,
                 dateTime,
                 dataChanges)
            VALUES
               ('$clientUserId',
                '$dataType',
                '$primaryId',
                '$actionType',
                '$dateTime',
                '$dbDataChanges')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbGetUserAuditRecordCount()
//   Returns the count of audit records
//
function dbGetUserAuditRecordCount($logId, PDO $db, $clientUserId, $startDateTime, $endDateTime, $dataType, $primaryId,
                                   $actionType)
{
    // Create the base query
    $sql = "SELECT COUNT(*) FROM user_audit WHERE TRUE";

    // Append any optional constraints
    $sql .= optionalConstraintsClause($clientUserId, $startDateTime, $endDateTime, $dataType, $primaryId, $actionType);

    // Get the audit record count
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetUserAuditRecords()
//   Returns an array of audit records
//
function dbGetUserAuditRecords($logId, PDO $db, $clientUserId, $startDateTime, $endDateTime, $dataType, $primaryId,
                               $actionType, $pageOffset, $pageLimit)
{
    // Create the base query
    $sql = "SELECT userAuditId, dateTime, clientUserId, actionType, dataType, primaryId, dataChanges
            FROM user_audit
            WHERE TRUE";

    // Append any optional constraints
    $sql .= optionalConstraintsClause($clientUserId, $startDateTime, $endDateTime, $dataType, $primaryId, $actionType);

    // Order by primary key
    $sql .= " ORDER BY userAuditId";

    // Limit the results
    if (strlen($pageOffset) > 0 && strlen($pageLimit) > 0)
        $sql .= " LIMIT " . $pageOffset . ', ' . $pageLimit;
    else
        $sql .= " LIMIT 0, " . MAX_AUDIT_RECORDS;

    // Get the audit records
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $auditRecords = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $auditRecords;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// optionalConstraintsClause()
//   Returns an optional constraints clause based on the selected options
//
function optionalConstraintsClause($clientUserId, $startDateTime, $endDateTime, $dataType, $primaryId, $actionType)
{
    $clause = "";

    if (strlen($clientUserId) > 0)
        $clause .= " AND clientUserId = '$clientUserId'";

    if (strlen($startDateTime) > 0)
        $clause .= " AND dateTime > '$startDateTime'";

    if (strlen($endDateTime) > 0)
        $clause .= " AND dateTime < '$endDateTime'";

    if (strlen($dataType) > 0)
    {
        $clause .= dataTypeSelectionClause($dataType);

        // Allow a primaryId constraint when only one data type is specified
        if (strlen($primaryId) > 0 && strpos($clause, ',') === false)
            $clause .= " AND primaryId = '$primaryId'";
    }

    if (strlen($actionType) > 0)
    {
        $clause .= actionTypeSelectionClause($actionType);
    }

    return $clause;
}
