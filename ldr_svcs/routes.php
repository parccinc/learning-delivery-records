<?php

//if ($restFramework == REST_FRAMEWORK_SLIM)
//{
    $adpTestAssignmentId = ':adpTestAssignmentId';
    $classId = ':classId';
    $embeddedScoreReportId = ':embeddedScoreReportId';
    $name = ':name';
    $organizationId = ':organizationId';
    $password = ':password';
    $studentRecordId = ':studentRecordId';
    $testAssignmentId = ':testAssignmentId';
    $testBatteryId = ':testBatteryId';
    $testFormRevisionId = ':testFormRevisionId';
//}
//else
//{
//    $adpTestAssignmentId = '{adpTestAssignmentId}';
//    $classId = '{classId}';
//    $name = '{name}';
//    $organizationId = '{organizationId}';
//    $password = '{password}';
//    $studentRecordId = '{studentRecordId}';
//    $testAssignmentId = '{testAssignmentId}';
//    $testBatteryId = '{testBatteryId}';
//}

// GET request routing
$app->get('/', 'getVersion');
$app->get('/activity-report', 'getDetailedActivityReport');
$app->get("/adp-ping", 'pingADP');
$app->get("/adp-results/$adpTestAssignmentId", 'getResultsADP');
$app->get("/assigned-tests", 'getAssignedTests');
$app->get('/audit-records', 'getUserAuditRecords');
$app->get("/s3-json-file-contents", 'getS3JsonFileContents');
$app->get('/s3-bucket-names', 'getS3BucketNames');
$app->get("/class/$classId", 'getClass');
$app->get("/class-assigned-tests/$classId", 'getClassAssignedTests');
$app->get("/class-students", 'getClassStudents');
$app->get("/class-students/$classId", 'getClassStudents');
$app->get("/classes/$organizationId", 'getClasses');
$app->get('/database/version', 'getDatabaseVersion');
$app->get('/data-export', 'getDataExport');
$app->get("/embedded-score-report/$embeddedScoreReportId", 'getEmbeddedScoreReport');
$app->get("/organization", 'getOrganization');
$app->get("/organization/$organizationId", 'getOrganization');
$app->get("/organization/hierarchy/$organizationId", 'getOrganizationHierarchy');
$app->get('/organization-id', 'getOrganizationId');
$app->get("/organization-ids/child/$organizationId", 'getChildOrganizationIds');
$app->get("/organization-ids/descendent/$organizationId", 'getDescendentOrganizationIds');
$app->get("/organizations/$organizationId", 'getChildOrganizations');
$app->get("/organizations-all", 'getAllOrganizations');
//$app->get("/password/$password", 'getPasswordHash');
$app->get("/performance", 'getPerformanceStatistics');
$app->get("/queue-processing", 'getQueueProcessingStatistics');
$app->get("/school-activity-report", 'getSchoolActivityReport');
$app->get("/school-activity-report/".$organizationId, 'getSchoolActivityReport');
$app->get("/student-classes/$studentRecordId", 'getStudentClasses');
$app->get("/student-record/$studentRecordId", 'getStudentRecord');
$app->get("/student-records/$organizationId", 'getStudentRecords');
$app->get('/tables', 'getTableNames');
$app->get("/test-assignments", 'getTestAssignments');
$app->get("/test-batteries", 'getTestBatteries');
$app->get("/test-battery/$testBatteryId", 'getTestBattery');
$app->get("/test-battery-forms/$testBatteryId", 'getTestBatteryForms');
$app->get("/test-battery-subjects", 'getTestBatterySubjects');
$app->get("/test-form-revision/$testFormRevisionId", 'getTestFormRevision');
$app->get('/version', 'getVersion');

// POST request routing
$app->post('/bulk-test-assignments', 'createBulkTestAssignments');
$app->post('/class', 'createClass');
$app->post("/class-students/$classId", 'createClassStudents');
$app->post('/log-entry', 'createLogEntry');
$app->post('/organization', 'createOrganization');
$app->post('/redis', 'sendRedis');
$app->post('/session-token', 'createSessionToken');
$app->post("/student-record", 'createStudentRecord');
$app->post("/table/$name", 'createTable');
$app->post('/tables', 'createAllTables');
$app->post("/test-assignment", 'createTestAssignment');
$app->post("/test-battery-form", 'createTestBatteryForm');
$app->post("/test-map", 'createTestMap');
$app->post('/user', 'createUser');
if (BATCHED_VERSION) {
    $app->post("/student-records", 'createStudentRecordsBatched');
    $app->post("/test-assignments", 'createTestAssignmentsBatched');
} else {
    $app->post("/student-records", 'createStudentRecords');
    $app->post("/test-assignments", 'createTestAssignments');
}

// PUT request routing
$app->put('/check-test-status-queue', 'checkTestStatusQueue');
//$app->post('/put-check-test-status-queue', 'checkTestStatusQueue');
$app->put("/class/$classId", 'updateClass');
$app->put('/database-production', 'selectProductionDatabase');
$app->put('/database-unit-test', 'selectUnitTestDatabase');
$app->put('/database', 'updateDatabase');
//$app->post('/put-database', 'updateDatabase');
$app->put("/organization/$organizationId", 'updateOrganization');
$app->put('/organization-nested-sets', 'updateOrganizationNestedSets');
$app->put("/student-record/$studentRecordId", 'updateStudentRecord');
//$app->post("/put-student-record/$studentRecordId", 'updateStudentRecord');
$app->put("/test-assignment/$testAssignmentId", 'updateTestAssignment');
//$app->post("/put-test-assignment/$testAssignmentId", 'updateTestAssignment');
$app->put("/test-assignments/randomize-statuses", 'randomizeTestAssignmentStatuses');
$app->put("/test-battery", 'updateTestBattery');
$app->put("/test-battery-form", 'updateTestBatteryForm');
$app->put("/textEntryInteraction-answer-key", 'storeTextEntryInteractionAnswerKey');
$app->put("/test-results", 'storeTestResults');
$app->put("/tests-results", 'storeTestsResults');
$app->post("/put-test-results", 'storeTestResults');
$app->put("/unlock-test/$testAssignmentId", 'unlockTestAssignment');
//$app->post("/put-unlock-test/$testAssignmentId", 'unlockTestAssignment');

// DELETE request routing
$app->delete("/class/$classId", 'deleteClass');
$app->delete("/class-students/$classId", 'deleteClassStudents');
$app->delete("/class-students", 'deleteAllClassStudents');
$app->delete("/organization/$organizationId", 'deleteOrganization');
$app->delete('/session-token', 'deleteSessionToken');
$app->delete("/student-record/$studentRecordId", 'deleteStudentRecord');
//$app->post("/delete-student-record/$studentRecordId", 'deleteStudentRecord');
$app->delete("/student-records/$organizationId", 'deleteStudentRecords');
$app->delete("/table/$name", 'dropTable');
$app->delete('/tables', 'dropAllTables');

if (DISABLED_IN_PRODUCTION === false) {
    $app->get("/password-hash/$password", 'getPasswordHash'); // DO NOT ENABLE IN PRODUCTION
    $app->post("/client", "createClient"); // DO NOT ENABLE IN PRODUCTION
}
