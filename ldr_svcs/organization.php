<?php
//
// LDR organization services
//
// createOrganization()                     create_organization.php
// deleteOrganization()                     delete_organization.php
// exportOrganizations()
// getAllOrganizations()                    get_all_organizations.php
// getChildOrganizationIds()                get_child_org_ids.php
// getChildOrganizations()                  get_child_orgs.php
// getDescendentOrganizationIds()           get_descendent_org_ids.php
// getOrganization()                        get_organization.php
// getOrganizationHierarchy()               get_organization_hierarchy.php
// getOrganizationId()                      get_organization_id.php
// updateOrganization()                     update_organization.php
// updateOrganizationNestedSets()           update_org_nested_sets.php
//
// LDR organization support functions
//
// buildStudentOrgQueryClause()
// dbAdjustStudentCounts()
// dbAdjustOrganizationsStudentCount()
// dbCreateOrganization()
// dbDeleteOrganization()
// dbExportOrganizations()
// dbGetAllOrganizations()
// dbGetAllOrganizationsCount()
// dbGetChildOrganizationCount()
// dbGetChildOrganizationIds()
// dbGetChildOrganizationIds_By_Name()
// dbGetChildOrganizations()
// dbGetDescendentOrganizationIds()
// REDIS dbGetOrganization()
// dbGetOrganizationName()
// dbGetOrganizationType()
// REDIS dbGetOrgId_ByParentOrgId_ByName()
// REDIS dbGetOrgId_ByStateOrgId_ByIdentifier()
// dbGetParentOrganizationId()
// REDIS dbGetStateOrganizationId()
// dbIncrementOrganizationChildCount()
// dbUpdateOrganization()
// dbUpdateOrganizationNestedSets()

// createOrganization()
//   Creates an organization record
//
function createOrganization()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Check if this is a skip processing performance test
    if (SKIP_PROCESSING)
    {
        skipService($logId, ['organizationId' => '1'], $startTime);
        return;
    }

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'organizationName' => DCO_REQUIRED,
            'organizationType' => DCO_REQUIRED,
            'organizationIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,
            'parentStateCode' => DCO_OPTIONAL | DCO_TOUPPER,
            'parentOrganizationIdentifier' => DCO_OPTIONAL,
            'parentOrganizationId' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $organizationName, $organizationType, $organizationIdentifier, $parentStateCode,
        $parentOrganizationIdentifier, $parentOrganizationId) = $args;

    // Create the organization
    $organizationId = dbCreateOrganization($logId, $db, $clientUserId, $organizationName, $organizationType,
                                           $organizationIdentifier, $parentStateCode, $parentOrganizationIdentifier,
                                           $parentOrganizationId);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the organizationId
    finishService($db, $logId, ['organizationId' => $organizationId], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteOrganization()
//   Deletes an organization record
//
function deleteOrganization($organizationId)
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId) = $args;

    // Delete the organization
    $result = dbDeleteOrganization($logId, $db, $clientUserId, $organizationId);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    $response = ['result' => $result];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// exportOrganizations()
//  Exports all organizations info for all students that have submitted test assignments within a date range
//
function exportOrganizations($logId, PDO $db, $startDate, $endDate)
{
    ini_set('memory_limit', DATA_EXPORT_MAX_MEMORY);
    $organizationsData = dbExportOrganizations($logId, $db, $startDate, $endDate);
    if ($organizationsData instanceof ErrorLDR) {
        return $organizationsData;
    }

    $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
    $reportName = ROOT_ORGANIZATION_NAME."-Organizations-".$dateTimeInfo;
    $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                ."/".$reportName.".tsv";
    $organizationsCSV = generateCSVData($logId, $organizationsData->report, null, true, null, "\t");
    if ($organizationsCSV instanceof ErrorLDR) {
        return $organizationsCSV;
    }

    $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $organizationsCSV);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
    if ($reportUrl instanceof ErrorLDR) {
        $reportUrl->logError(__FUNCTION__);
        return $reportUrl;
    }

    return $reportUrl;
}

// getAllOrganizations()
//   Returns a list of all organizations
//
function getAllOrganizations()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'pageOffset' => DCO_OPTIONAL,
            'pageLimit' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $pageOffset, $pageLimit) = $args;

    // Get the organizations
    $organizations = dbGetAllOrganizations($logId, $db, $pageOffset, $pageLimit);
    if ($organizations instanceof ErrorLDR)
    {
        $organizations->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the total count of organizations
    $totalCount = dbGetAllOrganizationsCount($logId, $db);
    if ($totalCount instanceof ErrorLDR)
    {
        $totalCount->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the organizations with the total count
    $result = ['totalCount' => $totalCount, 'organizations' => $organizations];
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getChildOrganizationIds()
//   Returns a flat list of child organization IDs
//
function getChildOrganizationIds($organizationId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'recurse' => DCO_OPTIONAL | DCO_TOLOWER,
            'organizationType' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $recurse, $organizationType) = $args;

    // Validate the organizationId
    $argName = 'organizationId';
    $organizationId = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName],
                                    DCO_REQUIRED | DCO_MUST_EXIST);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the recurse value
    if (in_array($recurse, ['1', 'y', 't', 'yes', 'true'], true))
        $recurse = true;
    else
        $recurse = false;

    // Validate the organizationType
    if (strlen($organizationType) > 0)
    {
        $organizationType = validateOrganizationType($logId, $organizationType);
        if ($organizationType instanceof ErrorLDR)
        {
            $organizationType->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the child organization IDs
    $organizationIds = dbGetChildOrganizationIds($logId, $db, $organizationId, $recurse, $organizationType);
    if ($organizationIds instanceof ErrorLDR)
    {
        $organizationIds->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the child organization IDs
    finishService($db, $logId, $organizationIds, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getChildOrganizations()
//   Returns a list of child organizations
//
function  getChildOrganizations($organizationId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'pageOffset' => DCO_OPTIONAL,
            'pageLimit' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $pageOffset, $pageLimit) = $args;

    // Validate the organizationId
    $argName = 'organizationId';
    $organizationId = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName],
                                  DCO_REQUIRED | DCO_MUST_EXIST);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the child organizations
    $organizations = dbGetChildOrganizations($logId, $db, $organizationId, $pageOffset, $pageLimit);
    if ($organizations instanceof ErrorLDR)
    {
        $organizations->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the total count of child organizations
    $totalCount = dbGetChildOrganizationCount($logId, $db, $organizationId);
    if ($totalCount instanceof ErrorLDR)
    {
        $totalCount->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the organizations with the total count
    $result = ['totalCount' => $totalCount, 'organizations' => $organizations];
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getDescendentOrganizationIds()
//   Returns a flat list of all descendent organization IDs
//
function getDescendentOrganizationIds($organizationId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'organizationType' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $organizationType) = $args;

    // Validate the organizationId
    $argName = 'organizationId';
    $organizationId = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName],
        DCO_REQUIRED);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Validate the organizationType
    if (strlen($organizationType) > 0)
    {
        $organizationType = validateOrganizationType($logId, $organizationType);
        if ($organizationType instanceof ErrorLDR)
        {
            $organizationType->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the IDs of the descendent organizations
    $organizationIds = dbGetDescendentOrganizationIds($logId, $db, $organizationId, $organizationType);
    if ($organizationIds instanceof ErrorLDR)
    {
        $organizationIds->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the descendent organization IDs
    finishService($db, $logId, $organizationIds, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getOrganization()
//   Returns an organization
//
function getOrganization($organizationId = null)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'stateCode' => DCO_OPTIONAL | DCO_TOUPPER,
            'organizationIdentifier' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $stateCode, $organizationIdentifier) = $args;

    // Check if there is an organizationId arg
    if (!is_null($organizationId))
    {
        $argName = 'organizationId';
        $name = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
        if ($name instanceof ErrorLDR)
        {
            $name->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    // Otherwise, check if there are stateCode and organizationIdentifier args
    elseif (strlen($stateCode) == 0 || strlen($organizationIdentifier) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED,
                                 'Required values organizationId or stateCode/organizationIdentifier not provided');
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    else
    {
        // Get the stateOrganizationId
        $stateOrganizationId = dbGetStateOrganizationId($logId, $db, $stateCode);
        if ($stateOrganizationId instanceof ErrorLDR)
        {
            $stateOrganizationId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // Get the organizationId
        $organizationId = dbGetOrgId_ByStateOrgId_ByIdentifier($logId, $db, $stateOrganizationId,
                                                               $organizationIdentifier);
        if ($organizationId instanceof ErrorLDR)
        {
            $organizationId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the organization
    $organization = dbGetOrganization($logId, $db, $organizationId);
    if ($organization instanceof ErrorLDR)
    {
        $organization->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the organization
    finishService($db, $logId, $organization, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getOrganizationHierarchy()
//   Returns the parent hierarchy for an organization
//
function getOrganizationHierarchy($organizationId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the organizationId
    $argName = 'organizationId';
    $name = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Build a list of organizations
    $organizations = [];
    $organization = dbGetOrganization($logId, $db, $organizationId);
    if ($organization instanceof ErrorLDR)
    {
        $organization->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    array_unshift($organizations, $organization);
    while ($organization['organizationType'] > ORG_TYPE_ROOT)
    {
        $organization = dbGetOrganization($logId, $db, $organization['parentOrganizationId']);
        if ($organization instanceof ErrorLDR)
        {
            $organization->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        array_unshift($organizations, $organization);
    }

    // Return the organizations
    finishService($db, $logId, $organizations, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getOrganizationId()
//   Returns an organization ID
//
function getOrganizationId()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Check if this is a skip processing performance test
    if (SKIP_PROCESSING)
    {
        skipService($logId, ['organizationId' => '1'], $startTime);
        return;
    }

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'organizationPath' => DCO_OPTIONAL,
            'stateCode' => DCO_OPTIONAL | DCO_TOUPPER,
            'organizationIdentifier' => DCO_OPTIONAL,
            'parentOrganizationId' => DCO_OPTIONAL,
            'organizationName' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $organizationPath, $stateCode, $organizationIdentifier, $parentOrganizationId, $organizationName) = $args;

    // Check if we are using an organization name path to find the organization
    if (strlen($organizationPath) > 0)
    {
        // Find the organization
        $organizationId = 0;
        $organizationNames = explode(ORGANIZATION_PATH_SEPARATOR, $organizationPath);
        foreach ($organizationNames as $organizationName)
        {
            // Move down to the next organization level
            $parentOrganizationId = $organizationId;

            // Get the organizationId at this level
            $organizationId = dbGetOrgId_ByParentOrgId_ByName($logId, $db, $parentOrganizationId, $organizationName);
            if ($organizationId instanceof ErrorLDR)
            {
                $organizationId->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }
        }
    }
    elseif (strlen($stateCode) > 0 && strlen($organizationIdentifier) > 0)
    {
        // Get the stateOrganizationId
        $stateOrganizationId = dbGetStateOrganizationId($logId, $db, $stateCode);
        if ($stateOrganizationId instanceof ErrorLDR)
        {
            $stateOrganizationId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // Get the organizationId
        $organizationId = dbGetOrgId_ByStateOrgId_ByIdentifier($logId, $db, $stateOrganizationId,
                                                               $organizationIdentifier);
        if ($organizationId instanceof ErrorLDR)
        {
            $organizationId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    elseif (strlen($parentOrganizationId) > 0 && strlen($organizationName) > 0)
    {
        // Get the organizationId
        $organizationId = dbGetOrgId_ByParentOrgId_ByName($logId, $db, $parentOrganizationId, $organizationName);
        if ($organizationId instanceof ErrorLDR)
        {
            $organizationId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    else
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED, 'Required values not provided');
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the organizationId
    finishService($db, $logId, ['organizationId' => $organizationId], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// updateOrganization()
//   Updates an organization
//
function updateOrganization($organizationId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'organizationName' => DCO_OPTIONAL,
            'organizationIdentifier' => DCO_OPTIONAL | DCO_TOUPPER,
            'fake' => DCO_OPTIONAL | DCO_TOUPPER,
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $organizationName, $organizationIdentifier, $fake) = $args;

    // Validate the organizationId
    $argName = 'organizationId';
    $arg = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName],
                        DCO_REQUIRED | DCO_MUST_EXIST);
    if ($arg instanceof ErrorLDR)
    {
        $arg->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Update the organization
    $result = dbUpdateOrganization($logId, $db, $clientUserId, $organizationId, $organizationName,
                                   $organizationIdentifier, $fake);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    $response = ['result' => $result];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// updateOrganizationNestedSets()
//   Updates the organization table nested-list values
//
function updateOrganizationNestedSets()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Perform the update starting with the root organization
    $nestedSetValue = dbUpdateOrganizationNestedSets($logId, $db, ROOT_ORGANIZATION_ID, 1);
    if ($nestedSetValue instanceof ErrorLDR)
    {
        $nestedSetValue->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => SUCCESS], $startTime, __FUNCTION__);
}

//
// LDR organization support functions
//
// buildStudentOrgQueryClause()
//   Returns an SQL query clause to select the student organization(s) at or under the specified organization
//
function buildStudentOrgQueryClause($logId, PDO $db, $organizationId, $field)
{
    $organizationType = dbGetOrganizationType($logId, $db, $organizationId);
    if ($organizationType instanceof ErrorLDR)
    {
        $organizationType->logError(__FUNCTION__);
        return $organizationType;
    }
    if ($organizationType == ORG_TYPE_SCHOOL)
        $queryClause = " AND " . $field . " = '$organizationId'";
    else
    {
        $schoolOrgIds = dbGetDescendentOrganizationIds($logId, $db, $organizationId, ORG_TYPE_SCHOOL);
        if ($schoolOrgIds instanceof ErrorLDR)
        {
            $schoolOrgIds->logError(__FUNCTION__);
            return $schoolOrgIds;
        }
        if (sizeof($schoolOrgIds) > 0)
            $queryClause = " AND " . $field . " IN (" . implode(',', $schoolOrgIds) . ")";
        else
            $queryClause = false;
    }

    return $queryClause;
}

// dbAdjustStudentCounts()
//   Adjusts the student counts for an organization and all ancestor organizations
//
//   Typical values for $adjustment are '+ 1' or '- 1'
//
function dbAdjustStudentCounts($logId, PDO $db, $organizationId, $adjustment)
{
    // Lock the organization table
    $sql = "LOCK TABLE organization WRITE";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    while ($organizationId >= ROOT_ORGANIZATION_ID)
    {
        // Increment the organization student count
        $sql = "UPDATE organization SET studentCount = studentCount " . $adjustment .
               " WHERE organizationId = '$organizationId'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException $error) {
            dbUnlockTables($logId, $db);
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }

        // Get the parent organization
        $organizationId = dbGetParentOrganizationId($logId, $db, $organizationId);
        if ($organizationId instanceof ErrorLDR)
        {
            $organizationId->logError(__FUNCTION__);
            dbUnlockTables($logId, $db);
            return $organizationId;
        }
    }

    // Unlock the organization table
    $result = dbUnlockTables($logId, $db);
    if ($result instanceof ErrorLDR)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }
    return SUCCESS;
}

// dbAdjustOrganizationsStudentCount()
//     Adjusting the student count for the school, district and state
//
function dbAdjustOrganizationsStudentCount($logId, PDO $db, $adjustments)
{
    $schoolOrgIds = array_keys($adjustments);
    $sql = "SELECT
            	sc.organizationId 'schoolOrgId', sc.studentCount 'schoolCount'
                , di.organizationId 'districtOrgId', di.studentCount 'districtCount'
                , st.organizationId 'stateOrgId', st.studentCount 'stateCount'
                , root.organizationId 'rootOrgId', root.studentCount 'rootCount'
            FROM organization sc
            	JOIN organization di ON sc.parentOrganizationId = di.organizationId
            	JOIN organization st ON st.organizationId = sc.stateOrganizationId
                JOIN organization root ON root.organizationId = st.parentOrganizationId
            WHERE
            	sc.organizationId IN (".implode(',', $schoolOrgIds).");";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $orgsStudentCounts = $stmt->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    $newCounts = [];
    foreach ($orgsStudentCounts as $key => $orgStudentCounts) {
        $adjustment = $adjustments[$orgStudentCounts['schoolOrgId']];
        if (!isset($newCounts[$orgStudentCounts['schoolOrgId']])) {
            $newCounts[$orgStudentCounts['schoolOrgId']] = $orgStudentCounts['schoolCount'] + $adjustment;
        } else {
            $newCounts[$orgStudentCounts['schoolOrgId']] = $newCounts[$orgStudentCounts['schoolOrgId']] + $adjustment;
        }
        if (!isset($newCounts[$orgStudentCounts['districtOrgId']])) {
            $newCounts[$orgStudentCounts['districtOrgId']] = $orgStudentCounts['districtCount'] + $adjustment;
        } else {
            $newCounts[$orgStudentCounts['districtOrgId']] = $newCounts[$orgStudentCounts['districtOrgId']] + $adjustment;
        }
        if (!isset($newCounts[$orgStudentCounts['stateOrgId']])) {
            $newCounts[$orgStudentCounts['stateOrgId']] = $orgStudentCounts['stateCount'] + $adjustment;
        } else {
            $newCounts[$orgStudentCounts['stateOrgId']] = $newCounts[$orgStudentCounts['stateOrgId']] + $adjustment;
        }
        if (!isset($newCounts[$orgStudentCounts['rootOrgId']])) {
            $newCounts[$orgStudentCounts['rootOrgId']] = $orgStudentCounts['rootCount'] + $adjustment;
        } else {
            $newCounts[$orgStudentCounts['rootOrgId']] = $newCounts[$orgStudentCounts['rootOrgId']] + $adjustment;
        }
    }

    $sqlNewCounts = [];
    foreach ($newCounts as $orgId => $newCount) {
        $sqlNewCounts[] = "(".$orgId.", ".$newCount.")";
    }

    $sql = "INSERT INTO organization (organizationId, studentCount) VALUES ".implode(',', $sqlNewCounts)."
            ON DUPLICATE KEY UPDATE studentCount = VALUES(studentCount);";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ((count($newCounts)*2) != $stmt->rowCount()) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_COULDNOT_ADJUST_STUDENT_COUNT, LDR_ED_COULDNOT_ADJUST_STUDENT_COUNT);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    } catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbCreateOrganization()
//   Creates an organization
//
function dbCreateOrganization($logId, PDO $db, $clientUserId, $organizationName, $organizationType,
                              $organizationIdentifier, $parentStateCode, $parentOrganizationIdentifier,
                              $parentOrganizationId)
{
    // Validate the organizationType
    $organizationType = validateOrganizationType($logId, $organizationType);
    if ($organizationType instanceof ErrorLDR)
    {
        $organizationType->logError(__FUNCTION__);
        return $organizationType;
    }

    // Check the organizationIdentifier
    if ($organizationType > ORG_TYPE_STATE)
    {
        if (strlen($organizationIdentifier) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED,
                                     'Required value organizationIdentifier not provided');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }

    // Check that the parent state code is valid
    if (strlen($parentStateCode) > 0)
    {
        $parentStateCode = validateStateCode($logId, $parentStateCode);
        if ($parentStateCode instanceof ErrorLDR)
        {
            $parentStateCode->logError(__FUNCTION__);
            return $parentStateCode;
        }
    }

    // For ORG_TYPE_STATE
    if ($organizationType == ORG_TYPE_STATE)
    {
        // Check that the state code is valid
        $organizationName = validateStateCode($logId, $organizationName);
        if ($organizationName instanceof ErrorLDR)
        {
            $organizationName->logError(__FUNCTION__);
            return $organizationName;
        }

        // Check that the state organization does not already exist
        $organizationId = dbGetOrgId_ByParentOrgId_ByName($logId, $db, ROOT_ORGANIZATION_ID, $organizationName, false);
        if ($organizationId instanceof ErrorLDR)
        {
            // It is not an error if the organization was not found
            if ($organizationId->getErrorCode() != LDR_EC_ORGANIZATION_ID_NOT_FOUND)
            {
                // Some other error occurred so return it
                $organizationId->logError(__FUNCTION__);
                return $organizationId;
            }
        }
        else
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_STATE_ORGANIZATION_CONFLICT,
                LDR_ED_STATE_ORGANIZATION_CONFLICT . $organizationName);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Set the parent organization ID
        $parentOrganizationId = ROOT_ORGANIZATION_ID;
    }
    // For ORG_TYPE_DISTRICT
    else if ($organizationType == ORG_TYPE_DISTRICT)
    {
        // Get the parent organization ID
        if (strlen($parentStateCode) == 0 && strlen($parentOrganizationId) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED,
                                    'One or more parent organization values not provided');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        if (strlen($parentOrganizationId) == 0)
        {
            $parentOrganizationId =
                dbGetOrgId_ByParentOrgId_ByName($logId, $db, ROOT_ORGANIZATION_ID, $parentStateCode, false);
            if ($parentOrganizationId instanceof ErrorLDR)
            {
                $parentOrganizationId->logError(__FUNCTION__);
                return $parentOrganizationId;
            }
        }
    }
    // For ORG_TYPE_SCHOOL
    else if ($organizationType == ORG_TYPE_SCHOOL)
    {
        // Get the parent organization ID
        if (strlen($parentOrganizationId) == 0)
        {
            if (strlen($parentStateCode) == 0 || strlen($parentOrganizationIdentifier) == 0)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED,
                                         'One or more parent organization values not provided');
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
            $stateOrganizationId = dbGetStateOrganizationId($logId, $db, $parentStateCode);
            if ($stateOrganizationId instanceof ErrorLDR)
            {
                $stateOrganizationId->logError(__FUNCTION__);
                return $stateOrganizationId;
            }
            $parentOrganizationId = dbGetOrgId_ByStateOrgId_ByIdentifier($logId, $db, $stateOrganizationId,
                $parentOrganizationIdentifier);
            if ($parentOrganizationId instanceof ErrorLDR)
            {
                $parentOrganizationId->logError(__FUNCTION__);
                return $parentOrganizationId;
            }
        }
    }

    // Get the parent organization
    $parentOrganization = dbGetOrganization($logId, $db, $parentOrganizationId);
    if ($parentOrganization instanceof ErrorLDR)
    {
        $parentOrganization->logError(__FUNCTION__);
        return $parentOrganization;
    }

    // For ORG_TYPE_DISTRICT and ORG_TYPE_SCHOOL types check...
    if ($organizationType > ORG_TYPE_STATE)
    {
        // ...that the parent organization is the correct type
        $requiredParentOrgType = null;
        if ($organizationType == ORG_TYPE_DISTRICT)
            $requiredParentOrgType = ORG_TYPE_STATE;
        else if ($organizationType == ORG_TYPE_SCHOOL)
            $requiredParentOrgType = ORG_TYPE_DISTRICT;
        if ($parentOrganization['organizationType'] != $requiredParentOrgType)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_PARENT_ORGANIZATION_TYPE_MISMATCH,
                'Parent organization type is not correct for this organizationType: ' . $organizationType);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // ...that there is NOT already an organization in the state that has the same organizationIdentifier
        $organizationId = dbGetOrgId_ByStateOrgId_ByIdentifier($logId, $db, $parentOrganization['stateOrganizationId'],
                                              $organizationIdentifier, false);
        if ($organizationId instanceof ErrorLDR)
        {
            // It is not an error if an organization was not found
            if ($organizationId->getErrorCode() != LDR_EC_ORGANIZATION_ID_NOT_FOUND)
            {
                // Some other error occurred so return it
                $organizationId->logError(__FUNCTION__);
                return $organizationId;
            }
        }
        else
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_PEER_ORGANIZATION_CONFLICT,
                                     LDR_ED_PEER_ORGANIZATION_CONFLICT . $organizationIdentifier);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }

    // Turn off autocommit
    $db->beginTransaction();

    // Lock the organization and user_audit tables
    $sql = "LOCK TABLES organization WRITE, user_audit WRITE";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    if (ENABLE_NESTED_SETS)
    {
        // Update the nested-set values
        $parentNestedSetRight = $parentOrganization['nestedSetRight'];
        $sql = "UPDATE organization SET nestedSetLeft = nestedSetLeft + 2
                WHERE nestedSetLeft >= '$parentNestedSetRight'
                AND deleted = 'N'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException $error) {
            dbUnlockTables($logId, $db);
            $db->rollBack();
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
        $sql = "UPDATE organization SET nestedSetRight = nestedSetRight + 2
                WHERE nestedSetRight >= '$parentNestedSetRight'
                AND deleted = 'N'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException $error) {
            dbUnlockTables($logId, $db);
            $db->rollBack();
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }
    else
    {
        $parentNestedSetRight = 0;
    }

    // Create the organization record
    $stateOrganizationId = $parentOrganization['stateOrganizationId'];
    $dbOrganizationName = dbPrepareForSQL($organizationName, 'organizationName');
    $dbOrganizationIdentifier = dbPrepareForSQL($organizationIdentifier, 'organizationIdentifier');
    $sql = "INSERT INTO organization
                (organizationType,
                 stateOrganizationId,
                 parentOrganizationId,
                 organizationIdentifier,
                 organizationName,
                 nestedSetLeft,
                 nestedSetRight)
            VALUES
               ('$organizationType',
                '$stateOrganizationId',
                '$parentOrganizationId',
                '$dbOrganizationIdentifier',
                '$dbOrganizationName',
                '$parentNestedSetRight',
                '$parentNestedSetRight' + 1)";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $organizationId = $db->lastInsertId();
    }
    catch(PDOException $error) {
        dbUnlockTables($logId, $db);
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Increment the parentOrganization child count
    $result = dbIncrementOrganizationChildCount($logId, $db, $parentOrganizationId);
    if ($result instanceof ErrorLDR)
    {
        $result->logError(__FUNCTION__);
        dbUnlockTables($logId, $db);
        $db->rollBack();
        return $result;
    }

    // Make sure the stateOrganizationId is not zero (this occurs when a state is added under the root org)
    if ($stateOrganizationId == 0)
    {
        $stateOrganizationId = $organizationId;
        $sql = "UPDATE organization SET stateOrganizationId = '$stateOrganizationId'
                WHERE organizationId = '$organizationId'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException $error) {
            dbUnlockTables($logId, $db);
            $db->rollBack();
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    // Create a user audit record
    $dataChanges = json_encode(['organizationName' => $organizationName,
                                'organizationType' => $organizationType,
                                'parentOrganizationId' => $parentOrganizationId,
                                'organizationIdentifier' => $organizationIdentifier]);
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_ORGANIZATION, $organizationId,
                                     AUDIT_ACTION_CREATE, $dataChanges);
    if ($audit instanceof ErrorLDR)
    {
        dbUnlockTables($logId, $db);
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    // Unlock the tables
    $result = dbUnlockTables($logId, $db);
    if ($result instanceof ErrorLDR)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }

    // Update the Redis cache
    redisCreateOrganization($logId, $organizationType, $parentOrganizationId, $organizationIdentifier, $organizationName);

    return $organizationId;
}

// dbDeleteOrganization()
//   Deletes an organization
//
function dbDeleteOrganization($logId, PDO $db, $clientUserId, $organizationId)
{
    // Get the organization
    $organization = dbGetOrganization($logId, $db, $organizationId);
    if ($organization instanceof ErrorLDR)
    {
        $organization->logError(__FUNCTION__);
        return $organization;
    }

    // Check if this organization has child organizations
    if ($organization['childCount'] > 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_IS_PARENT, 'This organization is a parent organization' .
                                 ' and cannot be deleted - organizationId: ' . $organizationId);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if this organization has students
    if ($organization['studentCount'] > 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_HAS_STUDENTS, 'This organization has students' .
                                 ' and cannot be deleted - organizationId: ' . $organizationId);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if this organization has classes
    $classCount = dbGetClassCount($logId, $db, $organizationId);
    if ($classCount instanceof ErrorLDR)
    {
        $classCount->logError(__FUNCTION__);
        return $classCount;
    }
    if ($classCount > 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_HAS_CLASSES, 'This organization has classes' .
                                 ' and cannot be deleted - organizationId: ' . $organizationId);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Turn off autocommit
    $db->beginTransaction();

    // Delete the organization record
    $sql = "UPDATE organization SET deleted = 'Y' WHERE organizationId = '" . $organizationId . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Decrement the parentOrganization child count
    $sql = "UPDATE organization SET childCount = childCount - 1
            WHERE organizationId = '" . $organization['parentOrganizationId'] . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create a user audit record
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_ORGANIZATION, $organizationId,
                                     AUDIT_ACTION_DELETE);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return SUCCESS;
}

// dbDataExportOrganizations()
//  Build the SQL query to export organizations
//
function dbDataExportOrganizations($testStatus)
{
    $sql = "SELECT DISTINCT o.organizationId 'School Organization Identifier', o.organizationIdentifier 'School Code', o.organizationName 'School Name'
                , os.organizationName 'State'
                , od.organizationIdentifier 'District Code', od.organizationName 'District Name'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization o ON o.organizationId = sr.enrollOrgId
                JOIN organization od ON o.parentOrganizationId = od.organizationId
                JOIN organization os ON o.stateOrganizationId = os.organizationId
            WHERE ta.deleted = 'N' AND o.fake = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime;";
    return $sql;
}

// dbExportOrganizations()
//  Getting Data Export Data for the Organizations File
//
function dbExportOrganizations($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT DISTINCT o.organizationId 'School Organization Identifier', o.organizationIdentifier 'School Code', o.organizationName 'School Name'
                , os.organizationName 'State'
                , od.organizationIdentifier 'District Code', od.organizationName 'District Name'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization o ON o.organizationId = sr.enrollOrgId
                JOIN organization od ON o.parentOrganizationId = od.organizationId
                JOIN organization os ON o.stateOrganizationId = os.organizationId
            WHERE ta.deleted = 'N' AND o.fake = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if ($stmt->rowCount() === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_ORGANIZATIONS_EXPORT_DATA, LDR_ED_MISSING_ORGANIZATIONS_EXPORT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $organizationsData = new stdClass();
        $organizationsData->report = null;
        $organizationsData->rowsCount = 0;

        $rawOrganizationsData = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $organizationsData->report = $rawOrganizationsData;
        $organizationsData->rowsCount = $stmt->rowCount();

        return $organizationsData;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetAllOrganizations()
//   Returns all organizations as a flat list
//
function dbGetAllOrganizations($logId, PDO $db, $pageOffset, $pageLimit)
{
    // Create the base query
    $sql = "SELECT organizationId, parentOrganizationId, organizationType,
                   organizationIdentifier, organizationName, studentCount, childCount, fake
            FROM organization
            WHERE deleted = 'N'
            ORDER BY parentOrganizationId, organizationId";

    // Check if the output is limited to a page of data
    if (strlen($pageOffset) > 0 && strlen($pageLimit) > 0)
        $sql .= " LIMIT " . $pageOffset . ', ' . $pageLimit;

    // Get the organizations
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $organizations = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $organizations;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetAllOrganizationsCount()
//   Returns the count of all organizations
//
function dbGetAllOrganizationsCount($logId, PDO $db)
{
    $sql = "SELECT COUNT(*) FROM organization WHERE deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetChildOrganizationCount()
//   Returns the count of child organizations
//
function dbGetChildOrganizationCount($logId, PDO $db, $organizationId)
{
    $sql = "SELECT COUNT(*) FROM organization WHERE parentOrganizationId = '$organizationId' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetChildOrganizationIds()
//   Returns a flat array of child organization IDs
//
function dbGetChildOrganizationIds($logId, PDO $db, $organizationId, $recurse = false, $organizationType = null)
{
    // Get the child organization IDs
    $organizationIds = [];
    $sql = "SELECT organizationId FROM organization
            WHERE parentOrganizationId = '" . $organizationId . "'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM))
        {
            // Append this child organization ID
            if ($organizationType === null)
                $organizationIds[] = $row[0];
            else
            {
                $thisOrganizationType = dbGetOrganizationType($logId, $db, $row[0]);
                if ($thisOrganizationType instanceof ErrorLDR)
                    return $thisOrganizationType;
                if ($thisOrganizationType == $organizationType)
                    $organizationIds[] = $row[0];
            }

            if ($recurse)
            {
                // Get the children of the child
                $childOrganizationIds = dbGetChildOrganizationIds($logId, $db, $row[0], $recurse, $organizationType);

                // If an error occurred then propagate it upward
                if ($childOrganizationIds instanceof ErrorLDR)
                    return $childOrganizationIds;

                // Append each to build a flat list
                foreach ($childOrganizationIds as $childOrganizationId)
                    $organizationIds[] = $childOrganizationId;
            }
        }

        return $organizationIds;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetChildOrganizationIds_By_Name()
//   Returns the child organization IDs matching the given name
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
//function dbGetChildOrganizationIds_By_Name($logId, PDO $db, $parentOrganizationId, $organizationName)
//{
//    $organizationIds = [];
//
//    // Prepare the query
//    $dbOrganizationName = dbPrepareForSQL($organizationName, 'organizationName');
//    $sql = "SELECT organizationId FROM organization
//            WHERE organizationName = '$dbOrganizationName'
//            AND parentOrganizationId = '$parentOrganizationId'
//            AND deleted = 'N'";
//
//    // Execute the query
//    try {
//        $stmt = $db->prepare($sql);
//        $stmt->execute();
//        $results = $stmt->fetchAll(PDO::FETCH_OBJ);
//        foreach ($results as $result)
//            $organizationIds[] = $result->organizationId;
//    }
//    catch(PDOException $error) {
//        return errorQueryException($logId, $error, __FUNCTION__, $sql);
//    }
//
//    return $organizationIds;
//}

// dbGetChildOrganizations()
//   Returns the child organizations
//
function dbGetChildOrganizations($logId, PDO $db, $organizationId, $pageOffset, $pageLimit)
{
    // Create the base query
    $sql = "SELECT organizationId, parentOrganizationId, organizationType,
                   organizationIdentifier, organizationName, studentCount, childCount
            FROM organization
            WHERE parentOrganizationId = '$organizationId'
            AND deleted = 'N'";

    // Check if the output is limited to a page of data
    if (strlen($pageOffset) > 0 && strlen($pageLimit) > 0)
        $sql .= " LIMIT " . $pageOffset . ', ' . $pageLimit;

    // Get the organizations
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $organizations = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $organizations;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetDescendentOrganizationIds()
//   Returns a flat array of all descendent organization IDs
//
function dbGetDescendentOrganizationIds($logId, PDO $db, $organizationId, $organizationType = null)
{
    $organizationIds = [];

    // Checked if nested-sets are enabled
    if (ENABLE_NESTED_SETS)
    {
        // Get the organization nested set values
        $sql = "SELECT nestedSetLeft, nestedSetRight FROM organization
                WHERE organizationId = '$organizationId'
                AND deleted = 'N'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (sizeof($row) == 2)
            {
                $nestedSetLeft = $row[0];
                $nestedSetRight = $row[1];
            }
            else
            {
                // The organization was not found
                $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND,
                                         LDR_ED_ORGANIZATION_ID_NOT_FOUND . $organizationId);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        }
        catch(PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }

        // Get the descendent organization IDs
        $sql = "SELECT organizationId FROM organization
                WHERE nestedSetLeft > '$nestedSetLeft'
                AND nestedSetRight < '$nestedSetRight'
                AND deleted = 'N'";
        if ($organizationType !== null)
            $sql .= " AND organizationType = '$organizationType'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            while ($row = $stmt->fetch())
                $organizationIds[] = $row[0];
        }
        catch(PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }
    else
    {
        // Get the descendent organization IDs using parent-child relationships recursively
        $organizationIds = dbGetChildOrganizationIds($logId, $db, $organizationId, true, $organizationType);
        if ($organizationIds instanceof ErrorLDR)
        {
            $organizationIds->logError(__FUNCTION__);
            return $organizationIds;
        }
    }

    return $organizationIds;
}

// dbGetOrganization()
//   Returns an organization
//
function dbGetOrganization($logId, PDO $db, $organizationId)
{
    // Query the cache
    $organization = redisGetOrganization($logId, $organizationId);
    if ($organization !== null && $organization !== false)
        return $organization;

    // Query the database
    $sql = "SELECT organizationId, organizationType, stateOrganizationId, parentOrganizationId,
                   organizationIdentifier, organizationName, studentCount, childCount, fake
            FROM organization
            WHERE organizationId IN ('".implode("','", (array)$organizationId)."')
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1 && is_array($organizationId) === false)
        {
            $organization = $stmt->fetch(PDO::FETCH_ASSOC);
            return $organization;
        } elseif (is_array($organizationId) === true) {
            $organizations = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
            return $organizations;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The organization was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND,
                             LDR_ED_ORGANIZATION_ID_NOT_FOUND . $organizationId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetOrgId_ByStateCode_ByOrgIdentifier()
//   Returns the organization ID for an organization based on the stateCode and organizationIdentifier
//
function dbGetOrgId_ByStateCode_ByOrgIdentifier($logId, PDO $db, $stateCodeOrgIdentifier, $logError = true)
{
    $stateCodeOrgIdentifier = array_unique($stateCodeOrgIdentifier);
    $sql = "SELECT
                CONCAT_WS('".FIELD_SEPARATOR."', os.organizationName, o.organizationIdentifier) AS 'StateCode_OrgIdentifier'
                , o.organizationId
            FROM organization o JOIN organization os ON os.organizationId = o.stateOrganizationId
            WHERE o.organizationType > ".ORG_TYPE_STATE."
	            AND CONCAT_WS('-', os.organizationName, o.organizationIdentifier)
                    IN ('".implode("','", $stateCodeOrgIdentifier)."')
                AND o.deleted = 'N' AND os.deleted = 'N';";
    try{
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        return $data;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetOrgId_ByParentOrgId_ByName()
//   Returns the organization ID for an organization based on the parentOrganizationId and organizationName
//
function dbGetOrgId_ByParentOrgId_ByName($logId, PDO $db, $parentOrganizationId, $organizationName, $logError = true)
{
    // Query the cache
    $organizationId = redisGetOrgId_ByParentOrgId_ByName($logId, $parentOrganizationId, $organizationName, $logError);
    if ($organizationId instanceof ErrorLDR || ($organizationId !== null && $organizationId !== false))
        return $organizationId;

    // Query the database
    $dbOrganizationName = dbPrepareForSQL($organizationName, 'organizationName');
    $sql = "SELECT organizationId FROM organization
            WHERE parentOrganizationId = '$parentOrganizationId'
            AND organizationName = '$dbOrganizationName'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        if (strlen($row[0]) > 0)
            return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The organization was not found
    $parentName = dbGetOrganizationName($logId, $db, $parentOrganizationId);
    if ($parentName instanceof ErrorLDR)
        $parentName = '';
    $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND,
                             'Organization not found for ' . $parentName . ' organization ' . $organizationName);
    if ($logError)
        $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetOrgId_ByStateOrgId_ByIdentifier()
//   Returns the organization ID for an organization based on the stateOrganizationId and organizationIdentifier
//
function dbGetOrgId_ByStateOrgId_ByIdentifier($logId, PDO $db, $stateOrganizationId, $organizationIdentifier,
                                              $logError = true)
{
    // Query the cache
    $organizationId = redisGetOrgId_ByStateOrgId_ByIdentifier($logId, $stateOrganizationId, $organizationIdentifier,
                                                             $logError);
    if ($organizationId instanceof ErrorLDR || ($organizationId !== null && $organizationId !== false))
        return $organizationId;

    // Query the database
    $dbOrganizationIdentifier = dbPrepareForSQL($organizationIdentifier, 'organizationIdentifier');
    $sql = "SELECT organizationId FROM organization
        WHERE stateOrganizationId = '$stateOrganizationId'
        AND organizationIdentifier = '$dbOrganizationIdentifier'
        AND deleted = 'N'";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        if (strlen($row[0]) > 0)
            return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The organization was not found
    $stateCode = dbGetOrganizationName($logId, $db, $stateOrganizationId);
    if ($stateCode instanceof ErrorLDR)
        $stateCode = '';
    $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND, 'Organization not found for ' . $stateCode .
                             ' organization identifier ' . $organizationIdentifier);
    if ($logError)
        $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetOrganizationName()
//   Returns the organization name
//
function dbGetOrganizationName($logId, PDO $db, $organizationId)
{
    $sql = "SELECT organizationName FROM organization
            WHERE organizationId = '$organizationId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        if (strlen($row[0]) > 0)
            return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The organization was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND,
        LDR_ED_ORGANIZATION_ID_NOT_FOUND . $organizationId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetOrganizationType()
//   Returns the organization type (ORG_TYPE_SCHOOL, ORG_TYPE_DISTRICT, etc.) for an organization
//
function dbGetOrganizationType($logId, PDO $db, $organizationId)
{
    // Get the organization type
    $sql = "SELECT organizationType FROM organization
            WHERE organizationId = '$organizationId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        if (strlen($row[0]) > 0)
            return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The organization was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND,
                             LDR_ED_ORGANIZATION_ID_NOT_FOUND . $organizationId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetParentOrganizationId()
//   Returns the parent organization ID for an organization
//
function dbGetParentOrganizationId($logId, PDO $db, $organizationId)
{
    $sql = "SELECT parentOrganizationId FROM organization
            WHERE organizationId = '$organizationId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // There is no parent organization ID value
    $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND,
                             LDR_ED_ORGANIZATION_ID_NOT_FOUND . $organizationId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetStateOrganizationId()
//   Returns an organization ID for a state
//
function dbGetStateOrganizationId($logId, PDO $db, $stateCode)
{
    // Query the cache
    $organizationId = redisGetOrgId_ByParentOrgId_ByName($logId, ROOT_ORGANIZATION_ID, $stateCode);
    if ($organizationId instanceof ErrorLDR || ($organizationId !== null && $organizationId !== false))
        return $organizationId;

    // Query the database
    $sql = "SELECT organizationId, organizationName FROM organization
            WHERE organizationType = '" . ORG_TYPE_STATE . "'
            AND organizationName IN ('".implode("', '", (array)$stateCode)."')
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if (is_array($stateCode) === true && $stmt->rowCount() >= 1) {
            $row = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
            $row = array_flip($row);
            return $row;
        } elseif (is_array($stateCode) === false && $stmt->rowCount() == 1) {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The organization was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_STATE_ORGANIZATION_NOT_FOUND,
                             'State organization not found for state code ' . $stateCode);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbIncrementOrganizationChildCount()
//   Increments the organization child count
//
function dbIncrementOrganizationChildCount($logId, $db, $organizationId)
{
    $sql = "UPDATE organization SET childCount = childCount + 1
            WHERE organizationId = '$organizationId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Update the Redis cache
    redisIncrementOrganizationChildCount($logId, $organizationId);

    return SUCCESS;
}

// dbUpdateOrganization()
//   Updates an organization
//
function dbUpdateOrganization($logId, PDO $db, $clientUserId, $organizationId, $organizationName,
                              $organizationIdentifier, $fake)
{
    // Get the organization
    $organization = dbGetOrganization($logId, $db, $organizationId);
    if ($organization instanceof ErrorLDR)
    {
        $organization->logError(__FUNCTION__);
        return $organization;
    }

    // Check that the organization is not a state or the root organization
    if ($organization['organizationType'] == ORG_TYPE_STATE || $organization['organizationType'] == ORG_TYPE_ROOT)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_CANNOT_BE_MODIFIED,
            "State organizations and the root organization cannot be modified");
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Determine what and if changes are being made
    $dataChanges = [];
    $sql = "UPDATE organization SET ";
    if (strlen($organizationName) > 0 && $organizationName != $organization['organizationName'])
    {
        $dataChanges['organizationName'] = $organizationName;
        $dbOrganizationName = dbPrepareForSQL($organizationName, 'organizationName');
        $sql .= "organizationName = '$dbOrganizationName', ";
    }
    if (strlen($organizationIdentifier) > 0 && $organizationIdentifier != $organization['organizationIdentifier'])
    {
        // Check that there is NOT already an organization in the state that has the same organizationIdentifier
        $otherOrgId = dbGetOrgId_ByStateOrgId_ByIdentifier($logId, $db, $organization['stateOrganizationId'],
            $organizationIdentifier);
        if ($otherOrgId instanceof ErrorLDR)
        {
            // It is not an error if an organization was not found
            if ($otherOrgId->getErrorCode() != LDR_EC_ORGANIZATION_ID_NOT_FOUND)
            {
                // Some other error occurred so return it
                $otherOrgId->logError(__FUNCTION__);
                return $otherOrgId;
            }
        }
        else
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_PEER_ORGANIZATION_CONFLICT,
                LDR_ED_PEER_ORGANIZATION_CONFLICT . $organizationIdentifier);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $dataChanges['organizationIdentifier'] = $organizationIdentifier;
        $dbOrganizationIdentifier = dbPrepareForSQL($organizationIdentifier, 'organizationIdentifier');
        $sql .= "organizationIdentifier = '$dbOrganizationIdentifier', ";
    }
    if (strlen($fake) > 0 && $fake != $organization['fake']) {
        $dataChanges['fake'] = $fake;
        $sql .= "fake = '$fake', ";
    }
    if (sizeof($dataChanges) < 1)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_UPDATE_HAS_NO_CHANGES, LDR_ED_UPDATE_HAS_NO_CHANGES);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    $sql = substr($sql, 0, -2); // Strip off the trailing ", "
    $sql .= " WHERE organizationId = '" . $organizationId . "'";

    // Turn off autocommit
    $db->beginTransaction();

    // Update the organization record
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create a user audit record
    $dataChanges = json_encode($dataChanges);
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_ORGANIZATION, $organizationId,
        AUDIT_ACTION_UPDATE, $dataChanges);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return SUCCESS;
}

// dbUpdateOrganizationNestedSets()
//   Updates the nested-set values for an organization and all descendent organizations
//
function dbUpdateOrganizationNestedSets($logId, PDO $db, $organizationId, $nestedSetValue)
{
    // Check that nested-sets are enabled
    if (!ENABLE_NESTED_SETS)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_NESTED_SETS_ARE_DISABLED, LDR_ED_NESTED_SETS_ARE_DISABLED);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Set the nested-set left value
    $sql = "UPDATE organization SET nestedSetLeft = '$nestedSetValue'
            WHERE organizationId = '$organizationId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Increment the nested-set value
    $nestedSetValue++;

    // Get the child organization IDs
    $childOrganizationIds = dbGetChildOrganizationIds($logId, $db, $organizationId);

    // If an error occurred then propagate it upward
    if ($childOrganizationIds instanceof ErrorLDR)
        return $childOrganizationIds;

    // Update the nested-set values for all descendent organizations
    foreach ($childOrganizationIds as $childOrganizationId)
    {
        $nestedSetValue = dbUpdateOrganizationNestedSets($logId, $db, $childOrganizationId, $nestedSetValue);

        // If an error occurred then propagate it upward
        if ($nestedSetValue instanceof ErrorLDR)
            return $nestedSetValue;
    }

    // Set the nested-set right value
    $sql = "UPDATE organization SET nestedSetRight = '$nestedSetValue'
            WHERE organizationId = '$organizationId'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Increment the nested-set value
    $nestedSetValue++;

    // Return the nested-set value
    return $nestedSetValue;
}
