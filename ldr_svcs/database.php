<?php

define('LDR_DATABASE_VERSION', '1.8.2');

//
// LDR database services
//
// createAllTables()                create_all_tables.php
// createTable()                    create_table.php
// dropAllTables()                  drop_all_tables.php
// dropTable()                      drop_table.php
// getDatabaseVersions()            get_database_versions.php
// selectDatabase()
// selectProductionDatabase()       select_production_database.php
// selectUnitTestDatabase()         select_unit_test_database.php
// updateDatabase()                 update_database.php
//
// LDR database support functions
//
// dbAddPerformanceRecord()
// dbAddQueueProcessingRecord()
// dbGetClientName_By_ClientId()
// dbConnect()
// dbCreateSessionToken()
// dbCreateTable()
// dbCreateUser()
// dbGetDatabaseVersion()
// dbDeleteSessionToken()
// dbDropAllTables()
// dbDropTable()
// dbGetClientId_By_Token()
// dbGetOrganizationId_By_StudentId()
// dbGetPerformanceRecord()
// dbGetPerformanceStatistics()
// dbGetQueueProcessingStatistics()
// dbGetReferenceIdValue()
// dbGetReferenceValueId()
// dbGetSessionDatetime_By_Token()
// dbGetTableNames()
// dbInsertWithBindParams()
// dbMigrateDatabase()
// dbMigrateDatabaseBackward()
// dbMigrateDatabaseForward()
// dbMultiInsertWithBindParams()
// dbPrepareForSQL()
// dbRecordPerformance()
// dbSelectDatabase()
// dbTableValueExists()
// dbTouchSessionToken()
// dbUnlockTables()
// dbUpdateDatabaseVersion()
// dbUpdatePerformanceRecord()
// dbYieldSelect()
// errorQueryException()

// LDR database table definitions
$dbTables =
[
    'calculator_type' =>
    [
        'create' =>
            '(calculatorTypeId INT NOT NULL AUTO_INCREMENT,
              calculatorType VARCHAR(' . MAXLEN_CALCULATOR_TYPE . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (calculatorTypeId))',

        'primaryKey' => 'calculatorTypeId'
    ],

    'class' =>
    [
        'create' =>
            '(classId INT NOT NULL AUTO_INCREMENT,
              organizationId INT NOT NULL,
              classIdentifier VARCHAR(' . MAXLEN_CLASS_IDENTIFIER . ') NOT NULL,
              sectionNumber VARCHAR(' . MAXLEN_SECTION_NUMBER . ') NULL,
              gradeLevel CHAR(' . LENGTH_GRADE_LEVEL . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (classId),
              INDEX (classIdentifier),
              INDEX (organizationId, sectionNumber))',

        'primaryKey' => 'classId'
    ],

    'client' =>
    [
        'create' =>
            '(clientId INT NOT NULL AUTO_INCREMENT,
              clientType INT NOT NULL DEFAULT "' . CLIENT_TYPE_DEVELOPMENT .'",
              clientName VARCHAR(40) NOT NULL,
              clientPassword CHAR(128),
              clientURL VARCHAR('.MAXLEN_CLIENT_URL.'),
              PRIMARY KEY (clientId),
              UNIQUE (clientName))',

        'primaryKey' => 'clientId'
    ],

    'embedded_score_report' =>
    [
        'create' =>
            '(embeddedScoreReportId INT NOT NULL AUTO_INCREMENT,
              embeddedScoreReport TEXT,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (embeddedScoreReportId))',

        'primaryKey' => 'embeddedScoreReportId'
    ],

    'organization' =>
    [
        'create' =>
            '(organizationId INT NOT NULL AUTO_INCREMENT,
              organizationType TINYINT NOT NULL,
              stateOrganizationId INT NOT NULL,
              parentOrganizationId INT NOT NULL,
              organizationIdentifier VARCHAR(' . MAXLEN_ORGANIZATION_IDENTIFIER . ') NOT NULL DEFAULT "",
              organizationName VARCHAR(' . MAXLEN_ORGANIZATION_NAME . ') NOT NULL,
              nestedSetLeft INT NOT NULL,
              nestedSetRight INT NOT NULL,
              studentCount INT NOT NULL DEFAULT 0,
              childCount INT NOT NULL DEFAULT 0,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              fake ENUM("Y","N") NOT NULL DEFAULT "N",
              PRIMARY KEY (organizationId),
              INDEX (stateOrganizationId),
              INDEX (parentOrganizationId),
              INDEX (nestedSetLeft),
              INDEX (nestedSetRight),
              INDEX (organizationIdentifier),
              INDEX (organizationName))',

        'primaryKey' => 'organizationId'
    ],

    'performance' =>
    [
        'create' =>
            '(performanceId INT NOT NULL AUTO_INCREMENT,
              statisticsDate DATE NULL,
              serviceName VARCHAR(60) NULL,
              serviceCount INT NULL,
              totalElapsedTime DOUBLE NULL,
              maximumElapsedTime DOUBLE NULL,
              minimumElapsedTime DOUBLE NULL,
              PRIMARY KEY (performanceId),
              INDEX (statisticsDate, serviceName))',

        'primaryKey' => 'performanceId'
    ],

    'qti_class' =>
    [
        'create' =>
            '(qtiClassId INT NOT NULL AUTO_INCREMENT,
              qtiClass VARCHAR(' . MAXLEN_QTI_CLASS . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (qtiClassId))',

        'primaryKey' => 'qtiClassId'
    ],

    'queue_processing' =>
    [
        'create' =>
            '(queueProcessingId INT NOT NULL AUTO_INCREMENT,
              statisticsDate DATE NULL,
              hostname TEXT NOT NULL,
              totalMessagesProcessed INT NOT NULL,
              sqsCreateClientElapsedTime DOUBLE NULL,
              sqsReceiveMessageElapsedTime DOUBLE NULL,
              sqsDeleteMessageElapsedTime DOUBLE NULL,
              ldrGetTestAssignmentIdElapsedTime DOUBLE NULL,
              ldrGetTestAssignmentElapsedTime DOUBLE NULL,
              ldrUpdateLastQueueEventTime DOUBLE NULL,
              ldrUpdateTestStatusElapsedTime DOUBLE NULL,
              ldrStoreTestResultsElapsedTime DOUBLE NULL,
              adpUpdateTestStatusElapsedTime DOUBLE NULL,
              halLoggingElapsedTime DOUBLE NULL,
              submittedMessagesProcessed INT NOT NULL,
              submittedMessagesIgnored INT NOT NULL,
              submittedMessagesInvalid INT NOT NULL,
              submittedMessagesFailed INT NOT NULL,
              submittedElapsedTime DOUBLE NULL,
              startedMessagesProcessed INT NOT NULL,
              startedMessagesIgnored INT NOT NULL,
              startedMessagesInvalid INT NOT NULL,
              startedMessagesFailed INT NOT NULL,
              startedElapsedTime DOUBLE NULL,
              pausedMessagesProcessed INT NOT NULL,
              pausedMessagesIgnored INT NOT NULL,
              pausedMessagesInvalid INT NOT NULL,
              pausedMessagesFailed INT NOT NULL,
              pausedElapsedTime DOUBLE NULL,
              resumedMessagesProcessed INT NOT NULL,
              resumedMessagesIgnored INT NOT NULL,
              resumedMessagesInvalid INT NOT NULL,
              resumedMessagesFailed INT NOT NULL,
              resumedElapsedTime DOUBLE NULL,
              unknownStatusChangesProcessed INT NOT NULL,
              unknownMessagesProcessed INT NOT NULL,
              unknownElapsedTime DOUBLE NULL,
              PRIMARY KEY (queueProcessingId),
              INDEX (statisticsDate))',

        'primaryKey' => 'queueProcessingId'
    ],

    'response_cardinality' =>
    [
        'create' =>
            '(responseCardinalityId INT NOT NULL AUTO_INCREMENT,
              responseCardinality VARCHAR(' . MAXLEN_RESPONSE_CARDINALITY . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (responseCardinalityId))',

        'primaryKey' => 'responseCardinalityId'
    ],

    'session_token' =>
    [
        'create' =>
            '(sessionTokenId INT NOT NULL AUTO_INCREMENT,
              clientId INT NOT NULL,
              token CHAR(' . LENGTH_SESSION_TOKEN . ') NOT NULL,
              dateTime DATETIME NOT NULL,
              PRIMARY KEY (sessionTokenId),
              UNIQUE (token))',

        'primaryKey' => 'sessionTokenId'
    ],

    'student' =>
    [
        'create' =>
            '(studentId INT NOT NULL AUTO_INCREMENT,
              globalUniqueIdentifier CHAR(' . LENGTH_GUID_RFC4122 . ') NOT NULL,
              lastName VARCHAR(' . MAXLEN_STUDENT_LAST_NAME . ') NOT NULL,
              firstName VARCHAR(' . MAXLEN_STUDENT_FIRST_NAME . ') NOT NULL,
              middleName VARCHAR(' . MAXLEN_STUDENT_MIDDLE_NAME . ') NOT NULL,
              dateOfBirth DATE NOT NULL,
              gender ENUM("F", "M") NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (studentId),
              UNIQUE (globalUniqueIdentifier),
              INDEX (firstName),
              INDEX (lastName),
              INDEX (dateOfBirth))',

        'primaryKey' => 'studentId'
    ],

    'student_class' =>
    [
        'create' =>
            '(studentClassId INT NOT NULL AUTO_INCREMENT,
              studentRecordId INT NOT NULL,
              classId INT NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (studentClassId),
              INDEX (studentRecordId),
              INDEX (classId))',

        'primaryKey' => 'studentClassId'
    ],

    'student_record' =>
    [
        'create' =>
            '(studentRecordId INT NOT NULL AUTO_INCREMENT,
              studentId INT NOT NULL,
              enrollOrgId INT NOT NULL,
              stateOrgId INT NOT NULL,
              stateIdentifier VARCHAR(' . MAXLEN_STUDENT_STATE_IDENTIFIER . ') NOT NULL,
              localIdentifier VARCHAR(' . MAXLEN_STUDENT_LOCAL_IDENTIFIER . ') NULL,
              optionalStateData1 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              gradeLevel CHAR(' . LENGTH_GRADE_LEVEL . ') NOT NULL,
              raceAA ENUM("Y", "N") NULL,
              raceAN ENUM("Y", "N") NULL,
              raceAS ENUM("Y", "N") NULL,
              raceHL ENUM("Y", "N") NULL,
              raceHP ENUM("Y", "N") NULL,
              raceWH ENUM("Y", "N") NULL,
              statusDIS ENUM("Y", "N") NULL,
              statusECO ENUM("Y", "N") NULL,
              statusELL ENUM("Y", "N") NULL,
              statusGAT ENUM("Y", "N") NULL,
              statusLEP ENUM("Y", "N") NULL,
              statusMIG ENUM("Y", "N") NULL,
              disabilityType VARCHAR(' . MAXLEN_DISABILITY_TYPE . ') NULL,
              optionalStateData2 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              optionalStateData3 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              optionalStateData4 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              optionalStateData5 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              optionalStateData6 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              optionalStateData7 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              optionalStateData8 VARCHAR(' . MAXLEN_OPTIONAL_STATE_DATA . ') NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (studentRecordId),
              INDEX (studentId),
              INDEX (enrollOrgId),
              INDEX (stateOrgId),
              INDEX (stateIdentifier))',

        'primaryKey' => 'studentRecordId'
    ],

    'test_battery_grade' =>
    [
        'create' =>
            '(testBatteryGradeId INT NOT NULL AUTO_INCREMENT,
              testBatteryGrade VARCHAR(' . MAXLEN_TEST_BATTERY_GRADE . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatteryGradeId))',

        'primaryKey' => 'testBatteryGradeId'
    ],

    'test_battery_permissions' =>
    [
        'create' =>
            '(testBatteryPermissionsId INT NOT NULL AUTO_INCREMENT,
              testBatteryPermissions VARCHAR(' . MAXLEN_TEST_BATTERY_PERMISSIONS . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatteryPermissionsId))',

        'primaryKey' => 'testBatteryPermissionsId'
    ],

    'test_battery_program' =>
    [
        'create' =>
            '(testBatteryProgramId INT NOT NULL AUTO_INCREMENT,
              testBatteryProgram VARCHAR(' . MAXLEN_TEST_BATTERY_PROGRAM . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatteryProgramId))',

        'primaryKey' => 'testBatteryProgramId'
    ],

    'test_battery_scoring' =>
    [
        'create' =>
            '(testBatteryScoringId INT NOT NULL AUTO_INCREMENT,
              testBatteryScoring VARCHAR(' . MAXLEN_TEST_BATTERY_SCORING . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatteryScoringId))',

        'primaryKey' => 'testBatteryScoringId'
    ],

    'test_battery_security' =>
    [
        'create' =>
            '(testBatterySecurityId INT NOT NULL AUTO_INCREMENT,
              testBatterySecurity VARCHAR(' . MAXLEN_TEST_BATTERY_SECURITY . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatterySecurityId))',

        'primaryKey' => 'testBatterySecurityId'
    ],

    'test_battery_subject' =>
    [
        'create' =>
            '(testBatterySubjectId INT NOT NULL AUTO_INCREMENT,
              testBatterySubject VARCHAR(' . MAXLEN_TEST_BATTERY_SUBJECT . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatterySubjectId))',

        'primaryKey' => 'testBatterySubjectId'
    ],

    'test_status' =>
    [
        'create' =>
            '(testStatusId INT NOT NULL AUTO_INCREMENT,
              testStatus VARCHAR(' . MAXLEN_TEST_STATUS . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testStatusId))',

        'primaryKey' => 'testStatusId'
    ],

    'test_battery' =>
    [
        'create' =>
            '(testBatteryId INT NOT NULL,
              testBatteryName VARCHAR(' . MAXLEN_TEST_BATTERY_NAME . ') NOT NULL,
              testBatterySubjectId INT NOT NULL,
              testBatteryGradeId INT NOT NULL,
              testBatteryProgramId INT NOT NULL,
              testBatterySecurityId INT NOT NULL,
              testBatteryPermissionsId INT NOT NULL,
              testBatteryScoringId INT NOT NULL,
              testBatteryDescription VARCHAR(' . MAXLEN_TEST_BATTERY_DESCRIPTION . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatteryId),
              INDEX (testBatteryName),
              FOREIGN KEY (testBatterySubjectId)
                REFERENCES test_battery_subject(testBatterySubjectId),
              FOREIGN KEY (testBatteryGradeId)
                REFERENCES test_battery_grade(testBatteryGradeId),
              FOREIGN KEY (testBatteryProgramId)
                REFERENCES test_battery_program(testBatteryProgramId),
              FOREIGN KEY (testBatterySecurityId)
                REFERENCES test_battery_security(testBatterySecurityId),
              FOREIGN KEY (testBatteryPermissionsId)
                REFERENCES test_battery_permissions(testBatteryPermissionsId),
              FOREIGN KEY (testBatteryScoringId)
                REFERENCES test_battery_scoring(testBatteryScoringId))',

        'primaryKey' => 'testBatteryId'
    ],

    'test_battery_form' =>
    [
        'create' =>
            '(testBatteryId INT NOT NULL,
              testBatteryFormId INT NOT NULL,
              testBatteryFormName VARCHAR(' . MAXLEN_TEST_BATTERY_FORM_NAME . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testBatteryFormId),
              INDEX (testBatteryFormName),
              FOREIGN KEY (testBatteryId)
                REFERENCES test_battery(testBatteryId))',

        'primaryKey' => 'testBatteryFormId'
    ],

    'test_assignment' =>
    [
        'create' =>
            '(testAssignmentId INT NOT NULL AUTO_INCREMENT,
              adpTestAssignmentId INT NULL,
              studentRecordId INT NOT NULL,
              testBatteryFormId INT NOT NULL,
              testBatteryId INT NOT NULL,
              testStatusId INT NOT NULL,
              testResultsId INT NULL,
              testFormRevisionId INT NULL,
              lastQueueEventTime INT NOT NULL DEFAULT 0,
              embeddedScoreReportId INT NOT NULL DEFAULT 0,
              testKey CHAR(' . LENGTH_TEST_KEY . ') NOT NULL,
              instructionStatus INT NOT NULL DEFAULT 0,
              enableLineReader ENUM("Y", "N") NOT NULL DEFAULT "N",
              enableTextToSpeech ENUM("Y", "N") NOT NULL DEFAULT "N",
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testAssignmentId),
              FOREIGN KEY (studentRecordId)
                REFERENCES student_record(studentRecordId),
              FOREIGN KEY (testBatteryFormId)
                REFERENCES test_battery_form(testBatteryFormId),
              FOREIGN KEY (testBatteryId)
                REFERENCES test_battery(testBatteryId),
              FOREIGN KEY (testStatusId)
                REFERENCES test_status(testStatusId),
              UNIQUE (testKey))',

        'primaryKey' => 'testAssignmentId'
    ],

    'test_form_revision' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              title VARCHAR(' . MAXLEN_TEST_FORM_REVISION_TITLE . ') NULL,
              identifier VARCHAR(' . MAXLEN_TEST_FORM_REVISION_IDENTIFIER . ') NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormRevisionId))',

        'primaryKey' => 'testFormRevisionId'
    ],

    'test_form_part' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormPartId INT NOT NULL AUTO_INCREMENT,
              identifier VARCHAR(' . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ') NULL,
              navigationMode VARCHAR(' . MAXLEN_TEST_FORM_PART_NAVIGATION_MODE . ') NULL,
              submissionMode VARCHAR(' . MAXLEN_TEST_FORM_PART_SUBMISSION_MODE . ') NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormPartId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId))',

        'primaryKey' => 'testFormPartId'
    ],

    'test_form_assessment_section' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormPartId INT NOT NULL,
              testFormAssessmentSectionId INT NOT NULL AUTO_INCREMENT,
              identifier VARCHAR(' . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ') NULL,
              title VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_SECTION_TITLE . ') NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormAssessmentSectionId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testFormPartId)
                REFERENCES test_form_part(testFormPartId))',

        'primaryKey' => 'testFormAssessmentSectionId'
    ],

    'test_form_assessment_item' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormAssessmentSectionId INT NOT NULL,
              testFormAssessmentItemId INT NOT NULL AUTO_INCREMENT,
              uin VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_UIN . ') NULL,
              itemId VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEMID . ') NULL,
              identifier VARCHAR(' . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ') NULL,
              serial VARCHAR(' . MAXLEN_TEST_FORM_ANY_SERIAL . ') NULL,
              domain VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_DOMAIN . ') NULL,
              cluster VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_CLUSTER . ') NULL,
              contentStandard VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_CONTENT_STANDARD . ') NULL,
              evidenceStatement VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_EVIDENCE_STATEMENT . ') NULL,
              fluencySkillArea VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_FLUENCY_SKILL_AREA . ') NULL,
              itemStrand VARCHAR(' . MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEM_STRAND . ') NULL,
              itemGrade CHAR(' . LENGTH_GRADE_LEVEL . ') NULL,
              maxScorePoints INT NULL,
              calculatorTypeId INT NULL,
              qtiClassId INT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormAssessmentItemId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testFormAssessmentSectionId)
                REFERENCES test_form_assessment_section(testFormAssessmentSectionId))',

        'primaryKey' => 'testFormAssessmentItemId'
    ],

    'test_form_system_item' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormAssessmentSectionId INT NOT NULL,
              testFormSystemItemId INT NOT NULL AUTO_INCREMENT,
              itemId VARCHAR(' . MAXLEN_TEST_FORM_SYSTEM_ITEM_ITEMID . ') NULL,
              identifier VARCHAR(' . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ') NULL,
              serial VARCHAR(' . MAXLEN_TEST_FORM_ANY_SERIAL . ') NULL,
              qtiClassId INT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormSystemItemId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testFormAssessmentSectionId)
                REFERENCES test_form_assessment_section(testFormAssessmentSectionId))',

        'primaryKey' => 'testFormSystemItemId'
    ],

    'test_form_item_element' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormAssessmentItemId INT NOT NULL,
              testFormItemElementId INT NOT NULL AUTO_INCREMENT,
              serial VARCHAR(' . MAXLEN_TEST_FORM_ANY_SERIAL . ') NULL,
              responseIdentifier VARCHAR(' . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ') NULL,
              qtiClassId INT NULL,
              maxChoices INT NULL,
              minChoices INT NULL,
              shuffle ENUM("Y", "N") NULL DEFAULT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormItemElementId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testFormAssessmentItemId)
                REFERENCES test_form_assessment_item(testFormAssessmentItemId))',

        'primaryKey' => 'testFormItemElementId'
    ],

    'test_form_item_element_response' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormItemElementId INT NOT NULL,
              testFormItemElementResponseId INT NOT NULL AUTO_INCREMENT,
              serial VARCHAR(' . MAXLEN_TEST_FORM_ANY_SERIAL . ') NULL,
              identifier VARCHAR(' . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ') NULL,
              responseCardinalityId INT NULL,
              qtiClassId INT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormItemElementResponseId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testFormItemElementId)
                REFERENCES test_form_item_element(testFormItemElementId))',

        'primaryKey' => 'testFormItemElementResponseId'
    ],

    'test_form_item_correct_response' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormItemElementResponseId INT NOT NULL,
              testFormItemCorrectResponseId INT NOT NULL AUTO_INCREMENT,
              value TEXT NOT NULL,
              position TINYINT NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testFormItemCorrectResponseId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testFormItemElementResponseId)
                REFERENCES test_form_item_element_response(testFormItemElementResponseId))',

        'primaryKey' => 'testFormItemCorrectResponseId'
    ],

    'test_map' =>
    [
        "(`testMapId` INT unsigned NOT NULL AUTO_INCREMENT,
            `taoId` VARCHAR(".MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEMID.") COLLATE utf8_bin NOT NULL,
            `itemId` VARCHAR(".MAXLEN_TEST_FORM_ASSESSMENT_ITEM_ITEMID.") COLLATE utf8_bin NOT NULL,
            `gradeLevel` CHAR(2) COLLATE utf8_bin NOT NULL,
            `qtiClassId` INT NOT NULL,
            `responseCardinalityId` INT NOT NULL,
            `maxPoints` INT NOT NULL,
            `scoringStatus` VARCHAR(".MAXLEN_SCORING_STATUS.") COLLATE utf8_bin DEFAULT NULL,
            PRIMARY KEY (`testMapId`),
            UNIQUE KEY `taoId` (`taoId`),
            UNIQUE KEY `itemId` (`itemId`))",

        "primaryKey" => 'testMapId'
    ],

    'test_results' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testResultsId INT NOT NULL AUTO_INCREMENT,
              testStartDateTime DATETIME NOT NULL,
              testSubmitDateTime DATETIME NOT NULL,
              timeSpentOnItems INT NOT NULL,
              timeSpentOnAllItems INT NOT NULL,
              testDuration INT NOT NULL,
              totalItemsPresented INT NOT NULL,
              totalQuestionsPresented INT NOT NULL,
              totalItemsAnsweredCorrectly INT NOT NULL,
              totalItemsAnsweredIncorrectly INT NOT NULL,
              totalItemsNotAnswered INT NOT NULL,
              totalItemsNotScored INT NOT NULL,
              testRawScore INT NOT NULL,
              testScoreTheta INT NOT NULL,
              testScoreScale INT NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testResultsId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              INDEX (testStartDateTime),
              INDEX (testSubmitDateTime))',

        'primaryKey' => 'testResultsId'
    ],

    'test_results_assessment_item' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormAssessmentItemId INT NOT NULL,
              testResultsId INT NOT NULL,
              testResultsAssessmentItemId INT NOT NULL AUTO_INCREMENT,
              timeSpent INT NOT NULL,
              itemScore INT NOT NULL,
              isItemCorrect ENUM("Y", "N") NOT NULL,
              isItemAnswered ENUM("Y", "N") NOT NULL,
              isItemScored ENUM("Y", "N") NOT NULL,
              toolsUsed VARCHAR(' . MAXLEN_TEST_RESULTS_TOOLS_USED . ') NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testResultsAssessmentItemId),
              FOREIGN KEY (testFormAssessmentItemId)
                REFERENCES test_form_assessment_item(testFormAssessmentItemId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testResultsId)
                REFERENCES test_results(testResultsId))',

        'primaryKey' => 'testResultsId'
    ],

    'test_results_item_element_response' =>
    [
        'create' =>
            '(testFormRevisionId INT NOT NULL,
              testFormAssessmentItemId INT NOT NULL,
              testResultsId INT NOT NULL,
              testResultsAssessmentItemId INT NOT NULL,
              testResultsItemElementResponseId INT NOT NULL AUTO_INCREMENT,
              responseIdentifier VARCHAR(' . MAXLEN_TEST_FORM_ANY_IDENTIFIER . ') NULL,
              value TEXT NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (testResultsItemElementResponseId),
              FOREIGN KEY (testResultsAssessmentItemId)
                REFERENCES test_results_assessment_item(testResultsAssessmentItemId),
              FOREIGN KEY (testFormAssessmentItemId)
                REFERENCES test_form_assessment_item(testFormAssessmentItemId),
              FOREIGN KEY (testFormRevisionId)
                REFERENCES test_form_revision(testFormRevisionId),
              FOREIGN KEY (testResultsId)
                REFERENCES test_results(testResultsId))',

        'primaryKey' => 'testResultsId'
    ],

    'user' =>
    [
        'create' =>
            '(userId INT NOT NULL AUTO_INCREMENT,
              organizationId INT NOT NULL,
              clientUserId INT NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (userId),
              UNIQUE (organizationId, clientUserId),
              INDEX (clientUserId))',

        'primaryKey' => 'userId'
    ],

    'user_audit' =>
    [
        'create' =>
            '(userAuditId INT NOT NULL AUTO_INCREMENT,
              dateTime DATETIME NOT NULL,
              clientUserId INT NOT NULL,
              actionType INT NOT NULL,
              dataType INT NOT NULL,
              primaryId INT NOT NULL,
              dataChanges TEXT NOT NULL,
              PRIMARY KEY (userAuditId),
              INDEX (clientUserId),
              INDEX (actionType),
              INDEX (dataType, primaryId),
              INDEX (dateTime))',

        'primaryKey' => 'userAuditId'
    ],

    'version_history' =>
    [
        'create' =>
            '(versionHistoryId INT NOT NULL AUTO_INCREMENT,
              version VARCHAR(8) NOT NULL,
              dateTime DATETIME NOT NULL,
              PRIMARY KEY (versionHistoryId),
              INDEX (version),
              INDEX (dateTime))',

        'primaryKey' => 'versionHistoryId'
    ]
];

// TODO: ADD MISSING TABLES ???
$valueColumnNames =
[
    'classId' => 'classId',
    'clientUserId' => 'clientUserId',
    'enrollOrgId' => 'organizationId',
    'organizationId' => 'organizationId',
    'studentRecordId' => 'studentRecordId',
    'testAssignmentId' => 'testAssignmentId',
    'testBatteryId' => 'testBatteryId'
];

// TODO: ADD MISSING TABLES ???
$columnTableNames =
[
    'classId' => 'class',
    'clientUserId' => 'user',
    'organizationId' => 'organization',
    'studentRecordId' => 'student_record',
    'testAssignmentId' => 'test_assignment',
    'testBatteryId' => 'test_battery'
];

// Used by dbGetValueReferenceId()
$referenceIdColumns =
[
    'qti_class' => 'qtiClassId',
    'test_status' => 'testStatusId',
    'calculator_type' => 'calculatorTypeId',
    'test_battery_grade' => 'testBatteryGradeId',
    'test_battery_program' => 'testBatteryProgramId',
    'test_battery_scoring' => 'testBatteryScoringId',
    'test_battery_subject' => 'testBatterySubjectId',
    'response_cardinality' => 'responseCardinalityId',
    'test_battery_security' => 'testBatterySecurityId',
    'test_battery_permissions' => 'testBatteryPermissionsId'
];
$referenceValueColumns =
[
    'qti_class' => 'qtiClass',
    'test_status' => 'testStatus',
    'calculator_type' => 'calculatorType',
    'test_battery_grade' => 'testBatteryGrade',
    'test_battery_program' => 'testBatteryProgram',
    'test_battery_scoring' => 'testBatteryScoring',
    'test_battery_subject' => 'testBatterySubject',
    'response_cardinality' => 'responseCardinality',
    'test_battery_security' => 'testBatterySecurity',
    'test_battery_permissions' => 'testBatteryPermissions'
];

//
// LDR database services
//
// createAllTables()
//   Creates any LDR database tables that do not already exist
//
function createAllTables()
{
    global $dbTables;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Create the tables
    $result = null;
    foreach ($dbTables as $tableName => $table)
    {
        // Create the table
        $result = dbCreateTable($logId, $db, $tableName);
        if ($result instanceof ErrorLDR)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__);
}

// createTable()
//   Creates an LDR database table if it does not already exist
//
function createTable($name)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Validate the table name
    $argName = 'tableName';
    $name = validateArg($logId, $db, [$argName => $name], $argName, $argFilterMap[$argName], DCO_TOLOWER);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create the table
    $result = dbCreateTable($logId, $db, $name);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__);
}

// dropAllTables()
//   Drops all tables from the LDR database except the session_token table
//
function dropAllTables()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Drop all LDR tables
    $result = dbDropAllTables($logId, $db);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__);
}

// dropTable()
//   Drops a table from the LDR database
//
function dropTable($name)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the table name
    $argName = 'tableName';
    $name = validateArg($logId, $db, [$argName => $name], $argName, $argFilterMap[$argName], DCO_TOLOWER);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Drop the table
    $result = dbDropTable($logId, $db, $name);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__);
}

// getDatabaseVersions()
//   Returns the production and unit test database versions
//
function getDatabaseVersion()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Select the production database
    $token = dbSelectDatabase($logId, DB_PRODUCTION);
    if ($token instanceof ErrorLDR)
    {
        $token->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Get the production database version
    $productionVersion = dbGetDatabaseVersion($logId, $db);
    if ($productionVersion instanceof ErrorLDR)
    {
        $productionVersion->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Close the database connection
    $db = null;

    // Select the production database
    $token = dbSelectDatabase($logId, DB_PRODUCTION);
    if ($token instanceof ErrorLDR)
    {
        $token->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the new session token with the database versions
    $response = ['token' => $token, 'requiredVersion' => LDR_DATABASE_VERSION,
                 'productionVersion' => $productionVersion];
    $response = json_encode($response);
    echo $response;

    // Log the response
    logResponse($logId, $response);

    // Log the elapsed time
    $elapsedTime = microtime(true) - $startTime;
    logElapsed($logId, $elapsedTime);
}

// selectDatabase()
//   Selects the specified database and returns a session token
//
function selectDatabase($database)
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Validate the current session token
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }

    // // Select the database and get a new session token
    // $token = dbSelectDatabase($logId, $database);
    // if ($token instanceof ErrorLDR)
    // {
    //     $token->returnErrorJSON(__FUNCTION__);
    //     return;
    // }

    // // Return the new session token
    // $response = ['token' => $token];
    $response = ['SERVICE' => 'is deprecated'];
    finishService($db, $logId, $response, $startTime, __FUNCTION__);
}

// selectProductionDatabase()
//   Selects the production database and returns a session token
//
function selectProductionDatabase()
{
    selectDatabase(DB_PRODUCTION);
}

// selectUnitTestDatabase()
//   Selects the unit test database and returns a session token
//
function selectUnitTestDatabase()
{
    selectDatabase('');
}

// updateDatabase()
//   Performs the required forward or backward migration on the production database
//
function updateDatabase()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Get a database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Update the production database
    $result = dbMigrateDatabase($logId, $db);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the new session token with the migration result
    $response = json_encode(['result' => $result]);
    echo $response;

    // Log the response
    logResponse($logId, $response);

    // Log the elapsed time
    $elapsedTime = microtime(true) - $startTime;
    logElapsed($logId, $elapsedTime);
}

//
// LDR database support functions
//
// dbAddPerformanceRecord()
//   Creates an LDR service performance record
//
function dbAddPerformanceRecord($logId, $db, $statisticsDate, $serviceName, $elapsedTime)
{
    $sql = "INSERT INTO performance " .
        "(statisticsDate, " .
        "serviceName, " .
        "serviceCount, " .
        "totalElapsedTime, " .
        "maximumElapsedTime, " .
        "minimumElapsedTime) " .
        "VALUES " .
        "('$statisticsDate', " .
        "'$serviceName', " .
        "'1', " .
        "'$elapsedTime', " .
        "'$elapsedTime', " .
        "'$elapsedTime')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbAddQueueProcessingRecord()
//   Creates an LDR service performance record
//
function dbAddQueueProcessingRecord($logId, $db, $statisticsDate, $hostname, $queueProcessing)
{
    $sql = "INSERT INTO queue_processing
             (statisticsDate, hostname, totalMessagesProcessed,
              sqsCreateClientElapsedTime, sqsReceiveMessageElapsedTime,
              sqsDeleteMessageElapsedTime, ldrGetTestAssignmentIdElapsedTime,
              ldrGetTestAssignmentElapsedTime, ldrUpdateLastQueueEventTime,
              ldrUpdateTestStatusElapsedTime,
              ldrStoreTestResultsElapsedTime, adpUpdateTestStatusElapsedTime,
              halLoggingElapsedTime, submittedMessagesProcessed,
              submittedMessagesIgnored, submittedMessagesInvalid,
              submittedMessagesFailed, submittedElapsedTime,
              startedMessagesProcessed, startedMessagesIgnored,
              startedMessagesInvalid, startedMessagesFailed,
              startedElapsedTime, pausedMessagesProcessed,
              pausedMessagesIgnored, pausedMessagesInvalid,
              pausedMessagesFailed, pausedElapsedTime,
              resumedMessagesProcessed, resumedMessagesIgnored,
              resumedMessagesInvalid, resumedMessagesFailed,
              resumedElapsedTime, unknownStatusChangesProcessed,
              unknownMessagesProcessed, unknownElapsedTime) " .
           "VALUES " .
           "('$statisticsDate', '$hostname', '$queueProcessing->totalMessagesProcessed',
             '$queueProcessing->sqsCreateClientElapsedTime', '$queueProcessing->sqsReceiveMessageElapsedTime',
             '$queueProcessing->sqsDeleteMessageElapsedTime', '$queueProcessing->ldrGetTestAssignmentIdElapsedTime',
             '$queueProcessing->ldrGetTestAssignmentElapsedTime', '$queueProcessing->ldrUpdateLastQueueEventTime',
             '$queueProcessing->ldrUpdateTestStatusElapsedTime',
             '$queueProcessing->ldrStoreTestResultsElapsedTime', '$queueProcessing->adpUpdateTestStatusElapsedTime',
             '$queueProcessing->halLoggingElapsedTime', '$queueProcessing->submittedMessagesProcessed',
             '$queueProcessing->submittedMessagesIgnored', '$queueProcessing->submittedMessagesInvalid',
             '$queueProcessing->submittedMessagesFailed', '$queueProcessing->submittedElapsedTime',
             '$queueProcessing->startedMessagesProcessed', '$queueProcessing->startedMessagesIgnored',
             '$queueProcessing->startedMessagesInvalid', '$queueProcessing->startedMessagesFailed',
             '$queueProcessing->startedElapsedTime', '$queueProcessing->pausedMessagesProcessed',
             '$queueProcessing->pausedMessagesIgnored', '$queueProcessing->pausedMessagesInvalid',
             '$queueProcessing->pausedMessagesFailed', '$queueProcessing->pausedElapsedTime',
             '$queueProcessing->resumedMessagesProcessed', '$queueProcessing->resumedMessagesIgnored',
             '$queueProcessing->resumedMessagesInvalid', '$queueProcessing->resumedMessagesFailed',
             '$queueProcessing->resumedElapsedTime', '$queueProcessing->unknownStatusChangesProcessed',
             '$queueProcessing->unknownMessagesProcessed', '$queueProcessing->unknownElapsedTime')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbConnect()
//   Connects to a database and returns a PDO object
//
function dbConnect($logId)
{
    global $selectedDatabase;

    // Open the database server connection
    $dbHostname = DB_HOSTNAME;
    $dsn = "mysql:host=$dbHostname";
    $dsn .= defined('DB_PORT') && DB_PORT ? ';port=' . DB_PORT : "";
    try	{
        $pdo = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
        $pdo->exec("SET NAMES utf8");
    }
    catch(PDOException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_CONNECT_FAILED,
                                 $error->getMessage() . ' : DSN = |' . $dsn . '|');
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        $errorLDR->logMessage(__FUNCTION__, 'DSN = |' . $dsn . '|');
        return $errorLDR;
    }

    // Configure the connection to throw exceptions
    if (!$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION))
    {
        $pdo = null; // Close the database connection
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_SET_ATTRIBUTE_FAILED,
                                 'Database connection configuration failed');
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Get the selected database name
    if (defined('DB_PRODUCTION') === true) {
      $selectedDatabase = DB_PRODUCTION;
    } else {
      $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_SELECTION_FAILED, 'DB_PRODUCTION is undefined');
      logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
      $errorLDR->logError(__FUNCTION__);
      return $errorLDR;
    }

    // Select the LDR database
    try {
        $pdo->exec("USE $selectedDatabase;");
    }
    catch(PDOException $error) {
        $pdo = null; // Close the database connection
        $errorLDR = new ErrorLDR($logId, LDR_EC_USE_DATABASE_FAILED, $error->getMessage());
        logHAL($logId, HAL_SEVERITY_ERROR, $errorLDR->getErrorCode(), $errorLDR->getErrorDetail());
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Return the connection
    return $pdo;
}

// dbCreateSessionToken()
//   Creates a session_token record
//
function dbCreateSessionToken($logId, $db, $clientId, $token)
{
    // TODO: IMPLEMENT SERVICE INVOKED BY CRON JOB WHICH REMOVES EXPIRED RECORDS

    $sql = "INSERT INTO session_token (clientId, token, dateTime)
            VALUES ('$clientId', '$token', '" . date("Y-m-d H:i:s") . "')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbCreateTable
//   Creates an LDR database table if it does not already exist
//
function dbCreateTable($logId, $db, $tableName)
{
    global $dbTables;

    // Check that the table is known
    if (!array_key_exists($tableName, $dbTables))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_TABLE_UNKNOWN_TABLE,
                                 LDR_ED_DATABASE_TABLE_UNKNOWN_TABLE . $tableName);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if the table already exists
    $sql = "SHOW TABLES LIKE '$tableName'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
            return SUCCESS;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create the table
    $sql = "CREATE TABLE " . $tableName . " " . $dbTables[$tableName]['create'] .
           " ENGINE=InnoDB COLLATE=utf8_bin";
    if ($tableName == 'student')
        $sql .= " AUTO_INCREMENT=" . DB_FIRST_STUDENT_RECORD;
    elseif ($tableName == 'test_assignment')
        $sql .= " AUTO_INCREMENT=" . DB_FIRST_TEST_ASSIGNMENT;
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create built-in records
    switch ($tableName)
    {
        case 'calculator_type':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO calculator_type (calculatorType)
                         VALUES ('No Calculator'), ('Scientific Calculator')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'client':
            // Add the client accounts - THESE MUST MATCH THE 'CLIENT_*' DEFINITIONS IN definitions.php
            $sql = "INSERT INTO client (clientType, clientName, clientPassword) VALUES " .
                "('" . CLIENT_TYPE_DEVELOPMENT . "', 'ldr_root', " .
                     "'BF5B500768FF23D0F48BE873467B94161CBEEFF4BC0FCA8BDFAEEB67C3A6812A3D074475E2B2445E35E5F19E12C42176E9A9CDC1EC5142478986CC807C027F69'), " .
                "('" . CLIENT_TYPE_DEVELOPMENT . "', 'parcc_adp', " .
                     "'9B19FBEF3B836B33B7EC9627F76C7F0D0056A30F18BD6E17E13BB37748A1DE54DC5BA113E45B16A57121A4BD123ABEDF7FF40731FA7859E86360313A447BD092'), " .
                "('" . CLIENT_TYPE_DEVELOPMENT . "', 'parcc_ci', " .
                     "'F6C3BC93F9D78351CDF404098021E5CAC8F001974954A6095CA8B8823B6BBA319ABEEEB314E4A2E85373275D05800B48690D99EB5C6262A778E79DCF25025128'), " .
                "('" . CLIENT_TYPE_DEVELOPMENT . "', 'parcc_dev', " .
                     "'94693C2FD7FB62797298EAC54A0FF8DC29F568ED67989C03B7B432292A1CC10A309DC398729F015BCDACBB61A577C144E3D51E325A3DD64D6EBE730C8A7F2161'), " .
                "('" . CLIENT_TYPE_DEVELOPMENT . "', 'parcc_qa', " .
                     "'81D0C0189A9D6A494E8EB53598D04447DA89907E60FB33BAA545BAB744DE78AE4E4E30C0CAE278F4547DCFFD60D7820EDCC9D942A711FA2F71520058322F16C5'), " .
                "('" . CLIENT_TYPE_DEVELOPMENT . "', 'parcc_tap', " .
                     "'927AE4001EBD15026831839536260745E2C0F8CD417702BB47B565AFED2A38F0B5CA787DC004A6E9AFACB1113AB94945943D95BCC1D37BCA6617231AA93D5C16'), " .
                "('" . CLIENT_TYPE_DEVELOPMENT . "', 'parcc_user', " .
                     "'A7E012D819B0EDD6E6A52F10796F93FDC80E9F7F7B86A18110E2BDD5494D1D9442F885FC845701D48FCE5F5C8432C102B4817C96B98F499EA93DD6ED88364B66'), " .
                "('" . CLIENT_TYPE_PRODUCTION . "', 'prod_adp', " .
                     "'EDC8E69ED34B58399815F9C58E56DCF085EEF4C6FDD149DF7CFB036EF10E89485052F1179DA80697F9FA97C468ABE857B5784EBF85F5F29BA30FD1D214815EB9'), " .
                "('" . CLIENT_TYPE_PRODUCTION . "', 'prod_tap', " .
                     "'DCC1B52E292C05AE6F17DF1219B0DF1728208BE947C9C12F595366889B701CBB8BC80390178AD3FF6AF963F13873F36863ED8021B8EFBBAB5670BA2F9A6AF5E1'), " .
                "('" . CLIENT_TYPE_PRODUCTION . "', 'prod_user', " .
                     "'2A9F5571308B7E5EC834BACF4C60337CDEF74730DF5EB41FFBD932E9FE61DA871C0D604D2DA647B2BA1B5B415A40125BBF1A03F2D85CA094A686AEF024938A49')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'organization':
            // Add the top-level root organization
            $sql = "INSERT INTO organization
                        (organizationType,
                         stateOrganizationId,
                         parentOrganizationId,
                         organizationName,
                         nestedSetLeft,
                         nestedSetRight)
                    VALUES
                        ('" . ORG_TYPE_ROOT . "',
                         '0',
                         '" . ROOT_PARENT_ORGANIZATION_ID . "',
                         '" . ROOT_ORGANIZATION_NAME . "',
                         '1',
                         '2')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }

            // Update the Redis cache
            redisCreateOrganization($logId, ORG_TYPE_ROOT, ROOT_PARENT_ORGANIZATION_ID, '', ROOT_ORGANIZATION_NAME);
            break;
        case 'qti_class':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO qti_class (qtiClass) VALUES ('assessmentItem'), ('choiceInteraction'), ('simpleChoice'),
                    ('responseDeclaration'), ('textEntryInteraction')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'response_cardinality':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO response_cardinality (responseCardinality)
                         VALUES ('single'), ('multiple')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'student_record':
            $sql = "UPDATE organization SET studentCount = 0";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'test_battery_grade':
            $sql = "INSERT INTO test_battery_grade (testBatteryGrade) VALUES ('K'), ('1'), ('2'), ('3'), ('4'),
                    ('5'), ('6'), ('7'), ('8'), ('9'), ('10'), ('11'), ('12'), ('Multi-level')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'test_battery_permissions':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO test_battery_permissions (testBatteryPermissions) VALUES ('Non-Restricted'),
                    ('Restricted')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'test_battery_program':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO test_battery_program (testBatteryProgram) VALUES ('Diagnostic Assessment'),
                    ('K2 Formative'), ('Mid-Year/Interim'), ('Practice Test'), ('Speaking & Listening'), ('Summative')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'test_battery_scoring':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO test_battery_scoring (testBatteryScoring) VALUES ('Immediate'), ('Delayed')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'test_battery_security':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO test_battery_security (testBatterySecurity) VALUES ('Non-Secure'), ('Secure')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'test_battery_subject':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO test_battery_subject (testBatterySubject) VALUES ('ELA'), ('Math'), ('N/A')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'test_status':
            // NOTE: TO MAINTAIN SEMANTIC INTEGRITY ONLY APPEND NEW VALUES - NEVER CHANGE OR REORDER THESE VALUES
            $sql = "INSERT INTO test_status (testStatus)
                    VALUES ('Scheduled'), ('InProgress'), ('Paused'), ('Submitted'), ('Completed'), ('Canceled')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'user':
            // Add a top-level root client
            $sql = "INSERT INTO user (organizationId, clientUserId)
                    VALUES ('" . ROOT_ORGANIZATION_ID . "', '" . ROOT_CLIENT_USER_ID . "')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($logId, $error, __FUNCTION__, $sql);
            }
            break;
        case 'version_history':
            // Update the database version
            $result = dbUpdateDatabaseVersion($logId, $db);
            if ($result instanceof ErrorLDR)
            {
                $result->logError(__FUNCTION__);
                return $result;
            }
            break;
    }

    return SUCCESS;
}

// dbCreateUser()
//   Creates a user record
//
function dbCreateUser($logId, $db, $clientUserId, $organizationId)
{
    $sql = "INSERT INTO user (organizationId, clientUserId) VALUES ('$organizationId', '$clientUserId')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $userId = $db->lastInsertId();
        return $userId;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbDeleteSessionToken()
//   Deletes a session_token record
//
function dbDeleteSessionToken($logId, $db, $token)
{
    // Prepare the query
    $sql = "DELETE FROM session_token WHERE token = '$token'";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbDropAllTables()
//   Drops all tables from the LDR database except the session_token table
//
function dbDropAllTables($logId, $db)
{
    // Delete tables ordered by dependencies such as foreign key constraints
    $sql = "DROP TABLE IF EXISTS
            student_class,
            class,
            client,
            test_assignment,
            embedded_score_report,
            student_record,
            student,
            organization,
            performance,
            queue_processing,
            test_results_item_element_response,
            test_results_assessment_item,
            test_results,
            test_battery_form,
            test_battery,
            test_battery_grade,
            test_battery_permissions,
            test_battery_program,
            test_battery_scoring,
            test_battery_security,
            test_battery_subject,
            test_form_item_correct_response,
            test_form_item_element_response,
            test_form_item_element,
            test_form_system_item,
            test_form_assessment_item,
            test_form_assessment_section,
            test_form_part,
            test_form_revision,
            response_cardinality,
            calculator_type,
            qti_class,
            test_status,
            user_audit,
            user,
            version_history";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Clear the cache
    redisFlushAll($logId);

    return SUCCESS;
}

// dbDropTable()
//   Drops a table from the LDR database
//
function dbDropTable($logId, $db, $tableName)
{
    global $dbTables;

    // Check that the table is known
    if (!array_key_exists($tableName, $dbTables))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_TABLE_UNKNOWN_TABLE,
                                 LDR_ED_DATABASE_TABLE_UNKNOWN_TABLE . $tableName);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    $sql = "DROP TABLE IF EXISTS " . $tableName;
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbGetClientId_By_Token()
//   Returns a session clientId if found, null otherwise
//
function dbGetClientId_By_Token($logId, $db, $token)
{
    $sql = "SELECT clientId FROM session_token WHERE token = '$token'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null; // Token was not found
}

// dbGetClientName_By_ClientId()
//   Returns a clientName if found, null otherwise
//
function dbGetClientName_By_ClientId($logId, $db, $clientId)
{
    $sql = "SELECT clientName FROM client WHERE clientId = '$clientId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null; // Client record was not found
}

// dbGetDatabaseVersion()
//   Returns the database version if found, null otherwise
//
function dbGetDatabaseVersion($logId, PDO $db)
{
    // Get the last entry
    $sql = "SELECT versionHistoryId, version FROM version_history
            ORDER BY versionHistoryId DESC LIMIT 1";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[1];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null; // Version not found
}

// dbGetPerformanceRecord()
//   Returns an LDR service performance record
//
function dbGetPerformanceRecord($logId, $db, $statisticsDate, $serviceName)
{
    $performanceData = null;

    $sql = "SELECT performanceId, serviceCount, totalElapsedTime, maximumElapsedTime, minimumElapsedTime
            FROM performance WHERE statisticsDate = '$statisticsDate' AND serviceName = '$serviceName'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $performanceData = $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return $performanceData;
}

// dbGetPerformanceStatistics()
//   Returns an array of performance statistics for a given date range
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetPerformanceStatistics($logId, $db, $startDate, $endDate)
{
    // Get the performance records
    $sql = "SELECT serviceName,
                   serviceCount,
                   totalElapsedTime,
                   maximumElapsedTime,
                   minimumElapsedTime
            FROM performance WHERE TRUE";
    if (strlen($startDate) > 0)
        $sql .= " AND statisticsDate >= '$startDate'";
    if (strlen($endDate) > 0)
        $sql .= " AND statisticsDate <= '$endDate'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Aggregate the performance data by serviceName
    $services = [];
    foreach ($records as $record)
    {
        // Get this service from the array or add it
        if (array_key_exists($record->serviceName, $services))
            $service = $services[$record->serviceName];
        else
        {
            $service = new Performance();
            $services[$record->serviceName] = $service;
        }

        // Aggregate the values for this service
        $service->serviceCount += $record->serviceCount;
        $service->totalElapsedTime += $record->totalElapsedTime;
        if ($service->maximumElapsedTime < $record->maximumElapsedTime)
            $service->maximumElapsedTime = $record->maximumElapsedTime;
        if ($service->minimumElapsedTime > $record->minimumElapsedTime)
            $service->minimumElapsedTime = $record->minimumElapsedTime;
    }

    // Calculate the average elapsed time for each service
    foreach ($services as $service)
        $service->averageElapsedTime = $service->totalElapsedTime / $service->serviceCount;

    // Sort the services by name
    ksort($services);

    return $services;
}

// dbGetQueueProcessingStatistics()
//   Returns an array of queue processing statistics for a given date range
//
function dbGetQueueProcessingStatistics($logId, $db, $startDate, $endDate)
{
    // Get the performance records
    $sql = "SELECT totalMessagesProcessed,
                   sqsCreateClientElapsedTime,
                   sqsReceiveMessageElapsedTime,
                   sqsDeleteMessageElapsedTime,
                   ldrGetTestAssignmentIdElapsedTime,
                   ldrGetTestAssignmentElapsedTime,
                   ldrUpdateTestStatusElapsedTime,
                   ldrStoreTestResultsElapsedTime,
                   adpUpdateTestStatusElapsedTime,
                   halLoggingElapsedTime,
                   submittedMessagesProcessed,
                   submittedMessagesIgnored,
                   submittedMessagesInvalid,
                   submittedMessagesFailed,
                   submittedElapsedTime,
                   startedMessagesProcessed,
                   startedMessagesIgnored,
                   startedMessagesInvalid,
                   startedMessagesFailed,
                   startedElapsedTime,
                   pausedMessagesProcessed,
                   pausedMessagesIgnored,
                   pausedMessagesInvalid,
                   pausedMessagesFailed,
                   pausedElapsedTime,
                   resumedMessagesProcessed,
                   resumedMessagesIgnored,
                   resumedMessagesInvalid,
                   resumedMessagesFailed,
                   resumedElapsedTime,
                   unknownStatusChangesProcessed,
                   unknownMessagesProcessed,
                   unknownElapsedTime
            FROM queue_processing WHERE TRUE";
    if (strlen($startDate) > 0)
        $sql .= " AND statisticsDate >= '$startDate'";
    if (strlen($endDate) > 0)
        $sql .= " AND statisticsDate <= '$endDate'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Aggregate the queue processing data
    $queueProcessing = new QueueProcessing();
    foreach ($records as $record)
    {
        $queueProcessing->totalMessagesProcessed += $record->totalMessagesProcessed;
        $queueProcessing->sqsCreateClientElapsedTime += $record->sqsCreateClientElapsedTime;
        $queueProcessing->sqsReceiveMessageElapsedTime += $record->sqsReceiveMessageElapsedTime;
        $queueProcessing->sqsDeleteMessageElapsedTime += $record->sqsDeleteMessageElapsedTime;
        $queueProcessing->ldrGetTestAssignmentIdElapsedTime += $record->ldrGetTestAssignmentIdElapsedTime;
        $queueProcessing->ldrGetTestAssignmentElapsedTime += $record->ldrGetTestAssignmentElapsedTime;
        $queueProcessing->ldrUpdateTestStatusElapsedTime += $record->ldrUpdateTestStatusElapsedTime;
        $queueProcessing->ldrStoreTestResultsElapsedTime += $record->ldrStoreTestResultsElapsedTime;
        $queueProcessing->adpUpdateTestStatusElapsedTime += $record->adpUpdateTestStatusElapsedTime;
        $queueProcessing->halLoggingElapsedTime += $record->halLoggingElapsedTime;
        $queueProcessing->submittedMessagesProcessed += $record->submittedMessagesProcessed;
        $queueProcessing->submittedMessagesIgnored += $record->submittedMessagesIgnored;
        $queueProcessing->submittedMessagesInvalid += $record->submittedMessagesInvalid;
        $queueProcessing->submittedMessagesFailed += $record->submittedMessagesFailed;
        $queueProcessing->submittedElapsedTime += $record->submittedElapsedTime;
        $queueProcessing->startedMessagesProcessed += $record->startedMessagesProcessed;
        $queueProcessing->startedMessagesIgnored += $record->startedMessagesIgnored;
        $queueProcessing->startedMessagesInvalid += $record->startedMessagesInvalid;
        $queueProcessing->startedMessagesFailed += $record->startedMessagesFailed;
        $queueProcessing->startedElapsedTime += $record->startedElapsedTime;
        $queueProcessing->pausedMessagesProcessed += $record->pausedMessagesProcessed;
        $queueProcessing->pausedMessagesIgnored += $record->pausedMessagesIgnored;
        $queueProcessing->pausedMessagesInvalid += $record->pausedMessagesInvalid;
        $queueProcessing->pausedMessagesFailed += $record->pausedMessagesFailed;
        $queueProcessing->pausedElapsedTime += $record->pausedElapsedTime;
        $queueProcessing->resumedMessagesProcessed += $record->resumedMessagesProcessed;
        $queueProcessing->resumedMessagesIgnored += $record->resumedMessagesIgnored;
        $queueProcessing->resumedMessagesInvalid += $record->resumedMessagesInvalid;
        $queueProcessing->resumedMessagesFailed += $record->resumedMessagesFailed;
        $queueProcessing->resumedElapsedTime += $record->resumedElapsedTime;
        $queueProcessing->unknownStatusChangesProcessed += $record->unknownStatusChangesProcessed;
        $queueProcessing->unknownMessagesProcessed += $record->unknownMessagesProcessed;
        $queueProcessing->unknownElapsedTime += $record->unknownElapsedTime;
    }

    return $queueProcessing;
}

// dbGetSessionDatetime_By_Token()
//   Returns a session datetime if found, null otherwise
//
function dbGetSessionDatetime_By_Token($logId, $db, $token)
{
    // Get the session token timestamp
    $sql = "SELECT dateTime FROM session_token WHERE token = '$token'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null; // Token was not found
}

// dbGetTableNames()
//   Returns an array of the LDR table names
//
function dbGetTableNames($logId, $db)
{
    // Get the table names
    $sql = "SHOW TABLES";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_NUM);
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Extract the table names
    $tables = [];
    foreach ($rows as $row)
        $tables[] = $row[0];

    return $tables;
}

// dbGetReferenceIdValue()
//   Returns the value for a reference table reference ID if it exists, null otherwise
//
function dbGetReferenceIdValue($logId, $db, $tableName, $referenceId)
{
    global $referenceIdColumns, $referenceValueColumns;

    // Check the tableName
    if (!array_key_exists($tableName, $referenceIdColumns))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_028, "LDR internal server error - tableName " .
            $tableName . " is undefined");
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if the value exists in the table
    $sql = "SELECT " . $referenceValueColumns[$tableName] . " FROM $tableName WHERE " . $referenceIdColumns[$tableName] .
        " = '$referenceId' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null;
}

// dbGetReferenceValueId()
//   Returns the reference ID for a reference table value if it exists, null otherwise
//
function dbGetReferenceValueId($logId, $db, $tableName, $referenceValue)
{
    global $referenceIdColumns, $referenceValueColumns;

    // Check the tableName
    if (!array_key_exists($tableName, $referenceValueColumns))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_001, 'LDR internal server error - tableName ' .
                                 $tableName . ' is undefined');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if the value exists in the table
    $sql = "SELECT " . $referenceIdColumns[$tableName] . " FROM $tableName WHERE " . $referenceValueColumns[$tableName] .
           " = '$referenceValue' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null;
}

// dbInsertWithBindParams()
//  Insert into database using bind params
//
function dbInsertWithBindParams($logId, PDO $db, $table, $columnsAndValues)
{
    if (strlen($table) == 0) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_TABLE_NAME, LDR_ED_MISSING_TABLE_NAME);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    if (is_array($columnsAndValues) === false) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATA_IS_NOT_AN_ARRAY, LDR_ED_DATA_IS_NOT_AN_ARRAY);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    foreach ($columnsAndValues as $col => $val) {
        if (strlen($val) === 0) {
            unset($columnsAndValues[$col]);
        }
    }

    if (count($columnsAndValues) === 0) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATA_IS_EMPTY, LDR_ED_DATA_IS_EMPTY);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    $bindVals = array_fill(0, count($columnsAndValues), '?');

    $sql = "INSERT INTO ".$table."(`".implode("`,`", array_keys($columnsAndValues))."`)
                VALUES (".implode(',', $bindVals).");";

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute(array_values($columnsAndValues));

        return $db->lastInsertId();
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbMigrateDatabase()
//   Migrates an LDR database to the required version
//
function dbMigrateDatabase($logId, $db)
{
    // Get the database version
    $databaseVersion = dbGetDatabaseVersion($logId, $db);
    if ($databaseVersion instanceof ErrorLDR)
    {
        $databaseVersion->logError(__FUNCTION__);
        return $databaseVersion;
    }

    // Determine the migration type
    if ($databaseVersion < LDR_DATABASE_VERSION)
        $result = dbMigrateDatabaseForward($logId, $db, $databaseVersion);
    elseif ($databaseVersion > LDR_DATABASE_VERSION)
        $result = dbMigrateDatabaseBackward($logId, $db, $databaseVersion);
    else
        $result = SUCCESS;

    return $result;
}

// dbMigrateDatabaseBackward()
//   Migrates an LDR database backward to the required version
//
function dbMigrateDatabaseBackward($logId, $db, $databaseVersion)
{
    if (!is_file(DB_DIR."/migrate_backward.php")) {
        $errorLDR = new ErrorLDR(
                                $logId,
                                LDR_EC_CANNOT_FIND_DATABASE_MIGRATION_FILE,
                                LDR_ED_CANNOT_FIND_DATABASE_MIGRATION_FILE."migrate_backward.php"
                            );
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    require_once DB_DIR."/migrate_backward.php";

    // Disable foreign key checks for this session
    try {
        $stmt = $db->prepare('SET FOREIGN_KEY_CHECKS = 0');
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, 'Unable to disable foreign key checks');
    }

    // Check the backward migration versions
    foreach ($backwardMigrationVersions as $migrationVersion => $migrationCommands)
    {
        // Skip all backward migration versions down to and including the current database version
        if ($migrationVersion > $databaseVersion)
            continue;

        // Process subsequent backward migration versions down to but excluding the required database version
        if ($migrationVersion > LDR_DATABASE_VERSION)
        {
            foreach ($migrationCommands as $migrationCommand)
            {
                // Execute the migration command
                try {
                    $stmt = $db->prepare($migrationCommand);
                    $stmt->execute();
                }
                catch(PDOException $error) {
                    return errorQueryException($logId, $error, __FUNCTION__, $migrationCommand);
                }
            }
        }
        else
        {
            // If we are at the required database version then we are done
            break;
        }
    }

    // Update the database version
    $result = dbUpdateDatabaseVersion($logId, $db);
    if ($result instanceof ErrorLDR)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }

    return SUCCESS;
}

// dbMigrateDatabaseForward()
//   Migrates an LDR database forward to the required version
//
function dbMigrateDatabaseForward($logId, $db, $databaseVersion)
{
    if (!is_file(DB_DIR."/migrate_forward.php")) {
        $errorLDR = new ErrorLDR(
                                $logId,
                                LDR_EC_CANNOT_FIND_DATABASE_MIGRATION_FILE,
                                LDR_ED_CANNOT_FIND_DATABASE_MIGRATION_FILE."migrate_forward.php"
                            );
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    require_once DB_DIR."/migrate_forward.php";

    // Disable foreign key checks for this session
    try {
        $stmt = $db->prepare('SET FOREIGN_KEY_CHECKS = 0');
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, 'Unable to disable foreign key checks');
    }

    // Check the forward migration versions
    foreach ($forwardMigrationVersions as $migrationVersion => $migrationCommands)
    {
        // Skip all forward migration versions up to and including the current database version
        if ($migrationVersion <= $databaseVersion)
            continue;

        // Process subsequent forward migration versions up to and including the required database version
        if ($migrationVersion <= LDR_DATABASE_VERSION)
        {
            foreach ($migrationCommands as $migrationCommand)
            {
                // Execute the migration command
                try {
                    $stmt = $db->prepare($migrationCommand);
                    $stmt->execute();
                }
                catch(PDOException $error) {
                    return errorQueryException($logId, $error, __FUNCTION__, $migrationCommand);
                }
            }
        }
        else
        {
            // If we are above the required database version then we are done
            break;
        }
    }

    // Update the database version
    $result = dbUpdateDatabaseVersion($logId, $db);
    if ($result instanceof ErrorLDR)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }

    return SUCCESS;
}

// dbMultiInsertWithBindParams()
//  Insert multiple rows with bind parameters
//
function dbMultiInsertWithBindParams($logId, PDO $db, $table, $cols, $params)
{
    if (strlen($table) == 0) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_TABLE_NAME, LDR_ED_MISSING_TABLE_NAME);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    if (is_array($cols) === false || is_array($params) === false) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATA_IS_NOT_AN_ARRAY, LDR_ED_DATA_IS_NOT_AN_ARRAY);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    foreach ($params as $insertParams) {
        if (count($insertParams) != count($cols)) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_INSERT_DATA, LDR_ED_MISSING_INSERT_DATA);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }

    // Create the bind placeholders
    $bindVals = "(".implode(',', array_fill(0, count($cols), '?')).")";
    $bindVals = array_fill(0, count($params), $bindVals);

    $sql = "INSERT INTO ".$table."(`".implode("`,`", $cols)."`)
                VALUES ".implode(',', $bindVals).";";

    try {
        $stmt = $db->prepare($sql);
        $params = flattenArray($params);
        $stmt->execute($params);

        // Return number of affected rows
        return $stmt->rowCount();
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbPrepareForSQL()
//   Prepares a value for inclusion in an SQL query
//
function dbPrepareForSQL($value, $valueName)
{
    global $argMaxLength;

    $value = preg_replace("/'/", "''", $value);

    if ($valueName == 'classIdentifier')
        $value = preg_replace("/\\\\/", "\\\\\\\\", $value);

    if ($valueName != 'dataChanges') // See dbCreateUserAuditRecord() in user_audit.php
        $value = substr($value, 0, $argMaxLength[$valueName]);

    return $value;
}

// dbRecordPerformance()
//   Records the elapsed time performance for an LDR service call
//
function dbRecordPerformance($logId, $db, $statisticsDate, $serviceName, $elapsedTime)
{
    // Check if a performance record exists
    $performanceData = dbGetPerformanceRecord($logId, $db, $statisticsDate, $serviceName);
    if ($performanceData instanceof ErrorLDR)
    {
        $performanceData->logError(__FUNCTION__);
        return;
    }
    if (is_null($performanceData))
    {
        // Performance record does not exist so create it
        $result = dbAddPerformanceRecord($logId, $db, $statisticsDate, $serviceName, $elapsedTime);
        if ($result instanceof ErrorLDR)
        {
            $result->logError(__FUNCTION__);
            return;
        }
    }
    else
    {
        // Performance record exists so update it
        $performanceData['serviceCount']++;
        $performanceData['totalElapsedTime'] += $elapsedTime;
        if ($performanceData['maximumElapsedTime'] < $elapsedTime)
            $performanceData['maximumElapsedTime'] = $elapsedTime;
        if ($performanceData['minimumElapsedTime'] > $elapsedTime)
            $performanceData['minimumElapsedTime'] = $elapsedTime;

        $result = dbUpdatePerformanceRecord($logId, $db, $performanceData['performanceId'],
            $performanceData['serviceCount'], $performanceData['totalElapsedTime'],
            $performanceData['maximumElapsedTime'], $performanceData['minimumElapsedTime']);
        if ($result instanceof ErrorLDR)
        {
            $result->logError(__FUNCTION__);
            return;
        }
    }
}

// dbSelectDatabase()
//   Selects the specified database and returns a session token
//
function dbSelectDatabase($logId, $database)
{
    global $sessionClientId;

    // Get the database connection
    $db = dbConnect($logId);
    if ($db instanceof ErrorLDR)
    {
        $db->logError(__FUNCTION__);
        return $db;
    }

    // Create a session token
    $token = strtoupper(sha1(uniqid(rand(), true)));
    if ($token === false)
    {
        $db = null;
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_CREATION_FAILED, LDR_ED_TOKEN_CREATION_FAILED);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    $result = dbCreateSessionToken($logId, $db, $sessionClientId, $token);
    if ($result instanceof ErrorLDR)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }

    // Close the database connection
    $db = null;

    // Return the session token
    return $token;
}

// dbTableValueExists()
//   Returns true if a value exists in a database table column, false otherwise
//
function dbTableValueExists($logId, PDO $db, $tableName, $columnName, $value)
{
    $valueExists = false;

    $sql = "SELECT COUNT(*) FROM $tableName WHERE $columnName = '$value' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        if ($row[0] > 0)
            $valueExists = true;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return $valueExists;
}

// dbTouchSessionToken()
//   Makes a session_token dateTime value current
//
function dbTouchSessionToken($logId, $db, $token)
{
    // Make the session token timestamp current
    $sql = "UPDATE session_token SET dateTime = '" . date("Y-m-d H:i:s") . "' WHERE token = '$token'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbUpdateDatabaseVersion()
//   Creates a database_version record
//
function dbUpdateDatabaseVersion($logId, PDO $db)
{
    // Add the current database version
    $sql = "INSERT INTO version_history (version, dateTime)
            VALUES ('" . LDR_DATABASE_VERSION . "', '" . date("Y-m-d H:i:s") . "')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

//
// UNVETTED FUNCTIONS
//

// dbGetOrganizationId_By_StudentId()
//   Returns a student organization if found, null otherwise
//
function dbGetOrganizationId_By_StudentId($logId, $db, $studentId)
{
    // Prepare the query
    $sql = "SELECT organizationId FROM student WHERE studentId = '$studentId'";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return null; // Student record not found
}

// dbUnlockTables()
//   Unlocks any locked database tables
//
function dbUnlockTables($logId, PDO $db)
{
    $sql = "UNLOCK TABLES";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbUpdatePerformanceRecord()
//   Updates an LDR service performance record
//
function dbUpdatePerformanceRecord($logId, $db, $performanceId, $serviceCount, $totalElapsedTime, $maximumElapsedTime,
    $minimumElapsedTime)
{
    $sql = "UPDATE performance SET " .
        "serviceCount = '$serviceCount', " .
        "totalElapsedTime = '$totalElapsedTime', " .
        "maximumElapsedTime = '$maximumElapsedTime', " .
        "minimumElapsedTime = '$minimumElapsedTime' " .
        "WHERE performanceId = '$performanceId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbYieldSelect()
//  Return selected data one by one to handle bigger data
//
function dbYieldSelect($logId, PDO $db, $sql, &$err, $bindParams = null)
{
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute($bindParams);

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            yield $row;
        }
    } catch (PDOException $error) {
        $err = errorQueryException($logId, $error, __FUNCTION__, $sql);
    } finally {
        return null;
    }
}

// errorQueryException()
//   Logs a database query exception and returns an ErrorLDR object
//
function errorQueryException($logId, $error, $functionName, $sql)
{
    $errorLDR = new ErrorLDR($logId, LDR_EC_QUERY_EXCEPTION, $error->getMessage());
    $errorLDR->logError($functionName);
    $errorLDR->logMessage($functionName, 'SQL = |' . $sql . '|');
    return $errorLDR;
}

// checkColumnValues()
//   Checks column data for invalid data
//
//function checkColumnValues()
//{
//    global $dbTables;
//
//    // Log the service call
//    $startTime = microtime(true);
//    $logId = logRequest(__FUNCTION__);
//
//    // Get the database connection and parameters
//    $parameters = beginService($logId,
//        array
//        (
//            'tableName' => DCO_REQUIRED,
//            'columnName' => DCO_REQUIRED,
//            'dataType' => DCO_REQUIRED
//        ));
//    if ($parameters instanceof ErrorLDR)
//    {
//        $parameters->returnErrorJSON(__FUNCTION__);
//        return;
//    }
//    list($db, $tableName, $columnName, $dataType) = $parameters;
//
//    // Check that the table's primary key is defined
//    if (!array_key_exists($tableName, $dbTables))
//    {
//        $errorLDR = new ErrorLDR($logId, LDR_EC_TABLE_NAME_UNKNOWN_TABLE, LDR_ED_TABLE_NAME_UNKNOWN_TABLE . $tableName);
//        $errorLDR->returnErrorJSON(__FUNCTION__);
//        $db = null;
//        return;
//    }
//
//    // Get the records
//    $sql = "SELECT " . $dbTables[$tableName] . ", " . $columnName . " FROM " . $tableName;
//    try {
//        $stmt = $db->prepare($sql);
//        $stmt->execute();
//        $dataset = $stmt->fetchAll(PDO::FETCH_ASSOC);
//    }
//    catch(PDOException $error) {
//        errorQueryExceptionJSON($logId, $error, __FUNCTION__, $sql);
//        $db = null;
//        return;
//    }
//
//    // Check the records
//    $recordIds = [];
//    foreach ($dataset as $data)
//    {
//        if (hasInvalidCharacters($data[$columnName], $dataType))
//            $recordIds[] = $data[$primaryKey[$tableName]];
//    }
//
//    // Close the database connection
//    $db = null;
//
//    // Return the records that failed
//    echo json_encode($recordIds);
//
//    // Log the elapsed time
//    logElapsed($logId, microtime(true) - $startTime);
//}
