<?php

//
// LDR data validation functions
//
// checkReferenceValueArg() ................. 2015-05-14 implemented
//
// checktime() .............................. 2015-02-17 implemented
//
// datesInOrder() ........................... 2015-02-18 copied from NCSC-LDR
//
// validateArg() ............................ 2015-01-02 Added support for DCO_TOUPPER
//
// validateAuditActionType() ................ 2015-02-17 implemented
//
// validateAuditDataType() .................. 2015-02-17 implemented
//
// validateDate() ........................... 2015-02-17 generalized for any date value
//
// validateDateTime() ....................... 2015-02-17 implemented
//
// validateDisabilityType() ................. 2015-01-31 implemented
//
// validateGradeLevel() ..................... 2015-07-07 grade levels changed to match ADP test battery grade levels
//
// validateInstructionStatus() .............. 2015-08-18 implemented
//
// validateOrganizationType() ............... 2015-01-06 implemented
//
// validateStateCode() ...................... 2015-01-06 implemented
//
// validateTestStatus() ..................... 2015-10-19 implemented
//
// validateToken() .......................... 2014-10-03 VETTED, 2014-09-19 original
//

// TODO: CHANGE TO STATIC ARRAY INSIDE FUNCTION
$argFilterMap =
[
    'actionType' => DFT_NATURAL_NUMBER,
    'adpTestAssignmentId' => DFT_NATURAL_NUMBER,
    'adpTestResultsId' => DFT_NATURAL_NUMBER,
    'allOrNone' => DFT_YES_NO,
    'classAssignmentX' => DFT_ALPHANUM_MIXED_PUNCT_H,
    'classId' => DFT_NATURAL_NUMBER,
    'classIdentifier' => DFT_ALPHANUM_MIXED_PUNCT_G,
    'classIds' => DFT_NATURAL_NUMBER_LIST,
    'clientName' => DFT_ALPHANUM_MIXED_PUNCT_A,
    'clientPassword' => DFT_ALPHANUM_MIXED,
    'clientType' => DFT_NATURAL_NUMBER,
    'clientURL' => DFT_NONE,
    'clientUserId' => DFT_NATURAL_NUMBER,
    'dataType' => DFT_NATURAL_NUMBER,
    'dateOfBirth' => DFT_DATE,
    'disabilityType' => DFT_ALPHA_MIXED,
    'enrollOrgId' => DFT_NATURAL_NUMBER,
    'enableLineReader' => DFT_YES_NO,
    'enableTextToSpeech' => DFT_YES_NO,
    'endDate' => DFT_DATE,
    'endDateTime' => DFT_DATETIME,
    'embeddedScoreReportId' => DFT_NATURAL_NUMBER,
    'enrollSchoolIdentifier' => DFT_ALPHANUM_MIXED,
    'enrollStateCode' => DFT_ALPHA_MIXED,
    'eventDesc' => DFT_ALPHANUM_MIXED_PUNCT_E,
    'eventType' => DFT_NATURAL_NUMBER,
    'fake' => DFT_YES_NO,
    'firstName' => DFT_ALPHANUM_MIXED_PUNCT_F,
    'gender' => DFT_GENDER,
    'gradeLevel' => DFT_ALPHANUM_MIXED,
    'hasClasses' => DFT_YES_NO,
    'instructionStatus' => DFT_NATURAL_NUMBER,
    'isDryRun' => DFT_YES_NO,
    'itemId' => DFT_ITEMS,
    'lastName' => DFT_ALPHANUM_MIXED_PUNCT_F,
    'localIdentifier' => DFT_ALPHANUM_MIXED,
    'maxPoints' => DFT_NATURAL_NUMBER,
    'middleName' => DFT_ALPHANUM_MIXED_PUNCT_F,
    'modValue' => DFT_NATURAL_NUMBER,
    'optionalStateData' => DFT_YES_NO,
    'optionalStateDataX' => DFT_ALPHANUM_MIXED_PUNCT_F,
    'organizationId' => DFT_NATURAL_NUMBER,
    'organizationIdentifier' => DFT_ALPHANUM_MIXED,
    'organizationIds' => DFT_NATURAL_NUMBER_LIST,
    'organizationName' => DFT_ALPHANUM_MIXED_PUNCT_C,
    'organizationPath' => DFT_ALPHANUM_MIXED_PUNCT_D,
    'organizationType' => DFT_NATURAL_NUMBER,
    'pageLimit' => DFT_NATURAL_NUMBER,
    'pageOffset' => DFT_NATURAL_NUMBER,
    'parentOrganizationId' => DFT_NATURAL_NUMBER,
    'parentOrganizationIdentifier' => DFT_ALPHANUM_MIXED,
    'parentStateCode' => DFT_ALPHA_MIXED,
    'password' => DFT_ALPHANUM_MIXED,
    'pattern' => DFT_ALPHANUM_MIXED,
    'primaryId' => DFT_NATURAL_NUMBER,
    'qtiInteraction' => DFT_ALPHA_MIXED,
    'raceAA' => DFT_YES_NO,
    'raceAN' => DFT_YES_NO,
    'raceAS' => DFT_YES_NO,
    'raceHL' => DFT_YES_NO,
    'raceHP' => DFT_YES_NO,
    'raceWH' => DFT_YES_NO,
    'recurse' => DFT_ALPHANUM_MIXED,
    'redisCmd' => DFT_ALPHA_MIXED,
    'redisField' => DFT_REDIS_FIELD,
    'redisFields' => DFT_NONE,
    'redisKey' => DFT_REDIS_KEY,
    'redisKeys' => DFT_NONE,
    'redisValue' => DFT_REDIS_VALUE,
    'redisValues' => DFT_NONE,
    'cardinality' => DFT_ALPHA_MIXED,
    's3Bucket' => DFT_ALPHANUM_MIXED_PUNCT_B,
    's3Key' => DFT_AWS_S3_KEY,
    'scoringStatus' => DFT_ALPHANUM_MIXED_PUNCT_E,
    'sectionNumber' => DFT_ALPHANUM_MIXED_PUNCT_D,
    'severity' => DFT_NATURAL_NUMBER,
    'startDate' => DFT_DATE,
    'startDateTime' => DFT_DATETIME,
    'stateCode' => DFT_ALPHA_MIXED,
    'stateIdentifier' => DFT_ALPHANUM_MIXED_PUNCT_A,
    'statusDIS' => DFT_YES_NO,
    'statusECO' => DFT_YES_NO,
    'statusELL' => DFT_YES_NO,
    'statusGAT' => DFT_YES_NO,
    'statusLEP' => DFT_YES_NO,
    'statusMIG' => DFT_YES_NO,
    'studentGradeLevel' => DFT_ALPHANUM_MIXED,
    'studentRecordId' => DFT_NATURAL_NUMBER,
    'studentRecordIds' => DFT_NATURAL_NUMBER_LIST,
    'students' => DFT_NONE,
    'tableName' => DFT_ALPHA_MIXED_PUNCT_A,
    'taoId' => DFT_ITEMS,
    'testAssignmentId' => DFT_NATURAL_NUMBER,
    'testAssignmentStatus' => DFT_ALPHA_MIXED,
    'testBatteryDescription' => DFT_UNICODE,
    'testBatteryFormId' => DFT_NATURAL_NUMBER,
    'testBatteryFormName' => DFT_UNICODE,
    'testBatteryGrade' => DFT_ALPHANUM_MIXED_PUNCT_B,
    'testBatteryId' => DFT_NATURAL_NUMBER,
    'testBatteryName' => DFT_UNICODE,
    'testBatteryPermissions' => DFT_ALPHANUM_MIXED_PUNCT_B,
    'testBatteryProgram' => DFT_ALPHANUM_MIXED_PUNCT_C,
    'testBatteryScoring' => DFT_ALPHA_MIXED,
    'testBatterySecurity' => DFT_ALPHANUM_MIXED_PUNCT_B,
    'testBatterySubject' => DFT_ALPHANUM_MIXED_PUNCT_C,
    'testFormRevisionId' => DFT_NATURAL_NUMBER,
    'testFormRevisionIds' => DFT_NATURAL_NUMBER_LIST,
    'testKeysOnly' => DFT_YES_NO,
    'testingDistrictIdentifier' => DFT_ALPHANUM_MIXED,
    'testingSchoolIdentifier' => DFT_ALPHANUM_MIXED,
    'testingStateCode' => DFT_ALPHA_MIXED,
    'testMapContent' => DFT_NONE,
    'testStatus' => DFT_ALPHA_MIXED_PUNCT_B,
    'token' => DFT_HEXADECIMAL_UPPER,
    'updateNestedSetValues' => DFT_YES_NO, // TODO: OBSOLETE?
    'useBuffer' => DFT_YES_NO,
    'withClasses' => DFT_YES_NO
];

$argMaxLength =
[
    'classIdentifier' => MAXLEN_CLASS_IDENTIFIER,
    'clientPassword' => MAXLEN_CLIENT_PASSWORD,
    'clientType' => MAXLEN_CLIENT_TYPE,
    'disabilityType' => MAXLEN_DISABILITY_TYPE,
    'firstName' => MAXLEN_STUDENT_FIRST_NAME,
    'globalUniqueIdentifier' => LENGTH_GUID_RFC4122,
    'gradeLevel' => LENGTH_GRADE_LEVEL,
    'lastName' => MAXLEN_STUDENT_LAST_NAME,
    'localIdentifier' => MAXLEN_STUDENT_LOCAL_IDENTIFIER,
    'middleName' => MAXLEN_STUDENT_MIDDLE_NAME,
    'optionalStateData1' => MAXLEN_OPTIONAL_STATE_DATA,
    'optionalStateData2' => MAXLEN_OPTIONAL_STATE_DATA,
    'optionalStateData3' => MAXLEN_OPTIONAL_STATE_DATA,
    'optionalStateData4' => MAXLEN_OPTIONAL_STATE_DATA,
    'optionalStateData5' => MAXLEN_OPTIONAL_STATE_DATA,
    'optionalStateData6' => MAXLEN_OPTIONAL_STATE_DATA,
    'optionalStateData7' => MAXLEN_OPTIONAL_STATE_DATA,
    'optionalStateData8' => MAXLEN_OPTIONAL_STATE_DATA,
    'organizationIdentifier' => MAXLEN_ORGANIZATION_IDENTIFIER,
    'organizationName' => MAXLEN_ORGANIZATION_NAME,
    'scoringStatus' => MAXLEN_SCORING_STATUS,
    'sectionNumber' => MAXLEN_SECTION_NUMBER,
    'stateIdentifier' => MAXLEN_STUDENT_STATE_IDENTIFIER,
    'testBatteryDescription' => MAXLEN_TEST_BATTERY_DESCRIPTION,
    'testBatteryFormName' => MAXLEN_TEST_BATTERY_FORM_NAME,
    'testBatteryName' => MAXLEN_TEST_BATTERY_NAME
];

// checkReferenceValueArg()
//   Returns a reference value ID or null if the value is empty
//
function checkReferenceValueArg($logId, $db, $value, $valueName, $tableName, $isRequired)
{
    // Check if the value is required but missing
    if (strlen($value) == 0 && $isRequired)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED, "Required value $valueName not provided");
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    elseif (strlen($value) > 0)
    {
        $valueId = dbGetReferenceValueId($logId, $db, $tableName, $value);
        if ($valueId instanceof ErrorLDR)
        {
            $valueId->logError(__FUNCTION__);
            return $valueId;
        }
        elseif ($valueId === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_VALUE_IS_UNDEFINED,
                                     "$valueName value |" . $value . '| is undefined');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
    else
        $valueId = null;

    return $valueId;
}

// checktime()
//   Checks time values and returns true if the values are valid, false otherwise
//
function checktime($hour, $minute, $second)
{
    if ($hour < 0 || $hour > 23)
        return false;

    if ($minute < 0 || $minute > 59)
        return false;

    if ($second < 0 || $second > 59)
        return false;

    return true;
}

// datesInOrder()
//   Returns true if the second date is not before the first date, false otherwise
//
function datesInOrder($firstDate, $secondDate)
{
    // Convert the date strings to UNIX timestamps
    $firstTS = strtotime($firstDate);
    $secondTS = strtotime($secondDate);

    // If either of the conversions failed then return false
    if ($firstTS === false || $secondTS === false)
        return false;

    // Check that the second date is not before the first date
    if ($firstTS <= $secondTS)
        return true;
    else
        return false;
}

// hasInvalidCharacters()
//   Returns true if an argument has invalid characters, false otherwise
//
function hasInvalidCharacters($arg, $dft)
{
    $regex = "/[^" . $dft . "]/";

    $result = preg_match($regex, $arg);

    return $result;
}

// validateArg()
//   Returns a validated argument
//
function validateArg($logId, PDO $db, $args, $argName, $dft, $dco)
{
    global $argMaxLength, $columnTableNames, $valueColumnNames;

    // Get the argument
    $arg = null;
    if (array_key_exists($argName, $args))
        $arg = $args[$argName];

    if (SKIP_VALIDATION)
        return $arg;

    // Check if the argument is required
    if ($dco & DCO_REQUIRED)
    {
        $notProvided = false;
        switch ($argName)
        {
            case 'redisKeys':
            case 'redisValues':
            case 'studentRecordIds':
            case 'students':
                if (!is_array($arg))
                    $notProvided = true;
                break;
            default:
                if (strlen($arg) == 0)
                    $notProvided = true;
                break;
        }
        if ($notProvided)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED, 'Required value ' . $argName . ' not provided');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }

    // Check if further validation should be bypassed
    if ($dco & DCO_NO_CHECK)
        return $arg;

    // Check if the argument should be decoded
    if ($dco & DCO_URL_DECODE)
        $arg = rawurldecode($arg);

    // Check if the argument should have leading and trailing whitespace removed
    if ($dco & DCO_TRIM)
        $arg = trim($arg);

    // Check if the argument should be converted to lower case
    if ($dco & DCO_TOLOWER)
        $arg = strtolower($arg);

    // Check if the argument should be converted to upper case
    if ($dco & DCO_TOUPPER)
        $arg = strtoupper($arg); // Note: This converts null to an empty string

    // If the argument is non-empty
    if (strlen($arg) > 0)
    {
        // If a data filter type is defined then check if the argument contains any invalid characters
        if ($dft !== null && hasInvalidCharacters($arg, $dft))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_INVALID_CHARACTERS, 'This ' . $argName .
                                     ' has invalid characters: ' . $arg);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Check if the argument must exist in the database
        if ($dco & DCO_MUST_EXIST)
        {
            // Get the column name
            if (array_key_exists($argName, $valueColumnNames))
                $columnName = $valueColumnNames[$argName];
            else
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_009,
                                         'No column name is defined for this parameter: ' . $argName);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }

            // Get the table name
            if (array_key_exists($columnName, $columnTableNames))
                $tableName = $columnTableNames[$columnName];
            else
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_010,
                                         'No table name is defined for this parameter: ' . $argName);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }

            // Check that the argument exists
            $valueExists = dbTableValueExists($logId, $db, $tableName, $columnName, $arg);
            if ($valueExists instanceof ErrorLDR)
            {
                $valueExists->logError(__FUNCTION__);
                return $valueExists;
            }
            else if ($valueExists === false)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_DATABASE_RECORD_NOT_FOUND,
                                         LDR_ED_DATABASE_RECORD_NOT_FOUND . $argName . ' ' . $arg);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        }
    }

    // Check string arguments for maximum length
    switch ($dft)
    {
        case DFT_GENDER:
        case DFT_YES_NO:
            if (strlen($arg) > 1)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_LENGTH_TOO_LONG, 'The ' . $argName . ' value |' . $arg .
                    '| is longer than the maximum allowed length of 1 character');
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
            break;
        default:
            if (array_key_exists($argName, $argMaxLength))
            {
                if (strlen($arg) > $argMaxLength[$argName])
                {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_LENGTH_TOO_LONG, 'The ' . $argName . ' value |' . $arg .
                        '| is longer than the maximum allowed length of ' . $argMaxLength[$argName] . ' characters');
                    $errorLDR->logError(__FUNCTION__);
                    return $errorLDR;
                }
            }
            break;
    }

    return $arg;
}

// validateAuditActionType()
//   Returns a validated audit action type
//
function validateAuditActionType($logId, $actionType)
{
    if ($actionType < AUDIT_ACTION_CREATE || $actionType > AUDIT_ACTION_ALL_TYPES)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_AUDIT_ACTION_TYPE_UNKNOWN,
                                 'This audit actionType is unknown: ' . $actionType);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $actionType;
}

// validateAuditDataType()
//   Returns a validated audit data type
//
function validateAuditDataType($logId, $dataType)
{
    if ($dataType < AUDIT_DATA_ORGANIZATION || $dataType > AUDIT_DATA_ALL_TYPES)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_AUDIT_DATA_TYPE_UNKNOWN,
                                 'This audit dataType is unknown: ' . $dataType);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $dataType;
}

// validateDate()
//   Returns a validated date value
//
function validateDate($logId, $date, $argName)
{
    // Check the format
    if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $date))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATE_INVALID_FORMAT, 'The format of this ' .  $argName .
                                 ' is not YYYY-MM-DD: ' . $date);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check the date
    list($year, $month, $day) = explode('-', $date);
    if (!checkdate(intval($month), intval($day), intval($year)))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATE_INVALID_DATE,
                                 'This ' .  $argName . ' is not a valid date: ' . $date);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $date;
}

// validateDateTime()
//   Returns a validated dateTime value
//
function validateDateTime($logId, $dateTime, $argName)
{
    // Get the date and time values. The time value is optional.
    $checkTime = true;
    $args = explode(' ', $dateTime);
    if (sizeof($args) == 1)
    {
        $date = $args[0];
        $checkTime = false;
        $time = null;
    }
    elseif (sizeof($args) == 2)
    {
        $date = $args[0];
        $time = $args[1];
    }
    else
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATETIME_INVALID_FORMAT, 'The date/time format of this ' .  $argName .
            ' is not YYYY-MM-DD HH:MM:SS or YYYY-MM-DD: ' . $dateTime);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check the date format
    if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $date))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATE_INVALID_FORMAT, 'The date format of this ' .  $argName .
                                 ' is not YYYY-MM-DD: ' . $dateTime);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check the time format
    if ($checkTime && !preg_match("/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/", $time))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TIME_INVALID_FORMAT, 'The time format of this ' .  $argName .
                                 ' is not HH:MM:SS: ' . $dateTime);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check the date
    list($year, $month, $day) = explode('-', $date);
    if (!checkdate(intval($month), intval($day), intval($year)))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DATE_INVALID_DATE,
                                 'This ' .  $argName . ' is not a valid date: ' . $dateTime);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check the time
    if ($checkTime)
    {
        list($hour, $minute, $second) = explode(':', $time);
        if (!checktime(intval($hour), intval($minute), intval($second)))
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TIME_INVALID_TIME,
                                     'This ' .  $argName . ' time is not a valid time: ' . $dateTime);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }

    return $dateTime;
}

// validateDisabilityType()
//   Returns a validated student disability type
//
function validateDisabilityType($logId, $disabilityType)
{
    $disabilityTypes = ['AUT', 'DB', 'DD', 'EMN', 'HI', 'ID', 'MD', 'OI', 'OHI', 'SLD', 'SLI', 'TBI', 'VI'];

    $disabilityType = strtoupper($disabilityType);

    if (!in_array($disabilityType, $disabilityTypes))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_DISABILITY_TYPE_UNKNOWN, 'This disabilityType is unknown: ' .
                                 $disabilityType);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $disabilityType;
}

// validateGradeLevel()
//   Returns a validated student grade level
//
function validateGradeLevel($logId, $gradeLevel)
{
    $gradeLevels = ['K', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];

    $gradeLevel = strtoupper($gradeLevel);

    if (!in_array($gradeLevel, $gradeLevels))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_GRADE_LEVEL_UNKNOWN, 'This gradeLevel is unknown: ' . $gradeLevel);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $gradeLevel;
}

// validateInstructionStatus()
//   Returns a validated test assignment instuction status
//
function validateInstructionStatus($logId, $instructionStatus)
{
    if ($instructionStatus > INSTRUCTION_STATUS_POST)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_INSTRUCTION_STATUS_UNKNOWN,
                                 'This test assignment instruction status is unknown: ' . $instructionStatus);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $instructionStatus;
}

// validateOrganizationType()
//   Returns a validated organization type
//
function validateOrganizationType($logId, $organizationType)
{
    if ($organizationType < ORG_TYPE_ROOT || $organizationType > ORG_TYPE_MAX)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_TYPE_UNKNOWN, 'Organization type is unknown');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $organizationType;
}

// validateStateCode()
//   Returns a validated state code
//
function validateStateCode($logId, $stateCode)
{
    // State code XX is used for testing purposes
    $stateCodes = ['AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'DD', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA',
        'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC',
        'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY', 'XX'];

    $stateCode = strtoupper($stateCode);

    if (!in_array($stateCode, $stateCodes))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_STATE_CODE_UNKNOWN, "State code $stateCode is unknown");
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $stateCode;
}

// validateTestStatus()
//  Returns a validated test status ID
//
function validateTestStatus($logId, PDO $db, $testStatus)
{
    $testStatus = ucfirst(strtolower($testStatus));
    $testStatusId = dbGetReferenceValueId($logId, $db, 'test_status', $testStatus);
    if ($testStatusId instanceof ErrorLDR)
    {
        $testStatusId->logError(__FUNCTION__);
        return $testStatusId;
    }
    if ($testStatusId === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_STATUS_UNKNOWN, LDR_ED_TEST_STATUS_UNKNOWN . $testStatus);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $testStatus;
}

// validateToken()
//   Returns a validated token
//
function validateToken($logId, $db, $token)
{
    global $sessionClientId;

    // Check if a token was passed
    if (strlen($token) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_NOT_PROVIDED, 'Session token not provided');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check the token length
    if (strlen($token) != LENGTH_SESSION_TOKEN)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_LENGTH_INVALID, LDR_ED_TOKEN_LENGTH_INVALID);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Filter the token
    if (hasInvalidCharacters($token, DFT_HEXADECIMAL_UPPER))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_INVALID_CHARACTERS, LDR_ED_TOKEN_INVALID_CHARACTERS . $token);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if the token exists
    $dateTime = dbGetSessionDatetime_By_Token($logId, $db, $token);
    if ($dateTime instanceof ErrorLDR)
    {
        $dateTime->logError(__FUNCTION__);
        return $dateTime;
    }
    if ($dateTime == null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_NOT_FOUND, LDR_ED_TOKEN_NOT_FOUND);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Check if the token has expired
    if (time() - strtotime($dateTime) > TOKEN_TIMEOUT)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TOKEN_HAS_EXPIRED, LDR_ED_TOKEN_HAS_EXPIRED);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Set the global $sessionClientId
    $clientId = dbGetClientId_By_Token($logId, $db, $token);
    if ($clientId instanceof ErrorLDR)
    {
        $clientId->logError(__FUNCTION__);
        return $clientId;
    }
    $sessionClientId = $clientId;

    // Token is valid so make it current
    $touch = dbTouchSessionToken($logId, $db, $token);
    if ($touch instanceof ErrorLDR)
    {
        $touch->logError(__FUNCTION__);
        return $touch;
    }

    // Return the validated token
    return $token;
}
