<?php
//
// LDR class services
//
// createClass()        create_class.php
// deleteClass()        delete_class.php
// getClass()           get_class.php
// getClasses()         get_classes.php
// updateClass()        update_class.php
//
// LDR organization support functions
//
// dbCreateClass()
// dbDeleteClass()
// dbGetClass()
// dbGetClassCount()
// dbGetClassesForBulkAssignment()
// dbGetClassId_By_ClassIdentifier()
// dbGetClassId_By_SectionNumber()
// dbGetClasses()
// dbUpdateClass()

// createClass()
//   Creates a class record
//
function createClass()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'classIdentifier' => DCO_REQUIRED,
            'gradeLevel' => DCO_REQUIRED | DCO_TOUPPER,
            'sectionNumber' => DCO_OPTIONAL,
            'stateCode' => DCO_OPTIONAL | DCO_TOUPPER,
            'organizationIdentifier' => DCO_OPTIONAL,
            'organizationId' => DCO_OPTIONAL | DCO_MUST_EXIST,
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $classIdentifier, $gradeLevel, $sectionNumber, $stateCode, $organizationIdentifier,
         $organizationId) = $args;

    // Check that the state code is valid
    if (strlen($stateCode) > 0)
    {
        $stateCode = validateStateCode($logId, $stateCode);
        if ($stateCode instanceof ErrorLDR)
        {
            $stateCode->returnErrorJSON(__FUNCTION__);
            return;
        }
    }

    // Get the organization ID
    // TODO: CONSIDER PLACING THIS CODE BLOCK IN AN organization SUPPORT FUNCTION
    if (strlen($organizationId) == 0)
    {
        if (strlen($stateCode) == 0 || strlen($organizationIdentifier) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED, 'One or more organization values not provided');
            $errorLDR->returnErrorJSON(__FUNCTION__);
            return;
        }
        $stateOrganizationId = dbGetStateOrganizationId($logId, $db, $stateCode);
        if ($stateOrganizationId instanceof ErrorLDR)
        {
            $stateOrganizationId->returnErrorJSON(__FUNCTION__);
            return;
        }
        $organizationId = dbGetOrgId_ByStateOrgId_ByIdentifier($logId, $db, $stateOrganizationId,
                                                               $organizationIdentifier);
        if ($organizationId instanceof ErrorLDR)
        {
            $organizationId->returnErrorJSON(__FUNCTION__);
            return;
        }
    }

    // Check that the organization is ORG_TYPE_SCHOOL
    $organizationType = dbGetOrganizationType($logId, $db, $organizationId);
    if ($organizationType instanceof ErrorLDR)
    {
        $organizationType->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    if ($organizationType != ORG_TYPE_SCHOOL)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_CANNOT_HAVE_CLASSES,
                                 'The organization with organizationId ' . $organizationId . ' cannot have classes');
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // If there's a section number argument then check that a class with the same section number does not already exist
    if (strlen($sectionNumber) > 0)
    {
        $classId = dbGetClassId_By_SectionNumber($logId, $db, $organizationId, $sectionNumber);
        if ($classId instanceof ErrorLDR)
        {
            $classId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        if ($classId > 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_SECTION_DUP, 'A class for organizationId ' .
                                     $organizationId . ' with section number ' . $sectionNumber . ' already exists');
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    else
    {
        // Otherwise check that a class with the same identifier does not already exist
        $classId = dbGetClassId_By_ClassIdentifier($logId, $db, $organizationId, $classIdentifier);
        if ($classId instanceof ErrorLDR)
        {
            $classId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        if ($classId > 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_IDENTIFIER_DUP, 'A class for organizationId ' .
                $organizationId . ' with class identifier ' . $classIdentifier . ' already exists');
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the gradeLevel
    $gradeLevel = validateGradeLevel($logId, $gradeLevel);
    if ($gradeLevel instanceof ErrorLDR)
    {
        $gradeLevel->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create the class
    $classId = dbCreateClass($logId, $db, $clientUserId, $organizationId, $classIdentifier, $sectionNumber,
                             $gradeLevel);
    if ($classId instanceof ErrorLDR)
    {
        $classId->logError(__FUNCTION__);
        $classId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the classId
    $response = ['classId' => $classId];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteClass()
//   Deletes a class record
//
function deleteClass($classId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId) = $args;

    // Validate the classId
    $argName = 'classId';
    $organizationId = validateArg($logId, $db, [$argName => $classId], $argName, $argFilterMap[$argName],
        DCO_REQUIRED | DCO_MUST_EXIST);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Delete the class
    $result = dbDeleteClass($logId, $db, $clientUserId, $classId);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    $response = ['result' => $result];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getClass()
//   Returns a class
//
function getClass($classId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the classId
    $argName = 'classId';
    $name = validateArg($logId, $db, [$argName => $classId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the class
    $class = dbGetClass($logId, $db, $classId);
    if ($class instanceof ErrorLDR)
    {
        $class->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the class
    finishService($db, $logId, $class, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getClasses()
//   Returns a list of classes
//
function getClasses($organizationId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_OPTIONAL, __FUNCTION__,
        array
        (
            'pageOffset' => DCO_OPTIONAL,
            'pageLimit' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $pageOffset, $pageLimit) = $args;

    // Validate the organizationId
    $argName = 'organizationId';
    $organizationId = validateArg($logId, $db, [$argName => $organizationId], $argName, $argFilterMap[$argName],
        DCO_REQUIRED | DCO_MUST_EXIST);
    if ($organizationId instanceof ErrorLDR)
    {
        $organizationId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the classes
    $classes = dbGetClasses($logId, $db, $organizationId, $pageOffset, $pageLimit);
    if ($classes instanceof ErrorLDR)
    {
        $classes->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the total count of classes
    $totalCount = dbGetClassCount($logId, $db, $organizationId);
    if ($totalCount instanceof ErrorLDR)
    {
        $totalCount->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the classes with the total count
    $result = ['totalCount' => $totalCount, 'classes' => $classes];
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// updateClass()
//   Updates a class
//
function updateClass($classId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'classIdentifier' => DCO_OPTIONAL,
            'sectionNumber' => DCO_OPTIONAL,
            'gradeLevel' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $classIdentifier, $sectionNumber, $gradeLevel) = $args;

    // Validate the classId
    $argName = 'classId';
    $arg = validateArg($logId, $db, [$argName => $classId], $argName, $argFilterMap[$argName],
                       DCO_REQUIRED | DCO_MUST_EXIST);
    if ($arg instanceof ErrorLDR)
    {
        $arg->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Validate the gradeLevel
    if (strlen($gradeLevel) > 0)
    {
        $gradeLevel = validateGradeLevel($logId, $gradeLevel);
        if ($gradeLevel instanceof ErrorLDR)
        {
            $gradeLevel->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Update the class
    $result = dbUpdateClass($logId, $db, $clientUserId, $classId, $classIdentifier, $sectionNumber, $gradeLevel);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    $response = ['result' => $result];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

//
// LDR class support functions
//
// dbCreateClass()
//   Creates a class
//
function dbCreateClass($logId, PDO $db, $clientUserId, $organizationId, $classIdentifier, $sectionNumber, $gradeLevel)
{
    // Turn off autocommit
    $db->beginTransaction();

    // Create the class
    $dbClassIdentifier = dbPrepareForSQL($classIdentifier, 'classIdentifier');
    $dbSectionNumber = dbPrepareForSQL($sectionNumber, 'sectionNumber');
    $sql = "INSERT INTO class (organizationId, classIdentifier, sectionNumber, gradeLevel)
            VALUES ('$organizationId', '$dbClassIdentifier', '$dbSectionNumber', '$gradeLevel')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $classId = $db->lastInsertId();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create a user audit record
    $dataChanges = json_encode(['organizationId' => $organizationId,
                                'classIdentifier' => $classIdentifier,
                                'sectionNumber' => $sectionNumber,
                                'gradeLevel' => $gradeLevel]);
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_CLASS, $classId,
                                     AUDIT_ACTION_CREATE, $dataChanges);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return $classId;
}

// dbDeleteClass()
//   Deletes a class
//
function dbDeleteClass($logId, PDO $db, $clientUserId, $classId)
{
    // Check if this class has students
    $studentCount = dbGetClassStudentCount($logId, $db, $classId);
    if ($studentCount instanceof ErrorLDR)
    {
        $studentCount->logError(__FUNCTION__);
        return $studentCount;
    }
    if ($studentCount > 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_HAS_STUDENTS,
                                 'This class has students and cannot be deleted - classId: ' . $classId);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Turn off autocommit
    $db->beginTransaction();

    // Delete the class record
    $sql = "UPDATE class SET deleted = 'Y' WHERE classId = '" . $classId . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create a user audit record
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_CLASS, $classId, AUDIT_ACTION_DELETE);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return SUCCESS;
}

// dbGetClass()
//   Returns a class
//
function dbGetClass($logId, PDO $db, $classId)
{
    $sqlClassId = '';
    if (is_array($classId)) {
        $classId = array_unique($classId);
        $sqlClassId = implode(',', $classId);
    } else {
        $sqlClassId = $classId;
    }

    // Get the class data
    $sql = "SELECT classId, organizationId, classIdentifier, sectionNumber, gradeLevel
            FROM class
            WHERE classId IN ($sqlClassId)
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1 && !is_array($classId)) {
            $class = $stmt->fetch(PDO::FETCH_ASSOC);
            return $class;
        } else {
            $classes = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
            return $classes;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The class was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_ID_NOT_FOUND, LDR_ED_CLASS_ID_NOT_FOUND . $classId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetClassesForBulkAssignment()
//  Returns the classId and it's corresponding grade level for classes that have students but missing test assignments
//
function dbGetClassesForBulkAssignment($logId, PDO $db, $modValue = null)
{
    $sql = "SELECT DISTINCT expected.classId, expected.gradeLevel
            FROM(
                SELECT c.classId, c.gradeLevel, sc.studentRecordId, tb.testBatteryId
                FROM class c, student_class sc, test_battery tb, test_battery_grade tbg
                WHERE c.classId = sc.classId AND tb.testBatteryGradeId = tbg.testBatteryGradeId AND c.gradeLevel = tbg.testBatteryGrade
                    AND c.deleted = 'N' AND sc.deleted = 'N' AND tb.deleted = 'N') AS expected
            LEFT JOIN test_assignment ta ON ta.testBatteryId = expected.testBatteryId AND ta.studentRecordId = expected.studentRecordId AND ta.deleted = 'N'
            WHERE ta.testBatteryId IS NULL";
    if (isset($modValue)) {
        $sql .= " AND MOD(expected.classId, 10) = ".$modValue;
    }

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $classes = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);

        return $classes;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetClassId_By_ClassIdentifier()
//   Returns a classId or zero if the class is not found
//
function dbGetClassId_By_ClassIdentifier($logId, PDO $db, $organizationId, $classIdentifier)
{
    $dbClassIdentifier = dbPrepareForSQL($classIdentifier, 'classIdentifier');
    $sql = "SELECT classId FROM class
            WHERE organizationId = '$organizationId'
            AND classIdentifier = '$dbClassIdentifier'
            AND sectionNumber = ''
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The class was not found
    return 0;
}

// dbGetClassId_By_SectionNumber()
//   Returns a classId or zero if the class is not found
//
function dbGetClassId_By_SectionNumber($logId, PDO $db, $organizationId, $sectionNumber)
{
    $dbSectionNumber = dbPrepareForSQL($sectionNumber, 'sectionNumber');
    $sql = "SELECT classId FROM class
            WHERE organizationId = '$organizationId'
            AND sectionNumber = '$dbSectionNumber'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The class was not found
    return 0;
}

/**
 * [dbGetClassId_ByOrgId_BySectionNumber description]
 * @param  [type] $logId              [description]
 * @param  [type] $db                 [description]
 * @param  [type] $orgIdSectionNumber [description]
 * @return [type]                     [description]
 */
function dbGetClassId_ByOrgId_BySectionNumber($logId, $db, $orgIdSectionNumber)
{
    $orgIdSectionNumber = "'".implode("', '", $orgIdSectionNumber)."'";
    $sql = "SELECT
                CONCAT_WS('".FIELD_SEPARATOR."', organizationId, sectionNumber) AS 'orgId_ClassIdentifier', classId
            FROM class
            WHERE
                CONCAT_WS('".FIELD_SEPARATOR."', organizationId, sectionNumber) IN (".$orgIdSectionNumber.")
                AND deleted = 'N';";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        return $data;
    } catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

/**
 * [dbGetClassId_ByOrgId_ByClassIdentifier description]
 * @param  [type] $logId                [description]
 * @param  [type] $db                   [description]
 * @param  [type] $orgIdClassIdentifier [description]
 * @return [type]                       [description]
 */
function dbGetClassId_ByOrgId_ByClassIdentifier($logId, $db, $orgIdClassIdentifier)
{
    $orgIdClassIdentifier = "'".implode("', '", $orgIdClassIdentifier)."'";
    $sql = "SELECT
                CONCAT_WS('".FIELD_SEPARATOR."', organizationId, classIdentifier) AS 'orgId_ClassIdentifier', classId
            FROM class
            WHERE
                CONCAT_WS('".FIELD_SEPARATOR."', organizationId, classIdentifier) IN (".$orgIdClassIdentifier.")
                AND deleted = 'N';";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
        return $data;
    } catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetClassCount()
//   Returns the count of classes for an organization
//
function dbGetClassCount($logId, PDO $db, $organizationId)
{
    // Prepare the query organization selection clause based on the organization type
    $orgClause = buildStudentOrgQueryClause($logId, $db, $organizationId, 'organizationId');
    if ($orgClause === false)
        return 0;

    $sql = "SELECT COUNT(*) FROM class WHERE deleted = 'N'";
    $sql .= $orgClause;

    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetClasses()
//   Returns the classes for an organization
//
function dbGetClasses($logId, PDO $db, $organizationId, $offset, $limit)
{
    // Prepare the query organization selection clause based on the organization type
    $orgClause = buildStudentOrgQueryClause($logId, $db, $organizationId, 'organizationId');
    if ($orgClause === false)
        return [];

    // Create the base query
    $sql = "SELECT classId, organizationId, classIdentifier, sectionNumber, gradeLevel
            FROM class
            WHERE deleted = 'N'";
    $sql .= $orgClause;
    $sql .= " ORDER BY organizationId, classIdentifier, sectionNumber";

    // Check if the output is limited to a page of data
    if (strlen($offset) > 0 && strlen($limit) > 0)
        $sql .= " LIMIT " . $offset . ', ' . $limit;

    // Get the classes
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $classes = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $classes;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbUpdateClass()
//   Updates a class
//
function dbUpdateClass($logId, PDO $db, $clientUserId, $classId, $classIdentifier, $sectionNumber, $gradeLevel)
{
    // Get the class
    $class = dbGetClass($logId, $db, $classId);
    if ($class instanceof ErrorLDR)
    {
        $class->logError(__FUNCTION__);
        return $class;
    }

    // Determine what and if changes are being made
    $dataChanges = [];
    $sql = "UPDATE class SET ";
    if (strlen($classIdentifier) > 0 && $classIdentifier != $class['classIdentifier'])
    {
        // If there is no sectionNumber value then check that a class with the same identifier does not already exist
        if (strlen($sectionNumber) == 0 && strlen($class['sectionNumber']) == 0)
        {
            $chkClassId = dbGetClassId_By_ClassIdentifier($logId, $db, $class['organizationId'], $classIdentifier);
            if ($chkClassId instanceof ErrorLDR)
            {
                $chkClassId->logError(__FUNCTION__);
                return $chkClassId;
            }
            if ($chkClassId > 0)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_IDENTIFIER_DUP, 'A class for organizationId ' .
                    $class['organizationId'] . ' with class identifier ' . $classIdentifier . ' already exists');
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        }

        $dataChanges['classIdentifier'] = $classIdentifier;
        $dbClassIdentifier = dbPrepareForSQL($classIdentifier, 'classIdentifier');
        $sql .= "classIdentifier = '$dbClassIdentifier', ";
    }
    if (strlen($sectionNumber) > 0 && $sectionNumber != $class['sectionNumber'])
    {
        // Check that a class with the same section number does not already exist
        $chkClassId = dbGetClassId_By_SectionNumber($logId, $db, $class['organizationId'], $sectionNumber);
        if ($chkClassId instanceof ErrorLDR)
        {
            $chkClassId->logError(__FUNCTION__);
            return $chkClassId;
        }
        if ($chkClassId > 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_CLASS_SECTION_DUP, 'A class for organizationId ' .
                $class['organizationId'] . ' with section number ' . $sectionNumber . ' already exists');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        $dataChanges['sectionNumber'] = $sectionNumber;
        $dbSectionNumber = dbPrepareForSQL($sectionNumber, 'sectionNumber');
        $sql .= "sectionNumber = '$dbSectionNumber', ";
    }
    if (strlen($gradeLevel) > 0 && $gradeLevel != $class['gradeLevel'])
    {
        $dataChanges['gradeLevel'] = $gradeLevel;
        $sql .= "gradeLevel = '$gradeLevel', ";
    }

    if (sizeof($dataChanges) < 1)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_UPDATE_HAS_NO_CHANGES, LDR_ED_UPDATE_HAS_NO_CHANGES);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    $sql = substr($sql, 0, -2); // Strip off the trailing ", "
    $sql .= " WHERE classId = '" . $classId . "'";

    // Turn off autocommit
    $db->beginTransaction();

    // Update the class record
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $db->rollBack();
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Create a user audit record
    $dataChanges = json_encode($dataChanges);
    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_CLASS, $classId,
                                     AUDIT_ACTION_UPDATE, $dataChanges);
    if ($audit instanceof ErrorLDR)
    {
        $db->rollBack();
        $audit->logError(__FUNCTION__);
        return $audit;
    }

    // Commit the changes and turn autocommit back on
    $db->commit();

    return SUCCESS;
}
