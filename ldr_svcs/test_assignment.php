<?php
//
// LDR test assignment services
//
// autoCreateTestAssignment()
// createBulkTestAssignments()
// createNewTestAssignment()
// createTestAssignment()                   create_test_assignment.php
// createTestAssignments()                  create_test_assignments.php
// createTestAssignmentsBatched()
// exportTestAssignments()
// getAssignedTests()                       get_assigned_tests.php
// getClassAssignedTests()                  ...
// getTestAssignments()                     get_test_assignments.php
// randomizeTestAssignmentStatuses()
// unlockTestAssignment()                   unlock_test_assignment.php
// updateTestAssignment()                   update_test_assignement.php
//
// LDR test assignment support functions
//
// dbCreateTestAssignment()
// dbCreateTestAssignments()
// dbDeleteTestAssignments()
// dbDataExportTestAssignments()
// dbExportTestAssignments()
// dbGetAssignedTests()
// dbGetClassAssignedTests()
// dbGetInactiveTestAssignmentFormIds()
// dbGetTestAssignment_ById()
// dbGetTestAssignmentId_ByAdpId()
// dbGetTestAssignments()
// dbRandomizeTestAssignmentStatuses()
// dbTestAssignmentIsActive()
// dbTestAssignmentsAreActive()
// dbUpdateLastQueueEventTime()
// dbUpdateTestAssignment()
// dbUpdateTestAssignmentResults()
// generateTestKey()
//

//
// LDR test assignment services
//
function autoCreateTestAssignment($logId, $db, $clientUserId, $studentRecordId, $classId = null, $gradeLevel = null)
{
    if (AUTO_TEST_ASSIGNMENT) {
        $testBatteries = [];
        $result = new stdClass();
        $result->testAssignmentIds = [];
        $result->errors = [];

        $testBatteries = dbGetTestBatteries($logId, $db, null, $gradeLevel, null, null, null, null);
        $result->expectedCount = count($testBatteries);

        foreach ($testBatteries as $testBattery) {
            $testBatteryId = $testBattery['testBatteryId'];
            $newTestAssignmentId = createNewTestAssignment($logId, $db, $clientUserId, $studentRecordId, $testBatteryId, null, null, null, true);
            if ($newTestAssignmentId instanceof ErrorLDR) {
                if ($newTestAssignmentId->getErrorCode() == LDR_EC_STUDENT_HAS_ACTIVE_TEST_ASSIGNMENT) {
                    // if the test assignment already exists, this is not an error, it may have been created when the
                    // student was added to another class with the same grade level
                    $result->expectedCount--;
                } else {
                    $result->errors[$testBatteryId] = $newTestAssignmentId->getErrorArray();
                }
            } else {
                $result->testAssignmentIds[$testBatteryId] = $newTestAssignmentId;
            }
        }

        return $result;
    }
    return true;
}

// createBulkTestAssignments()
//     Create bulk test assignments for the whole system
//
function createBulkTestAssignments()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
                        [
                            'clientUserId' => DCO_REQUIRED,
                            'modValue' => DCO_OPTIONAL
                        ]);
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $modValue) = $args;

    $result = new stdClass();
    $result->testsAssigned = 0;
    $result->classCount = 0;
    $result->testBatteriesCount = 0;
    $result->errors = [];

    // Get classIds and their grade level for classes that have students but are missing test assignments
    $classes = dbGetClassesForBulkAssignment($logId, $db, $modValue);
    if ($classes instanceof ErrorLDR) {
        $classes->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $result->classCount = count($classes);

    // Get all test batteries
    $testBatteries = dbGetTestBatteries($logId, $db, null, null, null, null, null, null);
    if ($testBatteries instanceof ErrorLDR) {
        $testBatteries->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $result->testBatteriesCount = count($testBatteries);
    $testBatteryIdGradeLevel = array_column($testBatteries, 'testBatteryGrade', 'testBatteryId');

    set_time_limit(BULK_ASSIGNMENT_TIMEOUT);

    foreach ($classes as $classId => $gradeLevel) {
        $testBatteryForGradeLevel = array_keys($testBatteryIdGradeLevel, $gradeLevel);
        if (count($testBatteryForGradeLevel) === 0) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_NO_TEST_BATTERY_FOR_GRADE, LDR_ED_NO_TEST_BATTERY_FOR_GRADE.$gradeLevel);
            $result->errors[$classId] = $errorLDR->getErrorArray();
            continue;
        }

        foreach ($testBatteryForGradeLevel as $testBatteryId) {
            // USE THE BATCH TEST ASSIGNMENTS INSTEAD OF DOING IT ONE BY ONE
            $countOfNewTestAssignments = createNewTestAssignments($logId, $db, 1, $classId, $testBatteryId, 0, 'N');
            if ($countOfNewTestAssignments instanceof ErrorLDR) {
                if ($countOfNewTestAssignments->getErrorCode() != LDR_EC_STUDENT_HAS_ACTIVE_TEST_ASSIGNMENT) {
                    $result->errors[$classId][$testBatteryId] = $countOfNewTestAssignments->getErrorArray();
                }
                continue;
            }

            $result->testsAssigned += $countOfNewTestAssignments;
        }
    }

    // Return the testAssignmentIds
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createNewTestAssignment()
//   Creates a test assignment
//
function createNewTestAssignment($logId, $db, $clientUserId, $studentRecordId, $testBatteryId, $instructionStatus
                                    , $enableLineReader = null, $enableTextToSpeech = null, $partOfTransaction = false)
{
    // Validate the instructionStatus
    if (strlen($instructionStatus) > 0)
    {
        $instructionStatus = validateInstructionStatus($logId, $instructionStatus);
        if ($instructionStatus instanceof ErrorLDR) {
            return $instructionStatus;
        }
    }

    // Check if the student already has an active test assignment for this test battery
    $isActive = dbTestAssignmentIsActive($logId, $db, $studentRecordId, $testBatteryId);
    if ($isActive instanceof ErrorLDR) {
        return $isActive;
    }

    if ($isActive) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_HAS_ACTIVE_TEST_ASSIGNMENT, 'Student record ' .
            $studentRecordId . ' already has an active test assignment for test battery ' . $testBatteryId);
        return $errorLDR;
    }

    // Get the test battery forms
    $testBatteryForms = dbGetTestBatteryForms($logId, $db, $testBatteryId);
    if ($testBatteryForms instanceof ErrorLDR) {
        return $testBatteryForms;
    }

    // Create an array of test battery form IDs
    $testBatteryFormIds = [];
    foreach ($testBatteryForms as $testBatteryForm)
        $testBatteryFormIds[] = $testBatteryForm['testBatteryFormId'];

    // Check if this student has already been assigned this test before but not yet assigned all forms
    $testAssignments = dbGetTestAssignments($logId, $db, null, $studentRecordId, null, null, null, null, null,
                                            $testBatteryId, null, null);
    if ($testAssignments instanceof ErrorLDR) {
        return $testAssignments;
    }
    if (sizeof($testAssignments) > 0 && sizeof($testAssignments) < sizeof($testBatteryForms))
    {
        // Create an array of test battery form IDs for the tests that have been taken
        $takenTestBatteryFormIds = [];
        foreach ($testAssignments as $testAssignment)
            $takenTestBatteryFormIds[] = $testAssignment['testBatteryFormId'];

        // Remove the forms that this student has already been assigned
        $testBatteryFormIds = array_diff($testBatteryFormIds, $takenTestBatteryFormIds);
        $testBatteryFormIds = array_values($testBatteryFormIds);
    }

    // Randomly select a test battery form to assign
    $idx = mt_rand(0, sizeof($testBatteryFormIds) - 1);
    $testBatteryFormId = $testBatteryFormIds[$idx];

    // Create the test assignment
    $testAssignmentId = dbCreateTestAssignment($logId, $db, $clientUserId, $studentRecordId, $testBatteryId,
                                               $testBatteryFormId, $instructionStatus, $enableLineReader,
                                               $enableTextToSpeech, $partOfTransaction);
    if ($testAssignmentId instanceof ErrorLDR) {
        return $testAssignmentId;
    }

    return $testAssignmentId;
}

// createNewTestAssignments()
//
//
function createNewTestAssignments($logId, PDO $db, $clientUserId, $classId, $testBatteryId, $instructionStatus, $allOrNone)
{
    // Validate the instructionStatus
    $instructionStatus = validateInstructionStatus($logId, $instructionStatus);
    if ($instructionStatus instanceof ErrorLDR) {
        $instructionStatus->logError(__FUNCTION__);
        return $instructionStatus;
    }

    // Get the student record IDs for the students in the class
    $studentRecordIds = dbGetClassStudentRecordIds($logId, $db, [$classId]);
    if ($studentRecordIds instanceof ErrorLDR) {
        $studentRecordIds->logError(__FUNCTION__);
        return $studentRecordIds;
    }

    // Get the test battery forms
    $testBatteryForms = dbGetTestBatteryForms($logId, $db, $testBatteryId);
    if ($testBatteryForms instanceof ErrorLDR) {
        $testBatteryForms->logError(__FUNCTION__);
        return $testBatteryForms;
    }

    // Get studentRecordIds with active test assignments for the selected testBatteryId
    $studentsWithActiveTestAssignments = dbTestAssignmentsAreActive($logId, $db, $studentRecordIds, $testBatteryId);
    if ($studentsWithActiveTestAssignments instanceof ErrorLDR) {
        $studentsWithActiveTestAssignments->logError(__FUNCTION__);
        return $studentsWithActiveTestAssignments;
    }

    $keyStudentRecordIds = array_flip($studentRecordIds);
    foreach ($studentsWithActiveTestAssignments as $studentRecordId => $testAssignmentCount) {
        if (strlen($allOrNone) == 0 || $allOrNone != 'N') {
            $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_HAS_ACTIVE_TEST_ASSIGNMENT,
                                     'Student record ' . $studentRecordId .
                                     ' already has an active test assignment for test battery ' . $testBatteryId);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        } else {
            unset($studentRecordIds[$keyStudentRecordIds[$studentRecordId]]);
        }
    }

    $testsAssigned = 0;
    if (count($studentRecordIds) > 0) {
        $studentsWithInactiveTestAssignments = dbGetInactiveTestAssignmentFormIds($logId, $db, $studentRecordIds, $testBatteryId);
        if ($studentsWithInactiveTestAssignments instanceof ErrorLDR) {
            $studentsWithInactiveTestAssignments->logError(__FUNCTION__);
            return $studentsWithInactiveTestAssignments;
        }

        // Create the test assignments
        $testBatteryFormIds = array_column($testBatteryForms, 'testBatteryFormId');
        $studentRecordIdFormIds = [];
        foreach ($studentRecordIds as $studentRecordId) {
            $availableFormIds = $testBatteryFormIds;

            if (isset($studentsWithInactiveTestAssignments[$studentRecordId])) {
                $previouslyTakenFormIds = (array)explode(',', $studentsWithInactiveTestAssignments[$studentRecordId]);
                $remainingFormIds = array_diff($availableFormIds, $previouslyTakenFormIds);
                if (count($remainingFormIds) > 0) {
                    $availableFormIds = $remainingFormIds;
                }
            }

            $idx = 0;
            $testBatteryFormId = 0;
            $keyAvailableFormIds = array_keys($availableFormIds);
            if (count($availableFormIds) > 1) {
                // Randomly select a test battery form to assign
                $idx = mt_rand(0, sizeof($availableFormIds) - 1);
            }
            $testBatteryFormId = $availableFormIds[$keyAvailableFormIds[$idx]];

            $studentRecordIdFormIds[$studentRecordId] = $testBatteryFormId;
        }

        // Create the test assignment
        $testAssignmentIds = dbCreateTestAssignments($logId, $db, $clientUserId, $studentRecordIdFormIds, $testBatteryId, $instructionStatus);
        if ($testAssignmentIds instanceof ErrorLDR) {
            $testAssignmentIds->logError($logId, __FUNCTION__);
            return $testAssignmentIds;
        }

        $testsAssigned = count($testAssignmentIds);
    }

    return $testsAssigned;
}

// createTestAssignment()
//   Creates a test assignment and returns the testAssignmentId as JSON
//     OR
//   Returns a JSON LDR error if there is an error
//
function createTestAssignment()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'studentRecordId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'testBatteryId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'instructionStatus' => DCO_OPTIONAL,
            'enableLineReader' => DCO_OPTIONAL | DCO_TOUPPER,
            'enableTextToSpeech' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $studentRecordId, $testBatteryId, $instructionStatus, $enableLineReader,
         $enableTextToSpeech) = $args;

    $testAssignmentId = createNewTestAssignment($logId, $db, $clientUserId, $studentRecordId, $testBatteryId
                            , $instructionStatus, $enableLineReader, $enableTextToSpeech);
    if ($testAssignmentId instanceof ErrorLDR) {
        $testAssignmentId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the testAssignmentId
    finishService($db, $logId, ['testAssignmentId' => $testAssignmentId], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createTestAssignments()
//   Creates test assignments for students in a class and returns the test count as JSON
//     OR
//   Returns a JSON LDR error if there is an error
//
function createTestAssignments()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'classId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'testBatteryId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'instructionStatus' => DCO_OPTIONAL,
            'allOrNone' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $classId, $testBatteryId, $instructionStatus, $allOrNone) = $args;

    // Validate the instructionStatus
    $instructionStatus = validateInstructionStatus($logId, $instructionStatus);
    if ($instructionStatus instanceof ErrorLDR)
    {
        $instructionStatus->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the student record IDs for the students in the class
    $studentRecordIds = dbGetClassStudentRecordIds($logId, $db, [$classId]);
    if ($studentRecordIds instanceof ErrorLDR)
    {
        $studentRecordIds->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check if any student already has an active test assignment for this test battery
    $studentRecordCount = sizeof($studentRecordIds);
    for ($idx = 0; $idx < $studentRecordCount; $idx++)
    {
        $isActive = dbTestAssignmentIsActive($logId, $db, $studentRecordIds[$idx], $testBatteryId);
        if ($isActive instanceof ErrorLDR)
        {
            $isActive->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        if ($isActive)
        {
            // Check if the test assignment mode is 'All or None'. If it is, then the test cannot be assigned
            // to any student in the class when it is already assigned to one or more students in the class.
            if (strlen($allOrNone) == 0 || $allOrNone != 'N')
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_HAS_ACTIVE_TEST_ASSIGNMENT,
                                         'Student record ' . $studentRecordIds[$idx] .
                                         ' already has an active test assignment for test battery ' . $testBatteryId);
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }
            else
            {
                // This student already has an active test assignment so remove them from the list of students
                // in the class that will be assigned the test
                unset($studentRecordIds[$idx]);
            }
        }
    }

    // Get the test battery forms
    $testBatteryForms = dbGetTestBatteryForms($logId, $db, $testBatteryId);
    if ($testBatteryForms instanceof ErrorLDR)
    {
        $testBatteryForms->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create an array of test battery form IDs
    $testBatteryFormIds = [];
    foreach ($testBatteryForms as $testBatteryForm)
        $testBatteryFormIds[] = $testBatteryForm['testBatteryFormId'];
    $savedTestBatteryFormIds = $testBatteryFormIds;

    // Create the test assignments
    $testsAssigned = 0;
    foreach ($studentRecordIds as $studentRecordId)
    {
        // Check if this student has already been assigned this test before but not yet assigned all forms
        $testAssignments = dbGetTestAssignments($logId, $db, null, $studentRecordId, null, null, null, null, null,
                                                $testBatteryId, null, null);
        if ($testAssignments instanceof ErrorLDR)
        {
            $testAssignments->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        if (sizeof($testAssignments) > 0 && sizeof($testAssignments) < sizeof($testBatteryForms))
        {
            // Create an array of test battery form IDs for the tests that have been taken
            $takenTestBatteryFormIds = [];
            foreach ($testAssignments as $testAssignment)
                $takenTestBatteryFormIds[] = $testAssignment['testBatteryFormId'];

            // Remove the forms that this student has already been assigned
            $testBatteryFormIds = array_diff($testBatteryFormIds, $takenTestBatteryFormIds);
            $testBatteryFormIds = array_values($testBatteryFormIds);
        }

        // Randomly select a test battery form to assign
        $idx = mt_rand(0, sizeof($testBatteryFormIds) - 1);
        $testBatteryFormId = $testBatteryFormIds[$idx];

        // Create the test assignment
        $testAssignmentId = dbCreateTestAssignment($logId, $db, $clientUserId, $studentRecordId, $testBatteryId,
                                                   $testBatteryFormId, $instructionStatus, false);
        if ($testAssignmentId instanceof ErrorLDR)
        {
            $testAssignmentId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        $testsAssigned++;

        // Restore the full list of test battery form IDs
        $testBatteryFormIds = $savedTestBatteryFormIds;
    }

    // Return the count of tests assigned
    finishService($db, $logId, ['testsAssigned' => $testsAssigned], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createTestAssignmentsBatched()
//      Creates new test assignments for a class using class Id and Test battery Id
//
function createTestAssignmentsBatched()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'classId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'testBatteryId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'instructionStatus' => DCO_OPTIONAL,
            'allOrNone' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR) {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $classId, $testBatteryId, $instructionStatus, $allOrNone) = $args;

    // Crete the new test assignments
    $testsAssigned = createNewTestAssignments($logId, $db, $clientUserId, $classId, $testBatteryId, $instructionStatus, $allOrNone);
    if ($testsAssigned instanceof ErrorLDR) {
        $testsAssigned->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the count of tests assigned
    finishService($db, $logId, ['testsAssigned' => $testsAssigned], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// exportTestAssignments()
//  Data Export of Test Assignments and storing on S3 bucket
//
function exportTestAssignments($logId, PDO $db, $startDate, $endDate)
{
    ini_set('memory_limit', DATA_EXPORT_MAX_MEMORY);
    $testAssignmentsData = dbExportTestAssignments($logId, $db, $startDate, $endDate);
    if ($testAssignmentsData instanceof ErrorLDR) {
        return $testAssignmentsData;
    }

    if ($testAssignmentsData->rowsCount > 0) {
        $dateTimeInfo = $startDate."_".$endDate."_".date('YmdHis');
        $reportName = ROOT_ORGANIZATION_NAME."-Test_Assignments-".$dateTimeInfo;
        $s3Key = LDR_S3_RESULTS_EXPORT_ROOT."/".TENANT_ID."/".LDR_S3_RESULTS_EXPORT_PATH."/".$dateTimeInfo
                    ."/".$reportName.".tsv";
        $testAssignmentsCSV = generateCSVData($logId, $testAssignmentsData->report, null, true, null, "\t");
        if ($testAssignmentsCSV instanceof ErrorLDR) {
            return $testAssignmentsCSV;
        }

        $result = awsStore2S3($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key, $testAssignmentsCSV);
        if ($result instanceof ErrorLDR) {
            $result->logError(__FUNCTION__);
            return $result;
        }

        $reportUrl = awsGetPresignedRequestUrl($logId, LDR_S3_RESULTS_EXPORT_BUCKET, $s3Key);
        if ($reportUrl instanceof ErrorLDR) {
            $reportUrl->logError(__FUNCTION__);
            return $reportUrl;
        }

        return $reportUrl;
    } else {
        return SUCCESS;
    }
}

// getAssignedTests()
//   Returns statistics for assigned tests as JSON
//     OR
//   Returns a JSON LDR error if there is an error
//
function getAssignedTests()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'organizationId' => DCO_OPTIONAL | DCO_MUST_EXIST,
            'classIds' => DCO_OPTIONAL,
            'studentGradeLevel' => DCO_OPTIONAL | DCO_TOUPPER,
            'testBatterySubject' => DCO_OPTIONAL,
            'testBatteryGrade' => DCO_OPTIONAL,
            'testBatteryId' => DCO_OPTIONAL  | DCO_MUST_EXIST,
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $organizationId, $classIds, $studentGradeLevel, $testBatterySubject, $testBatteryGrade,
         $testBatteryId) = $args;

    // Validate the studentGradeLevel
    if (strlen($studentGradeLevel) > 0)
    {
        $studentGradeLevel = validateGradeLevel($logId, $studentGradeLevel);
        if ($studentGradeLevel instanceof ErrorLDR)
        {
            $studentGradeLevel->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the assigned tests
    $assignedTests = dbGetAssignedTests($logId, $db, $organizationId, $classIds, $studentGradeLevel,
                                        $testBatterySubject, $testBatteryGrade, $testBatteryId);
    if ($assignedTests instanceof ErrorLDR)
    {
        $assignedTests->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the assigned tests
    finishService($db, $logId, $assignedTests, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getClassAssignedTests()
//   Returns statistics for the tests assigned to a class
//
function getClassAssignedTests($classId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the classId
    $argName = 'classId';
    $name = validateArg($logId, $db, [$argName => $classId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($name instanceof ErrorLDR)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test statistics for the class
    $classAssignedTests = dbGetClassAssignedTests($logId, $db, $classId);
    if ($classAssignedTests instanceof ErrorLDR)
    {
        $classAssignedTests->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the class
    finishService($db, $logId, $classAssignedTests, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// getTestAssignments()
//   Returns zero or more test assignments
//
function getTestAssignments()
{
    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'testAssignmentId' => DCO_OPTIONAL | DCO_MUST_EXIST,
            'studentRecordId' => DCO_OPTIONAL | DCO_MUST_EXIST,
            'organizationId' => DCO_OPTIONAL | DCO_MUST_EXIST,
            'classIds' => DCO_OPTIONAL,
            'studentGradeLevel' => DCO_OPTIONAL | DCO_TOUPPER,
            'testBatterySubject' => DCO_OPTIONAL,
            'testBatteryGrade' => DCO_OPTIONAL,
            'testBatteryId' => DCO_OPTIONAL  | DCO_MUST_EXIST,
            'testAssignmentStatus' => DCO_OPTIONAL | DCO_TOLOWER,
            'instructionStatus' => DCO_OPTIONAL,
            'testKeysOnly' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $testAssignmentId, $studentRecordId, $organizationId, $classIds, $studentGradeLevel, $testBatterySubject,
         $testBatteryGrade, $testBatteryId, $testAssignmentStatus, $instructionStatus, $testKeysOnly) = $args;

    // Validate the instructionStatus
    $instructionStatus = validateInstructionStatus($logId, $instructionStatus);
    if ($instructionStatus instanceof ErrorLDR)
    {
        $instructionStatus->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Validate the studentGradeLevel
    if (strlen($studentGradeLevel) > 0)
    {
        $studentGradeLevel = validateGradeLevel($logId, $studentGradeLevel);
        if ($studentGradeLevel instanceof ErrorLDR)
        {
            $studentGradeLevel->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the test assignments
    $testAssignmentStatus = ucfirst($testAssignmentStatus);
    $testAssignments = dbGetTestAssignments($logId, $db, $testAssignmentId, $studentRecordId, $organizationId,
                                            $classIds, $studentGradeLevel, $testBatterySubject, $testBatteryGrade,
                                            $testBatteryId, $testAssignmentStatus, $instructionStatus, $testKeysOnly);
    if ($testAssignments instanceof ErrorLDR)
    {
        $testAssignments->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the test assignments
    finishService($db, $logId, $testAssignments, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// randomizeTestAssignmentStatuses()
//   Randomly set the test status values of all test assignments
//
function randomizeTestAssignmentStatuses()
{
    global $sessionClientId;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest(__FUNCTION__);

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'pattern' => DCO_REQUIRED
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $pattern) = $args;

    // Check the session client ID
    if ($sessionClientId != CLIENT_ID_LDR_ROOT && $sessionClientId != CLIENT_ID_PARCC_QA &&
        $sessionClientId != CLIENT_ID_PARCC_CI)
    {
        $clientName = dbGetClientName_By_ClientId($logId, $db, $sessionClientId);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CLIENT_NOT_AUTHORIZED, LDR_ED_CLIENT_NOT_AUTHORIZED . $clientName);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Randomly set the test status values
    $result = dbRandomizeTestAssignmentStatuses($logId, $db, $pattern);
    if ($result instanceof ErrorLDR)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, $logId, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// unlockTestAssignment()
//   Unlocks a test assignment
//
function unlockTestAssignment($testAssignmentId)
{
    global $argFilterMap, $selectedDatabase;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the testAssignmentId
    $argName = 'testAssignmentId';
    $arg = validateArg($logId, $db, [$argName => $testAssignmentId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($arg instanceof ErrorLDR)
    {
        $arg->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test assignment
    $testAssignment = dbGetTestAssignment_ById($logId, $db, $testAssignmentId);
    if ($testAssignment instanceof ErrorLDR)
    {
        $testAssignment->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that the test can be unlocked
    if ($testAssignment['testStatusId'] != TEST_STATUS_ID_SCHEDULED &&
        $testAssignment['testStatusId'] != TEST_STATUS_ID_INPROGRESS &&
        $testAssignment['testStatusId'] != TEST_STATUS_ID_PAUSED)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_ASSIGNMENT_CANNOT_BE_UNLOCKED,
                                 'This test assignment has already been ' . $testAssignment['testStatus']);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Configure the unlock command
    if ($testAssignment['testStatusId'] == TEST_STATUS_ID_SCHEDULED)
        $adpTestStatusId = TEST_STATUS_ID_SCHEDULED;
    else
        $adpTestStatusId = TEST_STATUS_ID_PAUSED;
    $adpTestStatus = dbGetReferenceIdValue($logId, $db, 'test_status', $adpTestStatusId);
    if ($adpTestStatus instanceof ErrorLDR)
    {
        $adpTestStatus->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Unlock the test assignment in ADP
    if (SKIP_ADP_COMM == false)
    {
        $adpStudentData = ['studentId' => $testAssignment['studentRecordId']];
        $adpTestStatus = adpUnlockTestAssignment($logId, $testAssignment, $adpStudentData, $adpTestStatus);

        if ($adpTestStatus instanceof ErrorLDR)
        {
            $adpTestStatus->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // Check the ADP test assignment status
        $testStatusId = dbGetReferenceValueId($logId, $db, 'test_status', ucfirst(strtolower($adpTestStatus)));
        if ($testStatusId instanceof ErrorLDR)
        {
            $testStatusId->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        elseif ($testStatusId != TEST_STATUS_ID_PAUSED && $testStatusId != TEST_STATUS_ID_SCHEDULED)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ADP_STATUS_INVALID,
                                     'Invalid test status returned by ADP: ' . $adpTestStatus);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // Update the LDR test assignment status to match ADP
        $sql = "UPDATE test_assignment SET testStatusId = '$testStatusId' WHERE testAssignmentId = '$testAssignmentId'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException $error) {
            $errorLDR = errorQueryException($logId, $error, __FUNCTION__, $sql);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }
    // If we're skipping ADP and the test status is InProgress, then let's
    // pretend ADP changed the status to Paused.
    if (SKIP_ADP_COMM && $testAssignment['testStatusId'] == TEST_STATUS_ID_INPROGRESS)
    {
        $sql = 'UPDATE test_assignment SET testStatusId = :testStatusId where testAssignmentId = :testAssignmentId';
        try {
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':testStatusId', TEST_STATUS_ID_PAUSED, PDO::PARAM_INT);
            $stmt->bindValue(':testAssignmentId', $testAssignmentId, PDO::PARAM_INT);
            $stmt->execute();
        }
        catch(PDOException $error) {
            $errorLDR = errorQueryException($logId, $error, __FUNCTION__, $sql);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    finishService($db, $logId, ['result' => SUCCESS], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// updateTestAssignment()
//   Updates a test assignment
//
function updateTestAssignment($testAssignmentId)
{
    global $argFilterMap;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    // Initialize the service
    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'clientUserId' => DCO_REQUIRED | DCO_MUST_EXIST,
            'testAssignmentStatus' => DCO_OPTIONAL | DCO_TOLOWER,
            'instructionStatus' => DCO_OPTIONAL,
            'enableLineReader' => DCO_OPTIONAL | DCO_TOUPPER,
            'enableTextToSpeech' => DCO_OPTIONAL | DCO_TOUPPER
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientUserId, $testAssignmentStatus, $instructionStatus, $enableLineReader, $enableTextToSpeech) = $args;

    // Validate the testAssignmentId
    $argName = 'testAssignmentId';
    $arg = validateArg($logId, $db, [$argName => $testAssignmentId], $argName, $argFilterMap[$argName], DCO_REQUIRED);
    if ($arg instanceof ErrorLDR)
    {
        $arg->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the test assignment
    $testAssignment = dbGetTestAssignment_ById($logId, $db, $testAssignmentId);
    if ($testAssignment instanceof ErrorLDR)
    {
        $testAssignment->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Validate the testAssignmentStatus
    if (strlen($testAssignmentStatus) > 0)
    {
        // Check that the status is known
        $testAssignmentStatus = validateTestStatus($logId, $db, $testAssignmentStatus);
        if ($testAssignmentStatus instanceof ErrorLDR)
        {
            $testAssignmentStatus->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // Check that the test is being canceled
        $testStatusId = dbGetReferenceValueId($logId, $db, 'test_status', $testAssignmentStatus);
        if ($testStatusId != TEST_STATUS_ID_CANCELED)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_STATUS_CONTEXT_INVALID,
                "This testAssignmentStatus is not allowed for this service: " . $testAssignmentStatus);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }

        // Check that the test can be canceled
        if ($testAssignment['testStatusId'] != TEST_STATUS_ID_SCHEDULED &&
            $testAssignment['testStatusId'] != TEST_STATUS_ID_INPROGRESS &&
            $testAssignment['testStatusId'] != TEST_STATUS_ID_PAUSED)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_ASSIGNMENT_CANNOT_BE_CANCELED,
                                     'This test assignment has already been ' . $testAssignment['testStatus']);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Validate the instructionStatus
    if (strlen($instructionStatus) > 0)
    {
        $instructionStatus = validateInstructionStatus($logId, $instructionStatus);
        if ($instructionStatus instanceof ErrorLDR)
        {
            $instructionStatus->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Lock the test assignment and user_audit tables
    $sql = "LOCK TABLES test_assignment WRITE, user_audit WRITE";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        $result = errorQueryException($logId, $error, __FUNCTION__, $sql);
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Update the test assignment
    $db->beginTransaction();
    $result = dbUpdateTestAssignment($logId, $db, $clientUserId, $testAssignmentId, $testAssignment,
                                     $testAssignmentStatus, $instructionStatus, $enableLineReader, $enableTextToSpeech);
    if ($result instanceof ErrorLDR)
    {
        $db->rollBack();
        $unlock = dbUnlockTables($logId, $db);
        if ($unlock instanceof ErrorLDR)
            $unlock->logError(__FUNCTION__);
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $db->commit();

    // Unlock the tables
    $unlock = dbUnlockTables($logId, $db);
    if ($unlock instanceof ErrorLDR)
        $unlock->logError(__FUNCTION__);

    // Return the result
    $response = ['result' => $result];
    finishService($db, $logId, $response, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

//
// LDR test assignment support functions
//
// dbCreateTestAssignment()
//   Creates a test assignment
//
function dbCreateTestAssignment($logId, PDO $db, $clientUserId, $studentRecordId, $testBatteryId, $testBatteryFormId,
                                $instructionStatus, $enableLineReader = null, $enableTextToSpeech = null,
                                $partOfTransaction = false)
{
    global $selectedDatabase;

    // If accommodation fields are not set look for default values.
    if (is_null($enableLineReader)) {
        $enableLineReader = defined('TEST_ASSIGNMENT_LINE_READER_DEFAULT') ?
          TEST_ASSIGNMENT_LINE_READER_DEFAULT : 'N';
    }
    if (is_null($enableTextToSpeech)) {
        $enableTextToSpeech = defined('TEST_ASSIGNMENT_TTS_DEFAULT') ?
          TEST_ASSIGNMENT_TTS_DEFAULT : 'N';
    }

    // Sanity-check the optional values
    if (strlen($instructionStatus) == 0)
        $instructionStatus = 0;
    if (strlen($enableLineReader) == 0)
        $enableLineReader = 'N';
    if (strlen($enableTextToSpeech) == 0)
        $enableTextToSpeech = 'N';

    // Get the ADP-required student data
    $sql = "SELECT sr.studentRecordId, sr.stateIdentifier, s.firstName, s.lastName, s.dateOfBirth,
                   st.organizationName AS stateCode, o.organizationName, sr.gradeLevel
              FROM student_record sr
              JOIN student s on s.studentId = sr.studentId
              JOIN organization st on st.organizationId = sr.stateOrgId
              JOIN organization o on o.organizationId = sr.enrollOrgId
             WHERE sr.studentRecordId = '$studentRecordId'
               AND sr.deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $studentData = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        else
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_RECORD_ID_NOT_FOUND,
                                     LDR_ED_STUDENT_RECORD_ID_NOT_FOUND . $studentRecordId);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
    catch(PDOException $error) {
        $errorLDR = errorQueryException($logId, $error, __FUNCTION__, $sql);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Generate a test key
    $testKey = generateTestKey(LENGTH_TEST_KEY);

    // Lock the test_assignment and user_audit tables
    $sql = "LOCK TABLES test_assignment WRITE, user_audit WRITE";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        if (!$partOfTransaction) {
            $db->rollBack();
        }
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Turn off autocommit
    if (!$partOfTransaction) {
        $db->beginTransaction();
    }

    // Create the test assignment
    $sql = "INSERT INTO test_assignment
                (studentRecordId,
                 testBatteryFormId,
                 testBatteryId,
                 testStatusId,
                 testKey,
                 instructionStatus,
                 enableLineReader,
                 enableTextToSpeech)
            VALUES
               ('$studentRecordId',
                '$testBatteryFormId',
                '$testBatteryId',
                '" . TEST_STATUS_ID_SCHEDULED . "',
                '$testKey',
                '$instructionStatus',
                '$enableLineReader',
                '$enableTextToSpeech')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testAssignmentId = $db->lastInsertId();
    }
    catch(PDOException $error) {
        if (!$partOfTransaction) {
            $db->rollBack();
        }
        dbUnlockTables($logId, $db);
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // TODO: Create a user audit record

    // Perform ADP tasks
    if (SKIP_ADP_COMM == false)
    {
        // Create the test assignment in ADP
        $adpTestAssignmentId = adpCreateTestAssignment($logId, $testBatteryId, $testBatteryFormId, $testAssignmentId,
                                                       $testKey, $studentData, $enableLineReader, $enableTextToSpeech);
        if ($adpTestAssignmentId instanceof ErrorLDR)
        {
            if (!$partOfTransaction) {
                $db->rollBack();
            }
            dbUnlockTables($logId, $db);
            $adpTestAssignmentId->logError(__FUNCTION__);
            return $adpTestAssignmentId;
        }

        // Update the test assignment with the ADP test assignment ID
        $sql = "UPDATE test_assignment SET adpTestAssignmentId = '$adpTestAssignmentId'
                 WHERE testAssignmentId = '$testAssignmentId'";
        // Update the organization record
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException $error) {
            if (!$partOfTransaction) {
                $db->rollBack();
            }
            dbUnlockTables($logId, $db);
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    // Commit the changes and turn autocommit back on
    if (!$partOfTransaction) {
        $db->commit();
    }

    // Unlock the tables
    $result = dbUnlockTables($logId, $db);
    if ($result instanceof ErrorLDR)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }

    return $testAssignmentId;
}

// dbCreateTestAssignments()
//   Creates test assignments
//
function dbCreateTestAssignments($logId, PDO $db, $clientUserId, $studentRecordIdFormIds, $testBatteryId, $instructionStatus
                                    , $enableLineReader = null, $enableTextToSpeech = null)
{
    // If accommodation fields are not set look for default values.
    if (is_null($enableLineReader)) {
        $enableLineReader = defined('TEST_ASSIGNMENT_LINE_READER_DEFAULT') ?
          TEST_ASSIGNMENT_LINE_READER_DEFAULT : 'N';
    }
    if (is_null($enableTextToSpeech)) {
        $enableTextToSpeech = defined('TEST_ASSIGNMENT_TTS_DEFAULT') ?
          TEST_ASSIGNMENT_TTS_DEFAULT : 'N';
    }

    // Sanity-check the optional values
    if (strlen($instructionStatus) == 0)
        $instructionStatus = 0;
    if (strlen($enableLineReader) == 0)
        $enableLineReader = 'N';
    if (strlen($enableTextToSpeech) == 0)
        $enableTextToSpeech = 'N';

    $studentRecordIds = array_keys($studentRecordIdFormIds);

    // Get the ADP-required student data
    $sql = "SELECT sr.studentRecordId, sr.stateIdentifier, s.firstName, s.lastName, s.dateOfBirth,
                   st.organizationName AS stateCode, o.organizationName, sr.gradeLevel
              FROM student_record sr
              JOIN student s on s.studentId = sr.studentId
              JOIN organization st on st.organizationId = sr.stateOrgId
              JOIN organization o on o.organizationId = sr.enrollOrgId
             WHERE sr.studentRecordId IN ('".implode("','", $studentRecordIds)."')
               AND sr.deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if (count($studentRecordIds) != $stmt->rowCount()) {
            $errorLDR = new ErrorLDR($logId, LDR_EC_STUDENT_COUNT_MISMATCH
                                , "Students' data were requested for studentRecorIds: (".implode("','", $studentRecordIds).").".
                                "Only ".$stmt->rowCount()."/".count($studentRecordIds)." were retrieved");
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $studentsData = $stmt->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC);
    } catch(PDOException $error) {
        $errorLDR = errorQueryException($logId, $error, __FUNCTION__, $sql);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    $sqlTestAssignmentCols = [
        'studentRecordId'
        , 'testBatteryFormId'
        , 'testBatteryId'
        , 'testStatusId'
        , 'testKey'
        , 'instructionStatus'
        , 'enableLineReader'
        , 'enableTextToSpeech'
    ];
    $sqlTestAssignment = [];
    $testsKey = [];
    foreach ($studentRecordIdFormIds as $studentRecordId => $testBatteryFormId) {
        // Generate a test key
        $testsKey[$studentRecordId] = generateTestKey(LENGTH_TEST_KEY);

        $testAssignmentData = [
            $studentRecordId
            , $testBatteryFormId
            , $testBatteryId
            , TEST_STATUS_ID_SCHEDULED
            , $testsKey[$studentRecordId]
            , $instructionStatus
            , $enableLineReader
            , $enableTextToSpeech
        ];
        $sqlTestAssignment[] = "('".implode("','", $testAssignmentData)."')";
    }

    // Lock the test_assignment and user_audit tables
    $sql = "LOCK TABLES test_assignment WRITE, user_audit WRITE";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    } catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    $db->beginTransaction();

    // Create the test assignment
    $sql = "INSERT INTO test_assignment (`".implode('`,`', $sqlTestAssignmentCols)."`)
            VALUES ".implode(',', $sqlTestAssignment);
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        if (count($studentRecordIdFormIds) != $stmt->rowCount()) {
            dbUnlockTables($logId, $db);
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_ASSIGNMENT_IS_INCORRECT
                                        , "There are ".count($stmt->rowCount())." new test assignments, while ".
                                        count($studentRecordIdFormIds)." was expected");
            return $errorLDR;
        }
    } catch(PDOException $error) {
        $db->rollBack();
        dbUnlockTables($logId, $db);
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    $sql = "SELECT studentRecordId, testAssignmentId FROM test_assignment WHERE testKey IN ('".implode("','", $testsKey)."')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testAssignmentIds = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
    } catch(PDOException $error) {
        $db->rollBack();
        dbUnlockTables($logId, $db);
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // TODO: Create a user audit record
    //
    // Perform ADP tasks
    if (SKIP_ADP_COMM == false)
    {
        $sqlAdpTestAssignmentId = [];
        foreach ($studentsData as $studentRecordId => $studentData) {
            $studentData['studentRecordId'] = $studentRecordId;

            // Create the test assignment in ADP
            $adpTestAssignmentId = adpCreateTestAssignment($logId, $testBatteryId, $studentRecordIdFormIds[$studentRecordId]
                                                            , $testAssignmentIds[$studentRecordId]
                                                            , $testsKey[$studentRecordId]
                                                            , $studentData, $enableLineReader, $enableTextToSpeech);
            if ($adpTestAssignmentId instanceof ErrorLDR) {
                $db->rollBack();
                dbUnlockTables($logId, $db);
                $adpTestAssignmentId->logError(__FUNCTION__);
                return $adpTestAssignmentId;
            }

            $sqlAdpTestAssignmentId[] = "('".$testAssignmentIds[$studentRecordId]."', '".$adpTestAssignmentId."')";
        }

        // Update the test assignment with the ADP test assignment ID
        $sql = "INSERT INTO test_assignment (testAssignmentId, adpTestAssignmentId)
                VALUES ".implode(',', $sqlAdpTestAssignmentId).
                " ON DUPLICATE KEY UPDATE adpTestAssignmentId = VALUES(adpTestAssignmentId);";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();

            if ((count($studentRecordIdFormIds)*2) != $stmt->rowCount()) {
                $db->rollBack();
                dbUnlockTables($logId, $db);
                $errorLDR = new ErrorLDR($logId, LDR_EC_FAILED_TO_UPDATE_TEST_ASSIGNMENT
                                            , "Failed to update the adpTestAssignmentId for ".
                                            count($studentRecordIdFormIds)." test assignments");
                return $errorLDR;
            }
        } catch(PDOException $error) {
            $db->rollBack();
            dbUnlockTables($logId, $db);
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    $db->commit();

    // Unlock the tables
    $result = dbUnlockTables($logId, $db);
    if ($result instanceof ErrorLDR) {
        $result->logError(__FUNCTION__);
        return $result;
    }

    return $testAssignmentIds;
}

// dbGetClassAssignedTests()
//   Returns statistics for the tests assigned to a class
//
function dbGetClassAssignedTests($logId, $db, $classId)
{
    // Prepare the base query
    $sql = "SELECT ta.testBatteryId,
                   tb.testBatteryName,
                  tbs.testBatterySubject,
                  tbg.testBatteryGrade,
                   ta.instructionStatus,
                   ta.testStatusId,
                    s.lastName,
                    s.firstName
              FROM test_assignment ta
              JOIN test_battery tb on tb.testBatteryId = ta.testBatteryId
              JOIN test_battery_subject tbs on tbs.testBatterySubjectId = tb.testBatterySubjectId
              JOIN test_battery_grade tbg on tbg.testBatteryGradeId = tb.testBatteryGradeId
              JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
              JOIN student s on s.studentId = sr.studentId
             WHERE s.deleted = 'N'
               AND sr.deleted = 'N'
               AND ta.deleted = 'N'
               AND tbg.deleted = 'N'
               AND tbs.deleted = 'N'";

    // Get the student record IDs for the students in the class
    $studentRecordIds = dbGetClassStudentRecordIds($logId, $db, [$classId]);
    if ($studentRecordIds instanceof ErrorLDR)
    {
        $studentRecordIds->logError(__FUNCTION__);
        return $studentRecordIds;
    }
    if (sizeof($studentRecordIds) == 0)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_CLASSES_HAVE_NO_STUDENTS, 'The classId value ' . $classId .
                                 ' specifies a class that does not have any students');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    $studentRecordIds = implode(',', $studentRecordIds);
    $sql .= " AND sr.studentRecordId IN ($studentRecordIds)";

    // Sort by testBatterySubject, testBatteryName, instructionStatus
    $sql .= " ORDER BY tbs.testBatterySubject, tb.testBatteryName, ta.instructionStatus, s.lastName, s.firstName";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testAssignments = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Aggregate the test assignment data. Note that this algorithm relies on the test
    // assignments being sorted by testBatterySubject, testBatteryName, instructionStatus.
    $classAssignedTests = [];
    $classAssignedTest = new ClassAssignedTest(1);
    $testBatterySubject = null;
    $testBatteryName = null;
    $instructionStatus = null;
    $instructionStatusIdx = 0;
    foreach ($testAssignments as $testAssignment)
    {
        // Check if the test battery has changed
        if ($testAssignment['testBatteryName'] != $testBatteryName ||
            $testAssignment['testBatterySubject'] != $testBatterySubject)
        {
            $classAssignedTest = new ClassAssignedTest($testAssignment['testBatteryId']);
            $classAssignedTest->testBatterySubject = $testAssignment['testBatterySubject'];
            $classAssignedTest->testBatteryGrade = $testAssignment['testBatteryGrade'];
            $classAssignedTest->testBatteryName = $testAssignment['testBatteryName'];
            $classAssignedTest->instructionStatusList[] = new InstructionStatus($testAssignment['instructionStatus']);

            $instructionStatusIdx = 0;

            $testBatterySubject = $testAssignment['testBatterySubject'];
            $instructionStatus = $testAssignment['instructionStatus'];
            $testBatteryName = $testAssignment['testBatteryName'];

            $classAssignedTests[] = $classAssignedTest;
        }
        // Otherwise, check if the instruction status for this test battery has changed
        elseif ($testAssignment['instructionStatus'] != $instructionStatus)
        {
            $classAssignedTest->instructionStatusList[] = new InstructionStatus($testAssignment['instructionStatus']);

            $instructionStatusIdx++;

            $instructionStatus = $testAssignment['instructionStatus'];
        }

        // Add this test assignment to the aggregated data
        $student = ['lastName' => $testAssignment['lastName'], 'firstName' => $testAssignment['firstName']];
        switch ($testAssignment['testStatusId'])
        {
            case TEST_STATUS_ID_SCHEDULED:
                $classAssignedTest->instructionStatusList[$instructionStatusIdx]->statusScheduled['testCount']++;
                $classAssignedTest->instructionStatusList[$instructionStatusIdx]->statusScheduled['students'][] = $student;
                break;
            case TEST_STATUS_ID_SUBMITTED:
                $classAssignedTest->instructionStatusList[$instructionStatusIdx]->statusSubmitted['testCount']++;
                $classAssignedTest->instructionStatusList[$instructionStatusIdx]->statusSubmitted['students'][] = $student;
                break;
            case TEST_STATUS_ID_INPROGRESS:
            case TEST_STATUS_ID_PAUSED:
                $classAssignedTest->instructionStatusList[$instructionStatusIdx]->statusInProgress['testCount']++;
                $classAssignedTest->instructionStatusList[$instructionStatusIdx]->statusInProgress['students'][] = $student;
                break;
        }
    }

    return $classAssignedTests;
}

// dbDeleteTestAssignments()
//  Delete test assignments from LDR and ADP for a given list of testAssignmentids
//
function dbDeleteTestAssignments($logId, PDO $db, $testAssignments)
{
    $result = new stdClass();
    $result->expectedTestAssignmentsCount = count($testAssignments);
    $result->successCount = 0;
    $result->errors = [];
    $testAssignmentsToBeDeleted = [];
    foreach ($testAssignments as $testAssignment) {
        if (!SKIP_ADP_COMM) {
            $adpDeleteStatus = adpDeleteTestAssignment(
                                    $logId,
                                    $testAssignment['testAssignmentId'],
                                    $testAssignment['adpTestAssignmentId'],
                                    $testAssignment['testBatteryId'],
                                    $testAssignment['testBatteryFormId'],
                                    $testAssignment['testKey'],
                                    $testAssignment['studentRecordId']
            );

            if ($adpDeleteStatus instanceof ErrorLDR) {
                $result->errors[$testAssignment['testAssignmentId']] = $adpDeleteStatus->getErrorArray();
            } else {
                if ($adpDeleteStatus === SUCCESS) {
                    $testAssignmentsToBeDeleted[] = $testAssignment['testAssignmentId'];
                } else {
                    $errorLDR = new ErrorLDR($logId, LDR_EC_UNEXPECTED_ADP_RESPONSE, LDR_ED_UNEXPECTED_ADP_RESPONSE.$adpDeleteStatus);
                    $errorLDR->logError(__FUNCTION__);
                    $result->errors[$testAssignment['testAssignmentId']] = $errorLDR->getErrorArray();
                }
            }
        } else {
            $testAssignmentsToBeDeleted[] = $testAssignment['testAssignmentId'];
        }
    }

    if (count($testAssignmentsToBeDeleted) > 0) {
        $result->successCount = count($testAssignmentsToBeDeleted);

        $sql = "UPDATE test_assignment SET deleted = 'Y' WHERE testAssignmentId IN (".implode(',', $testAssignmentsToBeDeleted).");";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
            if (count($testAssignmentsToBeDeleted) != $stmt->rowCount()) {
                $errorLDR = new ErrorLDR($logId, LDR_EC_CANNOT_SOFT_DELETE_TEST_ASSIGNMENTS
                                            , LDR_ED_CANNOT_SOFT_DELETE_TEST_ASSIGNMENTS);
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        } catch (PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    return $result;
}

// dbExportTestAssignments()
//  Getting Data Export Data for the Test Assignments File
//
function dbExportTestAssignments($logId, PDO $db, $startDate, $endDate)
{
    $sql = "SELECT ta.testAssignmentId 'Student Test Assignment ID', ta.studentRecordId 'Student Record ID'
                    , ta.testFormRevisionId 'Form Revision ID', ts.testStatus 'Test Status'
                    , CONVERT_TZ(tr.testStartDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."') 'Start Timestamp'
                    , CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."') 'Submit Timestamp'
                    , tr.testDuration 'Test Duration', ta.instructionStatus 'Instruction Status'
                    , tr.testRawScore 'Test Raw Score'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN test_status ts ON ta.testStatusId = ts.testStatusId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
            WHERE ta.deleted = 'N' AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN '".$startDate."' AND '".$endDate."'
            ORDER BY tr.testSubmitDateTime;";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();

        $testAssignmentsData = new stdClass();
        $testAssignmentsData->report = null;
        $testAssignmentsData->rowsCount = 0;

        $rawTestAssignmentsData = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $testAssignmentsData->report = $rawTestAssignmentsData;
        $testAssignmentsData->rowsCount = $stmt->rowCount();

        return $testAssignmentsData;
    } catch (PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbDataExportTestAssignments()
//  Build the SQL query to export test assignments
//
function dbDataExportTestAssignments($testStatus)
{
    $sql = "SELECT ta.testAssignmentId 'Student Test Assignment ID', ta.studentRecordId 'Student Record ID'
                    , ta.testFormRevisionId 'Form Revision ID', ts.testStatus 'Test Status'
                    , CONVERT_TZ(tr.testStartDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."') 'Start Timestamp'
                    , CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."') 'Submit Timestamp'
                    , tr.testDuration 'Test Duration', ta.instructionStatus 'Instruction Status'
                    , tr.testRawScore 'Test Raw Score'
            FROM test_assignment ta JOIN test_results tr ON ta.testResultsId = tr.testResultsId
                JOIN test_status ts ON ta.testStatusId = ts.testStatusId
                JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
                JOIN organization fakeOrg ON fakeOrg.organizationId = sr.enrollOrgId AND fakeOrg.fake = 'N'
            WHERE ta.deleted = 'N'";

    if ($testStatus === TEST_STATUS_ID_SUBMITTED) {
        $sql .= " AND ta.testStatusId = ".TEST_STATUS_ID_SUBMITTED.
                " AND DATE(CONVERT_TZ(tr.testSubmitDateTime, 'UTC', '".DATA_EXPORT_TIME_ZONE."')) BETWEEN ? AND ?";
    } else {
        $sql .= " AND ta.testStatusId IN (".$testStatus.")";
    }
    $sql .= " ORDER BY tr.testSubmitDateTime;";
    return $sql;
}

// dbGetAssignedTests()
//   Returns statistics for assigned tests
//
function dbGetAssignedTests($logId, $db, $organizationId, $classIds, $studentGradeLevel, $testBatterySubject,
                            $testBatteryGrade, $testBatteryId)
{
    // Check the arguments
    $argCount = 0;
    if (strlen($organizationId) > 0)
        $argCount++;
    if (strlen($classIds) > 0)
        $argCount++;
    if ($argCount != 1)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_OR_MULTIPLE_ARGS, 'Missing or multiple parameters - ' .
                                 'please specify a classIds or organizationId parameter');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Prepare the base query
    $sql = "SELECT ta.testBatteryId,
                   ta.testStatusId
              FROM test_assignment ta
              JOIN test_battery tb on tb.testBatteryId = ta.testBatteryId
              JOIN student_record sr ON sr.studentRecordId = ta.studentRecordId
             WHERE sr.deleted = 'N'
               AND ta.deleted = 'N'";

    // Add the required constraint
    if (strlen($classIds) > 0)
    {
        // Check that the classes exist
        $classIds = explode(',', $classIds);
        foreach ($classIds as $classId)
        {
            $class = dbGetClass($logId, $db, $classId);
            if ($class instanceof ErrorLDR)
            {
                $class->logError(__FUNCTION__);
                return $class;
            }
        }

        // Get the student record IDs for the students in the class(es)
        $studentRecordIds = dbGetClassStudentRecordIds($logId, $db, $classIds);
        if ($studentRecordIds instanceof ErrorLDR)
        {
            $studentRecordIds->logError(__FUNCTION__);
            return $studentRecordIds;
        }
        if (sizeof($studentRecordIds) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_CLASSES_HAVE_NO_STUDENTS, 'The classIds value ' .
                implode(',', $classIds) . ' specifies classes that do not have any students');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $studentRecordIds = implode(',', $studentRecordIds);
        $sql .= " AND sr.studentRecordId IN ($studentRecordIds)";
    }
    else
    {
        // Get the student record IDs for the students in the organization
        $studentRecordIds = dbGetStudentRecordIds($logId, $db, $organizationId);
        if ($studentRecordIds instanceof ErrorLDR)
        {
            $studentRecordIds->logError(__FUNCTION__);
            return $studentRecordIds;
        }
        if (sizeof($studentRecordIds) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_HAS_NO_STUDENTS, 'The organizationId value ' .
                $organizationId . ' specifies an organization that does not have any students');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $studentRecordIds = implode(',', $studentRecordIds);
        $sql .= " AND sr.studentRecordId IN ($studentRecordIds)";
    }

    // Add optional constraints
    if (strlen($studentGradeLevel) > 0)
        $sql .= " AND sr.gradeLevel = '$studentGradeLevel'";
    if (strlen($testBatterySubject) > 0)
    {
        $testBatterySubjectId = dbGetReferenceValueId($logId, $db, 'test_battery_subject', $testBatterySubject);
        if ($testBatterySubjectId instanceof ErrorLDR)
        {
            $testBatterySubjectId->logError(__FUNCTION__);
            return $testBatterySubjectId;
        }
        if ($testBatterySubjectId === null)
            $testBatterySubjectId = 0;
        $sql .= " AND tb.testBatterySubjectId = '$testBatterySubjectId'";
    }
    if (strlen($testBatteryGrade) > 0)
    {
        $testBatteryGradeId = dbGetReferenceValueId($logId, $db, 'test_battery_grade', $testBatteryGrade);
        if ($testBatteryGradeId instanceof ErrorLDR)
        {
            $testBatteryGradeId->logError(__FUNCTION__);
            return $testBatteryGradeId;
        }
        if ($testBatteryGradeId === null)
            $testBatteryGradeId = 0;
        $sql .= " AND tb.testBatteryGradeId = '$testBatteryGradeId'";
    }
    if (strlen($testBatteryId) > 0)
        $sql .= " AND tb.testBatteryId = '$testBatteryId'";

    // Sort by testBatteryId
    $sql .= " ORDER BY ta.testBatteryId";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testAssignments = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Consolidate the test assignment data and aggregate the raw counts
    // Note: This algorithm relies on the test assignments being sorted by testBatteryId
    $assignedTests = [];
    $assignedTest = null;
    $testBatteryId = null;
    foreach ($testAssignments as $testAssignment)
    {
        if ($testAssignment['testBatteryId'] != $testBatteryId)
        {
            $testBatteryId = $testAssignment['testBatteryId'];
            $assignedTest = new AssignedTest($testBatteryId);
            $assignedTests[] = $assignedTest;
        }

        $assignedTest->countAssigned++;

        switch ($testAssignment['testStatusId'])
        {
            case TEST_STATUS_ID_SCHEDULED:
                $assignedTest->countScheduled++;
                break;
            case TEST_STATUS_ID_SUBMITTED:
                $assignedTest->countSubmitted++;
                break;
            case TEST_STATUS_ID_CANCELED:
                $assignedTest->countCanceled++;
                break;
            case TEST_STATUS_ID_INPROGRESS:
                $assignedTest->countInProgress++;
                break;
            case TEST_STATUS_ID_PAUSED:
                $assignedTest->countPaused++;
                break;
        }
    }

    // Get the remaining values for each assigned test
    for ($idx = 0; $idx < sizeof($assignedTests); $idx++)
    {
        $assignedTest = $assignedTests[$idx];

        // Get the test battery values
        $testBattery = dbGetTestBattery($logId, $db, $assignedTest->testBatteryId);
        if ($testBattery instanceof ErrorLDR)
        {
            $testBattery->logError(__FUNCTION__);
            return $testBattery;
        }
        $assignedTest->testBatteryName = $testBattery['testBatteryName'];
        $assignedTest->testBatterySubject = $testBattery['testBatterySubject'];
        $assignedTest->testBatteryGrade = $testBattery['testBatteryGrade'];

        // Calculate the percentage values
        if ($assignedTest->countAssigned > 0)
        {
            $assignedTest->percentScheduled =
                percentOneDecimalPlace($assignedTest->countScheduled, $assignedTest->countAssigned);
            $assignedTest->percentSubmitted =
                percentOneDecimalPlace($assignedTest->countSubmitted, $assignedTest->countAssigned);
            $assignedTest->percentCanceled =
                percentOneDecimalPlace($assignedTest->countCanceled, $assignedTest->countAssigned);
        }
        $assignedTest->percentSubmittedOrCanceled = $assignedTest->percentSubmitted + $assignedTest->percentCanceled;

        $assignedTests[$idx] = $assignedTest;
    }

    // Calculate the totals for all assigned tests
    $assignedTestTotals = new AssignedTest('');
    foreach ($assignedTests as $assignedTest)
    {
        $assignedTestTotals->countAssigned += $assignedTest->countAssigned;
        $assignedTestTotals->countScheduled += $assignedTest->countScheduled;
        $assignedTestTotals->countInProgress += $assignedTest->countInProgress;
        $assignedTestTotals->countSubmitted += $assignedTest->countSubmitted;
        $assignedTestTotals->countCanceled += $assignedTest->countCanceled;
        $assignedTestTotals->countPaused += $assignedTest->countPaused;
    }
    if ($assignedTestTotals->countAssigned > 0)
    {
        $assignedTestTotals->percentScheduled =
            percentOneDecimalPlace($assignedTestTotals->countScheduled, $assignedTestTotals->countAssigned);
        $assignedTestTotals->percentSubmitted =
            percentOneDecimalPlace($assignedTestTotals->countSubmitted, $assignedTestTotals->countAssigned);
        $assignedTestTotals->percentCanceled =
            percentOneDecimalPlace($assignedTestTotals->countCanceled, $assignedTestTotals->countAssigned);
    }
    $assignedTestTotals->percentSubmittedOrCanceled =
        $assignedTestTotals->percentSubmitted + $assignedTestTotals->percentCanceled;
    $assignedTests[] = $assignedTestTotals;

    return $assignedTests;
}

function percentOneDecimalPlace($dividend, $divisor)
{
    return intval(($dividend * 1000 / $divisor) + 0.5) / 10;
}

// dbGetInactiveTestAssignmentFormIds()
//      Get testBatteryFormIds for inactive test assignments with testBatteryId and given studentRecordIds
//
function dbGetInactiveTestAssignmentFormIds($logId, PDO $db, $studentRecordIds, $testBatteryId)
{
    $sql = "SELECT studentRecordId, GROUP_CONCAT(DISTINCT testBatteryFormId SEPARATOR ',') 'testBatteryFormIds'
            FROM test_assignment
            WHERE studentRecordId IN ('".implode("','", $studentRecordIds)."')
            	AND testBatteryId = '$testBatteryId'
            	AND testStatusId > ".MAX_ACTIVE_TEST_STATUS_ID."
            	AND deleted = 'N'
            GROUP BY studentRecordId";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_KEY_PAIR);

        return $data;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbGetTestAssignment_ById()
//   Returns a test assignment
//
function dbGetTestAssignment_ById($logId, PDO $db, $testAssignmentId)
{
    $sql = "SELECT ta.testAssignmentId,
                   ta.adpTestAssignmentId,
                   ta.studentRecordId,
                   ta.testBatteryFormId,
                   ta.testBatteryId,
                   ta.testStatusId,
                   ts.testStatus,
                   ta.testKey,
                   ta.instructionStatus,
                   ta.enableLineReader,
                   ta.enableTextToSpeech,
                   ta.lastQueueEventTime
              FROM test_assignment ta
              JOIN test_status ts on ts.testStatusId = ta.testStatusId
             WHERE ta.testAssignmentId = '$testAssignmentId'
               AND ta.deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $testAssignment = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        else
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_ASSIGNMENT_RECORD_ID_NOT_FOUND,
                                     LDR_ED_TEST_ASSIGNMENT_RECORD_ID_NOT_FOUND . $testAssignmentId);
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return $testAssignment;
}

// dbGetTestAssignmentId_ByAdpId()
//   Returns a test assignment ID
//
function dbGetTestAssignmentId_ByAdpId($logId, PDO $db, $adpTestAssignmentId)
{
    $sql = "SELECT testAssignmentId FROM test_assignment WHERE adpTestAssignmentId = '$adpTestAssignmentId'";
    try
    {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // The test assignment was not found
    $errorLDR = new ErrorLDR($logId, LDR_EC_TEST_ASSIGNMENT_RECORD_ID_NOT_FOUND,
        'Test assignment record not found for adpTestAssignmentId ' . $adpTestAssignmentId);
    $errorLDR->logError(__FUNCTION__);
    return $errorLDR;
}

// dbGetTestAssignments()
//   Returns zero or more test assignments
//
function dbGetTestAssignments($logId, PDO $db, $testAssignmentId, $studentRecordId, $organizationId, $classIds,
                              $studentGradeLevel,$testBatterySubject, $testBatteryGrade, $testBatteryId,
                              $testAssignmentStatus, $instructionStatus, $testKeysOnly = false)
{
    // Check the arguments
    $argCount = 0;
    if (strlen($testAssignmentId) > 0)
        $argCount++;
    if (strlen($studentRecordId) > 0)
        $argCount++;
    if (strlen($organizationId) > 0)
        $argCount++;
    if (strlen($classIds) > 0)
        $argCount++;
    if ($argCount != 1)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_MISSING_OR_MULTIPLE_ARGS, 'Missing or multiple parameters - ' .
            'please specify one testAssignmentId, studentRecordId, classIds, or organizationId parameter');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Prepare the base query
    if ($testKeysOnly == 'Y')
    {
        $sql = "SELECT s.firstName AS studentFirstName,
                       s.lastName AS studentLastName,
                       sr.stateIdentifier AS studentStateIdentifier,
                       ta.testKey";
    }
    else
    {
        $sql = "SELECT sr.studentRecordId,
                       s.firstName AS studentFirstName,
                       s.lastName AS studentLastName,
                       sr.stateIdentifier AS studentStateIdentifier,
                       sr.gradeLevel AS studentGradeLevel,
                       tb.testBatteryId,
                       tb.testBatteryName,
                       tbs.testBatterySubject,
                       tbg.testBatteryGrade,
                       tbf.testBatteryFormName AS testBatteryForm,
                       ta.adpTestAssignmentId,
                       ta.testBatteryFormId,
                       ta.testAssignmentId,
                       ts.testStatus AS testAssignmentStatus,
                       ta.testStatusId,
                       ta.testKey,
                       ta.instructionStatus,
                       ta.enableLineReader,
                       ta.enableTextToSpeech,
                       ta.embeddedScoreReportId,
                       ta.lastQueueEventTime";
    }
    $sql .= " FROM student_record sr
              JOIN student s on s.studentId = sr.studentId
              JOIN test_assignment ta on ta.studentRecordId = sr.studentRecordId
              JOIN test_battery_form tbf on tbf.testBatteryFormId = ta.testBatteryFormId
              JOIN test_battery tb on tb.testBatteryId = tbf.testBatteryId
              JOIN test_battery_subject tbs on tbs.testBatterySubjectId = tb.testBatterySubjectId
              JOIN test_battery_grade tbg on tbg.testBatteryGradeId = tb.testBatteryGradeId
              JOIN test_status ts on ts.testStatusId = ta.testStatusId
             WHERE sr.deleted = 'N'
               AND ta.deleted = 'N'";

    // Add the required constraint
    if (strlen($testAssignmentId) > 0)
    {
        $sql .= " AND ta.testAssignmentId = '$testAssignmentId'";
    }
    elseif (strlen($studentRecordId) > 0)
    {
        $sql .= " AND sr.studentRecordId = '$studentRecordId'";
    }
    elseif (strlen($classIds) > 0)
    {
        // Check that the classes exist
        $classIds = explode(',', $classIds);
        foreach ($classIds as $classId)
        {
            $class = dbGetClass($logId, $db, $classId);
            if ($class instanceof ErrorLDR)
            {
                $class->logError(__FUNCTION__);
                return $class;
            }
        }

        // Get the student record IDs for the students in the class(es)
        $studentRecordIds = dbGetClassStudentRecordIds($logId, $db, $classIds);
        if ($studentRecordIds instanceof ErrorLDR)
        {
            $studentRecordIds->logError(__FUNCTION__);
            return $studentRecordIds;
        }
        if (sizeof($studentRecordIds) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_CLASSES_HAVE_NO_STUDENTS, 'The classIds value ' .
                    implode(',', $classIds) . ' specifies classes that do not have any students');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $studentRecordIds = implode(',', $studentRecordIds);
        $sql .= " AND sr.studentRecordId IN ($studentRecordIds)";
    }
    else
    {
        // Check that the organization is a school or institution
        $organizationType = dbGetOrganizationType($logId, $db, $organizationId);
        if ($organizationType instanceof ErrorLDR)
        {
            $organizationType->logError(__FUNCTION__);
            return $organizationType;
        }
        if ($organizationType != ORG_TYPE_SCHOOL)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_CANNOT_ENROLL_STUDENTS,
                                'The organization with organizationId ' . $organizationId . ' cannot enroll students');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }

        // Get the student record IDs for the students in the organization
        $studentRecordIds = dbGetStudentRecordIds($logId, $db, $organizationId);
        if ($studentRecordIds instanceof ErrorLDR)
        {
            $studentRecordIds->logError(__FUNCTION__);
            return $studentRecordIds;
        }
        if (sizeof($studentRecordIds) == 0)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_HAS_NO_STUDENTS, 'The organizationId value ' .
                $organizationId . ' specifies an organization that does not have any students');
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $studentRecordIds = implode(',', $studentRecordIds);
        $sql .= " AND sr.studentRecordId IN ($studentRecordIds)";
    }

    // Add optional constraints
    if (strlen($studentGradeLevel) > 0)
        $sql .= " AND sr.gradeLevel = '$studentGradeLevel'";
    if (strlen($testBatterySubject) > 0)
    {
        $testBatterySubjectId = dbGetReferenceValueId($logId, $db, 'test_battery_subject', $testBatterySubject);
        if ($testBatterySubjectId instanceof ErrorLDR)
        {
            $testBatterySubjectId->logError(__FUNCTION__);
            return $testBatterySubjectId;
        }
        if ($testBatterySubjectId === null)
            $testBatterySubjectId = 0;
        $sql .= " AND tb.testBatterySubjectId = '$testBatterySubjectId'";
    }
    if (strlen($testBatteryGrade) > 0)
    {
        $testBatteryGradeId = dbGetReferenceValueId($logId, $db, 'test_battery_grade', $testBatteryGrade);
        if ($testBatteryGradeId instanceof ErrorLDR)
        {
            $testBatteryGradeId->logError(__FUNCTION__);
            return $testBatteryGradeId;
        }
        if ($testBatteryGradeId === null)
            $testBatteryGradeId = 0;
        $sql .= " AND tb.testBatteryGradeId = '$testBatteryGradeId'";
    }
    if (strlen($testBatteryId) > 0)
        $sql .= " AND tb.testBatteryId = '$testBatteryId'";
    if (strlen($testAssignmentStatus) > 0)
    {
        $testAssignmentStatus = validateTestStatus($logId, $db, $testAssignmentStatus);
        if ($testAssignmentStatus instanceof ErrorLDR)
        {
            $testAssignmentStatus->logError(__FUNCTION__);
            return $testAssignmentStatus;
        }
        $testStatusId = dbGetReferenceValueId($logId, $db, 'test_status', $testAssignmentStatus);
        $sql .= " AND ta.testStatusId = '$testStatusId'";
    }
    if (strlen($instructionStatus) > 0 && $instructionStatus != INSTRUCTION_STATUS_NONE )
        $sql .= " AND ta.instructionStatus = '$instructionStatus'";

    // Sort by lastname, stateIdentifier
    $sql .= " ORDER BY s.lastName, sr.stateIdentifier";

    // Execute the query
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $testAssignments = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $testAssignments;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbRandomizeTestAssignmentStatuses()
//   Randomly set the test status values of all test assignments
//
function dbRandomizeTestAssignmentStatuses($logId, $db, $pattern)
{
    $patternA = [1,1,4,4,1,4,4,6,6,1,4,2,1,4,1,4,1,4,2,4,1,6,4,3,1,4,4,4,4,6,4,4,4,2,1,1,3,4,4,1,4,4,1,4,4,1,6,4,1,6,
                 6,1,3,1,1,6,4,1,4,6,6,1,6,4,4,6,1,4,1,1,4,1,2,4,4,4,6,6,4,1,4,2,6,4,4,1,4,4,1,4,3,4,4,6,1,4,4,1,4,1,
                 1,2,1,4,4,1,4,4,1,1,1,1,1,4,1,1,6,1,1,4,4,3,4,4,4,4,2,1,1,2,3,1,4,1,4,1,6,1,4,4,4,4,1,4,3,1,4,1,6,4,
                 1,1,4,1,1,6,1,4,1,1,3,2,4,1,1,3,2,4,1,1,4,6,4,1,2,4,1,3,4,1,6,2,1,1,1,1,4,2,1,6,1,3,3,1,1,1,4,6,4,4,
                 6,4,1,1,1,4,1,1,4,1,4,1,1,1,1,4,4,1,1,4,6,6,1,1,2,1,1,1,4,1,1,2,4,1,6,4,3,1,1,4,4,4,6,4,4,4,2,1,1,3,
                 1,1,1,4,4,1,1,1,1,6,4,1,6,6,1,3,1,1,6,2,1,4,6,6,1,6,1,4,6,1,1,1,1,4,1,2,4,4,4,6,6,1,1,1,2,6,1,4,1,1,
                 1,1,4,3,4,4,6,1,1,1,1,4,1,1,2,1,4,4,1,4,4,1,1,1,1,1,4,1,1,6,1,1,4,4,3,4,4,4,4,1,1,1,2,3,1,4,1,4,1,6,
                 1,4,4,4,4,1,4,3,1,4,1,6,4,1,1,4,1,1,6,1,4,1,1,3,2,4,1,1,3,2,4,1,1,4,6,4,1,2,4,1,1,4,1,6,2,1,1,1,1,4,
                 2,1,6,1,1,3,1,1,1,4,6,4,4,6,4,1,1,1,4,1,1,4,1,4,1,1];

    $patternB = [6];

    // Check if following a pattern
    $patternIdx = 0;
    $patternMax = 0;
    if (strlen($pattern) > 0)
    {
        if (strtoupper($pattern) == 'NONE')
            $pattern = null;
        elseif ($pattern == 'patternA')
        {
            $pattern = $patternA;
            $patternMax = sizeof($patternA) - 1;
        }
        elseif ($pattern == 'patternB')
        {
            $pattern = $patternB;
            $patternMax = sizeof($patternB) - 1;
        }
        else
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_050, "Pattern $pattern is undefined");
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
    }
    else
        $pattern = null;

    // Get the test assignment IDs for all test assignments
    $testAssignmentIds = [];
    $sql = "SELECT testAssignmentId
            FROM test_assignment
            WHERE deleted = 'N'
            ORDER BY testAssignmentId";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_NUM))
            $testAssignmentIds[] = $row[0];
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // Select a random percent of Scheduled tests
    $percentScheduled = mt_rand(10, 70);

    // Update each test assignment status with a random value
    foreach ($testAssignmentIds as $testAssignmentId)
    {
        // Check if following a pattern
        if ($pattern !== null)
        {
            $testStatusId = $pattern[$patternIdx++];
            if ($patternIdx > $patternMax)
                $patternIdx = 0;
        }
        else
        {
            $status = mt_rand(1, 100);
            if ($status < $percentScheduled)
                $testStatus = 'Scheduled';
            elseif ($status < 82)
                $testStatus = 'Submitted';
            elseif ($status < 92)
                $testStatus = 'Canceled';
            elseif ($status < 98)
                $testStatus = 'InProgress';
            else
                $testStatus = 'Paused';

            $testStatusId = dbGetReferenceValueId($logId, $db, 'test_status', $testStatus);
            if ($testStatusId instanceof ErrorLDR)
            {
                $testStatusId->logError(__FUNCTION__);
                return $testStatusId;
            }
            elseif ($testStatusId === null)
            {
                $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_045,
                                         "LDR internal server error - testStatus $testStatus is undefined");
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }
        }

        $sql = "UPDATE test_assignment SET testStatusId = '$testStatusId' WHERE testAssignmentId = '$testAssignmentId'";
        try {
            $stmt = $db->prepare($sql);
            $stmt->execute();
        }
        catch(PDOException $error) {
            return errorQueryException($logId, $error, __FUNCTION__, $sql);
        }
    }

    return SUCCESS;
}

// dbTestAssignmentIsActive()
//   Returns true if the student has an active test assignment, false otherwise
//
function dbTestAssignmentIsActive($logId, PDO $db, $studentRecordId, $testBatteryId)
{
    $sql = "SELECT COUNT(*)
              FROM test_assignment
             WHERE studentRecordId = '$studentRecordId'
               AND testBatteryId = '$testBatteryId'
               AND testStatusId <= " . MAX_ACTIVE_TEST_STATUS_ID .
             " AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return ($row[0] > 0);
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbTestAssignmentsAreActive
//      Check if there are any active test assignments with testBatteryId for a given studentRecordIds
//
function dbTestAssignmentsAreActive($logId, PDO $db, $studentRecordIds, $testBatteryId)
{
    $sql = "SELECT studentRecordId, COUNT(*)
              FROM test_assignment
             WHERE studentRecordId IN ('".implode("','", $studentRecordIds)."')
               AND testBatteryId = '$testBatteryId'
               AND testStatusId <= " . MAX_ACTIVE_TEST_STATUS_ID .
             " AND deleted = 'N'
             GROUP BY studentRecordId";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);

        return $data;
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }
}

// dbUpdateLastQueueEventTime()
//   Updates a test assignment lastQueueEventTime value
//
function dbUpdateLastQueueEventTime($logId, PDO $db, $testAssignmentId, $queueEventTime)
{
    $sql = "UPDATE test_assignment
               SET lastQueueEventTime = '$queueEventTime'
             WHERE testAssignmentId = '$testAssignmentId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbUpdateTestAssignment()
//   Updates a test assignment
//
function dbUpdateTestAssignment($logId, PDO $db, $clientUserId, $testAssignmentId, $testAssignment,
                                $testAssignmentStatus, $instructionStatus, $enableLineReader, $enableTextToSpeech)
{
    // Check the arguments
    $argCount = 0;
    if (strlen($enableLineReader) > 0)
        $argCount++;
    if (strlen($instructionStatus) > 0)
        $argCount++;
    if (strlen($enableTextToSpeech) > 0)
        $argCount++;
    if (strlen($testAssignmentStatus) > 0)
        $argCount++;
    if ($argCount < 1)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_ARG_NOT_PROVIDED, 'At least one of testAssignmentStatus, ' .
            'instructionStatus, enableLineReader, or enableTextToSpeech is required');
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Determine what changes are being made
    $dataChanges = [];
    $updateADP = false;
    $adpTestStatus = null;
    $adpEnableLineReader = null;
    $adpEnableTextToSpeech = null;
    $sql = "UPDATE test_assignment SET ";
    if (strlen($instructionStatus) > 0 && $instructionStatus != $testAssignment['instructionStatus'])
    {
        $dataChanges['instructionStatus'] = $instructionStatus;
        $sql .= "instructionStatus = '$instructionStatus', ";
    }
    if (strlen($enableTextToSpeech) > 0 && $enableTextToSpeech != $testAssignment['enableTextToSpeech'])
    {
        $dataChanges['enableTextToSpeech'] = $enableTextToSpeech;
        $sql .= "enableTextToSpeech = '$enableTextToSpeech', ";
        $adpEnableTextToSpeech = $enableTextToSpeech;
        $updateADP = true;
    }
    if (strlen($testAssignmentStatus) > 0 && $testAssignmentStatus != $testAssignment['testStatus'])
    {
        $testStatusId = dbGetReferenceValueId($logId, $db, 'test_status', $testAssignmentStatus);
        if ($testStatusId instanceof ErrorLDR)
        {
            $testStatusId->logError(__FUNCTION__);
            return $testStatusId;
        }
        elseif ($testStatusId === null)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_046,
                                     "LDR internal server error - testStatus $testAssignmentStatus is undefined");
            $errorLDR->logError(__FUNCTION__);
            return $errorLDR;
        }
        $dataChanges['testStatus'] = $testAssignmentStatus;
        $sql .= "testStatusId = '$testStatusId', ";

        // Only call ADP to update test status when the test is being canceled
        if ($testStatusId == TEST_STATUS_ID_CANCELED)
        {
            $adpTestStatus = $testAssignmentStatus;
            $updateADP = true;
        }
    }
    if (strlen($enableLineReader) > 0 && $enableLineReader != $testAssignment['enableLineReader'])
    {
        $dataChanges['enableLineReader'] = $enableLineReader;
        $sql .= "enableLineReader = '$enableLineReader', ";
        $adpEnableLineReader = $enableLineReader;
        $updateADP = true;
    }
    if (sizeof($dataChanges) < 1)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_UPDATE_HAS_NO_CHANGES, LDR_ED_UPDATE_HAS_NO_CHANGES);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    $sql = substr($sql, 0, -2); // Strip off the trailing ", "
    $sql .= " WHERE testAssignmentId = '$testAssignmentId'";

    // Update the test assignment record
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    // TODO: Create a user audit record
//    $dataChanges = json_encode($dataChanges);
//    $audit = dbCreateUserAuditRecord($logId, $db, $clientUserId, AUDIT_DATA_CLASS, $testAssignmentId,
//        AUDIT_ACTION_UPDATE, $dataChanges);
//    if ($audit instanceof ErrorLDR)
//    {
//        $db->rollBack();
//        $audit->logError(__FUNCTION__);
//        return $audit;
//    }

    // Perform ADP tasks
    if ($updateADP && SKIP_ADP_COMM == false)
    {
        $adpStudentData = ['studentId' => $testAssignment['studentRecordId']];

        // Update the test assignment in ADP
        $result = adpUpdateTestAssignment($logId, $testAssignment, $adpStudentData, $adpTestStatus,
                                          $adpEnableLineReader, $adpEnableTextToSpeech);
        if ($result instanceof ErrorLDR)
        {
            $result->logError(__FUNCTION__);
            return $result;
        }
    }

    return SUCCESS;
}

// dbUpdateTestAssignmentResults()
//   Updates the testResultsId and testFormRevisionId values for a test assignment
//
function dbUpdateTestAssignmentResults($logId, PDO $db, $testAssignmentId, $testResultsId, $testFormRevisionId)
{
    $sql = "UPDATE test_assignment SET testResultsId = '$testResultsId', testFormRevisionId = '$testFormRevisionId'
             WHERE testAssignmentId = '$testAssignmentId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($logId, $error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// generateTestKey()
//   Returns a random upper-case alpha test key
//   TODO: EXCLUDE 'PRACT.....' PATTERN BASED ON CONFIGURATION
function generateTestKey($length)
{
    // Check for a static test key
    $testKey = TESTKEY;
    if (strlen($testKey) == LENGTH_TEST_KEY && !preg_match("/[^A-Z]/", $testKey))
        return $testKey;

    $testKeyCharSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charSetMaxIndex = strlen($testKeyCharSet) - 1;

    $testKey = "";
    while ($length-- > 0)
        $testKey .= substr($testKeyCharSet, mt_rand(0, $charSetMaxIndex), 1);

    return $testKey;
}
