<?php

require_once '../ldr_client/config.php';

function logHAL($logId, $severity, $eventType, $eventDesc = '', $msgId = null)
{
    // Append the LDR Log ID to the event description
    $eventDesc .= ' [' . $logId . ']';

    // If there is a message ID then append that as well
    if ($msgId !== null)
        $eventDesc .= ' [' . $msgId . ']';

    // Prepare the call arguments
    $args = [];
    $args['appType'] = 'LDR';
    $args['hostname'] = gethostname();
    $args['appDateTime'] = date("Y-m-d H:i:s");
    $args['severity'] = $severity;
    $args['eventType'] = $eventType;
    $args['eventDesc'] = $eventDesc;

    // Initialize and configure a cURL session
    $ch = curl_init();
    $url = HAL_REQUEST_SCHEME . '://' . HAL_HOSTNAME . HAL_RESOURCE_PATH . 'log-entry';
    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json; charset=UTF-8"]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $jsonResponse = curl_exec($ch);

    // Check the response
    if ($jsonResponse === false)
    {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR($logId, LDR_EC_CURL_ERROR_RESPONSE,
                                 'curl error code ' . $errorCode . ' : ' . $errorText);
        $errorLDR->logError(__FUNCTION__);
        curl_close($ch);
        return $errorLDR;
    }

    // Close the cURL session
    curl_close($ch);

    // Get the response
    $response = json_decode($jsonResponse, true);

    // Check the response
    if ($response === null)
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_HAL_RESPONSE_DECODE_FAILED,
                                 LDR_ED_HAL_RESPONSE_DECODE_FAILED . $jsonResponse);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    else if (array_key_exists('error', $response))
    {
        $errorLDR = new ErrorLDR($logId, LDR_EC_HAL_LOG_REQUEST_FAILED,
                                 $response['error'] . ' => ' . $response['detail']);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $response;
}
