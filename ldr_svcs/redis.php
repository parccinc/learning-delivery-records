<?php

//
// LDR Redis services
//
// sendRedis()          send_redis.php
//
// LDR Redis support functions
//

// LDR Redis definitions
define('REDIS_DATA_TYPE_NONE',      0);
define('REDIS_DATA_TYPE_STRING',    1);
define('REDIS_DATA_TYPE_SET',       2);
define('REDIS_DATA_TYPE_LIST',      3);
define('REDIS_DATA_TYPE_ZSET',      4);
define('REDIS_DATA_TYPE_HASH',      5);

$redisDataTypeNames =
[
    'no data type',
    'redis string',
    'redis set',
    'redis list',
    'redis zset',
    'redis hash'
];

$redisConnection = null;

//
// LDR Redis services
//
function updateRedisStatus($logId, $redisStatus, $redisStatusChange)
{
    if ($redisStatusChange == REDIS_STATUS_DOWNGRADE)
    {
        if ($redisStatus == 'Connection is up')
            $redisStatus = 'Connection retry 1';
        else
        {
            $count = substr($redisStatus, -1);
            $count += 1;
            if ($count > 4)
                $redisStatus = 'Connection is down';
            else
                $redisStatus = 'Connection retry ' . $count;
        }
    }
    else
        $redisStatus = 'Connection is up';

    try
    {
        $result = file_put_contents(REDIS_STATUS_FILENAME, $redisStatus);
        if ($result === false)
        {
            $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_048, 'Unable to update Redis status file');
            $errorLDR->logError(__FUNCTION__);
        }
    }
    catch(Exception $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_049,
                                 'Unable to update Redis status file: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
    }

    return $redisStatus;
}

function redisConnect($logId)
{
    global $redisConnection;

    // Check if Redis is enabled
    if (!REDIS_ENABLED)
        return false;

    // Check if we already have a connection
    if ($redisConnection !== null)
        return true;

    // Check the Redis server status
    $redisStatus = null;
    try
    {
        $redisStatus = trim(file_get_contents(REDIS_STATUS_FILENAME));
        if ($redisStatus === false)
            return false;
        if ($redisStatus == 'Connection is down')
            return false;
    }
    catch(Exception $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_INTERNAL_ERROR_047, $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }

    // Connect to the Redis server
    try
    {
        $redisConnection = new Redis();
        $connected = $redisConnection->connect(REDIS_HOSTNAME);
        if (!$connected)
        {
            // Downgrade the Redis status
            $redisStatus = updateRedisStatus($logId, $redisStatus, REDIS_STATUS_DOWNGRADE);
            $redisConnection = null;

            // Log the error
            $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_CONNECT_FAILED,
                                     'Unable to connect to Redis server - ' . $redisStatus);
            $errorLDR->logError(__FUNCTION__);
            return false;
        }
        else
        {
            // Check if the Redis cache needs to be restored
            if ($redisStatus != 'Connection is up')
            {
                // TODO: LOAD REDIS CACHE FROM DATABASE


                // Upgrade the redis status
                $redisStatus = updateRedisStatus($logId, $redisStatus, REDIS_STATUS_UPGRADE);
                $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_CONNECT_FAILED,
                                         'Redis connection has been restored - ' . $redisStatus);
                $errorLDR->logError(__FUNCTION__);
            }
            return true;
        }
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_CONNECT_FAILED,
                                 'Unable to connect to Redis server: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }
}

function redisCreateOrganization($logId, $organizationType, $parentOrganizationId, $organizationIdentifier,
                                 $organizationName)
{
    global $redisConnection;

    // Get a Redis connection
    if (!redisConnect($logId))
        return null;

    try
    {
        // Get the next organizationId
        $redisKey = 'last_organizationId';
        $organizationId = $redisConnection->incr($redisKey);
        if ($organizationId === false)
            return false;

        // If this is not the root organization then set the stateOrganizationId
        if ($parentOrganizationId != ROOT_PARENT_ORGANIZATION_ID)
        {
            // Get the parent organization state organization ID
            $redisKey = "organizationId:$parentOrganizationId";
            $redisField = 'stateOrganizationId';
            $stateOrganizationId = $redisConnection->hGet($redisKey, $redisField);
            if ($stateOrganizationId === false)
                return false;

            // Make sure the stateOrganizationId is not zero
            if ($stateOrganizationId == 0)
                $stateOrganizationId = $organizationId;

            // If this is not a state organization then create a state organizationIdentifier key
            if ($parentOrganizationId != ROOT_ORGANIZATION_ID)
            {
                $redisKey = "stateOrganizationIdentifier:$stateOrganizationId:$organizationIdentifier";
                $result = $redisConnection->set($redisKey, $organizationId);
                if ($result === false)
                    return false;
            }
        }
        else
            $stateOrganizationId = 0;

        // Create the organization
        $redisKey = "organizationId:$organizationId";
        $redisFields = ['organizationId', 'organizationType', 'stateOrganizationId', 'parentOrganizationId',
                        'organizationIdentifier', 'organizationName', 'studentCount', 'childCount'];
        $redisValues = [$organizationId, $organizationType, $stateOrganizationId, $parentOrganizationId,
                        $organizationIdentifier, $organizationName, 0, 0];
        $elements = array_combine($redisFields, $redisValues);
        $result = $redisConnection->hMset($redisKey, $elements);
        if ($result === false)
            return false;

        // Add this organization ID to the parent organization's list of child organization IDs
        $redisValues = ["childOrganizationIds:$parentOrganizationId", $organizationId];
        $result = call_user_func_array([$redisConnection, 'rPush'], $redisValues);
        if ($result === false)
            return false;

        return $organizationId;
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }
}

function redisFlushAll($logId)
{
    global $redisConnection;

    // Get a Redis connection
    if (!redisConnect($logId))
        return null;

    try
    {
        $redisConnection->flushAll();
        return SUCCESS;
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }
}

function redisGetOrganization($logId, $organizationId)
{
    global $redisConnection;

    // Get a Redis connection
    if (!redisConnect($logId))
        return null;

    // Get the organization
    try
    {
        $redisKey = "organizationId:$organizationId";
        $organization = $redisConnection->hGetAll($redisKey);
        return $organization;
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }
}

function redisGetOrgId_ByParentOrgId_ByName($logId, $parentOrganizationId, $targetOrganizationName, $logError = true)
{
    global $redisConnection;

    // Get a Redis connection
    if (!redisConnect($logId))
        return null;

    try
    {
        // Get the child organization IDs
        $redisKey = "childOrganizationIds:$parentOrganizationId";
        $organizationIds = $redisConnection->lRange($redisKey, 0, -1);
        if ($organizationIds === false)
            return false;

        // Check each child organizationName
        foreach ($organizationIds as $organizationId)
        {
            $redisKey = "organizationId:$organizationId";
            $redisField = 'organizationName';
            $organizationName = $redisConnection->hGet($redisKey, $redisField);
            if ($organizationName === false)
                return false;
            if ($organizationName == $targetOrganizationName)
                return $organizationId;
        }

        // The organization was not found
        $redisKey = "organizationId:$parentOrganizationId";
        $redisField = 'organizationName';
        $parentName = $redisConnection->hGet($redisKey, $redisField);
        if ($parentName === false)
            $parentName = '';
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND, 'Organization not found for ' .
                                 $parentName . ' organization ' . $targetOrganizationName);
        if ($logError)
            $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }
}

function redisGetOrgId_ByStateOrgId_ByIdentifier($logId, $stateOrganizationId, $organizationIdentifier, $logError = true)
{
    global $redisConnection;

    // Get a Redis connection
    if (!redisConnect($logId))
        return null;

    try
    {
        // Try to get the organization ID
        $redisKey = "stateOrganizationIdentifier:$stateOrganizationId:$organizationIdentifier";
        $organizationId = $redisConnection->get($redisKey);
        if ($organizationId !== false)
            return $organizationId;

        // The organization ID was not found
        $redisKey = "organizationId:$stateOrganizationId";
        $redisField = 'organizationName';
        $stateCode = $redisConnection->hGet($redisKey, $redisField);
        if ($stateCode === false)
            $stateCode = '';
        $errorLDR = new ErrorLDR($logId, LDR_EC_ORGANIZATION_ID_NOT_FOUND, 'Organization not found for ' . $stateCode .
                                 ' organization identifier ' . $organizationIdentifier);
        if ($logError)
            $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }
}

function redisIncrementOrganizationChildCount($logId, $organizationId)
{
    global $redisConnection;

    // Get a Redis connection
    if (!redisConnect($logId))
        return null;

    // Increment the child count
    try
    {
        $redisKey = "organizationId:$organizationId";
        $redisField = 'childCount';
        $childCount = $redisConnection->hGet($redisKey, $redisField);
        if ($childCount !== false)
        {
            $childCount++;
            $redisConnection->hSet($redisKey, $redisField, $childCount);
            return SUCCESS;
        }
        return false;
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
        $errorLDR->logError(__FUNCTION__);
        return false;
    }
}

//function redisInitializePrimaryKey($logId, $primaryKeyId)
//{
//    global $redisConnection;
//
//    // Get a Redis connection
//    if (!redisConnect($logId))
//        return null;
//
//    // Initialize the primary key
//    try
//    {
//        $redisKey = 'last_' . $primaryKeyId;
//        $result = $redisConnection->setnx($redisKey, '0');
//        if ($result === false)
//            return false;
//        else
//            return SUCCESS;
//    }
//    catch(RedisException $error) {
//        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
//        $errorLDR->logError(__FUNCTION__);
//        return false;
//    }
//}

// sendRedis()
//   Sends a command to the Redis server and returns the result
//
function sendRedis()
{
    global $redisConnection, $redisDataTypeNames;

    // Log the service call
    $startTime = microtime(true);
    $logId = logRequest();

    $args = beginService($logId, TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'redisCmd' => DCO_REQUIRED | DCO_TOUPPER,
            'redisKey' => DCO_OPTIONAL,
            'redisKeys' => DCO_OPTIONAL | DCO_NO_CHECK,
            'redisField' => DCO_OPTIONAL,
            'redisFields' => DCO_OPTIONAL | DCO_NO_CHECK,
            'redisValue' => DCO_OPTIONAL,
            'redisValues' => DCO_OPTIONAL | DCO_NO_CHECK
        ));
    if ($args instanceof ErrorLDR)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $redisCmd, $redisKey, $redisKeys, $redisField, $redisFields, $redisValue, $redisValues) = $args;

    // Get a Redis connection
    if (!redisConnect($logId))
        return;

    // Process the Redis command
    $result = null;
    try {
        switch ($redisCmd)
        {
            case 'DBSIZE':
                $result = ['redis command' => $redisCmd];
                $result['number of keys in database'] = $redisConnection->dbSize();
                break;
            case 'DECR':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING],
                               'key' => $redisKey];
                    $value = $redisConnection->decr($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key value is not an integer or is out of range)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'DECRBY':
                if (strlen($redisKey) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING],
                               'key' => $redisKey];
                    $value = $redisConnection->decrBy($redisKey, $redisValue);
                    if ($value === false)
                        $result['result'] = 'false (key value is not an integer or is out of range)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValue args are required'];
                break;
            case 'DEL':
                if (is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd];
                    $value = $redisConnection->del($redisKeys);
                    $result['result'] = $value . ' keys deleted';
                }
                else
                    $result = ['result' => 'redisKeys arg is required'];
                break;
            case 'EXISTS':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd, 'key' => $redisKey];
                    $value = $redisConnection->exists($redisKey);
                    if ($value === true)
                        $result['result'] = 'key exists';
                    else
                        $result['result'] = 'key not found';
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'EXPIRE':
                if (strlen($redisKey) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd, 'key' => $redisKey];
                    $value = $redisConnection->expire($redisKey, $redisValue);
                    if ($value === true)
                        $result['result'] = SUCCESS;
                    else
                        $result['result'] = REDIS_COMMAND_FAILED;
                }
                else
                    $result = ['result' => 'redisKey and redisValue args are required'];
                break;
            case 'FLUSHALL':
                $result = ['redis command' => $redisCmd];
                $value = $redisConnection->flushAll();
                if ($value === true)
                    $result['result'] = SUCCESS;
                else
                    $result['result'] = REDIS_COMMAND_FAILED;
                break;
            case 'FLUSHDB':
                $result = ['redis command' => $redisCmd];
                $value = $redisConnection->flushDB();
                if ($value === true)
                    $result['result'] = SUCCESS;
                else
                    $result['result'] = REDIS_COMMAND_FAILED;
                break;
            case 'GET':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING],
                               'key' => $redisKey];
                    $value = $redisConnection->get($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key not found | key value is not a string)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'GETSET':
                if (strlen($redisKey) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                        'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING],
                        'key' => $redisKey];
                    $value = $redisConnection->getSet($redisKey, $redisValue);
                    if ($value === false)
                        $result['result'] = 'false (key not found | key value is not a string)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValue args are required'];
                break;
            case 'HDEL':
                // Delete one or more hash fields
                if (strlen($redisKey) > 0 && is_array($redisFields) && sizeof($redisFields) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    // The hash key needs to be pushed to the front of the array passed to the hDel method
                    array_unshift($redisFields, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'hDel'], $redisFields);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a hash)';
                    else
                        $result['fields removed'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisFields args are required'];
                break;
            case 'HEXISTS':
                if (strlen($redisKey) > 0 && strlen($redisField) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey,
                               'field' => $redisField];
                    $value = $redisConnection->hExists($redisKey, $redisField);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a hash | field does not exist)';
                    else
                        $result['result'] = 'success (field exists)';
                }
                else
                    $result = ['result' => 'redisKey and redisField args are required'];
                break;
            case 'HGET':
                if (strlen($redisKey) > 0 && strlen($redisField) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey,
                               'field' => $redisField];
                    $value = $redisConnection->hGet($redisKey, $redisField);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a hash | field does not exist)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisField args are required'];
                break;
            case 'HGETALL':
                if (strlen($redisKey))
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    $values = $redisConnection->hGetAll($redisKey);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a hash)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'HKEYS':
                if (strlen($redisKey))
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    $fields = $redisConnection->hKeys($redisKey);
                    if ($fields === false)
                        $result['result'] = 'false (key value is not a hash)';
                    else
                        $result['fields'] = $fields;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'HLEN':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    $value = $redisConnection->hLen($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a hash)';
                    else
                        $result['length'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'HMGET':
                if (strlen($redisKey) > 0 && is_array($redisFields) && sizeof($redisFields) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    $values = $redisConnection->hMGet($redisKey, $redisFields);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a hash)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKey and redisFields args are required'];
                break;
            case 'HMSET':
                if (strlen($redisKey) > 0 &&
                    is_array($redisFields) && sizeof($redisFields) > 0 &&
                    is_array($redisValues) && sizeof($redisValues) == sizeof($redisFields))
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    $elements = array_combine($redisFields, $redisValues);
                    $value = $redisConnection->hMset($redisKey, $elements);
                    if ($value === true)
                        $result['result'] = SUCCESS;
                    else
                        $result['result'] = 'false (key value is not a hash)';
                }
                else
                    $result = ['result' => 'redisKey, redisFields and redisValues args are required'];
                break;
            case 'HSET':
                if (strlen($redisKey) > 0 && strlen($redisField) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    $value = $redisConnection->hSet($redisKey, $redisField, $redisValue);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a hash)';
                    elseif ($value == 1)
                        $result['result'] = 'success (new field added to hash)';
                    elseif ($value == 0)
                        $result['result'] = 'success (existing hash field updated)';
                    else
                        $result['result'] = $value;
                }
                else
                    $result = ['result' => 'redisKey, redisField and redisValue args are required'];
                break;
            case 'HVALS':
                if (strlen($redisKey))
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_HASH],
                               'key' => $redisKey];
                    $fields = $redisConnection->hVals($redisKey);
                    if ($fields === false)
                        $result['result'] = 'false (key value is not a hash)';
                    else
                        $result['fields'] = $fields;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'INCR':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING],
                               'key' => $redisKey];
                    $value = $redisConnection->incr($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key value is not an integer or is out of range)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'INCRBY':
                if (strlen($redisKey) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING],
                               'key' => $redisKey];
                    $value = $redisConnection->incrBy($redisKey, $redisValue);
                    if ($value === false)
                        $result['result'] = 'false (key value is not an integer or is out of range)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValue args are required'];
                break;
            case 'INFO':
                $result = ['redis command' => $redisCmd];
                if (strlen($redisValue) > 0)
                    $values= $redisConnection->info(strtoupper($redisValue));
                else
                    $values= $redisConnection->info();
                if ($values === false)
                    $result['result'] = 'false (connection is broken)';
                else
                    $result['values'] = $values;
                break;
            case 'KEYS':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd, 'pattern' => $redisKey];
                    $keys = $redisConnection->keys($redisKey);
                    $result['keys'] = $keys;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'LINDEX':
                if (strlen($redisKey) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    $value = $redisConnection->lIndex($redisKey, $redisValue);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a list | value is out of range)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValue args are required'];
                break;
            case 'LLEN':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    $value = $redisConnection->lLen($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a list)';
                    else
                        $result['length'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'LPOP':
                // Pop a value from the front of a list
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    $value = $redisConnection->lPop($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key not found | key value is not a list)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'LPUSH':
                // Push one or more values onto the front of a list (last value pushed will be at the front)
                if (strlen($redisKey) > 0 && is_array($redisValues) && sizeof($redisValues) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    // The list key needs to be pushed to the front of the array passed to the lPush method
                    array_unshift($redisValues, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'lPush'], $redisValues);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a list)';
                    else
                        $result['length'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValues args are required'];
                break;
            case 'LRANGE':
                if (strlen($redisKey) > 0 && is_array($redisValues) && sizeof($redisValues) == 2)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    $values = $redisConnection->lRange($redisKey, $redisValues[0], $redisValues[1]);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a list)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKey and redisValues (two integers for redisValues) args are required'];
                break;
            case 'LSET':
                if (strlen($redisKey) > 0 && is_array($redisValues) && sizeof($redisValues) == 2)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    $values = $redisConnection->lSet($redisKey, $redisValues[0], $redisValues[1]);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a list | value is out of range)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKey and redisValues (index and value for redisValues) args are required'];
                break;
            case 'LTRIM':
                // Trim a list to the range of values specified. An invalid range will delete the list and key.
                if (strlen($redisKey) > 0 && is_array($redisValues) && sizeof($redisValues) == 2)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    $value = $redisConnection->lTrim($redisKey, $redisValues[0], $redisValues[1]);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a list)';
                    else
                        $result['result'] = SUCCESS;
                }
                else
                    $result = ['result' => 'redisKey and redisValues (two integers for redisValues) args are required'];
                break;
            case 'MGET':
                if (is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING]];
                    $values = $redisConnection->mget($redisKeys);
                    for ($idx = 0; $idx < sizeof($redisKeys); $idx++)
                    {
                        // Get the result for each key
                        $element = ['key' => $redisKeys[$idx]];
                        if ($values[$idx] === false)
                            $element['result'] = 'false (key not found | key value is not a string)';
                        else
                            $element['value'] = $values[$idx];
                        $result[] = $element;
                    }
                }
                else
                    $result = ['result' => 'redisKeys arg is required'];
                break;
            case 'MSET':
                // NOTE: This command can change a key value type to a string
                if (is_array($redisKeys) && sizeof($redisKeys) > 0 &&
                    is_array($redisValues) && sizeof($redisValues) == sizeof($redisKeys))
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING]];
                    $elements = array_combine($redisKeys, $redisValues);
                    $value = $redisConnection->mset($elements);
                    if ($value === true)
                        $result['result'] = SUCCESS;
                    else
                        $result['result'] = REDIS_COMMAND_FAILED;
                }
                else
                    $result = ['result' => 'redisKeys and redisValues args are required and must be same length'];
                break;
            case 'PING':
                $result = ['redis command' => $redisCmd];
                $value = $redisConnection->ping();
                if ($value === false)
                    $result['result'] = 'false (connection is broken)';
                else
                    $result['value'] = $value;
                break;
            case 'RPOP':
                // Pop a value from the end of a list
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    $value = $redisConnection->rPop($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key not found | key value is not a list | list is empty)';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'RPUSH':
                // Push one or more values onto the end of a list (last value pushed will be at the end)
                if (strlen($redisKey) > 0 && is_array($redisValues) && sizeof($redisValues) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_LIST],
                               'key' => $redisKey];
                    // The list key needs to be pushed to the front of the array passed to the rPush method
                    array_unshift($redisValues, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'rPush'], $redisValues);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a list)';
                    else
                        $result['list length'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValues args are required'];
                break;
            case 'SADD':
                // Add one or more values to a set
                if (strlen($redisKey) > 0 && is_array($redisValues) && sizeof($redisValues) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    // The set key needs to be pushed to the front of the array passed to the sAdd method
                    array_unshift($redisValues, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'sAdd'], $redisValues);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['values added'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValues args are required'];
                break;
            case 'SCARD':
                // Get the number of elements in a set
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    $value = $redisConnection->sCard($redisKey);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['cardinality'] = $value;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'SDIFF':
                // Get the difference between a set and one or more successive sets
                //  i.e. - get the elements that are in a set but not in any of the successive sets
                if (is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET]];
                    $values = call_user_func_array([$redisConnection, 'sDiff'], $redisKeys);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKeys arg is required'];
                break;
            case 'SDIFFSTORE':
                // Create a set from the difference between a set and one or more successive sets
                // NOTE: This command can change a key value type to a set
                if (strlen($redisKey) > 0 && is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    // The set key needs to be pushed to the front of the array passed to the sDiffStore method
                    array_unshift($redisKeys, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'sDiffStore'], $redisKeys);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['count'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisKeys args are required'];
                break;
            case 'SELECT':
                // Note: This is implemented here just to test the command itself. It has no other effect
                //       because the next request will revert to database 0 (zero). Persisting this would
                //       require saving the selected database in a file.
                if (strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd, 'database' => $redisValue];
                    $value = $redisConnection->select($redisValue);
                    if ($value === false)
                        $result['result'] = 'false (value is not a valid database)';
                    else
                        $result['result'] = $value;
                }
                else
                    $result = ['result' => 'redisValue arg is required'];
                break;
            case 'SET':
                // NOTE: This command can change a key value type to a string
                if (strlen($redisKey) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_STRING],
                               'key' => $redisKey];
                    $value = $redisConnection->set($redisKey, $redisValue);
                    if ($value === true)
                        $result['result'] = SUCCESS;
                    else
                        $result['result'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValue args are required'];
                break;
            case 'SINTER':
                // Get the intersection of two or more sets
                //  i.e. - get the elements that are in all of the sets
                if (is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET]];
                    $values = call_user_func_array([$redisConnection, 'sInter'], $redisKeys);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKeys arg is required'];
                break;
            case 'SINTERSTORE':
                // Create a set from the intersection of two or more sets
                // NOTE: This command can change a key value type to a set
                if (strlen($redisKey) > 0 && is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    // The set key needs to be pushed to the front of the array passed to the sInterStore method
                    array_unshift($redisKeys, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'sInterStore'], $redisKeys);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['count'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisKeys args are required'];
                break;
            case 'SISMEMBER':
                if (strlen($redisKey) > 0 && strlen($redisValue) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    $value = $redisConnection->sIsMember($redisKey, $redisValue);
                    if ($value === false)
                        $result['result'] = 'false (value is not member of set | key value is not a set)';
                    elseif ($value == 1)
                        $result['result'] = 'value is member of set';
                    else
                        $result['value'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValue args are required'];
                break;
            case 'SMEMBERS':
                if (strlen($redisKey))
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    $values = $redisConnection->sMembers($redisKey);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'SREM':
                // Remove one or more values from a set
                if (strlen($redisKey) > 0 && is_array($redisValues) && sizeof($redisValues) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    // The set key needs to be pushed to the front of the array passed to the sRem method
                    array_unshift($redisValues, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'sRem'], $redisValues);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['values removed'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisValues args are required'];
                break;
            case 'SUNION':
                // Get the union of two or more sets
                //  i.e. - get the elements that are in any of the sets
                if (is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET]];
                    $values = call_user_func_array([$redisConnection, 'sUnion'], $redisKeys);
                    if ($values === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['values'] = $values;
                }
                else
                    $result = ['result' => 'redisKeys arg is required'];
                break;
            case 'SUNIONSTORE':
                // Create a set from the union of two or more sets
                // NOTE: This command can change a key value type to a set
                if (strlen($redisKey) > 0 && is_array($redisKeys) && sizeof($redisKeys) > 0)
                {
                    $result = ['redis command' => $redisCmd,
                               'command data type' => $redisDataTypeNames[REDIS_DATA_TYPE_SET],
                               'key' => $redisKey];
                    // The set key needs to be pushed to the front of the array passed to the sDiffStore method
                    array_unshift($redisKeys, $redisKey);
                    $value = call_user_func_array([$redisConnection, 'sUnionStore'], $redisKeys);
                    if ($value === false)
                        $result['result'] = 'false (key value is not a set)';
                    else
                        $result['count'] = $value;
                }
                else
                    $result = ['result' => 'redisKey and redisKeys args are required'];
                break;
            case 'TIME':
                $result = ['redis command' => $redisCmd];
                $values = $redisConnection->time();
                if ($values === false)
                    $result['result'] = 'false (connection is broken)';
                else
                    $result['values'] = $values;
                break;
            case 'TTL':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd, 'key' => $redisKey];
                    $value = $redisConnection->ttl($redisKey);
                    if ($value == -2)
                        $result['value'] = 'key not found';
                    elseif ($value == -1)
                        $result['value'] = 'key has no expiration';
                    else
                        $result['value'] = $value . ' seconds';
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            case 'TYPE':
                if (strlen($redisKey) > 0)
                {
                    $result = ['redis command' => $redisCmd, 'key' => $redisKey];
                    $value = $redisConnection->type($redisKey);
                    $result['value'] = $redisDataTypeNames[$value];
                }
                else
                    $result = ['result' => 'redisKey arg is required'];
                break;
            default:
                $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_UNKNOWN, 'Redis command unknown: ' . $redisCmd);
                $errorLDR->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
        }
    }
    catch(RedisException $error) {
        $errorLDR = new ErrorLDR($logId, LDR_EC_REDIS_COMMMAND_FAILED, 'Redis command failed: ' . $error->getMessage());
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, $logId, $result, $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}
