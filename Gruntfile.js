module.exports = function (grunt) {
    grunt.initConfig({
        exec: {
            run_only_organization_tests: {
                cmd: 'php vendor/codeception/codeception/codecept run api -g organization'
            }, run_only_student_tests: {
                cmd: 'php vendor/codeception/codeception/codecept run api -g student'
            }, run_only_test_services_tests: {
                cmd: 'php vendor/codeception/codeception/codecept run api -g test_services'
            }, run_only_misc_tests: {
                cmd: 'php vendor/codeception/codeception/codecept run api -g misc'
            }
        },
        watch: {
            organization: {
                files: [
                    'ldr_svcs/organization.php'
                    , 'ldr_svcs/class.php'
                ],
                tasks: ['exec:run_only_organization_tests']
            }, student: {
                files: ['ldr_svcs/student.php'],
                tasks: ['exec:run_only_student_tests']
            }, test_services: {
                files: ['ldr_svcs/test_*.php'],
                tasks: ['exec:run_only_test_services_tests']
            }, misc: {
                files: [
                    'ldr_svcs/index.php'
                    , 'ldr_svcs/adp_comm.php'
                    , 'ldr_svcs/aws.php'
                    , 'ldr_svcs/user_audit.php'
                    , 'ldr_svcs/validation.php'
                ],
                tasks: ['exec:run_only_misc_tests']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');
};
