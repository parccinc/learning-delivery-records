<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('PUT', 'tests-results', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $result = $response['result'];
        echo '<p class="message">Total Test Assignments = '.$result['totalTestAssignments'].'</p>';
        echo '<p class="message">Total Success Test Assignments = '.$result['successTestAssignments'].'</p>';

        if (count($result['errors']) > 0) {
            echo '<p class="error">';
            foreach ($result['errors'] as $key => $error) {
                echo "testAssignmentId: ".$key." => ".$error['detail'] .'<br/>';
            }
            echo '</p>';
        }
    }
}
displayFooter();
