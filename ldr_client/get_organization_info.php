<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'organization-info', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $indent = '&nbsp;&nbsp;&nbsp;&nbsp;';
        foreach ($response as $organizationId => $organizationInfo)
        {
            if (array_key_exists('error', $organizationInfo))
            {
                echo '<p class="error">';
                echo $organizationId . ' =><br />';
                foreach ($organizationInfo as $key => $value)
                    echo $indent . $key . ' => ' . $value . '<br />';
                echo '</p>';
            }
            else
            {
                echo '<p class="success">';
                echo $organizationId . ' =><br />';
                foreach ($organizationInfo as $key => $value)
                    echo $indent . $key . ' => ' . $value . '<br />';
                echo '</p>';
            }
        }
    }
}
displayFooter();
