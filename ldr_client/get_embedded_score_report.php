<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['embeddedScoreReportId']))
{
    displayHeader(true);
    displayErrorMessage('embeddedScoreReportId value is required in the query string');
    displayFooter();
    exit;
}

// Prepare the resource path
$resource = 'embedded-score-report/' . $_GET['embeddedScoreReportId'];

// Call the LDR service
$response = callLDR('GET', $resource);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $scoreReport = base64_decode($response['embeddedScoreReport']);

        // Just echo the whole score report within the existing HTML <body> element
        echo $scoreReport;
    }
}
displayFooter();
