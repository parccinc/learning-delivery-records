<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'test-assignments', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $suffix = (sizeof($response) == 1) ? '' : 's';
        displayOrganization( 'Showing ' . sizeof($response) . ' test assignment' . $suffix);

        echo '<table class="data">';
        if (array_key_exists('testKeysOnly', $_GET) && strtoupper($_GET['testKeysOnly']) == 'Y')
        {
            $columns = ['studentFirstName', 'studentLastName', 'studentStateIdentifier', 'testKey'];
        }
        else
        {
            $columns = ['studentRecordId', 'studentFirstName', 'studentLastName', 'studentStateIdentifier',
                'studentGradeLevel', 'testBatteryId', 'testBatteryName', 'testBatterySubject', 'testBatteryGrade',
                'testBatteryForm', 'testAssignmentId', 'testAssignmentStatus', 'testKey', 'instructionStatus',
                'enableLineReader', 'enableTextToSpeech', 'embeddedScoreReportId', 'lastQueueEventTime'];
        }
        displayTableHeader($columns);
        foreach ($response as $testAssignment)
            displayTableDataByKey($testAssignment, $columns);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
