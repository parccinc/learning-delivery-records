<?php

require_once 'ldr_client.php';

session_start();

$resource = 'school-activity-report';
if (isset($_GET['organizationId'])) {
    $resource .= "/".$_GET['organizationId'];
}

// Call the LDR service
$response = callLDR('GET', $resource);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $filename = explode('/', $response['filename']);
        $filename = end($filename);
        echo '<p class="message">';
        echo 'School Testing Activity Report: '.$filename;
        echo ' <a href="'.$response['fileURL'].'">CLICK HERE TO DOWNLOAD</a>';
        echo '</p>';
    }
}
displayFooter();
