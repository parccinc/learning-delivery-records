<?php
require_once 'ldr_client.php';

session_start();

if (isset($_POST["submit"])) {
    $file = $_FILES['fileUpload'];

    // Check extension and file type
    $extension = end(explode('.', $file['name']));
    if ($extension === 'csv' && $file['type'] === "text/csv") {
        // Check file size
        if ($file['size'] <= 2000 * 1024 * 1024) {
            if ($testMapContent = file_get_contents($file['tmp_name'])) {
                // Call the LDR service
                $response = callLDR('POST', 'test-map', ["testMapContent" => $testMapContent]);

                // Check the LDR response for an error
                $error = array_key_exists('error', $response);

                // Display the LDR response
                displayHeader($error);
                if (isset($_GET['json'])) {
                    displayJsonResponse($response);
                } else {
                    if ($error) {
                        displayErrorResponse($response);
                    } else {
                        echo '<p class="message">Uploaded Test Map count = '.$response['count']."</p>";
                        if (count($response['errors']) > 0) {
                            echo '<div class="error">LDR found the following error(s):<ul>';
                            foreach ($response['errors'] as $key => $errors) {
                                if (array_key_exists('error', $errors)) {
                                    echo "<li>Error [".$errors['error']."] [".$errors['logid']."] => ".
                                        $errors['detail']."</li>";
                                } else {
                                    foreach ($errors as $err) {
                                        echo "<li>Error [".$err['error']."] [".$err['logid']."] => ".$err['detail'].
                                            "</li>";
                                    }
                                }
                            }
                            echo '</ul></div>';
                        } else {
                            echo "<p class='message'>result => success</p>";
                        }
                    }
                }
            } else {
                displayHeader();
                echo "<p class='error'>Can not read file content</p>";
            }
        } else {
            echo "<p class='error'>File can not exceed 2 MB</p>";
        }
    } else {
        echo "<p class='error'>File must be CSV</p>";
    }
}
displayFooter();
