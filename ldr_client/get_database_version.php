<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'database/version');

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
if ($error)
{
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
}
elseif (isset($_GET['json']))
{
    displayHeader($error);
    displayJsonResponse($response);
    displayFooter();
}
else
{
    // Save the returned token
    $_SESSION['ldr_token'] = $response['token'];

    // Check the database versions against the application software required version
    $productionClass = ($response['productionVersion'] == $response['requiredVersion']) ? 'good' : 'bad';
    // $unitTestClass = ($response['unitTestVersion'] == $response['requiredVersion']) ? 'good' : 'bad';

    // Set the database status for CI
    if ($productionClass == 'bad') // || $unitTestClass == 'bad')
        $databaseVersionError = true;
    else
        $databaseVersionError = false;
    displayHeader($error, ['databaseVersionError' => $databaseVersionError]);

    // Display the database versions
    echo '<table class="data">';
    echo '<td>Required Database Version</td><td class="good">' . $response['requiredVersion'] . '</td>';
    echo '<td>Production Database Version</td><td class="' . $productionClass . '">' . $response['productionVersion'] . '</td>';
    // echo '<td>Unit Test Database Version</td><td class="' . $unitTestClass . '">' . $response['unitTestVersion'] . '</td>';
    echo '</table>';
    echo '<br />';
    displayFooter();
}
