<?php

require_once 'ldr_client.php';

session_start();

define('SRC_DIR', 'process/');

// Check that the user has been authenticated
if (!isset($_SESSION['ldr_token']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Please log in first');
    displayFooter();
    exit;
}

// Display the service list files
displayHeader(false);
echo '<p class="message">';
foreach (glob(SRC_DIR . '*.json') as $filename)
    echo basename($filename) . '<br />';
echo '</p>';
displayFooter();
