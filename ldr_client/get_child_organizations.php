<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['organizationId']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('organizationId value is required in the query string');
    displayFooter();
    exit;
}

if (!isset($_GET['json']))
{
    // Get the organization
    $resource = 'organization/' . $_GET['organizationId'];
    $organization = callLDR('GET', $resource);
    if (array_key_exists('error', $organization))
    {
        $error = true;
        displayHeader($error);
        displayErrorResponse($organization);
        displayFooter();
        exit;
    }
}

// Prepare the resource path
$resource = 'organizations/' . $_GET['organizationId'];

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $organizations = $response['organizations'];

        // Prepend the parent organization to the child organization data
        array_unshift($organizations, $organization);

        // Display the parent organization and child organizations
        foreach ($organizations as $organization)
        {
            switch ($organization['organizationType'])
            {
                case ORG_TYPE_SCHOOL:
                    echo '<p class="school">ORG_TYPE_SCHOOL [';
                    break;
                case ORG_TYPE_DISTRICT:
                    echo '<p class="district">ORG_TYPE_DISTRICT [';
                    break;
                case ORG_TYPE_STATE:
                    echo '<p class="state">ORG_TYPE_STATE [';
                    break;
                case ORG_TYPE_ROOT:
                    echo '<p class="root">ORG_TYPE_ROOT [';
                    break;
            }

            echo $organization['organizationId'] . '] => (' . $organization['organizationIdentifier'] . ') |' .
                 $organization['organizationName'] . '| {' . $organization['studentCount'] . '}';

            // Don't show the child organization count for schools (always zero)
            if ($organization['organizationType'] != ORG_TYPE_SCHOOL)
                  echo ' {' . $organization['childCount'] . '}';

            echo '</p>';
        }
    }
}
displayFooter();
