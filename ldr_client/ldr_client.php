<?php

require_once '../ldr_common/definitions.php';
require_once 'config.php';

$organizationTypes =
[
    ORG_TYPE_SCHOOL => 'ORG_TYPE_SCHOOL',
    'ORG_TYPE_SCHOOL' => ORG_TYPE_SCHOOL,
    ORG_TYPE_DISTRICT => 'ORG_TYPE_DISTRICT',
    'ORG_TYPE_DISTRICT' => ORG_TYPE_DISTRICT,
    ORG_TYPE_STATE => 'ORG_TYPE_STATE',
    'ORG_TYPE_STATE' => ORG_TYPE_STATE,
    ORG_TYPE_ROOT => 'ORG_TYPE_ROOT',
    'ORG_TYPE_ROOT' => ORG_TYPE_ROOT
];

$totalRequestTimeElapsed = 0.0;

function callLDR($method, $resource, $args = null)
{
    global $totalRequestTimeElapsed;

    // Check if any arguments need to be modified
    if (is_array($args) && $method != 'GET')
    {
        if (array_key_exists('redisKeys', $args))
        {
            $redisKeys = explode(',', $args['redisKeys']);
            $args['redisKeys'] = $redisKeys;
        }
        if (array_key_exists('redisFields', $args))
        {
            $redisFields = explode(',', $args['redisFields']);
            $args['redisFields'] = $redisFields;
        }
        if (array_key_exists('redisValues', $args))
        {
            $redisValues = explode(',', $args['redisValues']);
            $args['redisValues'] = $redisValues;
        }
        if (array_key_exists('studentRecordIds', $args))
        {
            $studentRecordIds = explode(',', $args['studentRecordIds']);
            $args['studentRecordIds'] = $studentRecordIds;
        }
    }

    // Initialize and configure a cURL session
    $ch = curl_init();
    $url = REQUEST_SCHEME . '://' . LDR_HOSTNAME . LDR_RESOURCE_PATH . $resource;
    switch ($method)
    {
        case 'GET':
            $query = '';
            if ($args)
            {
                // GET arguments are placed in the query string
                $query = '?' . http_build_query($args, null, '&', PHP_QUERY_RFC3986);
            }
            curl_setopt($ch, CURLOPT_URL, $url . $query);
            break;
        case 'POST':
        case 'PUT':
        case 'DELETE':
            if ($args)
            {
                // POST/PUT/DELETE arguments are placed as JSON in the request body
                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json; charset=UTF-8"]);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
            }
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_URL, $url);
            break;
        default:
            return formatError('Request method must be GET, POST, PUT, or DELETE');
            break;
    }
    if (isset($_SESSION['ldr_token'])) // Place the LDR session token in the HTTP header
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Token: " . $_SESSION['ldr_token']]);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Needed if server certificate common name (CN) is not the server hostname
    //    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, true);

    // Send the request
    $requestStartTime = microtime(true);
    $jsonResponse = curl_exec($ch);

    // Check the response
    if ($jsonResponse === false)
    {
        $errorText = curl_error($ch);
        $errorCode = curl_errno($ch);
        $errorLDR = new ErrorLDR('', LDR_EC_CURL_ERROR_RESPONSE, 'curl error code ' . $errorCode . ' : ' . $errorText);
        curl_close($ch);
        return $errorLDR;
    }

    // Close the cURL session
    curl_close($ch);

    // Add the request elapsed time
    $requestTimeElapsed = microtime(true) - $requestStartTime;
    $totalRequestTimeElapsed += $requestTimeElapsed;

    // Return the response
    return json_decode($jsonResponse, true);
}

//
// LDR Client support functions
//
function classifySchool($organizationName)
{
    // Classify the school based on the name
    $schoolType = SCHOOL_TYPE_NONE;
    if (strpos($organizationName, 'Primary') !== false)
        $schoolType = SCHOOL_TYPE_PRIMARY;
    else if (strpos($organizationName, 'Elementary') !== false)
        $schoolType = SCHOOL_TYPE_PRIMARY;
    else if (strpos($organizationName, 'Elem') !== false)
        $schoolType = SCHOOL_TYPE_PRIMARY;
    else if (strpos($organizationName, 'Junior High School') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;
    else if (strpos($organizationName, 'Jr High School') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;
    else if (strpos($organizationName, 'Middle/High') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE_HIGH;
    else if (strpos($organizationName, 'Jr/Sr') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE_HIGH;
    else if (strpos($organizationName, 'Jr-Sr') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE_HIGH;
    else if (strpos($organizationName, 'High School') !== false)
        $schoolType = SCHOOL_TYPE_HIGH;
    else if (strpos($organizationName, 'Middle') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;
    else if (strpos($organizationName, 'Junior') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;
    else if (strpos($organizationName, 'Jr') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;
    else if (strpos($organizationName, 'High') !== false)
        $schoolType = SCHOOL_TYPE_HIGH;
    else if (strpos($organizationName, 'Intermediate') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;
    else if (strpos($organizationName, 'School') !== false)
        $schoolType = SCHOOL_TYPE_PRIMARY;
    else if (strpos($organizationName, 'Academy') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;
    else if (strpos($organizationName, 'Center') !== false)
        $schoolType = SCHOOL_TYPE_MIDDLE;

    return $schoolType;
}

function compareResultArrays($expectedResults, $actualResults, $skipFields = null)
{
    foreach ($expectedResults as $key => $expectedValue)
    {
        // Check if this field should be skipped
        if (!is_null($skipFields) && in_array($key, $skipFields))
            continue;

        if (array_key_exists($key, $actualResults))
        {
            if (is_array($expectedValue))
            {
                $diff = compareResultArrays($expectedValue, $actualResults[$key], $skipFields);
                if ($diff !== null)
                    return $diff;
            }
            elseif ($actualResults[$key] != $expectedValue)
            {
                return '<br />' . $key . ' returned value<br />' . '&nbsp;&nbsp;&nbsp;&nbsp;|' .
                       $actualResults[$key] . '|<br />does not match expected value<br />&nbsp;&nbsp;&nbsp;&nbsp;|' .
                       $expectedValue . '|<br />';
            }
        }
        else
        {
            return $key . ' value not found';
        }
    }

    return null;
}

function displayFooter()
{
?>
</body>
</html>
<?php
}

function displayHeader($error, $options = [])
{
    if (session_status() == PHP_SESSION_ACTIVE)
    {
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="ldr_client.css" />
        </head>
        <body <?php
            if (array_key_exists('focus', $options))
                echo 'onload="document.main.' . $options['focus'] . '.focus();"';
        ?>>
        <?php
            if ($error)
                showStatusFail();
            else
                showStatusOkay();

            if (array_key_exists('databaseVersionError', $options))
            {
                if ($options['databaseVersionError'])
                    showDatabaseVersionsFail();
                else
                    showDatabaseVersionsOkay();
            }

            if (isset($_SESSION['ldr_token']))
            {
                ?>
                <table class="header">
                    <tr>
                        <th>LDR Client</th>
                        <th>
                            <a href="logout_user.php">LOG OUT</a>
                        </th>
                    </tr>
                </table>
                <?php
            }
    }
    else
    {
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="ldr_client.css" />
        </head>
        <body>
        <?php
            if ($error)
                showStatusFail();
            else
                showStatusOkay();
    }
}

function displayErrorMessage($message)
{
    echo '<p class="error">';
    echo $message;
    echo '</p>';
}

function displayErrorResponse($response)
{
    echo '<p class="error">';
    if (is_array($response))
    {
        foreach ($response as $key => $value)
            echo $key . ' => ' . $value . '<br />';
    }
    else
        echo $response;
    echo '</p>';
}

function displayErrorsResponse($errors)
{
    $indent = '&nbsp;&nbsp;&nbsp;&nbsp;';

    echo '<p class="error">';
    foreach ($errors as $reference => $error)
    {
        echo $reference . ' =><br />';
        foreach ($error as $key => $value)
            echo $indent . $key . ' => ' . $value . '<br />';
    }
    echo '</p>';
}

function displaySuccessMessage($message)
{
    echo '<p class="success">';
    echo $message;
    echo '</p>';
}

function displayWarningMessage($message)
{
    echo '<p class="warning">';
    echo $message;
    echo '</p>';
}

function formatExecutionTime($seconds)
{
    $hours = intval($seconds / SECONDS_PER_HOUR);
    $seconds = $seconds % SECONDS_PER_HOUR;
    $minutes = intval($seconds / 60);
    $seconds = $seconds % 60;

    $executionTime = '';
    if ($hours > 1)
        $executionTime .= $hours . ' hours ';
    else if ($hours == 1)
        $executionTime .= $hours . ' hour ';
    if ($minutes > 1)
        $executionTime .= $minutes . ' minutes ';
    else if ($minutes == 1)
        $executionTime .= $minutes . ' minute ';
    if ($seconds > 1)
        $executionTime .= $seconds . ' seconds';
    else if ($seconds == 1)
        $executionTime .= $seconds . ' second';

    if (strlen($executionTime) == 0)
        $executionTime = "less than 1 second";

    return $executionTime;
}

function displayJsonResponse($response)
{
    echo '<p class="json">';
    echo json_encode($response);
    echo '</p>';
}

function displayMessage($message)
{
    echo '<p class="message">';
    echo $message;
    echo '</p>';
}

function displayOrganization($organization)
{
    echo '<p class="organization">';
    echo $organization;
    echo '</p>';
}

function displayTableDataByIndex($data)
{
	echo '<tr>';
	foreach ($data as $datum)
		echo '<td><nobr>' . $datum . '</nobr></td>';
	echo '</tr>';
}

function displayTableDataByKey($data, $keys)
{
	echo '<tr>';
	foreach ($keys as $key)
    {
        $value = $data[$key];
        if (strpos($key, 'percent') === 0)
            $value .= '%';
        echo '<td class="strong"><nobr>' . $value . '</nobr></td>';

    }
	echo '</tr>';
}

function displayTableDataByKeyBold($data, $keys)
{
    echo '<tr>';
    foreach ($keys as $key)
    {
        $value = $data[$key];
        if (strpos($key, 'percent') === 0)
            $value .= '%';
        echo '<td class="strong"><nobr>' . $value . '</nobr></td>';

    }
    echo '</tr>';
}

function displayTableHeader($headings)
{
	echo '<tr>';
	foreach ($headings as $heading)
		echo '<th>' . $heading . '</th>';
	echo '</tr>';
}

function displayTableKeyValue($key, $value)
{
    echo '<tr>';
    echo '<td><nobr>' . $key . '</nobr></td><td><nobr>' . $value . '</nobr></td>';
    echo '</tr>';
}

function formatError($message)
{
	return ['error' => $message];
}

function roundThreeDecimalPoints($value)
{
    return intval(($value * 1000) + 0.5) / 1000;
}

function showDatabaseVersionsFail()
{
    echo '<p class="status">DATABASES: FAIL</p>';
}

function showDatabaseVersionsOkay()
{
    echo '<p class="status">DATABASES: OKAY</p>';
}

function showStatusFail()
{
    echo '<p class="status">STATUS: FAIL</p>';
}

function showStatusOkay()
{
    echo '<p class="status">STATUS: OKAY</p>';
}
