<?php

require_once 'ldr_client.php';

session_start();

// Prepare the resource path
$resource = 'organization';
if (isset($_GET['organizationId']))
    $resource .= '/' . $_GET['organizationId'];

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<table class="data">';
        $columns = ['Column', 'Value'];
        displayTableHeader($columns);
        foreach ($response as $key => $value)
        {
            if ($key == 'organizationType')
                $value = $organizationTypes[$value];
            displayTableKeyValue($key, $value);
        }
        echo '</table>';
    }
}
displayFooter();
