<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['organizationId']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('organizationId value is required in the query string');
    displayFooter();
    exit;
}

if (!isset($_GET['json']))
{
    // Get the organization
    $resource = 'organization/' . $_GET['organizationId'];
    $organization = callLDR('GET', $resource);
    if (array_key_exists('error', $organization))
    {
        $error = true;
        displayHeader($error);
        displayErrorResponse($organization);
        displayFooter();
        exit;
    }
}

// Prepare the resource path
$resource = 'classes/' . $_GET['organizationId'];

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $classes = $response['classes'];

        displayOrganization($organization['organizationName'] . ' (showing ' . sizeof($classes) . ' of ' .
            $response['totalCount'] . ' classes)');

        echo '<table class="data">';
        $columns = ['classId', 'organizationId', 'classIdentifier', 'sectionNumber', 'gradeLevel'];
        displayTableHeader($columns);
        foreach ($classes as $class)
            displayTableDataByKey($class, $columns);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
