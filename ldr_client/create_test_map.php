<?php

require_once 'ldr_client.php';

session_start();

// Display the LDR response
displayHeader();
?>
<form class='fileUpload' method="post" enctype="multipart/form-data" action="upload_test_map.php">
    <table>
        <tr>
            <td colspan="2" style="text-align: left;"><label for="fileUpload">Please upload test maps</label></td>
        </tr>
        <tr>
            <td><input type="file" name="fileUpload" id="fileUpload" onchange="fileValidation()"></td>
            <td><input type="submit" name="submit" id="submit" value="Upload" disabled></td>
        </tr>
    </table>
</form>

<p class="error" id="userMessage" style="display: none;">Error message placeholder</p>
<script type="text/javascript">
function fileValidation()
{
    try {
        var input = document.getElementById('fileUpload');
        var file = input.files[0];

        if (file.size > 2000 * 1024 * 1024) {
            throw "File is larger than 2 MB";
        }

        var extension = file.name.split('.');
        extension = extension[extension.length - 1];
        if (extension != 'csv' && file.type != 'text/csv') {
            throw "File must be a CSV file";
        }

        userMessage('File is good for upload');
    } catch (err) {
        userMessage(err, true);
    }

}

function userMessage(msg, error = false)
{
    var msgHolder = document.getElementById('userMessage');

    msgHolder.innerHTML = msg;
    msgHolder.style.display = 'block';
    if (error == true) {
        msgHolder.className = 'error';
        document.getElementById('submit').disabled = true;
    } else {
        msgHolder.className = 'success';
        document.getElementById('submit').disabled = false;
    }
}
</script>
<?php
displayFooter();
