<?php

require_once 'ldr_client.php';

define('SRC_DIR', 'process/');

set_time_limit(0);

session_start();

// Get required parameters
if (!isset($_GET['classId']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the class ID - e.g. classId=15');
    displayFooter();
    exit;
}
if (!isset($_GET['studentCount']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the student count - e.g. studentCount=20');
    displayFooter();
    exit;
}

// Get the class organization ID and grade level
$resource = 'class/' . $_GET['classId'];
$response = callLDR('GET', $resource);
if (array_key_exists('error', $response))
{
    $error = true;
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
    exit;
}

// Get the students in the organization at the grade level
$resource = 'student-records/' . $response['organizationId'];
$args = ['gradeLevel' => $response['gradeLevel']];
$response = callLDR('GET', $resource, $args);
if (array_key_exists('error', $response))
{
    $error = true;
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
    exit;
}

// Build a list of the student IDs for the grade level students
$gradeStudentIds = [];
foreach ($response['studentRecords'] as $studentRecord)
    $gradeStudentIds[] = $studentRecord['studentRecordId'];

// Get the students in the class
$resource = 'class-students/' . $_GET['classId'];
$response = callLDR('GET', $resource, $_GET);
if (array_key_exists('error', $response))
{
    $error = true;
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
    exit;
}

// Build a list of the student IDs for the class students
$classStudentIds = [];
foreach ($response as $studentRecord)
    $classStudentIds[] = $studentRecord['studentRecordId'];

// Check how many students can be added to the class
$availableStudentCount = sizeof($gradeStudentIds) - sizeof($classStudentIds);
if ($availableStudentCount < $_GET['studentCount'])
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Only ' . $availableStudentCount . ' grade-level students are available to assign to the class');
    displayFooter();
    exit;
}

// Generate a random list of students to add to the class by selecting random student IDs from the grade-level students
// and checking that the student is not already in the class or in the list of students to be added to the class
$studentId = 0;
$studentIds = [];
$maxGradeStudentsIndex = sizeof($gradeStudentIds) - 1;
for ($count = 0; $count < $_GET['studentCount']; $count++)
{
    $found = false;
    while (!$found)
    {
        $studentId = $gradeStudentIds[mt_rand(0, $maxGradeStudentsIndex)];
        if (!in_array($studentId, $classStudentIds) && !in_array($studentId, $studentIds))
            $found = true;
    }
    $studentIds[] = $studentId;
}

// Open the output file
$filename = SRC_DIR . 'create_' . $_GET['studentCount'] . '_class_' . $_GET['classId'] . '_class_students.json';
$fout = fopen($filename, 'wb');
if ($fout === false)
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Unable to open file ' . $filename);
    displayFooter();
    exit;
}

// Write the output file header
$out = '# Create ' . $_GET['studentCount'] . ' class students for classId ' . $_GET['classId'] . "\n";
if (fwrite($fout, $out) === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to write header to file ' . $filename);
    displayFooter();
    fclose($fout);
    exit;
}

// Write the CreateClassStudents command
$command =
    [
        'command' => 'CreateClassStudents',
        'clientUserId' => '1',
        'classId' => $_GET['classId'],
        'studentRecordIds' => join(',', $studentIds)
    ];
$jsonCommand = json_encode($command) . "\n";
if (fwrite($fout, $jsonCommand) === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to write command to file ' . $filename);
    displayFooter();
    fclose($fout);
    exit;
}

displayHeader(false);
displayMessage('File ' . $filename . ' written successfully');
displayFooter();
fclose($fout);

