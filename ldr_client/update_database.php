<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('PUT', 'database');

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
if ($error)
{
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
}
else
{
    // Display the database versions
    header("Location: get_database_version.php");
}
