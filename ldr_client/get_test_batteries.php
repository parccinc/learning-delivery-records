<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'test-batteries', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        displayOrganization( 'Showing ' . sizeof($response) . ' test batteries');

        echo '<table class="data">';
        $columns = ['testBatteryId', 'formCount', 'testBatteryName', 'testBatterySubject', 'testBatteryGrade',
                    'testBatteryProgram', 'testBatterySecurity', 'testBatteryPermissions', 'testBatteryScoring',
                    'testBatteryDescription'];
        displayTableHeader($columns);
        foreach ($response as $testBattery)
            displayTableDataByKey($testBattery, $columns);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
