<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('POST', 'bulk-test-assignments', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $result = $response['result'];
        echo '<p class="message">Count of newly created Test Assignments = '.$result['testsAssigned'].'</p>';
        echo '<p class="message">Count of classes which need new test assignments = '.$result['classCount'].'</p>';
        echo '<p class="message">Count of available test batteries = '.$result['testBatteriesCount'].'</p>';

        if (count($result['errors']) > 0) {
            echo '<p class="error">';
            foreach ($result['errors'] as $key => $error) {
                if (is_array($error)) {
                    foreach ($error as $k => $e) {
                        echo "Error when assigning testBatteryId: ".$k." to classId: ".$key.". Detail: ".$e['detail'] .'<br/>';
                    }
                } else {
                    echo "Error [".$error['logId']."] for classId: ".$key." => ".$error['detail'] .'<br/>';
                }
            }
            echo '</p>';
        }
    }
}
displayFooter();
