<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['organizationId']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('organizationId value is required in the query string');
    displayFooter();
    exit;
}

if (!isset($_GET['json']))
{
    // Get the organization
    $resource = 'organization/' . $_GET['organizationId'];
    $organization = callLDR('GET', $resource);
    if (array_key_exists('error', $organization))
    {
        $error = true;
        displayHeader($error);
        displayErrorResponse($organization);
        displayFooter();
        exit;
    }
}

// Prepare the resource path
$resource = 'student-records/' . $_GET['organizationId'];

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $studentRecords = $response['studentRecords'];

        displayOrganization($organization['organizationName'] . ' (showing ' . sizeof($studentRecords) . ' of ' .
            $response['totalCount'] . ' student records)');

        $studentColumns = ['studentRecordId', 'stateIdentifier', 'localIdentifier', 'lastName', 'firstName',
                           'middleName', 'dateOfBirth', 'gender', 'enrollOrgId', 'gradeLevel'];
        $classColumns = ['classId', 'organizationId', 'classIdentifier', 'sectionNumber', 'gradeLevel'];

        // Check if display of classes was requested
        if (isset($_GET['withClasses']) && (strtoupper($_GET['withClasses']) == 'Y'))
        {
            foreach ($studentRecords as $studentRecord)
            {
                // Display the student record
                echo '<table class="data">';
                displayTableHeader($studentColumns);
                displayTableDataByKey($studentRecord, $studentColumns);
                echo '</table>';

                // Display the student's classes
                if (sizeof($studentRecord['classes']) > 0)
                {
                    echo '<table class="class">';
                    displayTableHeader($classColumns);
                    foreach ($studentRecord['classes'] as $class)
                        displayTableDataByKey($class, $classColumns);
                    echo '</table>';
                }

                echo '<br />';
            }
        }
        else
        {
            echo '<table class="data">';
            displayTableHeader($studentColumns);
            foreach ($studentRecords as $studentRecord)
                displayTableDataByKey($studentRecord, $studentColumns);
            echo '</table>';
            echo '<br />';
        }
    }
}
displayFooter();
