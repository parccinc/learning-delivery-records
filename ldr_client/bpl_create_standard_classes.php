<?php

require_once 'ldr_client.php';

define('SRC_DIR', 'process/');

set_time_limit(0);

session_start();

// Get required parameters
if (!isset($_GET['organizationId']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the organization ID - e.g. organizationId=15');
    displayFooter();
    exit;
}

// Check that the organization is a school or institution
$resource = 'organization/' . $_GET['organizationId'];
$organization = callLDR('GET', $resource, $_GET);
$error = array_key_exists('error', $organization);
if ($error)
{
    displayHeader($error);
    displayErrorResponse($organization);
    displayFooter();
    exit;
}
if ($organization['organizationType'] != ORG_TYPE_SCHOOL)
{
    displayHeader(true);
    displayErrorMessage('Organization must be a school');
    displayFooter();
    exit;
}

// Get the school type
$schoolType = classifySchool($organization['organizationName']);
if ($schoolType == SCHOOL_TYPE_NONE)
{
    displayHeader(true);
    displayErrorMessage('Unable to determine the school type from the organization name');
    displayFooter();
    exit;
}

$primarySchoolClasses =
[
    ['', 'Ms. Baker', '1'],
    ['', 'Mrs. Taylor', '1'],
    ['', 'Mrs. Jackson', '2'],
    ['', 'Ms. Edelson', '2'],
    ['', 'Mr. Thompson', '3'],
    ['', 'Mrs. Kincaid', '3'],
    ['', 'Mrs. Bracken', '4'],
    ['', 'Mrs. Faulkner', '4'],
    ['', 'Mr. Moss', '5'],
    ['', 'Mrs. Stevenson', '5'],
    ['', 'Mr. Harrison', '6'],
    ['', 'Mr. Vincent', '6']
];

$middleSchoolClasses =
[
    ['ELA 101', 'English / Language Arts 1', '7'],
    ['ELA 102', 'English / Language Arts 1', '7'],
    ['ELA 103', 'English / Language Arts 1', '7'],
    ['ELA 104', 'English / Language Arts 1', '7'],
    ['ELA 201', 'English / Language Arts 2', '7'],
    ['ELA 202', 'English / Language Arts 2', '7'],
    ['ELA 203', 'English / Language Arts 2', '7'],
    ['ELA 204', 'English / Language Arts 2', '7'],
    ['ELA 301', 'English / Language Arts 3', '8'],
    ['ELA 302', 'English / Language Arts 3', '8'],
    ['ELA 303', 'English / Language Arts 3', '8'],
    ['ELA 401', 'English / Language Arts 4', '8'],
    ['ELA 402', 'English / Language Arts 4', '8'],
    ['ELA 403', 'English / Language Arts 4', '8'],
    ['MATH 010', 'Interactive Mathematics', '7'],
    ['MATH 011', 'Interactive Mathematics', '7'],
    ['MATH 012', 'Interactive Mathematics', '8'],
    ['MATH 020', 'Beginning Algebra', '7'],
    ['MATH 021', 'Beginning Algebra', '7'],
    ['MATH 022', 'Beginning Algebra', '7'],
    ['MATH 030', 'Basics of Geometry', '7'],
    ['MATH 031', 'Basics of Geometry', '7'],
    ['MATH 032', 'Basics of Geometry', '7'],
    ['MATH 040', 'Intermediate Algebra', '8'],
    ['MATH 041', 'Intermediate Algebra', '8'],
    ['MATH 050', 'Intermediate Geometry', '8'],
    ['MATH 051', 'Intermediate Geometry', '8']
];

$highSchoolClasses =
[
    ['ENG 101', 'Freshman Language & Composition', '9'],
    ['ENG 102', 'Freshman Language & Composition', '9'],
    ['ENG 103', 'Freshman Language & Composition', '9'],
    ['ENG 104', 'Freshman Language & Composition', '9'],
    ['ENG 105', 'Freshman Language & Composition', '9'],
    ['ENG 106', 'Freshman Language & Composition', '9'],
    ['ENG 110', 'American Literature 1', '9'],
    ['ENG 111', 'American Literature 1', '9'],
    ['ENG 112', 'American Literature 1', '9'],
    ['ENG 113', 'American Literature 1', '9'],
    ['ENG 201', 'Sophmore Language & Composition', '10'],
    ['ENG 202', 'Sophmore Language & Composition', '10'],
    ['ENG 203', 'Sophmore Language & Composition', '10'],
    ['ENG 204', 'Sophmore Language & Composition', '10'],
    ['ENG 210', 'American Literature 2', '10'],
    ['ENG 211', 'American Literature 2', '10'],
    ['ENG 212', 'American Literature 2', '10'],
    ['ENG 213', 'American Literature 2', '10'],
    ['ENG 301', 'Junior Language & Composition', '11'],
    ['ENG 302', 'Junior Language & Composition', '11'],
    ['ENG 303', 'Junior Language & Composition', '11'],
    ['ENG 310', 'World Literature 1', '11'],
    ['ENG 311', 'World Literature 1', '11'],
    ['ENG 312', 'World Literature 1', '11'],
    ['ENG 401', 'Senior Language & Composition', '12'],
    ['ENG 402', 'Senior Language & Composition', '12'],
    ['ENG 403', 'Senior Language & Composition', '12'],
    ['ENG 410', 'World Literature 2', '12'],
    ['ENG 411', 'World Literature 2', '12'],
    ['ENG 412', 'World Literature 2', '12'],
    ['MATH 101', 'Fundamentals of Mathematics', '9'],
    ['MATH 102', 'Fundamentals of Mathematics', '9'],
    ['MATH 103', 'Fundamentals of Mathematics', '9'],
    ['MATH 104', 'Fundamentals of Mathematics', '9'],
    ['MATH 105', 'Fundamentals of Mathematics', '9'],
    ['MATH 106', 'Fundamentals of Mathematics', '9'],
    ['MATH 110', 'Algebra 1', '9'],
    ['MATH 111', 'Algebra 1', '9'],
    ['MATH 112', 'Algebra 1', '9'],
    ['MATH 113', 'Algebra 1', '9'],
    ['MATH 114', 'Algebra 1', '9'],
    ['MATH 120', 'Algebra 2', '10'],
    ['MATH 121', 'Algebra 2', '10'],
    ['MATH 122', 'Algebra 2', '10'],
    ['MATH 123', 'Algebra 2', '10'],
    ['MATH 124', 'Algebra 2', '10'],
    ['MATH 230', 'Geometry 1', '9'],
    ['MATH 231', 'Geometry 1', '9'],
    ['MATH 232', 'Geometry 1', '9'],
    ['MATH 240', 'Geometry 2', '10'],
    ['MATH 241', 'Geometry 2', '10'],
    ['MATH 350', 'Trigonometry 1', '11'],
    ['MATH 351', 'Trigonometry 1', '11'],
    ['MATH 360', 'Trigonometry 2', '12'],
    ['MATH 361', 'Trigonometry 2', '12'],
    ['MATH 421', 'Honors Algebra 2', '11'],
    ['MATH 440', 'Honors Geometry 2', '11'],
    ['MATH 570', 'AP Calculus AB', '12'],
    ['MATH 580', 'AP Calculus BC', '12']
];

// Open the output file
$filename = SRC_DIR . 'create_org_' . $_GET['organizationId'] . '_standard_classes.json';
$fout = fopen($filename, 'wb');
if ($fout === false)
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Unable to open file ' . $filename);
    displayFooter();
    exit;
}

// Write the output file header
$out = '# Create standard classes for organizationId ' . $_GET['organizationId'] . "\n";
if (fwrite($fout, $out) === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to write header to file ' . $filename);
    displayFooter();
    fclose($fout);
    exit;
}

// Determine the list of classes to add based on the school type
switch ($schoolType)
{
    case SCHOOL_TYPE_PRIMARY:
        $classes = $primarySchoolClasses;
        break;
    case SCHOOL_TYPE_MIDDLE:
        $classes = $middleSchoolClasses;
        break;
    case SCHOOL_TYPE_HIGH:
        $classes = $highSchoolClasses;
        break;
    case SCHOOL_TYPE_MIDDLE_HIGH:
        $classes = array_merge($middleSchoolClasses, $highSchoolClasses);
        break;
    default:
        $classes = [];
        break;
}

// Generate the classes
foreach ($classes as $class)
{
    $command =
    [
        'command' => 'CreateClass',
        'clientUserId' => '1',
        'organizationId' => $_GET['organizationId'],
        'classIdentifier' => $class[1],
        'sectionNumber' => $class[0],
        'gradeLevel' => $class[2]
    ];
    $jsonCommand = json_encode($command) . "\n";

    if (fwrite($fout, $jsonCommand) === false)
    {
        displayHeader(true);
        displayErrorMessage('Unable to write command to file ' . $filename);
        displayFooter();
        fclose($fout);
        exit;
    }
}

displayHeader(false);
displayMessage('File ' . $filename . ' written successfully');
displayFooter();
fclose($fout);
