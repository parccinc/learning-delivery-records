<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'GetTableRecord', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        // Display the record
        echo '<p class="organization">' . $_GET['tableName'] . '</p>';

        echo '<table class="data">';
        $columns = ['Column', 'Value'];
        displayTableHeader($columns);
        foreach ($response as $key => $value)
        {
            displayTableKeyValue($key, $value);
        }
        echo '</table>';
    }
}
displayFooter();
