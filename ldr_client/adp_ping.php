<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'adp-ping');

// Check the LDR response
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<table class="data">';
        foreach ($response as $key => $value)
            displayTableKeyValue($key, $value);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
