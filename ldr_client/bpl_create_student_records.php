<?php

require_once 'ldr_client.php';

define('SRC_DIR', 'process/');

set_time_limit(0);

session_start();

// Get required parameters
if (!isset($_GET['organizationId']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the organization ID - e.g. organizationId=15');
    displayFooter();
    exit;
}
if (!isset($_GET['studentCount']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the student count - e.g. studentCount=200');
    displayFooter();
    exit;
}

// Get optional parameters
if (isset($_GET['gradeLevel']))
    $gradeLevel = $_GET['gradeLevel'];
else
    $gradeLevel = null;

// Open the output file
$filename = SRC_DIR . 'create_' . $_GET['studentCount'];
if ($gradeLevel !== null)
    $filename .= '_grade_' . $gradeLevel;
$filename .= '_org_' . $_GET['organizationId'] . '_student_records.json';
$fout = fopen($filename, 'wb');
if ($fout === false)
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Unable to open file ' . $filename);
    displayFooter();
    exit;
}

// Load boy first names
$boysNamesFile = SRC_DIR . 'boysNames.txt';
$boysNames = file($boysNamesFile);
if ($boysNames === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to load file ' . $boysNamesFile);
    displayFooter();
    fclose($fout);
    exit;
}
$countBoysNames = sizeof($boysNames);

// Load girl first names
$girlsNamesFile = SRC_DIR . 'girlsNames.txt';
$girlsNames = file($girlsNamesFile);
if ($girlsNames === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to load file ' . $girlsNamesFile);
    displayFooter();
    fclose($fout);
    exit;
}
$countGirlsNames = sizeof($girlsNames);

// Load last names
$lastNamesFile = SRC_DIR . 'lastNames.txt';
$lastNames = file($lastNamesFile);
if ($lastNames === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to load file ' . $lastNamesFile);
    displayFooter();
    fclose($fout);
    exit;
}
$countLastNames = sizeof($lastNames);

// Define the disability types
$disabilityTypes = ['AUT', 'DB', 'DD', 'EMN', 'HI', 'ID', 'MD', 'OHI', 'OI', 'SLD', 'SLI', 'TBI', 'VI'];
$countDisabilityTypes = sizeof($disabilityTypes);

// Save the start time
$timeStart = time();

// Write the output file header
$out = '# Create ' . $_GET['studentCount'];
if ($gradeLevel !== null)
    $out .= ' grade ' . $gradeLevel;
$out .= ' student records for organizationId ' . $_GET['organizationId'] . "\n";
if (fwrite($fout, $out) === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to write header to file ' . $filename);
    displayFooter();
    fclose($fout);
    exit;
}

// Get the organization name
$resource = 'organization/' . $_GET['organizationId'];
$response = callLDR('GET', $resource);
$error = array_key_exists('error', $response);
if ($error)
{
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
    fclose($fout);
    exit;
}

// Determine the school type from the organization name
$schoolType = classifySchool($response['organizationName']);
if ($schoolType == SCHOOL_TYPE_NONE)
{
    displayHeader(true);
    displayErrorMessage('Unable to determine the school type for organization ' . $response['organizationName']);
    displayFooter();
    fclose($fout);
    exit;
}

// Write the CreateStudentRecord commands
$isBoy = false;
for ($count = 0; $count < $_GET['studentCount']; $count++)
{
    // Generate a student state identifier
    $stateIdentifier = mt_rand(100000000, 999999999);

    // Generate a last name
    $lastName = $lastNames[mt_rand(1, $countLastNames) - 1];
    $lastName = trim($lastName);

    // Generate a first name
    if ($isBoy)
        $firstName = $boysNames[mt_rand(1, $countBoysNames) - 1];
    else
        $firstName = $girlsNames[mt_rand(1, $countGirlsNames) - 1];
    $firstName = trim($firstName);

    // Generate a middle name
    $middleName = $lastNames[mt_rand(1, $countLastNames) - 1];
    $middleName = trim($middleName);

    // If a grade level was not specified then generate a random one based on the school type
    $thisGradeLevel = $gradeLevel;
    if ($thisGradeLevel === null)
    {
        switch ($schoolType)
        {
            case SCHOOL_TYPE_PRIMARY:
                $thisGradeLevel = mt_rand(1, 6);
                break;
            case SCHOOL_TYPE_MIDDLE:
                $thisGradeLevel = mt_rand(7, 8);
                break;
            case SCHOOL_TYPE_HIGH:
                $thisGradeLevel = mt_rand(9, 12);
                break;
            case SCHOOL_TYPE_MIDDLE_HIGH:
                $thisGradeLevel = mt_rand(7, 12);
                break;
        }
    }

    // Generate a DOB
    $studentAge = $thisGradeLevel + 5;
    $studentAge *= 365; // Convert age from years to days
    $studentAge += (mt_rand(1, 365) - 182); // Randomize within +/- six months
    $dateOfBirth = date('Y-m-d', time() - $studentAge * SECONDS_PER_DAY);

    // Generate the gender
    $gender = $isBoy ? 'M' : 'F';

    // Generate the race values
    $raceAA = $raceAN = $raceAS = $raceHL = $raceHP = $raceWH = 'N';
    $race = mt_rand(1, 1000);
    if ($race < 550)
        $raceWH = 'Y';
    elseif ($race < 700)
        $raceAA = 'Y';
    elseif ($race < 850)
        $raceHL = 'Y';
    elseif ($race < 940)
        $raceAS = 'Y';
    elseif ($race < 970)
        $raceAN = 'Y';
    else
        $raceHP = 'Y';

    // Generate the status values
    $disabilityType = '';
    $statusDIS = $statusECO = $statusELL = $statusGAT = $statusLEP = $statusMIG = 'N';
    if (mt_rand(1, 1000) < 40)
    {
        $statusDIS = 'Y';
        $disabilityType = $disabilityTypes[mt_rand(1, $countDisabilityTypes) - 1];
    }
    if (mt_rand(1, 1000) < 80)
        $statusECO = 'Y';
    if (mt_rand(1, 1000) < 160)
        $statusELL = 'Y';
    if (mt_rand(1, 1000) < 30)
        $statusGAT = 'Y';
    if (mt_rand(1, 1000) < 30)
        $statusLEP = 'Y';
    if (mt_rand(1, 1000) < 20)
        $statusMIG = 'Y';

    $command =
        [
            'command' => 'CreateStudentRecord',
            'clientUserId' => '1',
            'enrollOrgId' => $_GET['organizationId'],
            'stateIdentifier' => $stateIdentifier,
            'lastName' => $lastName,
            'firstName' => $firstName,
            'middleName' => $middleName,
            'dateOfBirth' => $dateOfBirth,
            'gender' => $gender,
            'gradeLevel' => $thisGradeLevel,
            'raceAA' => $raceAA,
            'raceAN' => $raceAN,
            'raceAS' => $raceAS,
            'raceHL' => $raceHL,
            'raceHP' => $raceHP,
            'raceWH' => $raceWH,
            'statusDIS' => $statusDIS,
            'statusECO' => $statusECO,
            'statusELL' => $statusELL,
            'statusGAT' => $statusGAT,
            'statusLEP' => $statusLEP,
            'statusMIG' => $statusMIG,
            'disabilityType' => $disabilityType
        ];
    $jsonCommand = json_encode($command) . "\n";

    if (fwrite($fout, $jsonCommand) === false)
    {
        displayHeader(true);
        displayErrorMessage('Unable to write command to file ' . $filename);
        displayFooter();
        fclose($fout);
        exit;
    }

    // Flip the gender
    $isBoy = !$isBoy;
}

displayHeader(false);
displayMessage('File ' . $filename . ' written successfully');
displayFooter();
fclose($fout);
