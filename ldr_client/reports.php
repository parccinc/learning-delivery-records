<?php

require_once 'ldr_client.php';

session_start();

// Display the LDR response
displayHeader($error);
?>
<p class="message">Below is a list of reports that can be generated</p>
<table class="data">
    <tr>
        <th>#</th>
        <th>Title</th>
    </tr>
    <tr>
        <td>1</td>
        <td><a href="<?php echo $curServerHost; ?>/ldr_client/get_detailed_activity_report.php"><strong>Activity Report</strong></a></td>
    </tr>
    <tr>
        <td>2</td>
        <td><a href="<?php echo $curServerHost; ?>/ldr_client/get_school_activity_report.php"><strong>School Activity Report</strong></a></td>
    </tr>
    <tr>
        <td>3</td>
        <td><a href="<?php echo $curServerHost; ?>/ldr_client/generate_data_export.php"><strong>Data Export</strong></a></td>
    </tr>
</table>
<?php
displayFooter();
?>
