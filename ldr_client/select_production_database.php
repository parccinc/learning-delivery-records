<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('PUT', 'database-production');

// Check the LDR response for an error
$error = array_key_exists('error', $response);
if ($error)
{
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
}
else
{
    // Save the returned token
    $_SESSION['ldr_token'] = $response['token'];

    // Display the LDR version
    header("Location: get_version.php");
}
