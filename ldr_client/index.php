<?php

require_once 'ldr_client.php';

session_start();

// Check if the user has already been authenticated
if (isset($_SESSION['ldr_token']))
{
    displayHeader(false);
    displayFooter();
    exit;
}

//
// The user needs to log in
//

if (isset($_SESSION['error_message']) && strlen($_SESSION['error_message']) > 0)
    $error = true;
else
    $error = false;

// Initialize session values
if (!isset($_SESSION['error_message']))
    $_SESSION['error_message'] = '';

if (!isset($_SESSION['prompt_message']))
    $_SESSION['prompt_message'] = '';

if (!isset($_SESSION['clientName']))
    $_SESSION['clientName'] = '';

// Select which field should have focus
if (strlen($_SESSION['clientName']) == 0)
    $focus = "clientName";
else
    $focus = "password";

displayHeader($error, ['focus' => $focus]);
?>
<form method="post" action="login_user.php" name="main">
    <table class="login">
        <tr>
            <th colspan="2">LDR Client</th>
        </tr>
        <tr>
            <td>Client name:</td>
            <td>
                <input type="text" name="clientName" value="<?php echo $_SESSION['clientName']; ?>">
            </td>
        </tr>
        <tr>
            <td>Password:</td>
            <td>
                <input type="password" name="password">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Submit"
            </td>
        </tr>
    </table>
</form>
<?php
// If an error occurred then display it
if (strlen($_SESSION['error_message']) > 0)
{
    displayErrorMessage($_SESSION['error_message']);
    $_SESSION['error_message'] = '';
}

// Display the user prompt
displayMessage("Please enter your username and password");

displayFooter();
