<?php

require_once 'ldr_client.php';

session_start();

// Determine the response format
if (isset($_GET['json']))
    $format = RESPONSE_JSON;
else
    $format = RESPONSE_ARRAY;

// Call the LDR service
$parameters = $_GET;
if (isset($_SESSION['ldr_token']))
    $parameters['token'] = $_SESSION['ldr_token'];
$records = callLDR('CheckColumnValues', $format, $parameters);

// Display the LDR response
if (isset($_SESSION['ldr_token']))
    displayAuthHeader();
else
    displayHeader();
if ($format == RESPONSE_JSON)
    displayJsonResponse($records);
else
{
    if (array_key_exists('error', $records))
        displayErrorResponse($records);
    else
    {
        echo '<p class="message">';
        if (sizeof($records) > 0)
        {
            foreach ($records as $record)
                echo $record . '<br />';
        }
        else
            echo "All records passed the check!";
        echo '</p>';
    }
}
displayFooter();
