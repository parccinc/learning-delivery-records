<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'performance', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        if (isset($_GET['startDate']) && isset($_GET['endDate']))
            echo '<p class="organization">' . $_GET['startDate'] . ' to ' . $_GET['endDate'] . '</p>';
        elseif (isset($_GET['startDate']))
            echo '<p class="organization">' . $_GET['startDate'] . ' and after</p>';
        elseif (isset($_GET['endDate']))
            echo '<p class="organization">' . $_GET['endDate'] . ' and before</p>';

        // Display the performance statistics
        echo '<table class="data">';
        $columns = ['serviceName', 'serviceCount', 'averageElapsedTime', 'maximumElapsedTime', 'minimumElapsedTime',
            'totalElapsedTime'];
        displayTableHeader($columns);
        foreach (array_keys($response['services']) as $key)
        {
            // The serviceName is used as the key for each test summary in the LDR JSON response
            $service = $response['services'][$key];
            $service['serviceName'] = $key;

            // Round the time values
            $service['averageElapsedTime'] = round($service['averageElapsedTime'], 4);
            $service['totalElapsedTime'] = round($service['totalElapsedTime'], 2);
            $service['maximumElapsedTime'] = round($service['maximumElapsedTime'], 4);
            $service['minimumElapsedTime'] = round($service['minimumElapsedTime'], 4);

            displayTableDataByKey($service, $columns);
        }
        echo '</table>';
    }
}
displayFooter();
