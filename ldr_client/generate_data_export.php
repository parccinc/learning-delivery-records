<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'data-export', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else {
    if ($error) {
        displayErrorResponse($response);
    } else {
        $startDate = $response['startDate'];
        $endDate = $response['endDate'];
        $result = $response['report'];
        if (isset($result['fileCount'])) {
            if ($result['fileCount'] == 0) {
                echo '<p class="message">There are no test assignments between: '.$startDate.' AND '.$endDate.'</p>';
            } else {
                echo '<p class="message">There are '.$result['fileCount'].' file for the Data Export between '.$startDate.' AND '.$endDate.':</p>';
                echo '<table class="data">';
                echo '<tr><th>#</th><th>Title</th><th>Download Link</th></tr>';

                unset($result['fileCount']);
                $count = 1;
                foreach ($result as $reportName => $reportURL) {
                    echo '<tr><td>'.$count.'</td><td>'.$reportName.' File</td><td><a href="'.$reportURL.'">Download from HERE</a></td></tr>';
                    $count++;
                }

                echo '</table>';
            }
        }
    }
}
displayFooter();
