<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['classId']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('classId value is required in the query string');
    displayFooter();
    exit;
}

// Prepare the resource path
$resource = 'class-students/' . $_GET['classId'];

// Call the LDR service
$response = callLDR('DELETE', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);
$errors = array_key_exists('errors', $response) && (sizeof($response['errors']) > 0);

// Display the LDR response
displayHeader($error || $errors);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        displaySuccessMessage('studentsDeleted => ' . $response['studentsDeleted']);
        if ($errors)
            displayErrorsResponse($response['errors']);
    }
}
displayFooter();
