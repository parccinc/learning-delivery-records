<?php

require_once 'ldr_client.php';

session_start();

// Determine the response format
if (isset($_GET['json']))
    $format = RESPONSE_JSON;
else
    $format = RESPONSE_ARRAY;

// Prepare the parameters
$parameters = $_GET;
if (isset($_SESSION['ldr_token']))
    $parameters['token'] = $_SESSION['ldr_token'];

// Call the LDR service
$response = callLDR('EditStudent', $format, $parameters);

// Display the LDR response
if (isset($_SESSION['ldr_token']))
    displayAuthHeader();
else
    displayHeader();
if ($format == RESPONSE_JSON)
    displayJsonResponse($response);
else
{
    if (array_key_exists('error', $response))
        displayErrorResponse($response);
    else
    {
        echo '<p class="message">';
        foreach ($response as $key => $value)
            echo $key . ' => ' . $value . '<br />';
        echo '</p>';
    }
}
displayFooter();
