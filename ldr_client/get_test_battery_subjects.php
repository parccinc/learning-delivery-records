<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'test-battery-subjects', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<p class="message">';
        if (sizeof($response) > 0)
        {
            foreach ($response as $table)
                echo $table . '<br />';
        }
        else
            echo "There are no test batteries";
        echo '</p>';
    }
}
displayFooter();
