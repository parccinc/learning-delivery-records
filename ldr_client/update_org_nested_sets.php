<?php

require_once 'ldr_client.php';

set_time_limit(0);

session_start();

// Save the start time
$timeStart = time();

// Call the LDR service
$response = callLDR('PUT', '/organization-nested-sets');

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<p class="message">';
        foreach ($response as $key => $value)
            echo $key . ' => ' . $value . '<br />';
        echo '</p>';
        displayMessage('Command processed in ' . formatExecutionTime(time() - $timeStart));
    }
}
displayFooter();
