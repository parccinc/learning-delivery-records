<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['testFormRevisionIds']))
{
    displayHeader(true);
    displayErrorMessage('testFormRevisionIds value is required in the query string');
    displayFooter();
    exit;
}

// Call the LDR service
$response = callLDR('PUT', 'textEntryInteraction-answer-key', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $errors = $response['errors'];
        if (!empty($errors)) {
            foreach ($errors as $testFormRevisionId => $error) {
                echo '<p class="error">';
                echo "Error occured for TestFormRevisionId: ".$testFormRevisionId." => ERROR ".$error['error']." [".$error['logid']."] ".$error['detail'];
                echo '</p>';
            }
        }

        unset($response['errors']);

        echo '<p class="message">';
        foreach ($response as $key => $value) {
            echo $key . ' => ' . $value . '<br />';
        }
        echo '</p>';
    }
}
displayFooter();
