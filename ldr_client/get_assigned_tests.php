<?php

require_once 'ldr_client.php';

session_start();

$displayText = '';
if (!isset($_GET['json']))
{
    if (isset($_GET['organizationId']))
    {
        // Get the organization
        $resource = 'organization/' . $_GET['organizationId'];
        $organization = callLDR('GET', $resource);
        if (array_key_exists('error', $organization))
        {
            $error = true;
            displayHeader($error);
            displayErrorResponse($organization);
            displayFooter();
            exit;
        }
        $displayText = $organization['organizationName'];
    }
    elseif (isset($_GET['classIds']))
    {
        if (strpos($_GET['classIds'], ',') === false)
        {
            // Get the class
            $resource = 'class/' . $_GET['classIds'];
            $class = callLDR('GET', $resource);
            if (array_key_exists('error', $class))
            {
                $error = true;
                displayHeader($error);
                displayErrorResponse($class);
                displayFooter();
                exit;
            }
            $displayText = $class['classIdentifier'] . ' [' . $class['sectionNumber'] . ']';
        }
        else
            $displayText = "Multiple classes";
    }
}

// Call the LDR service
$response = callLDR('GET', 'assigned-tests', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $testCount = sizeof($response) - 1;
        displayOrganization($displayText . ' (showing ' . $testCount . ' assigned test' .
            (($testCount > 1) ? 's' : '') . ')');

        $testColumns = ['testBatteryId', 'testBatteryName', 'testBatterySubject', 'testBatteryGrade'];
        $countColumns = ['countAssigned', 'countScheduled', 'percentScheduled', 'countInProgress', 'countPaused',
            'countSubmitted', 'percentSubmitted', 'countCanceled', 'percentCanceled',
            'percentSubmittedOrCanceled'];
        foreach ($response as $testAssignment)
        {
            if (strlen($testAssignment['testBatteryId']) > 0)
            {
                echo '<table class="test_desc">';
                displayTableHeader($testColumns);
                displayTableDataByKey($testAssignment, $testColumns);
                echo '</table>';
            }
            else
                displayOrganization('Cumulative Totals for all assigned tests');

            echo '<table class="test_counts">';
            displayTableHeader($countColumns);
            displayTableDataByKey($testAssignment, $countColumns);
            echo '</table>';
            echo '<br />';
        }
    }
}
displayFooter();
