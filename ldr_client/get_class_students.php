<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['classIds']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('classIds value is required in the query string');
    displayFooter();
    exit;
}

$class = null;
$showClass = false;
if (!isset($_GET['json']) && strpos($_GET['classIds'], ',') === false)
{
    // Get the class
    $resource = 'class/' . $_GET['classIds'];
    $class = callLDR('GET', $resource);
    if (array_key_exists('error', $class))
    {
        $error = true;
        displayHeader($error);
        displayErrorResponse($class);
        displayFooter();
        exit;
    }
    $showClass = true;
}

// Call the LDR service
$response = callLDR('GET', 'class-students', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        if ($showClass)
            $classLabel = $class['classIdentifier'] . ' [' . $class['sectionNumber'] . ']';
        else
            $classLabel = 'Multiple classes';
        displayOrganization($classLabel . ' (showing ' . sizeof($response) . ' student records)');

        echo '<table class="data">';
        $columns = ['studentRecordId', 'stateIdentifier', 'localIdentifier', 'lastName', 'firstName',
            'middleName', 'dateOfBirth', 'gender', 'enrollOrgId', 'gradeLevel'];
        displayTableHeader($columns);
        foreach ($response as $studentRecord)
            displayTableDataByKey($studentRecord, $columns);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
