<?php

require_once 'ldr_client.php';

session_start();

// Prepare the resource path
$resource = 'organization-ids/descendent';
if (isset($_GET['organizationId']))
    $resource .= '/' . $_GET['organizationId'];

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        displayMessage('Organization ID count: ' . sizeof($response));

        echo '<p class="message">';
        if (sizeof($response) > 0)
        {
            foreach ($response as $table)
                echo $table . '<br />';
        }
        else
            echo "There are no descendent organizations";
        echo '</p>';
    }
}
displayFooter();
