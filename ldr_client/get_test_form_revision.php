<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['testFormRevisionId']))
{
    displayHeader(true);
    displayErrorMessage('testFormRevisionId value is required in the query string');
    displayFooter();
    exit;
}

// Prepare the resource path
$resource = 'test-form-revision/' . $_GET['testFormRevisionId'];

// Call the LDR service
$response = callLDR('GET', $resource);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<table class="data">';
        $columns = ['Column', 'Value'];
        displayTableHeader($columns);
        foreach ($response as $key => $value)
            displayTableKeyValue($key, $value);
        echo '</table>';
    }
}
displayFooter();
