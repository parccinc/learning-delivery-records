<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['organizationId']))
{
    displayHeader(true);
    displayErrorMessage('organizationId value is required in the query string');
    displayFooter();
    exit;
}

// Prepare the resource path
$resource = 'organization-ids/child/' . $_GET['organizationId'];

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        displayMessage('Organization ID count: ' . sizeof($response));

        echo '<p class="message">';
        if (sizeof($response) > 0)
        {
            foreach ($response as $table)
                echo $table . '<br />';
        }
        else
            echo "There are no child organizations";
        echo '</p>';
    }
}
displayFooter();
