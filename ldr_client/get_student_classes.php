<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['studentRecordId']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('studentRecordId value is required in the query string');
    displayFooter();
    exit;
}

// Prepare the resource path
$resource = 'student-classes/' . $_GET['studentRecordId'];

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<table class="data">';
        $columns = ['classId', 'organizationId', 'classIdentifier', 'sectionNumber', 'gradeLevel'];
        displayTableHeader($columns);
        foreach ($response as $class)
            displayTableDataByKey($class, $columns);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
