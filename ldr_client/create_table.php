<?php

require_once 'ldr_client.php';

session_start();

// Prepare the resource path
$resource = 'table';
if (isset($_GET['name']))
    $resource .= '/' . $_GET['name'];

// Call the LDR service
$response = callLDR('POST', $resource);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<p class="message">';
        foreach ($response as $key => $value)
            echo $key . ' => ' . $value . '<br />';
        echo '</p>';
    }
}
displayFooter();
