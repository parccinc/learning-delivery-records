<?php
/**
 * Created by PhpStorm.
 * User: richard.coleman
 * Date: 3/26/14
 * Time: 2:54 PM
 */

$requestScheme = "https";
if (isset($_SERVER['REQUEST_SCHEME']) === true) {
    $requestScheme = $_SERVER['REQUEST_SCHEME'];
} else {
    $requestScheme = (strcmp($_SERVER['HTTPS'], 'on') === 0 ? "https":"http");
}

$curServerHost = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'];
switch ($curServerHost) {
    case '<FULL URL GOES HERE':
        define('LDR_HOSTNAME', "localhost");
        break;
    default:
        exit(1);
        break;
}

define('REQUEST_SCHEME', "https");
define('LDR_RESOURCE_PATH', "/ldr_svcs/");
