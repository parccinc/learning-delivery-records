<?php

require_once 'ldr_client.php';

set_time_limit(0);

session_start();

define('SRC_DIR', 'process/');

// Check that the user has been authenticated
if (!isset($_SESSION['ldr_token']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Please log in first');
    displayFooter();
    exit;
}

// Open the service list file
if (!isset($_GET['filename']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Please specify the process list file name - e.g. filename=some_file.json');
    displayFooter();
    exit;
}
$fileMain = SRC_DIR . $_GET['filename'];
$fhMain = fopen($fileMain, 'rb');
if ($fhMain === false)
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Unable to open file ' . $fileMain);
    displayFooter();
    exit;
}
$fileCur = $fileMain;
$fhCur = $fhMain;
$mainLine = 0;
$curLine = 0;

// Save the start time
$startTime = microtime(true);

// Process the service list
$args = null;
$method = null;
$resource = null;
$unitTestCount = 0;
$commandsProcessed = 0;
$totalUnitTestCount = 0;
$skipProcessing = false;
$performanceTest = false;
$organizationId = null;
$showSkips = false;
$unitTests = false;
$unitTestName = '';
$jsonLine = null;
$errors = false;
$error = false;
$line = null;

$statesCreated = 0;
$schoolsCreated = 0;
$districtsCreated = 0;
$organizationsCreated = false;
$organizationType = ORG_TYPE_NONE;

$outputStart = '<p class="output">';
$output = $outputStart;
$errorExpected = false;
$actualResults = [];
$errorMessage = '';
$response = [];
$fhSub = null;
$subLine = 0;
while (true)
{
	// Read the next line
    if ($fhCur == $fhMain)
    {
        $mainLine++;
        $curLine = $mainLine;
    }
    elseif ($fhCur == $fhSub)
    {
        $subLine++;
        $curLine = $subLine;
    }
    $jsonLine = fgets($fhCur);
	if ($jsonLine === false)
    {
        // If we have reached the end of the main file we are done
        if ($fhCur == $fhMain)
            break;

        // If we have reached the end of a sub-file then close it and switch back to the main file
        if ($fhCur == $fhSub)
        {
            fclose($fhSub);
            $fhCur = $fhMain;
            $fhSub = null;
            continue;
        }
    }
	if (strlen(trim($jsonLine)) == 0) // Skip blank lines
		continue;
	if ($jsonLine[0] == '#') // Skip comment lines
		continue;

    // Decode the line
    $line = json_decode($jsonLine, true);
    if ($line === null)
    {
        $output .= $fileCur . ' line ' . $curLine . ': Unable to decode ' . $jsonLine . '</br >';
        $error = true;
        break;
    }

	// Check if this line should be skipped
	if ($skipProcessing && $line['command'] != 'SkipBlockEnd')
		continue;

	// Process the command
    $args = null;
    $method = null;
    $prefix = $fileCur . ' line ' . $curLine . ': ' . $line['command'] . ' ';
    $suffix = '<br />' . $unitTestName . ' UNIT TEST FAILED<br />';
	switch ($line['command'])
	{
        case 'CheckErrorResult':
            if (!array_key_exists('error', $line))
            {
                $output .= $prefix . ' is missing a value<br />';
                $error = true;
                break 2;
            }
            if ($actualResults['error'] != $line['error'])
            {
                $errorMessage = $prefix . 'failed<br />|error code ' . $actualResults['error'] .
                    '|<br />&nbsp;&nbsp;&nbsp;&nbsp;does not match expected<br />|error code ' .
                    $line['error'] . '|' . $suffix;
                $error = true;
                break 2;
            }
            if (array_key_exists('detail', $line))
            {
                if ($actualResults['detail'] != $line['detail'])
                {
                    $errorMessage = $prefix . 'failed<br />|' . $actualResults['detail'] .
                        '|<br />&nbsp;&nbsp;&nbsp;&nbsp;does not match expected error<br />|' .
                        $line['detail'] . '|' . $suffix;
                    $error = true;
                    break 2;
                }
            }
            $unitTestCount++;
            break;
        case 'CompareResultFile':
            if (!array_key_exists('filename', $line))
            {
                $output .= $prefix . ' is missing a value<br />';
                $error = true;
                break 2;
            }
            $jsonReference = file_get_contents(SRC_DIR . $line['filename']);
            if ($jsonReference === false)
            {
                $output .= $prefix . ' unable to load ' . $line['filename'] . $suffix;
                $error = true;
                break 2;
            }
            $expectedResults = json_decode($jsonReference, true);
            if ($expectedResults === null)
            {
                $output .= $prefix . ' unable to decode ' . $line['filename'] . $suffix;
                $error = true;
                break 2;
            }
            $skipFields = [];
            if (array_key_exists('skipFields', $line))
                $skipFields = explode(',', $line['skipFields']);
            $diff = compareResultArrays($expectedResults, $actualResults, $skipFields);
            if ($diff !== null)
            {
                $errorMessage = $prefix . $diff . $suffix;
                $error = true;
                break 2;
            }
            $unitTestCount++;
            break;
        case 'CreateAllTables':
            $resource = 'tables';
            $method = 'POST';
            break;
        case 'CreateClass':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];

            if (array_key_exists('organizationId', $line))
                $args['organizationId'] = $line['organizationId'];
            elseif ($organizationId !== null)
                $args['organizationId'] = $organizationId;
            if (array_key_exists('stateCode', $line))
                $args['stateCode'] = $line['stateCode'];
            if (array_key_exists('organizationIdentifier', $line))
                $args['organizationIdentifier'] = $line['organizationIdentifier'];

            if (array_key_exists('classIdentifier', $line))
                $args['classIdentifier'] = $line['classIdentifier'];
            if (array_key_exists('gradeLevel', $line))
                $args['gradeLevel'] = $line['gradeLevel'];
            if (array_key_exists('sectionNumber', $line))
                $args['sectionNumber'] = $line['sectionNumber'];
            $resource = 'class';
            $method = 'POST';
            break;
        case 'CreateClassStudents':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('studentRecordIds', $line))
                $args['studentRecordIds'] = $line['studentRecordIds'];
            $resource = 'class-students/';
            if (array_key_exists('classId', $line))
                $resource .= $line['classId'];
            $method = 'POST';
            break;
		case 'CreateOrganization':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('organizationName', $line))
                $args['organizationName'] = $line['organizationName'];

            if (array_key_exists('organizationType', $line))
            {
                $args['organizationType'] = $line['organizationType'];
                $organizationType = $line['organizationType'];
            }

            if (array_key_exists('parentOrganizationId', $line))
                $args['parentOrganizationId'] = $line['parentOrganizationId'];
            elseif ($organizationId !== null)
                $args['parentOrganizationId'] = $organizationId;

            if (array_key_exists('organizationIdentifier', $line))
                $args['organizationIdentifier'] = $line['organizationIdentifier'];

            if (array_key_exists('parentOrganizationIdentifier', $line))
                $args['parentOrganizationIdentifier'] = $line['parentOrganizationIdentifier'];

            if (array_key_exists('parentStateCode', $line))
                $args['parentStateCode'] = $line['parentStateCode'];
            $resource = 'organization';
            $method = 'POST';
			break;
        case 'CreateStudentRecord':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];

            if (array_key_exists('enrollStateCode', $line) || array_key_exists('enrollSchoolIdentifier', $line))
            {
                if (array_key_exists('enrollStateCode', $line))
                    $args['enrollStateCode'] = $line['enrollStateCode'];
                if (array_key_exists('enrollSchoolIdentifier', $line))
                    $args['enrollSchoolIdentifier'] = $line['enrollSchoolIdentifier'];
            }
            elseif (array_key_exists('enrollOrgId', $line))
                $args['enrollOrgId'] = $line['enrollOrgId'];
            elseif ($organizationId !== null)
                $args['enrollOrgId'] = $organizationId;
            if (array_key_exists('enrollDistrictIdentifier', $line))
                $args['enrollDistrictIdentifier'] = $line['enrollDistrictIdentifier'];

            if (array_key_exists('testingStateCode', $line) || array_key_exists('testingSchoolIdentifier', $line))
            {
                if (array_key_exists('testingStateCode', $line))
                    $args['testingStateCode'] = $line['testingStateCode'];
                if (array_key_exists('testingSchoolIdentifier', $line))
                    $args['testingSchoolIdentifier'] = $line['testingSchoolIdentifier'];
            }
            elseif (array_key_exists('testingOrgId', $line))
                $args['testingOrgId'] = $line['testingOrgId'];

            if (array_key_exists('stateIdentifier', $line))
                $args['stateIdentifier'] = $line['stateIdentifier'];
            if (array_key_exists('localIdentifier', $line))
                $args['localIdentifier'] = $line['localIdentifier'];
            if (array_key_exists('globalUniqueIdentifier', $line))
                $args['globalUniqueIdentifier'] = $line['globalUniqueIdentifier'];

            if (array_key_exists('lastName', $line))
                $args['lastName'] = $line['lastName'];
            if (array_key_exists('firstName', $line))
                $args['firstName'] = $line['firstName'];
            if (array_key_exists('middleName', $line))
                $args['middleName'] = $line['middleName'];
            if (array_key_exists('dateOfBirth', $line))
                $args['dateOfBirth'] = $line['dateOfBirth'];
            if (array_key_exists('gender', $line))
                $args['gender'] = $line['gender'];

            if (array_key_exists('optionalStateData1', $line))
                $args['optionalStateData1'] = $line['optionalStateData1'];
            if (array_key_exists('gradeLevel', $line))
                $args['gradeLevel'] = $line['gradeLevel'];
            if (array_key_exists('raceAA', $line))
                $args['raceAA'] = $line['raceAA'];
            if (array_key_exists('raceAN', $line))
                $args['raceAN'] = $line['raceAN'];
            if (array_key_exists('raceAS', $line))
                $args['raceAS'] = $line['raceAS'];
            if (array_key_exists('raceHL', $line))
                $args['raceHL'] = $line['raceHL'];
            if (array_key_exists('raceHP', $line))
                $args['raceHP'] = $line['raceHP'];
            if (array_key_exists('raceWH', $line))
                $args['raceWH'] = $line['raceWH'];
            if (array_key_exists('statusDIS', $line))
                $args['statusDIS'] = $line['statusDIS'];
            if (array_key_exists('statusECO', $line))
                $args['statusECO'] = $line['statusECO'];
            if (array_key_exists('statusELL', $line))
                $args['statusELL'] = $line['statusELL'];
            if (array_key_exists('statusGAT', $line))
                $args['statusGAT'] = $line['statusGAT'];
            if (array_key_exists('statusLEP', $line))
                $args['statusLEP'] = $line['statusLEP'];
            if (array_key_exists('statusMIG', $line))
                $args['statusMIG'] = $line['statusMIG'];
            if (array_key_exists('disabilityType', $line))
                $args['disabilityType'] = $line['disabilityType'];
            if (array_key_exists('optionalStateData2', $line))
                $args['optionalStateData2'] = $line['optionalStateData2'];
            if (array_key_exists('optionalStateData3', $line))
                $args['optionalStateData3'] = $line['optionalStateData3'];
            if (array_key_exists('optionalStateData4', $line))
                $args['optionalStateData4'] = $line['optionalStateData4'];
            if (array_key_exists('optionalStateData5', $line))
                $args['optionalStateData5'] = $line['optionalStateData5'];
            if (array_key_exists('optionalStateData6', $line))
                $args['optionalStateData6'] = $line['optionalStateData6'];
            if (array_key_exists('optionalStateData7', $line))
                $args['optionalStateData7'] = $line['optionalStateData7'];
            if (array_key_exists('optionalStateData8', $line))
                $args['optionalStateData8'] = $line['optionalStateData8'];

            if (array_key_exists('classAssignment1', $line))
                $args['classAssignment1'] = $line['classAssignment1'];
            if (array_key_exists('classAssignment2', $line))
                $args['classAssignment2'] = $line['classAssignment2'];
            if (array_key_exists('classAssignment3', $line))
                $args['classAssignment3'] = $line['classAssignment3'];
            if (array_key_exists('classAssignment4', $line))
                $args['classAssignment4'] = $line['classAssignment4'];
            if (array_key_exists('classAssignment5', $line))
                $args['classAssignment5'] = $line['classAssignment5'];
            if (array_key_exists('classAssignment6', $line))
                $args['classAssignment6'] = $line['classAssignment6'];
            if (array_key_exists('classAssignment7', $line))
                $args['classAssignment7'] = $line['classAssignment7'];
            if (array_key_exists('classAssignment8', $line))
                $args['classAssignment8'] = $line['classAssignment8'];
            if (array_key_exists('classAssignment9', $line))
                $args['classAssignment9'] = $line['classAssignment9'];
            if (array_key_exists('classAssignment10', $line))
                $args['classAssignment10'] = $line['classAssignment10'];

            if (array_key_exists('isDryRun', $line))
                $args['isDryRun'] = $line['isDryRun'];
            $resource = 'student-record';
            $method = 'POST';
            break;
        case 'CreateStudentRecords':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];

            if (array_key_exists('isDryRun', $line))
                $args['isDryRun'] = $line['isDryRun'];

            if (array_key_exists('students', $line))
                $args['students'] = $line['students'];
            $resource = 'student-records';
            $method = 'POST';
            break;
        case 'CreateTable':
            if (array_key_exists('table', $line))
            {
                $resource = 'table/' . $line['table'];
                $method = 'POST';
            }
            break;
        case 'CreateTestAssignment':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('studentRecordId', $line))
                $args['studentRecordId'] = $line['studentRecordId'];
            if (array_key_exists('testBatteryId', $line))
                $args['testBatteryId'] = $line['testBatteryId'];
            if (array_key_exists('instructionStatus', $line))
                $args['instructionStatus'] = $line['instructionStatus'];
            if (array_key_exists('enableLineReader', $line))
                $args['enableLineReader'] = $line['enableLineReader'];
            if (array_key_exists('enableTextToSpeech', $line))
                $args['enableTextToSpeech'] = $line['enableTextToSpeech'];
            $resource = 'test-assignment';
            $method = 'POST';
            break;
        case 'CreateTestAssignments':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('classId', $line))
                $args['classId'] = $line['classId'];
            if (array_key_exists('testBatteryId', $line))
                $args['testBatteryId'] = $line['testBatteryId'];
            if (array_key_exists('instructionStatus', $line))
                $args['instructionStatus'] = $line['instructionStatus'];
            if (array_key_exists('allOrNone', $line))
                $args['allOrNone'] = $line['allOrNone'];
            $resource = 'test-assignments';
            $method = 'POST';
            break;
        case 'CreateTestBatteryForm':
        case 'UpdateTestBatteryForm':
            $args = [];
            if (array_key_exists('testBatteryId', $line))
                $args['testBatteryId'] = $line['testBatteryId'];
            if (array_key_exists('testBatteryName', $line))
                $args['testBatteryName'] = rawurlencode($line['testBatteryName']);
            if (array_key_exists('testBatterySubject', $line))
                $args['testBatterySubject'] = $line['testBatterySubject'];
            if (array_key_exists('testBatteryGrade', $line))
                $args['testBatteryGrade'] = $line['testBatteryGrade'];
            if (array_key_exists('testBatteryProgram', $line))
                $args['testBatteryProgram'] = $line['testBatteryProgram'];
            if (array_key_exists('testBatterySecurity', $line))
                $args['testBatterySecurity'] = $line['testBatterySecurity'];
            if (array_key_exists('testBatteryPermissions', $line))
                $args['testBatteryPermissions'] = $line['testBatteryPermissions'];
            if (array_key_exists('testBatteryScoring', $line))
                $args['testBatteryScoring'] = $line['testBatteryScoring'];
            if (array_key_exists('testBatteryDescription', $line))
                $args['testBatteryDescription'] = rawurlencode($line['testBatteryDescription']);
            if (array_key_exists('testBatteryFormName', $line))
                $args['testBatteryFormName'] = rawurlencode($line['testBatteryFormName']);
            if (array_key_exists('testBatteryFormId', $line))
                $args['testBatteryFormId'] = $line['testBatteryFormId'];
            $resource = 'test-battery-form';
            $method = ($line['command'] == 'CreateTestBatteryForm') ? 'POST' : 'PUT';
            break;
        case 'CreateUser':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('organizationId', $line))
                $args['organizationId'] = $line['organizationId'];
            elseif ($organizationId !== null)
                $args['organizationId'] = $organizationId;
            $resource = 'user';
            $method = 'POST';
            break;
        case 'DecrementUnitTestCount':
            $unitTestCount--;
            break;
        case 'DeleteClass':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            $resource = 'class/';
            if (array_key_exists('classId', $line))
                $resource .= $line['classId'];
            $method = 'DELETE';
            break;
        case 'DeleteClassStudents':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('studentRecordIds', $line))
                $args['studentRecordIds'] = $line['studentRecordIds'];
            $resource = 'class-students/';
            if (array_key_exists('classId', $line))
                $resource .= $line['classId'];
            $method = 'DELETE';
            break;
        case 'DeleteOrganization':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('organizationId', $line))
                $resource = 'organization/' . $line['organizationId'];
            elseif ($organizationId !== null)
                $resource = 'organization/' . $organizationId;
            $method = 'DELETE';
            break;
        case 'DeleteStudentRecord':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            $resource = 'student-record/';
            if (array_key_exists('studentRecordId', $line))
                $resource .= $line['studentRecordId'];
            $method = 'DELETE';
            break;
        case 'DropAllTables':
            $resource = 'tables';
            $method = 'DELETE';
            break;
        case 'DropTable':
            if (array_key_exists('table', $line))
            {
                $resource = 'table/' . $line['table'];
                $method = 'DELETE';
            }
            break;
        case 'EndUnitTests':
            break 2;
        case 'GetAssignedTests':
            $args = [];
            if (array_key_exists('organizationId', $line))
                $args['organizationId'] = $line['organizationId'];
            if (array_key_exists('classIds', $line))
                $args['classIds'] = $line['classIds'];
            if (array_key_exists('studentGradeLevel', $line))
                $args['studentGradeLevel'] = $line['studentGradeLevel'];
            if (array_key_exists('testBatterySubject', $line))
                $args['testBatterySubject'] = $line['testBatterySubject'];
            if (array_key_exists('testBatteryGrade', $line))
                $args['testBatteryGrade'] = $line['testBatteryGrade'];
            if (array_key_exists('testBatteryId', $line))
                $args['testBatteryId'] = $line['testBatteryId'];
            $resource = 'assigned-tests';
            $method = 'GET';
            break;
        case 'GetAuditRecords':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('startDateTime', $line))
                $args['startDateTime'] = $line['startDateTime'];
            if (array_key_exists('endDateTime', $line))
                $args['endDateTime'] = $line['endDateTime'];
            if (array_key_exists('dataType', $line))
                $args['dataType'] = $line['dataType'];
            if (array_key_exists('primaryId', $line))
                $args['primaryId'] = $line['primaryId'];
            if (array_key_exists('actionType', $line))
                $args['actionType'] = $line['actionType'];
            if (array_key_exists('pageOffset', $line))
                $args['pageOffset'] = $line['pageOffset'];
            if (array_key_exists('pageLimit', $line))
                $args['pageLimit'] = $line['pageLimit'];
            $resource = 'audit-records';
            $method = 'GET';
            break;
        case 'GetClass':
            $args = [];
            $resource = 'class/';
            if (array_key_exists('classId', $line))
                $resource .= $line['classId'];
            $method = 'GET';
            break;
        case 'GetClassAssignedTests':
            $args = [];
            $resource = 'class-assigned-tests/';
            if (array_key_exists('classId', $line))
                $resource .= $line['classId'];
            $method = 'GET';
            break;
        case 'GetClassStudents':
            $args = [];
            $resource = 'class-students/';
            if (array_key_exists('classId', $line))
                $resource .= $line['classId'];
            $method = 'GET';
            break;
        case 'GetOrganization':
            if (array_key_exists('organizationId', $line))
                $resource = 'organization/' . $line['organizationId'];
            elseif ($organizationId !== null)
                $resource = 'organization/' . $organizationId;
            $method = 'GET';
            break;
		case 'GetOrganizationId':
            $args = [];
            if (array_key_exists('organizationPath', $line))
                $args['organizationPath'] = $line['organizationPath'];
            if (array_key_exists('stateCode', $line))
                $args['stateCode'] = $line['stateCode'];
            if (array_key_exists('organizationIdentifier', $line))
                $args['organizationIdentifier'] = $line['organizationIdentifier'];
            if (array_key_exists('parentOrganizationId', $line))
                $args['parentOrganizationId'] = $line['parentOrganizationId'];
            if (array_key_exists('organizationName', $line))
                $args['organizationName'] = $line['organizationName'];
            $resource = 'organization-id';
            $method = 'GET';
			break;
        case 'GetStudentRecord':
            $args = [];
            $resource = 'student-record/';
            if (array_key_exists('studentRecordId', $line))
                $resource .= $line['studentRecordId'];
            if (array_key_exists('optionalStateData', $line))
                $args['optionalStateData'] = $line['optionalStateData'];
            $method = 'GET';
            break;
        case 'GetTestAssignments':
            $args = [];
            if (array_key_exists('studentRecordId', $line))
                $args['studentRecordId'] = $line['studentRecordId'];
            if (array_key_exists('organizationId', $line))
                $args['organizationId'] = $line['organizationId'];
            if (array_key_exists('classIds', $line))
                $args['classIds'] = $line['classIds'];
            if (array_key_exists('studentGradeLevel', $line))
                $args['studentGradeLevel'] = $line['studentGradeLevel'];
            if (array_key_exists('testBatterySubject', $line))
                $args['testBatterySubject'] = $line['testBatterySubject'];
            if (array_key_exists('testBatteryGrade', $line))
                $args['testBatteryGrade'] = $line['testBatteryGrade'];
            if (array_key_exists('testBatteryId', $line))
                $args['testBatteryId'] = $line['testBatteryId'];
            if (array_key_exists('testAssignmentStatus', $line))
                $args['testAssignmentStatus'] = $line['testAssignmentStatus'];
            if (array_key_exists('instructionStatus', $line))
                $args['instructionStatus'] = $line['instructionStatus'];
            $resource = 'test-assignments';
            $method = 'GET';
            break;
        case 'GetTestBattery':
            $args = [];
            $resource = 'test-battery/';
            if (array_key_exists('testBatteryId', $line))
                $resource .= $line['testBatteryId'];
            $method = 'GET';
            break;
        case 'GetTestBatteryForms':
            $args = [];
            $resource = 'test-battery-forms/';
            if (array_key_exists('testBatteryId', $line))
                $resource .= $line['testBatteryId'];
            $method = 'GET';
            break;
        case 'GetVersion':
            $resource = 'version';
            $method = 'GET';
            break;
        case 'IncrementUnitTestCount':
            $unitTestCount++;
            break;
        case 'PauseProcessing':
            $seconds = 2;
            if (array_key_exists('seconds', $line))
                $seconds = $line['seconds'];
            sleep($seconds);
            continue 2;
        case 'RandomizeTestAssignmentStatuses':
            if (array_key_exists('pattern', $line))
            {
                $args = [];
                $args['pattern'] = $line['pattern'];
                $resource = 'test-assignments/randomize-statuses';
                $method = 'PUT';
            }
            break;
        case 'ReportUnitTestPassed':
            $noun = ($unitTestCount == 1) ? 'test' : 'tests';
            $output .= $unitTestName . ' unit tests passed ('. $unitTestCount . ' unit ' . $noun . ')<br />';
            $totalUnitTestCount += $unitTestCount;
            $error = $errors = false;
            break;
        case 'RunUnitTestFile':
            if (array_key_exists('filename', $line))
            {
                $fileSub = SRC_DIR . $line['filename'];
                $fhSub = fopen($fileSub, 'rb');
                if ($fhSub === false)
                {
                    $output .= $prefix . ' unable to load ' . $line['filename'] . $suffix;
                    $error = true;
                    break 2;
                }
                $fileCur = $fileSub;
                $fhCur = $fhSub;
                $subLine = 0;
            }
            break;
        case 'SelectProductionDatabase':
            $resource = 'database-production';
            $method = 'PUT';
            break;
        case 'SelectUnitTestDatabase':
            $resource = 'database-unit-test';
            $method = 'PUT';
            break;
        case 'SetErrorExpected':
            if (array_key_exists('value', $line))
            {
                if ($line['value'] == true)
                    $errorExpected = true;
                else
                    $errorExpected = false;
            }
            break;
        case 'SetOrganizationId':
            if (array_key_exists('organizationId', $line))
                $organizationId = $line['organizationId'];
            break;
        case 'SetUnitTestName':
            if (array_key_exists('name', $line))
                $unitTestName = $line['name'];
            $unitTestCount = 0;
            continue 2;
        case 'ShowMessage':
            if (array_key_exists('message', $line))
                $output .= $line['message'] . '<br />';
            break;
        case 'ShowResult':
            $output .= json_encode($actualResults);
            break;
		case 'ShowSkips':
			$showSkips = true;
			continue 2;
		case 'SkipBlockEnd':
			$skipProcessing = false;
			continue 2;
		case 'SkipBlockStart':
			$skipProcessing = true;
			continue 2;
        case 'StartPerformanceTest':
            $performanceTest = true;
            $startTime = time();
            continue 2;
        case 'StartUnitTests':
            $unitTests = true;
            continue 2;
        case 'UpdateClass':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('classIdentifier', $line))
                $args['classIdentifier'] = $line['classIdentifier'];
            if (array_key_exists('gradeLevel', $line))
                $args['gradeLevel'] = $line['gradeLevel'];
            if (array_key_exists('sectionNumber', $line))
                $args['sectionNumber'] = $line['sectionNumber'];
            $resource = 'class';
            if (array_key_exists('classId', $line))
                $resource .= '/' . $line['classId'];
            $method = 'PUT';
            break;
        case 'UpdateOrganization':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('organizationName', $line))
                $args['organizationName'] = $line['organizationName'];
            if (array_key_exists('organizationIdentifier', $line))
                $args['organizationIdentifier'] = $line['organizationIdentifier'];
            if (array_key_exists('organizationId', $line))
                $resource = 'organization/' . $line['organizationId'];
            elseif ($organizationId !== null)
                $resource = 'organization/' . $organizationId;
            $method = 'PUT';
            break;
        case 'UpdateStudentRecord':
            $args = [];
            if (array_key_exists('clientUserId', $line))
                $args['clientUserId'] = $line['clientUserId'];
            if (array_key_exists('stateIdentifier', $line))
                $args['stateIdentifier'] = $line['stateIdentifier'];
            if (array_key_exists('localIdentifier', $line))
                $args['localIdentifier'] = $line['localIdentifier'];
            if (array_key_exists('lastName', $line))
                $args['lastName'] = $line['lastName'];
            if (array_key_exists('firstName', $line))
                $args['firstName'] = $line['firstName'];
            if (array_key_exists('middleName', $line))
                $args['middleName'] = $line['middleName'];
            if (array_key_exists('dateOfBirth', $line))
                $args['dateOfBirth'] = $line['dateOfBirth'];
            if (array_key_exists('gender', $line))
                $args['gender'] = $line['gender'];
            if (array_key_exists('gradeLevel', $line))
                $args['gradeLevel'] = $line['gradeLevel'];
            if (array_key_exists('raceAA', $line))
                $args['raceAA'] = $line['raceAA'];
            if (array_key_exists('raceAN', $line))
                $args['raceAN'] = $line['raceAN'];
            if (array_key_exists('raceAS', $line))
                $args['raceAS'] = $line['raceAS'];
            if (array_key_exists('raceHL', $line))
                $args['raceHL'] = $line['raceHL'];
            if (array_key_exists('raceHP', $line))
                $args['raceHP'] = $line['raceHP'];
            if (array_key_exists('raceWH', $line))
                $args['raceWH'] = $line['raceWH'];
            if (array_key_exists('statusDIS', $line))
                $args['statusDIS'] = $line['statusDIS'];
            if (array_key_exists('statusECO', $line))
                $args['statusECO'] = $line['statusECO'];
            if (array_key_exists('statusELL', $line))
                $args['statusELL'] = $line['statusELL'];
            if (array_key_exists('statusGAT', $line))
                $args['statusGAT'] = $line['statusGAT'];
            if (array_key_exists('statusLEP', $line))
                $args['statusLEP'] = $line['statusLEP'];
            if (array_key_exists('statusMIG', $line))
                $args['statusMIG'] = $line['statusMIG'];
            if (array_key_exists('disabilityType', $line))
                $args['disabilityType'] = $line['disabilityType'];
            $resource = 'student-record/';
            if (array_key_exists('studentRecordId', $line))
                $resource .= $line['studentRecordId'];
            $method = 'PUT';
            break;
		default:
            $output .= $prefix . ' has unknown command<br />';
			break 2;
	}

    // Call the LDR service
    if ($method !== null)
    {
        $response = callLDR($method, $resource, $args);
        $commandsProcessed++;

        // Optionally display the JSON response
        if (array_key_exists('json', $line))
            echo json_encode($response) . '<br />';

        // Check the LDR response for one or more errors
        $error = array_key_exists('error', $response);
        $errors = array_key_exists('errors', $response) && (sizeof($response['errors']) > 0);
        if ($error || $errors)
        {
            if ($errorExpected)
            {
                // Save the results
                $actualResults = $response;
            }
            else
            {
                // If there was an unexpected error then stop processing the list
                $output .= $fileCur . ' line ' . $curLine . ': service call failed: ' . $line['command'] . '<br />';
                break;
            }
        }
        else
        {
            // Perform service-specific results processing
            switch ($line['command'])
            {
                case 'CreateOrganization':
                    // Increment the count of organizations created
                    switch ($organizationType)
                    {
                        case ORG_TYPE_SCHOOL:
                            $schoolsCreated++;
                            break;
                        case ORG_TYPE_DISTRICT:
                            $districtsCreated++;
                            break;
                        case ORG_TYPE_STATE:
                            $statesCreated++;
                            break;
                    }
                    $organizationsCreated = true;
                    break;
                case 'GetAssignedTests':
                case 'GetAuditRecords':
                case 'GetClass':
                case 'GetClassAssignedTests':
                case 'GetClassStudents':
                case 'GetOrganization':
                case 'GetStudentRecord':
                case 'GetTestAssignments':
                case 'GetTestBattery':
                case 'GetTestBatteryForms':
                    // Save the results
                    $actualResults = $response;
                    break;
                case 'GetOrganizationId':
                    // Save the organizationId
                    $organizationId = $response['organizationId'];
                    break;
                case 'SelectProductionDatabase':
                case 'SelectUnitTestDatabase':
                    $_SESSION['ldr_token'] = $response['token'];
                    break;
            }
        }
    }
}

displayHeader($error || $errors);

if ($output != $outputStart)
{
    $output .= '</p>';
    echo $output;
}

if ($error == false && $errors == false)
{
    if ($unitTests)
    {
        displaySuccessMessage('ALL UNIT TESTS PASSED (' . $totalUnitTestCount . ' unit tests)');
    }

    if (!$unitTests && $organizationsCreated > 0)
    {
        echo '<br /><table class="output">';
        displayTableKeyValue('States created', $statesCreated);
        displayTableKeyValue('Districts created', $districtsCreated);
        displayTableKeyValue('Schools created', $schoolsCreated);
        echo '</table>';
    }
    $noun = ($commandsProcessed == 1) ? 'COMMAND' : 'COMMANDS';
    displayMessage($commandsProcessed . ' ' . $noun . ' PROCESSED');

    $totalTimeElapsed = microtime(true) - $startTime;
    displayMessage('        Total time elapsed = ' . roundThreeDecimalPoints($totalTimeElapsed) . ' seconds');

    displayMessage('Total request time elapsed = ' . roundThreeDecimalPoints($totalRequestTimeElapsed) . ' seconds');

    if ($performanceTest)
    {
        // TODO: Get the performance statistics
        $response = callLDR('GET', 'performance');
        $error = array_key_exists('error', $response);
        if ($error)
            displayErrorResponse($response);
        else
        {
            // Display the performance statistics
            echo '<table class="data">';
            $columns = ['serviceName', 'serviceCount', 'averageElapsedTime', 'maximumElapsedTime', 'minimumElapsedTime',
                'totalElapsedTime'];
            displayTableHeader($columns);
            foreach (array_keys($response['services']) as $key)
            {
                // The serviceName is used as the key for each test summary in the LDR JSON response
                $service = $response['services'][$key];
                $service['serviceName'] = $key;

                // Round the time values
                $service['averageElapsedTime'] = round($service['averageElapsedTime'], 4);
                $service['totalElapsedTime'] = round($service['totalElapsedTime'], 2);
                $service['maximumElapsedTime'] = round($service['maximumElapsedTime'], 4);
                $service['minimumElapsedTime'] = round($service['minimumElapsedTime'], 4);

                displayTableDataByKey($service, $columns);
            }
            echo '</table>';
        }
    }

    displayFooter();
}
else
{
    if (strlen($errorMessage) > 0)
        displayErrorMessage($errorMessage);
    elseif ($error)
        displayErrorResponse($response);
    elseif ($errors)
        displayErrorsResponse($response['errors']);

    displayFooter();
}

fclose($fhMain);

