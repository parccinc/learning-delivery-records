<?php

require_once 'ldr_client.php';

session_start();

// Delete the LDR token
if (isset($_SESSION['ldr_token']))
    callLDR('DELETE', 'session-token');

// Destroy the session
$_SESSION = array();
if (isset($_COOKIE[session_name()]))
    setcookie(session_name(), '', time() - 3600);
session_destroy();

header("Location: index.php");
