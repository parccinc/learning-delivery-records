<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 's3-json-file-contents', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if ($error)
    displayErrorResponse($response);
else
    displayJsonResponse($response);
displayFooter();
