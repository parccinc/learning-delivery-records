<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['testBatteryId']))
{
    displayHeader(true);
    displayErrorMessage('testBatteryId value is required in the query string');
    displayFooter();
    exit;
}

if (!isset($_GET['json']))
{
    // Get the test battery
    $resource = 'test-battery/' . $_GET['testBatteryId'];
    $testBattery = callLDR('GET', $resource);
    if (array_key_exists('error', $testBattery))
    {
        $error = true;
        displayHeader($error);
        displayErrorResponse($testBattery);
        displayFooter();
        exit;
    }
}

// Prepare the resource path
$resource = 'test-battery-forms/' . $_GET['testBatteryId'];

// Call the LDR service
$response = callLDR('GET', $resource);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        displayOrganization($testBattery['testBatteryName']);

        echo '<table class="data">';
        $columns = ['testBatteryId', 'testBatteryFormId', 'testBatteryFormName'];
        displayTableHeader($columns);
        foreach ($response as $testBattery)
            displayTableDataByKey($testBattery, $columns);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
