<?php

require_once 'ldr_client.php';

session_start();

// Prepare the resource path
$resource = 'audit-records';

// Call the LDR service
$response = callLDR('GET', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $auditRecords = $response['auditRecords'];

        displayOrganization('Showing ' . sizeof($auditRecords) . ' of ' . $response['totalCount'] . ' audit records');

        $columns = ['userAuditId', 'dateTime', 'clientUserId', 'actionType', 'dataType', 'primaryId'];
        foreach ($auditRecords as $auditRecord)
        {
            // Display the audit record
            echo '<table class="data">';
            displayTableHeader($columns);
            displayTableDataByKey($auditRecord, $columns);
            echo '</table>';

            // Display the data changes
            if ($auditRecord['actionType'] != 'Delete')
            {
                echo '<table class="audit">';
                foreach ($auditRecord['dataChanges'] as $key => $value)
                    displayTableKeyValue($key, $value);
                echo '</table>';
            }

            echo '<br />';
        }
    }
}
displayFooter();
