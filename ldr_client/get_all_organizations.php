<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'organizations-all', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $organizations = $response['organizations'];

        displayOrganization('Showing ' . sizeof($organizations) . ' of ' . $response['totalCount'] . ' organizations');

        echo '<table class="data">';
        $columns = ['organizationId', 'parentOrganizationId', 'organizationType', 'organizationIdentifier',
                    'organizationName', 'studentCount', 'childCount', 'fake'];
        displayTableHeader($columns);
        foreach ($organizations as $organization)
            displayTableDataByKey($organization, $columns);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
