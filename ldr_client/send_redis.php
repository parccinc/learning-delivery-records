<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('POST', '/redis', $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $tab = '&nbsp;&nbsp;&nbsp;&nbsp;';
        echo '<p class="message">';
        foreach ($response as $key => $value)
        {
            if (is_array($value))
            {
                echo $key . ' =><br />';
                foreach ($value as $subkey => $subvalue)
                    echo $tab . $subkey . ' => |' . $subvalue . '|<br />';
                echo '<br />';
            }
            else
                echo $key . ' => |' . $value . '|<br />';
        }
        echo '</p>';
    }
}
displayFooter();
