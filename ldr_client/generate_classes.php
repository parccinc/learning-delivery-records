<?php

require_once 'ldr_client.php';

define('SRC_DIR', 'process/');

set_time_limit(0);

session_start();

// Get required parameters
if (!isset($_GET['organizationId']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the root organization ID - e.g. organizationId=2');
    displayFooter();
    exit;
}

// Save the start time
$timeStart = time();

// Get the organization IDs for the schools
$resource = 'organization-ids/descendent/' . $_GET['organizationId'];
$organizationIds = callLDR('GET', $resource, ['organizationType' => ORG_TYPE_SCHOOL]);
if (array_key_exists('error', $organizationIds))
{
    displayHeader(true);
    displayErrorResponse($organizationIds);
    displayFooter();
    exit;
}

$primarySchoolClasses =
[
    ['', 'Ms. Baker', '01'],
    ['', 'Mrs. Taylor', '01'],
    ['', 'Mrs. Jackson', '02'],
    ['', 'Ms. Edelson', '02'],
    ['', 'Mr. Thompson', '03'],
    ['', 'Mrs. Kincaid', '03'],
    ['', 'Mrs. Bracken', '04'],
    ['', 'Mrs. Faulkner', '04'],
    ['', 'Mr. Moss', '05'],
    ['', 'Mrs. Stevenson', '05'],
    ['', 'Mr. Harrison', '06'],
    ['', 'Mr. Vincent', '06']
];

$middleSchoolClasses =
[
    ['ELA 101', 'English / Language Arts 1', '07'],
    ['ELA 102', 'English / Language Arts 1', '07'],
    ['ELA 103', 'English / Language Arts 1', '07'],
    ['ELA 104', 'English / Language Arts 1', '07'],
    ['ELA 201', 'English / Language Arts 2', '07'],
    ['ELA 202', 'English / Language Arts 2', '07'],
    ['ELA 203', 'English / Language Arts 2', '07'],
    ['ELA 204', 'English / Language Arts 2', '07'],
    ['ELA 301', 'English / Language Arts 3', '08'],
    ['ELA 302', 'English / Language Arts 3', '08'],
    ['ELA 303', 'English / Language Arts 3', '08'],
    ['ELA 401', 'English / Language Arts 4', '08'],
    ['ELA 402', 'English / Language Arts 4', '08'],
    ['ELA 403', 'English / Language Arts 4', '08'],
    ['MATH 010', 'Interactive Mathematics', '07'],
    ['MATH 011', 'Interactive Mathematics', '07'],
    ['MATH 012', 'Interactive Mathematics', '08'],
    ['MATH 100', 'Beginning Algebra', '07'],
    ['MATH 101', 'Beginning Algebra', '07'],
    ['MATH 102', 'Beginning Algebra', '07'],
    ['MATH 200', 'Basics of Geometry', '07'],
    ['MATH 201', 'Basics of Geometry', '07'],
    ['MATH 202', 'Basics of Geometry', '07'],
    ['MATH 300', 'Intermediate Algebra', '08'],
    ['MATH 301', 'Intermediate Algebra', '08'],
    ['MATH 350', 'Intermediate Geometry', '08'],
    ['MATH 351', 'Intermediate Geometry', '08']
];

$highSchoolClasses =
[
    ['ENG 101', 'Freshman Language & Composition', '09'],
    ['ENG 102', 'Freshman Language & Composition', '09'],
    ['ENG 103', 'Freshman Language & Composition', '09'],
    ['ENG 104', 'Freshman Language & Composition', '09'],
    ['ENG 105', 'Freshman Language & Composition', '09'],
    ['ENG 106', 'Freshman Language & Composition', '09'],
    ['ENG 110', 'American Literature 1', '09'],
    ['ENG 111', 'American Literature 1', '09'],
    ['ENG 112', 'American Literature 1', '09'],
    ['ENG 113', 'American Literature 1', '09'],
    ['ENG 201', 'Sophmore Language & Composition', '10'],
    ['ENG 202', 'Sophmore Language & Composition', '10'],
    ['ENG 203', 'Sophmore Language & Composition', '10'],
    ['ENG 204', 'Sophmore Language & Composition', '10'],
    ['ENG 210', 'American Literature 2', '10'],
    ['ENG 211', 'American Literature 2', '10'],
    ['ENG 212', 'American Literature 2', '10'],
    ['ENG 213', 'American Literature 2', '10'],
    ['ENG 301', 'Junior Language & Composition', '11'],
    ['ENG 302', 'Junior Language & Composition', '11'],
    ['ENG 303', 'Junior Language & Composition', '11'],
    ['ENG 310', 'World Literature 1', '11'],
    ['ENG 311', 'World Literature 1', '11'],
    ['ENG 312', 'World Literature 1', '11'],
    ['ENG 401', 'Senior Language & Composition', '12'],
    ['ENG 402', 'Senior Language & Composition', '12'],
    ['ENG 403', 'Senior Language & Composition', '12'],
    ['ENG 410', 'World Literature 2', '12'],
    ['ENG 411', 'World Literature 2', '12'],
    ['ENG 412', 'World Literature 2', '12'],
    ['MATH 010', 'Fundamentals of Mathematics', '09'],
    ['MATH 011', 'Fundamentals of Mathematics', '09'],
    ['MATH 012', 'Fundamentals of Mathematics', '09'],
    ['MATH 013', 'Fundamentals of Mathematics', '09'],
    ['MATH 014', 'Fundamentals of Mathematics', '09'],
    ['MATH 015', 'Fundamentals of Mathematics', '09'],
    ['MATH 110', 'Algebra 1', '09'],
    ['MATH 111', 'Algebra 1', '09'],
    ['MATH 112', 'Algebra 1', '09'],
    ['MATH 113', 'Algebra 1', '09'],
    ['MATH 114', 'Algebra 1', '09'],
    ['MATH 120', 'Algebra 2', '10'],
    ['MATH 121', 'Algebra 2', '10'],
    ['MATH 122', 'Algebra 2', '10'],
    ['MATH 123', 'Algebra 2', '10'],
    ['MATH 124', 'Algebra 2', '10'],
    ['MATH 230', 'Geometry 1', '09'],
    ['MATH 231', 'Geometry 1', '09'],
    ['MATH 232', 'Geometry 1', '09'],
    ['MATH 240', 'Geometry 2', '10'],
    ['MATH 241', 'Geometry 2', '10'],
    ['MATH 350', 'Trigonometry 1', '11'],
    ['MATH 351', 'Trigonometry 1', '11'],
    ['MATH 360', 'Trigonometry 2', '12'],
    ['MATH 361', 'Trigonometry 2', '12'],
    ['MATH 421', 'Honors Algebra 2', '11'],
    ['MATH 440', 'Honors Geometry 2', '11'],
    ['MATH 570', 'AP Calculus AB', '12'],
    ['MATH 580', 'AP Calculus BC', '12']
];

// Add classes to each school
displayHeader(false);
foreach ($organizationIds as $organizationId)
{
    // Get the school name
    $resource = 'organization/' . $organizationId;
    $organization = callLDR('GET', $resource);
    if (array_key_exists('error', $organization))
    {
        displayErrorResponse($organization);
        displayFooter();
        exit;
    }
    $organizationName = $organization['organizationName'];

    // Get the school type
    $schoolType = classifySchool($organizationName);

    // If not able to classify the school then skip it
    if ($schoolType == SCHOOL_TYPE_NONE)
        continue;

    // Determine the list of classes to add based on the school type
    switch ($schoolType)
    {
        case SCHOOL_TYPE_PRIMARY:
            $classes = $primarySchoolClasses;
            break;
        case SCHOOL_TYPE_MIDDLE:
            $classes = $middleSchoolClasses;
            break;
        case SCHOOL_TYPE_HIGH:
            $classes = $highSchoolClasses;
            break;
        default:
            $classes = [];
            break;
    }

    echo '<p class="output">';
    echo $organizationId . ' : |' . $organizationName . '| is type ' . $schoolTypeText[$schoolType] . '<br />';

    // Generate the classes
    foreach ($classes as $class)
    {
        // Create the class records
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adding ' . $class[1] .
             ' [' . $class[0] . '] for grade level ' . $class[2] . '<br />';

        $args = ['clientUserId' => 1,
                 'organizationId' => $organizationId,
                 'classIdentifier' => $class[1],
                 'sectionNumber' => $class[0],
                 'gradeLevel' => $class[2]];
        $response = callLDR('POST', 'class', $args);
        if (array_key_exists('error', $response))
        {
            echo '</p>';
            displayErrorResponse($response);
            break 2;
        }
    }

    echo '</p>';
}

displayMessage('Classes added in ' . formatExecutionTime(time() - $timeStart));
displayFooter();
