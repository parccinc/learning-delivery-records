<?php

require_once 'ldr_client.php';

define('SRC_DIR', 'process/');

set_time_limit(0);

session_start();

// Get required parameters
if (!isset($_GET['organizationId']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the root organization ID - e.g. organizationId=2');
    displayFooter();
	exit;
}

// Load boy first names
$boysNamesFile = SRC_DIR . 'boysNames.txt';
$boysNames = file($boysNamesFile);
if ($boysNames === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to load file ' . $boysNamesFile);
    displayFooter();
	exit;
}
$countBoysNames = sizeof($boysNames);

// Load girl first names
$girlsNamesFile = SRC_DIR . 'girlsNames.txt';
$girlsNames = file($girlsNamesFile);
if ($girlsNames === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to load file ' . $girlsNamesFile);
    displayFooter();
	exit;
}
$countGirlsNames = sizeof($girlsNames);

// Load last names
$lastNamesFile = SRC_DIR . 'lastNames.txt';
$lastNames = file($lastNamesFile);
if ($lastNames === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to load file ' . $lastNamesFile);
    displayFooter();
	exit;
}
$countLastNames = sizeof($lastNames);

// Define the disability types
$disabilityTypes = ['AUT', 'DB', 'DD', 'EMN', 'HI', 'ID', 'MD', 'OHI', 'OI', 'SLD', 'SLI', 'TBI', 'VI'];
$countDisabilityTypes = sizeof($disabilityTypes);

// Save the start time
$timeStart = time();

// Get the organization IDs for the schools
$resource = 'organization-ids/descendent/' . $_GET['organizationId'];
$organizationIds = callLDR('GET', $resource, ['organizationType' => ORG_TYPE_SCHOOL]);
if (array_key_exists('error', $organizationIds))
{
    displayHeader(true);
    displayErrorResponse($organizationIds);
    displayFooter();
    exit;
}

// Add students to each school
$isBoy = false;
$studentsAdded = 0;
displayHeader(false);
foreach ($organizationIds as $organizationId)
{
    // Get the school name
    $resource = 'organization/' . $organizationId;
    $organization = callLDR('GET', $resource);
    if (array_key_exists('error', $organization))
    {
        displayErrorResponse($organization);
        displayFooter();
        exit;
    }
    $organizationName = $organization['organizationName'];

    // Get the school type
    $schoolType = classifySchool($organizationName);

    // If not able to classify the school then skip it
    if ($schoolType == SCHOOL_TYPE_NONE)
        continue;

    // Determine the number of students to add based on the school type
    $studentCount = 0;
    switch ($schoolType)
    {
        case SCHOOL_TYPE_PRIMARY:
            $studentCount = mt_rand(40, 100);
            break;
        case SCHOOL_TYPE_MIDDLE:
            $studentCount = mt_rand(80, 200);
            break;
        case SCHOOL_TYPE_HIGH:
            $studentCount = mt_rand(120, 800);
            break;
        case SCHOOL_TYPE_MIDDLE_HIGH:
            $studentCount = mt_rand(100, 400);
            break;
    }

    echo '<p class="output">';
    echo $organizationId . ' : |' . $organizationName . '| is type ' . $schoolTypeText[$schoolType] . ' and has ' .
         $studentCount . ' students<br>';

    // Generate the students
    while ($studentCount-- > 0)
    {
        // Generate a student state identifier
        $stateIdentifier = mt_rand(100000000, 999999999);

        // Generate a last name
        $lastName = $lastNames[mt_rand(1, $countLastNames) - 1];
        $lastName = trim($lastName);

        // Generate a first name
        if ($isBoy)
            $firstName = $boysNames[mt_rand(1, $countBoysNames) - 1];
        else
            $firstName = $girlsNames[mt_rand(1, $countGirlsNames) - 1];
        $firstName = trim($firstName);

        // Generate a middle name
        $middleName = $lastNames[mt_rand(1, $countLastNames) - 1];
        $middleName = trim($middleName);

        // Generate a grade level
        switch ($schoolType)
        {
            case SCHOOL_TYPE_PRIMARY:
                $gradeLevel = mt_rand(1, 6);
                break;
            case SCHOOL_TYPE_MIDDLE:
                $gradeLevel = mt_rand(7, 8);
                break;
            case SCHOOL_TYPE_HIGH:
                $gradeLevel = mt_rand(9, 12);
                break;
            case SCHOOL_TYPE_MIDDLE_HIGH:
                $gradeLevel = mt_rand(7, 12);
                break;
        }

        // Generate a DOB
        $studentAge = $gradeLevel + 5;
        $studentAge *= 365; // Convert age from years to days
        $studentAge += (mt_rand(1, 365) - 182); // Randomize within +/- six months
        $dateOfBirth = date('Y-m-d', time() - $studentAge * SECONDS_PER_DAY);

        // Generate the gender
        $gender = $isBoy ? 'M' : 'F';

        // Generate the race values
        $raceAA = $raceAN = $raceAS = $raceHL = $raceHP = $raceWH = 'N';
        $race = mt_rand(1, 1000);
        if ($race < 550)
            $raceWH = 'Y';
        elseif ($race < 700)
            $raceAA = 'Y';
        elseif ($race < 850)
            $raceHL = 'Y';
        elseif ($race < 940)
            $raceAS = 'Y';
        elseif ($race < 970)
            $raceAN = 'Y';
        else
            $raceHP = 'Y';

        // Generate the status values
        $disabilityType = '';
        $statusDIS = $statusECO = $statusELL = $statusGAT = $statusLEP = $statusMIG = 'N';
        if (mt_rand(1, 1000) < 40)
        {
            $statusDIS = 'Y';
            $disabilityType = $disabilityTypes[mt_rand(1, $countDisabilityTypes) - 1];
        }
        if (mt_rand(1, 1000) < 80)
            $statusECO = 'Y';
        if (mt_rand(1, 1000) < 160)
            $statusELL = 'Y';
        if (mt_rand(1, 1000) < 30)
            $statusGAT = 'Y';
        if (mt_rand(1, 1000) < 30)
            $statusLEP = 'Y';
        if (mt_rand(1, 1000) < 20)
            $statusMIG = 'Y';

        // Create the student records
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adding ' . $firstName . ' ' . $middleName . ' ' .
             $lastName . ' to grade ' . $gradeLevel . '<br />';

        $args = ['clientUserId' => 1,
                 'enrollOrgId' => $organizationId,
                 'stateIdentifier' => $stateIdentifier,
                 'lastName' => $lastName,
                 'firstName' => $firstName,
                 'middleName' => $middleName,
                 'dateOfBirth' => $dateOfBirth,
                 'gender' => $gender,
                 'gradeLevel' => $gradeLevel,
                 'raceAA' => $raceAA,
                 'raceAN' => $raceAN,
                 'raceAS' => $raceAS,
                 'raceHL' => $raceHL,
                 'raceHP' => $raceHP,
                 'raceWH' => $raceWH,
                 'statusDIS' => $statusDIS,
                 'statusECO' => $statusECO,
                 'statusELL' => $statusELL,
                 'statusGAT' => $statusGAT,
                 'statusLEP' => $statusLEP,
                 'statusMIG' => $statusMIG,
                 'disabilityType' => $disabilityType];
        $response = callLDR('POST', 'student-record', $args);
        if (array_key_exists('error', $response))
        {
            $code = substr($response['error'], 0, 4);
            if ($code == LDR_EC_STUDENT_STATE_IDENTIFIER_DUP)
            {
                echo 'DUPLICATE STUDENT STATE IDENTIFIER' . '<br />';
            }
            else
            {
                echo '</p>';
                displayErrorResponse($response);
                break 2;
            }
        }

        // Flip the gender
        $isBoy = !$isBoy;

        $studentsAdded++;
    }

    echo '</p>';
}

displayMessage($studentsAdded . ' STUDENTS ADDED in ' . formatExecutionTime(time() - $timeStart));
displayFooter();
