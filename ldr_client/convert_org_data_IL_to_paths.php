<?php

require_once 'ldr_client.php';

set_time_limit(0);

session_start();

define('SRC_DIR', '../import/');
define('STATE_PATH', 'PARCC_IL');

// Check that the user has been authenticated
if (!isset($_SESSION['ldr_token']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Please log in first');
    displayFooter();
    exit;
}

// Get the input file name
if (!isset($_GET['input']))
{
    $error = true;
    displayAuthHeader($error);
    displayErrorMessage('Please specify the input file name - e.g. input=some_input_file.csv');
    displayFooter();
    exit;
}

// Open the input file
$filename = SRC_DIR . $_GET['input'];
$fin = fopen($filename, 'rb');
if ($fin === false)
{
    $error = true;
    displayAuthHeader($error);
    displayErrorMessage('Unable to open file ' . $filename);
    displayFooter();
    exit;
}

// Get the output file name
if (!isset($_GET['output']))
{
    $error = true;
    displayAuthHeader($error);
    displayErrorMessage('Please specify the output file name - e.g. output=some_output_file.csv');
    displayFooter();
    closeAllFiles();
    exit;
}

// Open the output file
$filename = SRC_DIR . $_GET['output'];
$fout = fopen($filename, 'wb');
if ($fout === false)
{
    $error = true;
    displayAuthHeader($error);
    displayErrorMessage('Unable to open file ' . $filename);
    displayFooter();
    closeAllFiles();
    exit;
}

// Save the start time
$timeStart = time();

// Initialize processing variables
$error = false;
$currentDistrictIdentifier = 0;

// Get the column count from the header line
$header = fgetcsv($fin);
$columnCount = sizeof($header);
$line = 1;

$outputStart = '<p class="output">';
$output = $outputStart;
while (true)
{
    // Read the next line
    $line++;
    $values = fgetcsv($fin);
    if ($values === false)
        break;

    // Check for a blank line
    if (sizeof($values) == 1)
        continue;

    $prefix = 'Line ' . $line . ': ';
    $suffix = ' > ' . implode(',', $values);

    // Check the column count
    if (sizeof($values) != $columnCount)
    {
        $output .= $prefix . 'COLUMN COUNT DOES NOT MATCH HEADER' . $suffix;
        $error = true;
        break;
    }

    //
    // Map and check the values
    //
    $organizationType = trim($values[0]);
    if (strlen($organizationType) == 0)
    {
        $output .= $prefix . 'ORGANIZATION TYPE IS MISSING' . $suffix;
        $error = true;
        break;
    }
    if (!in_array($organizationType, ['Sch', 'Dist']))
    {
        $output .= $prefix . 'ORGANIZATION TYPE IS UNKNOWN' . $suffix;
        $error = true;
        break;
    }

    $districtIdentifier = trim($values[1]);
    if (strlen($districtIdentifier) != 9)
    {
        $output .= $prefix . 'DISTRICT IDENTIFIER LENGTH NOT 9 DIGITS' . $suffix;
        $error = true;
        break;
    }

    $organizationIdentifier = trim($values[1]) .  trim($values[2]) . trim($values[3]);
    if (strlen($organizationIdentifier) != 15)
    {
        $output .= $prefix . 'ORGANIZATION IDENTIFIER LENGTH NOT 15 DIGITS' . $suffix;
        $error = true;
        break;
    }

    $organizationName = trim($values[4]);
    if (strlen($organizationName) == 0)
    {
        $output .= $prefix . 'ORGANIZATION NAME IS MISSING' . $suffix;
        $error = true;
        break;
    }

    //
    // Generate the output
    //
    switch ($organizationType)
    {
        case 'Dist':
            $out = 'GetOrganizationId,' . STATE_PATH . "\n";
            $out .= 'CreateOrganization,ORG_TYPE_ADMIN,' . $organizationName . ',' . $organizationIdentifier . "\n";
            $out .= 'GetOrganizationId,' . STATE_PATH . '_' . $organizationName . "\n";
            $currentDistrictIdentifier = $districtIdentifier;
            break;
        case 'Sch':
            if ($districtIdentifier != $currentDistrictIdentifier)
            {
                $output .= $prefix . 'DISTRICT IDENTIFIER MISMATCH' . $suffix;
                $error = true;
                break 2;
            }
            $out = 'CreateOrganization,ORG_TYPE_SCHOOL,' . $organizationName . ',' . $organizationIdentifier . "\n";
            break;
    }
    if (fwrite($fout, $out) === false)
    {
        $output .= $prefix . 'ERROR WRITING OUTPUT' . $suffix;
        $error = true;
        break;
    }
}

closeAllFiles();

displayAuthHeader($error);
if ($output != $outputStart)
{
    $output .= '</p>';
    echo $output;
}
displayMessage('Execution Time: ' . formatExecutionTime(time() - $timeStart));
displayFooter();

function closeAllFiles()
{
    global $fin, $fout;

    // Close any open files
    if ($fin !== null)
        fclose($fin);
    if ($fout !== null)
        fclose($fout);
}
