{
    
    "auditRecords": [
        {
            
            "clientUserId": "1",
            "actionType": "Create",
            "dataType": "Class",
            "primaryId": "1",
            "dataChanges": {
                "organizationId": "4",
                "classIdentifier": "English / Language Arts 1",
                "sectionNumber": "ELA 101",
                "gradeLevel": "7"
            }
        },
        {
            
            "clientUserId": "1",
            "actionType": "Create",
            "dataType": "Class",
            "primaryId": "2",
            "dataChanges": {
                "organizationId": "4",
                "classIdentifier": "English / Language Arts 1",
                "sectionNumber": "ELA 102",
                "gradeLevel": "7"
            }
        },
        {
            
            "clientUserId": "1",
            "actionType": "Create",
            "dataType": "Class",
            "primaryId": "3",
            "dataChanges": {
                "organizationId": "4",
                "classIdentifier": "English / Language Arts 1",
                "sectionNumber": "ELA 103",
                "gradeLevel": "7"
            }
        }
    ]
}