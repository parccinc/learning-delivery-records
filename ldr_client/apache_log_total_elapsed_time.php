<?php

require_once 'ldr_client.php';

define('SRC_DIR', '../import/');

// Open the request log file
if (!isset($_GET['filename']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Please specify the request log file name - e.g. filename=some_file.log');
    displayFooter();
    exit;
}
$file = SRC_DIR . $_GET['filename'];
$fh = fopen($file, 'rb');
if ($fh === false)
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Unable to open file ' . $file);
    displayFooter();
    exit;
}

$totalElapsedTime = 0.0;
while (true)
{
    // Get the next line
    $line = fgets($fh);
    if ($line === false)
        break;

    // Parse the line
    $values = explode(' ', $line);

    // Add the elapsed time to the total
    $elapsedTime = $values[3];
    $totalElapsedTime += $elapsedTime;
}

$totalElapsedTime /= 1000000;

displayHeader(false);
displayMessage('Total elapsed time in ' . $_GET['filename'] . ' = ' .
    roundThreeDecimalPoints($totalElapsedTime) . ' seconds');
displayFooter();

fclose($fh);
