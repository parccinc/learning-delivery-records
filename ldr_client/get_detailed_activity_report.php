<?php

require_once 'ldr_client.php';

session_start();

// Call the LDR service
$response = callLDR('GET', 'activity-report');

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        $headers = [
            "Battery Name"
            , "Subject"
            , "Grade"
            , "Form Name"
            , "# of Assignments - Scheduled"
            , "# of Assignments - Submitted"
            , "# of Assignments - Paused"
            , "# of Assignments - In Progress"
            , "# of Assignments - Canceled"
        ];

        $report = [];
        echo "<br/>";
        echo "<table class='test_report'>";
        echo "<tr><th>#</th><th>".implode("</th><th>", $headers)."</th></tr>";
        $report[] = implode(",", $headers);
        $count = 0;
        foreach ($response as $key => $formDetails) {
            echo "<tr><td>".++$count."</td><td>".implode("</td><td>", $formDetails)."</td></tr>";
            $report[] = '\"'.implode('\",\"', $formDetails).'\"';
        }
        echo "</table>";
        echo "<br/>";
?>
    <script type="text/javascript">
    function downloadReport(report, filename)
    {
        reportContent = JSON.parse(report);
        reportLength = reportContent.length;
        var report = new String();
        for (var i = 0; i < reportLength; i++) {
            report = report+reportContent[i]+"\n";
        }
        var downloadLink = document.createElement("a");
        var blob = new Blob(["\ufeff", report]);
        var url = URL.createObjectURL(blob);
        downloadLink.href = url;
        downloadLink.download = filename;

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    }
    </script>
<?php
        // Download Activity Report
        $filename = "Activity-Report_".date("YmdHis").".csv";
        echo PHP_EOL.'<script type="text/javascript">downloadReport(\''.json_encode($report).'\', "'.$filename.'");</script>';
    }
}
displayFooter();
