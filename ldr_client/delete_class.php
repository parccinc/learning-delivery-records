<?php

require_once 'ldr_client.php';

session_start();

if (!isset($_GET['classId']))
{
    displayHeader(true);
    displayErrorMessage('classId value is required in the query string');
    displayFooter();
    exit;
}

// Prepare the resource path
$resource = 'class/' . $_GET['classId'];

// Call the LDR service
$response = callLDR('DELETE', $resource, $_GET);

// Check the LDR response for an error
$error = array_key_exists('error', $response);

// Display the LDR response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<p class="message">';
        foreach ($response as $key => $value)
            echo $key . ' => ' . $value . '<br />';
        echo '</p>';
    }
}
displayFooter();
