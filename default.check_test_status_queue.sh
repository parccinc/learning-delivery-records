#!/bin/bash
# This script calls LDR to check the test status queue

# Get an LDR session token
TOKEN=$(/usr/bin/curl -s -k --request POST https://localhost/ldr_svcs/session-token --data '{"clientName": "parcc_xxx", "password": "xxxxxxxxxxxxxxx"}' | /bin/cut -b 11-50)

# Request that LDR check the test status queue
/usr/bin/curl -s -k --request PUT https://localhost/ldr_svcs/check-test-status-queue --header "token: $TOKEN"

# Delete the session token
/usr/bin/curl -s -k --request DELETE https://localhost/ldr_svcs/session-token --header "token: $TOKEN"
